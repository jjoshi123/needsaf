# Project Setup #
### For Mac Users ###
* Install Virtualbox
* Install docker(v.1.10.0) and docker-compose(v.1.8.0) using homebrew (NOT "docker for mac")
```
$ brew install docker docker-machine
```
* Install dinghy
```
$ brew tap codekitchen/dinghy
$ brew install dinghy
```
* Create VM (only first time)
```
$ dinghy create --provider virtualbox
```

### For Linux Users ###
* Install docker and docker-compose
* Change git config to ignore file-mode changes
```
$ git config core.fileMode false
```
* Run project using docker-compose
```
$ docker-compose up
```
This will take a while at first time, it will create images of all docker containers and then install Magento. Wait until you see magento setup installation in your console.
* After you see in logs that magento is installed by docker-compose, get the IP address of nginx container and add it into your /etc/hosts file
```
$ docker-compose up
$ docker inspect --format '{{ .NetworkSettings.IPAddress }}' needsaf
```
The above command will return the IP address, add it into your hosts file as follows
```
<ip> needsaf.docker
```

# How to run the project #
### For Mac Users ###
* Start dinghy
```
$ dinghy start
```
This command will return some environment variables, you need to run those in your current shell.

* Run project using docker-compose
```
$ docker-compose up
```
This will take a while at first time, it will create images of all docker containers and then install Magento. Wait until you see magento setup installation in your console.

* Once the above command installs magento (keep checking logs of above command), then you can access the project at http://needsaf.docker/
* To stop the project, simply press "control + c"

### For Linux Users ###
* Run project using docker-compose
```
$ docker-compose up
```
* You can access the project at http://needsaf.docker/
* To stop the project, simply press "control + c"