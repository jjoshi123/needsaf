<?php
namespace Needsaf\Category\Controller\Index;
use \Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Authorization\Model\Acl\Role\Group as RoleGroup;
use Magento\Authorization\Model\UserContextInterface;
use Needsaf\Membership\Model\Payment\Paypal;
use Needsaf\Membership\Model\MembershipFactory;
use Magento\Catalog\Api\CategoryRepositoryInterface;
use Magento\Store\Api\StoreRepositoryInterface;
use Magento\Store\Api\StoreCookieManagerInterface;
use Magento\Framework\App\Http\Context as HttpContext;
use Magento\Store\Model\Store;
/**
 * Class Post
 * @package Needsaf\Membership\Controller\Index
 */
class Post extends Action
{
    protected $_storeManager;
    protected $categoryRepository;
    protected $storeRepository;
    protected $storeInterface;
    protected $types = array('config','layout','block_html','collections','reflection','db_ddl','eav','config_integration','config_integration_api','full_page','translate','config_webservice');

    const DEFAULT_STORE_VIEW_CODE = 'default';

    public function __construct(
        Context $context,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        CategoryRepositoryInterface $categoryRepository,
        StoreRepositoryInterface $storeRepository,
        StoreCookieManagerInterface $storeCookieManager,
        HttpContext $httpContext,
        \Magento\Store\Api\Data\StoreInterface $store,
        \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList, 
        \Magento\Framework\App\Cache\Frontend\Pool $cacheFrontendPool    
    ) {
        $this->_storeManager = $storeManager;
        $this->categoryRepository = $categoryRepository;
        $this->storeRepository = $storeRepository;
        $this->storeCookieManager = $storeCookieManager;
        $this->httpContext = $httpContext;
        $this->_store = $store;
        $this->cacheTypeList = $cacheTypeList;
        $this->cacheFrontendPool = $cacheFrontendPool;
        parent::__construct($context);
    }

    /**
     * Execute when request is received
     */
    public function execute()
    {
        $post = $this->getRequest()->getParams();
        $category = $post['category'];
        $subcategoryName ="subcategory".$category;
        $subCategoryId = $post[$subcategoryName];
        if(isset($post['zipcode_service'])) {
            $zipcode = $post['zipcode_service'];
        }
        else
        {
            $zipcode = $post['zipcode_consulting'];
        }    
        $resource = $this->_objectManager->get('Magento\Framework\App\ResourceConnection');
        $localeInterface = $this->_objectManager->create('Magento\Framework\Locale\ResolverInterface');
        $connection = $resource->getConnection();
        $tableName = $resource->getTableName('zipcode_store_mapping');
        $getMemberStoreViewSql = "Select * FROM " . $tableName." Where `zipcode`='".$zipcode."'";
        $getMemberStoreViewResult = $connection->fetchAll($getMemberStoreViewSql);
        if(!empty($getMemberStoreViewResult)){
            $zipcodeStoreId = $getMemberStoreViewResult[0]['store_id'];
            $this->_storeManager->setCurrentStore($zipcodeStoreId);
        }
        else
        {
            $this->_storeManager->setCurrentStore(self::DEFAULT_STORE_VIEW_CODE);
        } 
        $this->changeStore($this->_storeManager->getStore()->getCode());
        $final_url = $this->getCategory($subCategoryId,$this->_storeManager->getStore()->getId());
        $this->cleanPageCache();
        return $this->_redirect($final_url);
    }
    public  function getNewCategory($categoryId,$storeId)
    {
        $category = $this->categoryRepository->get($categoryId, $storeId);
        return parse_url($category->getUrl());
    }
    public  function getCategory($categoryId,$storeId)
    {
        $category = $this->categoryRepository->get($categoryId, $storeId);
        return $category->getUrl();
    }
    public function getWebsiteId()
    {
        return $this->_storeManager->getStore()->getWebsiteId();
    }

    public function changeStore($storeCode) {
        try {
            $store = $this->storeRepository->getActiveStoreByCode($storeCode);
        } catch (StoreIsInactiveException $e) {
            $error = __('Requested store is inactive');
        } catch (NoSuchEntityException $e) {
            $error = __('Requested store is not found');
        }

        $defaultStoreView = $this->_storeManager->getDefaultStoreView();
        if ($defaultStoreView->getId() == $store->getId()) {
            $this->storeCookieManager->deleteStoreCookie($store);
        } else {
            $this->httpContext->setValue(Store::ENTITY, $store->getCode(), $defaultStoreView->getCode());
            $this->storeCookieManager->setStoreCookie($store);
        }
    }
    
    public function cleanPageCache() {
        foreach ($this->types as $type) {
            $this->cacheTypeList->cleanType($type);
        }   
        foreach ($this->cacheFrontendPool as $cacheFrontend) {
            $cacheFrontend->getBackend()->clean();
        }
    }
}
