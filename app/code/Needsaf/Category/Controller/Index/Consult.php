<?php
namespace Needsaf\Category\Controller\Index;
use \Magento\Framework\App\Action\Action;
use \Magento\Framework\Exception\LocalizedException;
use Magento\Framework\App\Action\Context;


/**
 * Class Post
 * @package Needsaf\Membership\Controller\Index
 */
class Consult extends Action
{

    protected $_mediaDirectory;
    protected $_fileUploaderFactory;
    protected $customerSession;

    public function __construct(
        \Magento\Framework\Filesystem $filesystem,
        \Magento\MediaStorage\Model\File\UploaderFactory $fileUploaderFactory,
        \Magento\Customer\Model\Session $customerSession,
        Context $context
    ) {

        $this->_mediaDirectory = $filesystem->getDirectoryWrite(\Magento\Framework\App\Filesystem\DirectoryList::MEDIA);
        $this->_fileUploaderFactory = $fileUploaderFactory;
        $this->customerSession = $customerSession;
        parent::__construct($context);
    }

    /**
     * Execute when request is received
     */
    public function execute()
    {
        $post = $this->getRequest()->getParams();
        $this->customerSession->setConsult($post);
		if(isset($post['country_service'])) {
             $country = $post['country_service'];
        }
        else
        {
            $country = $post['country_consulting'];
		}    
        $this->customerSession->setCountryName($country);
        $this->customerSession->setConsultCategoryId($post['category']);
        if($_FILES['image']['name']) {
            $file = $this->uploadAction();
            $this->customerSession->setImage($file['file']);
        }

        $this->_redirect('categories/index/vendorlist');
    }

    /**
     *
     * upload image
     * @return string|null
     */
    public function uploadAction()
    {

        $resultRedirect = $this->resultRedirectFactory->create();
        try {
            $target = $this->_mediaDirectory->getAbsolutePath('customerAttachment/');
            $uploader = $this->_fileUploaderFactory->create(['fileId' => 'image']);
            $uploader->setAllowedExtensions(['jpg','jpeg','psd','gif','png','svg','doc','pdf','docx','rtf','avi','dvr-ms','wtv','mp4','mov','m4v','3gp','3g2','k3g','m4v','ppt','pptx','pptm','swf','txt','xls','xlsx','xlsm','xlsb','csv','zip']);
            $uploader->setAllowRenameFiles(true);
            $result = $uploader->save($target);
            return $result;
        }catch (\Magento\Framework\Exception\LocalizedException $e) {
            if ($e->getCode() != \Magento\Framework\File\Uploader::TMP_NAME_EMPTY) {
                throw new FrameworkException($e->getMessage());
            }
        }

    }

}
