<?php
namespace Needsaf\Category\Controller\Index;
use \Magento\Framework\App\Action\Action;
use Magento\Framework\ObjectManagerInterface;

/**
 * Class Index
 * @package Needsaf\Membership\Controller\Index
 */
class VendorList extends Action
{

    /** @var  \Magento\Framework\View\Result\Page */
    protected $resultPageFactory;

    protected $customerSession;

    protected $_categoryFactory;


    /**
     * Index constructor.
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(\Magento\Framework\App\Action\Context $context,
                                \Magento\Framework\View\Result\PageFactory $resultPageFactory,
                                \Magento\Customer\Model\Session $customerSession,
                                \Magento\Catalog\Model\CategoryFactory $categoryFactory
    )
    {
        $this->resultPageFactory = $resultPageFactory;
        $this->customerSession = $customerSession;
        $this->_categoryFactory = $categoryFactory;
        parent::__construct($context);
    }

    /**
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        return $this->resultPageFactory->create();
    }

}