<?php
namespace Needsaf\Category\Controller\Index;
use \Magento\Framework\App\Action\Action;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\ScopeInterface;

/**
 * Class Index
 * @package Needsaf\Membership\Controller\Index
 */
class Save extends Action
{

    /** @var  \Magento\Framework\View\Result\Page */
    protected $resultPageFactory;

    protected $customerSession;

    protected $_categoryFactory;

    protected $_inquiryFactory;

    /**
     * Index constructor.
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(\Magento\Framework\App\Action\Context $context,
                                \Magento\Framework\View\Result\PageFactory $resultPageFactory,
                                \Magento\Customer\Model\Session $customerSession,
                                \Magento\Catalog\Model\CategoryFactory $categoryFactory,
                                \Needsaf\Inquiry\Model\InquiryFactory $inquiryFactory,
                                ScopeConfigInterface $scopeConfig
    )
    {
        $this->resultPageFactory = $resultPageFactory;
        $this->customerSession = $customerSession;
        $this->_categoryFactory = $categoryFactory;
        $this->_inquiryFactory = $inquiryFactory;
        $this->_scopeConfig = $scopeConfig;
        parent::__construct($context);
    }

    /**
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        $post = $this->getRequest()->getParams();
        if(!empty($post)) {
            $vendorId = $post['order_id'];
            $this->customerSession->setVendorId($vendorId);
        }
        if (!$this->customerSession->isLoggedIn()) {
            $urlInterface = $this->_objectManager->get('\Magento\Framework\UrlInterface');
            $this->customerSession->setAfterAuthUrl($urlInterface->getCurrentUrl());
            $this->customerSession->authenticate();

        }else{
            $data = $this->customerSession->getConsult();
            $categoryId = $data['category'];
            $subcategoryName = "subcategory" . $categoryId;
            if (array_key_exists($subcategoryName,$data)){
                $subCategoryId = $data[$subcategoryName];
            }else{
                $subCategoryId = '';
            }

            $budget = $data['budget'];
            $budgetDescription = $data['budget_description'];
            $customerId = $this->customerSession->getCustomer()->getId();
            $category = $this->_categoryFactory->create()->load($categoryId);
            $categoryName = $category->getName();
            if ($subCategoryId != ''){
                $subCategory = $this->_categoryFactory->create()->load($subCategoryId);
                $subcategoryName = $subCategory->getName();
            }else{
                $subcategoryName = '';
            }
            $imageName = $this->customerSession->getImage();
            $inquiryModel = $this->_inquiryFactory->create();
            $collection = $inquiryModel->getCollection()->addFieldToFilter('customer_id', $customerId)
                ->addFieldToFilter('order_conjcat_name', $categoryName)
                ->addFieldToFilter('inquiry_status', 'pending')
                ->load();
            if($collection->getData())
            {
                $inquiryTime = $this->_scopeConfig->getValue('needsaf_category/inquirytime/inquiry_time', ScopeInterface::SCOPE_STORE);
                $this->messageManager->addError(
                    __('We can\'t process your request right now. Sorry, You already submit inquiry. Try after ' .$inquiryTime.' hours.')
                );
            }
            else {
                $vendorID = $this->customerSession->getVendorId();
                $inquiryInfo = [
                    'order_conjcat_name' => $categoryName,
                    'object_type' => $subcategoryName,
                    'budget' => $budget,
                    'image' => $imageName,
                    'description' => $budgetDescription,
                    'inquiry_status' => 'pending',
                    'vendor_id' => $vendorID,
                    'customer_id' => $customerId,
                ];
                $inquiryModel->setData($inquiryInfo);
                $inquiryModel->save();
                $this->messageManager->addSuccess(
                    __('Thanks for submitting your inquiry request.')
                );
            }
            return $this->_redirect('');

        }

        return $this->resultPageFactory->create();

    }


}