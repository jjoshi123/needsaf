<?php
namespace Needsaf\Category\Block;


use Magento\Framework\View\Element\Template;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\ScopeInterface;

/**
 * Class Index
 * @package Panda\CustomQuote\Block
 */
class Category extends \Magento\Framework\View\Element\Template
{

    /**
     * @var \Magento\Catalog\Model\Category
     */
    protected $_categoryObject;

    /**
     * @var \Magento\Catalog\Model\CategoryRepository
     */
    protected $_categoryRepository;




    /**
     * Index constructor.
     * @param Template\Context $context
     * @param \Magento\Catalog\Model\CategoryFactory $categoryFactory
     * @param \Magento\Catalog\Model\CategoryRepository $categoryRepository
     * @param \Magento\Customer\Model\Session $customerSession
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     * @param \Magento\Customer\Model\AddressFactory $address
     * @param \Magento\Catalog\Model\Category $categoryObject
     * @param ScopeConfigInterface $scopeConfig
     * @param array $data
     */
    public function __construct(
        Template\Context $context,
        \Magento\Catalog\Model\CategoryRepository $categoryRepository,
        \Magento\Catalog\Model\Category $categoryObject,
        array $data = [])
    {
        $this->_categoryObject = $categoryObject;
        $this->_categoryRepository = $categoryRepository;

        parent::__construct($context, $data);
    }


    /**
     * @param $parent_category_id
     * @return null|string
     */
    public function getSubCategory($parent_category_id)
    {
        $parentCategoryObject = $this->_categoryRepository->get($parent_category_id);
        return $parentCategoryObject->getChildrenCategories();
    }


    /**
     * @return int
     */
    public function getCustomCategoryId()
    {
        return $this->_scopeConfig->getValue('needsaf_category/needsaf/category_id', ScopeInterface::SCOPE_STORE);

    }

    public function getCategoryName($categoryId)
    {
       return $this->_categoryObject->load($categoryId)->getName();
    }

    public function getConsultId()
    {
        return $this->_scopeConfig->getValue('needsaf_category/needsafConsult/consult_id', ScopeInterface::SCOPE_STORE);
    }

}