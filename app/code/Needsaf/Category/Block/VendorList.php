<?php
namespace Needsaf\Category\Block;

/**
 * Class Member
 * @package Needsaf\Membership\Block
 */
class VendorList extends \Magento\Framework\View\Element\Template implements
    \Magento\Framework\DataObject\IdentityInterface
{



    protected $_vendorCollectionFactory;
    protected $customerSession;

    /**
     * Member constructor.
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Needsaf\Membership\Model\ResourceModel\Membership\CollectionFactory $membershipCollectionFactory
     * @param array $data
     */
    public function __construct(\Magento\Framework\View\Element\Template\Context $context,
                                \Needsaf\Membership\Model\ResourceModel\Vendor\CollectionFactory $vendorCollectionFactory,
                                \Magento\Customer\Model\Session $customerSession,
                                array $data = []
    )
    {

        parent::__construct($context,$data);

        $this->_vendorCollectionFactory = $vendorCollectionFactory;
        $this->customerSession = $customerSession;
    }

    /**
     * @return $this
     */
    public function getVendorList()
    {
        $name =  $this->customerSession->getCountryName();
        $final = $name;
        if($final == 'DE')
        {
            $website = 'needsaf_dech';
        }
        elseif ($final == 'FR')
        {
            $website = 'needsaf_romandy';
        }
        elseif($final == 'SZ')
        {
            $website = 'needsaf_ticino';
        }
        else{
            $website ='needsaf_dech';
        }
        $membership = $this->_vendorCollectionFactory
            ->create()
            ->addFieldToFilter('vendor_category', array('neq'=>'services'))
            ->addFieldToFilter('vendor_status' , 'approved')
            ->addFieldToFilter('consult_category',array('finset'=>$this->customerSession->getConsultCategoryId()))
            ->setOrder('vendor_rating' , 'DESC');

        return $membership;
    }
    public function getIdentities()
    {
        return [\Needsaf\Membership\Model\Vendor::CACHE_TAG . '_' . 'list'];
    }

    /**
     * Return media path
     * @return string|null
     */
    public function getMediaPath()
    {
        return $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
    }

}