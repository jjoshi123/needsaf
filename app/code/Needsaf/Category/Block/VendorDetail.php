<?php
namespace Needsaf\Category\Block;


/**
 * Class VendorDetail
 * @package Needsaf\Category\Block
 */
class VendorDetail extends \Magento\Framework\View\Element\Template implements
    \Magento\Framework\DataObject\IdentityInterface
{



    protected $vendorFactory;
    protected $vendorRatingFactory;
    protected $customerSession;
    protected $inquiryFactory;

    /**
     * VendorDetail constructor.
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Needsaf\Membership\Model\VendorFactory $vendorFactory
     * @param array $data
     */
    public function __construct(\Magento\Framework\View\Element\Template\Context $context,
                                \Needsaf\VendorRating\Model\VendorRatingFactory $vendorRatingFactory,
                                \Needsaf\Membership\Model\VendorFactory $vendorFactory,
                                \Magento\Customer\Model\Session $customerSession,
                                \Needsaf\Inquiry\Model\InquiryFactory $inquiryFactory,
                                array $data = []
    )
    {
        $this->vendorFactory = $vendorFactory;
        $this->vendorRatingFactory = $vendorRatingFactory;
        $this->customerSession = $customerSession;
        $this->inquiryFactory = $inquiryFactory;
        parent::__construct($context,$data);


    }

    /**
     * @return $this
     */
    public function getVendorDetails()
    {
        $orderId = $this->getRequest()->getParams('order_id');
       $vendorData = $this->vendorFactory->create()->load($orderId);
       return $vendorData;
    }
    public function getIdentities()
    {
        return [\Needsaf\Membership\Model\Vendor::CACHE_TAG . '_' . 'list'];
    }
    /**
     * Return media path
     * @return string|null
     */
    public function getMediaPath()
    {
        return $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
    }

    public function getVendorRatings($vendorId)
    {
        $vendorRatingModel = $this->vendorRatingFactory->create();
        return $vendorRatingModel->getCollection()->addFieldToFilter('vendor_id', $vendorId)->addFieldToFilter('status', 1)->load()->getData();
    }

    public function getPreviousInquiries($vendorId)
    {
        $inquiryModel = $this->inquiryFactory->create();
        return $inquiryModel->getcollection()->addFieldToFilter('vendor_id',$vendorId)->addFieldToFilter('customer_id',$this->customerSession->getCustomer()->getId())->load()->addFieldToFilter('inquiry_status',array('neq'=>'auto_rejected'))->getData();
    }

}