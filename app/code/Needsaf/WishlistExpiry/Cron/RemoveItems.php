<?php
namespace Needsaf\WishlistExpiry\Cron;


use Magento\Catalog\Model\ProductRepository;
use \Magento\Wishlist\Model\Wishlist;
use \Magento\Wishlist\Model\ItemFactory;
use \Magento\Wishlist\Model\ResourceModel\Item\CollectionFactory as ItemCollectionFactory;
use \Magento\Customer\Model\CustomerFactory;
class RemoveItems
{
    protected $_wishlist;
    protected $_itemCollectionFactory;
    protected $_timezoneInterface;
    protected $_itemFactory;
    protected $scopeConfig;
    protected $_customerFactory;
    protected $inlineTranslation;
    protected $_transportBuilder;
    protected $_productRepository;

    public function __construct(Wishlist $wishlist,
                                ItemCollectionFactory $itemCollectionFactory,
                                ItemFactory $itemFactory,
                                \Magento\Catalog\Model\ProductRepository $productRepository,
                                \Magento\Framework\Stdlib\DateTime\TimezoneInterface $timezoneInterface,
                                \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder,
                                \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation,
                                \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
                                CustomerFactory $customerFactory)
    {
        $this->_wishlist = $wishlist;
        $this->_itemCollectionFactory = $itemCollectionFactory;
        $this->_timezoneInterface = $timezoneInterface;
        $this->scopeConfig = $scopeConfig;
        $this->_transportBuilder = $transportBuilder;
        $this->inlineTranslation = $inlineTranslation;
        $this->_itemFactory = $itemFactory;
        $this->_customerFactory = $customerFactory;
        $this->_productRepository = $productRepository;

    }

    public function execute()
    {

        $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
        $dayDifference =  (int)$this->scopeConfig->getValue('needsaf_wishlist/needsaf/wishlist_expiry', $storeScope);
        $itemCollection = $this->_itemCollectionFactory->create();
        $itemData = $itemCollection->getData();
        $currentDate = $this->_timezoneInterface->date();
        $itemModel = $this->_itemFactory->create();
        $customerModel = $this->_customerFactory->create();

        foreach ($itemData as $item){
            $added_at = $this->_timezoneInterface->date($item['added_at']);

            $totalHours = ($currentDate->getTimestamp() - $added_at->getTimestamp()) / 3600;
//            if($totalHours >= $dayDifference){
                $itemModel->load($item['wishlist_item_id']);
                $wishlistData = $this->_wishlist->load($item['wishlist_id']);
                $productData = $this->_productRepository->getById($item['product_id']);
                $postObject = new \Magento\Framework\DataObject();
                $productName = $productData->getName();
                $customerData = $customerModel->load($wishlistData->getCustomerId());
                $email = $customerData->getEmail();
                $data = array();
                $data['email'] = $email;
                $data['productname']= $productName;
                $postObject->setData($data);
                $itemModel->delete();
                $itemModel->save();
                $transport = $this->_transportBuilder
                ->setTemplateVars(['data' => $postObject])
                ->setTemplateIdentifier('wishlist_expire_template')
                ->setTemplateOptions(
                    [
                        'area' => \Magento\Framework\App\Area::AREA_FRONTEND,
                        'store' => \Magento\Store\Model\Store::DEFAULT_STORE_ID
                    ]
                )
                ->setFrom(
                    $this->scopeConfig->getValue('contact/email/sender_email_identity', \Magento\Store\Model\ScopeInterface::SCOPE_STORE)
                )
                ->addTo($email)
                ->getTransport();
            $transport->sendMessage();
            $this->inlineTranslation->resume();

//            }

        }
    }

}