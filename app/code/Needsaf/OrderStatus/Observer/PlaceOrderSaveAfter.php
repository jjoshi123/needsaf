<?php 
namespace Needsaf\OrderStatus\Observer;

use Magento\Framework\Event\ObserverInterface;
use Needsaf\Membership\Model\Vendor;
use Needsaf\Membership\Model\VendorFactory;

class PlaceOrderSaveAfter implements ObserverInterface
{	
	protected $_objectManager;
	protected $orderFactory;
	protected $productFactory;
	protected $scopeConfig;
	protected $_vendorFactory;

	public function __construct(
		\Magento\Framework\ObjectManagerInterface $objectmanager,
		\Magento\Sales\Model\OrderFactory $orderFactory,
		\Magento\Catalog\Model\ProductFactory $productFactory,
		\Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        VendorFactory $vendorFactory
		)
	{
		$this->_objectManager = $objectmanager;
		$this->orderFactory = $orderFactory;
		$this->productFactory = $productFactory;
		$this->scopeConfig = $scopeConfig;
		$this->_vendorFactory = $vendorFactory;
	}
	public function execute(\Magento\Framework\Event\Observer $observer)
    {
    	$orderIds = $observer->getOrderIds();
    	$orderId = $orderIds[0];
    	$order = $this->orderFactory->create()->load($orderId);
		$orderItems = $order->getAllItems();
		$orderItem = $orderItems[0];
		$product = $this->productFactory->create()->load($orderItem->getProductId());
		$vendorId = $product->getResource()->getAttribute('vendor_id')->getFrontend()->getValue($product);
        $vendorModel =  $this->_vendorFactory->create();
        $vendorModel->load($vendorId);
        $vendorModel->setIsEligible(1);
        $vendorModel->setLeadCount($vendorModel->getLeadCount()+1);
        $vendorModel->save();
		$email = $vendorModel->getEmail();
		$order->setCustomerEmail($email);
        $this->_objectManager->create('\Magento\Sales\Model\OrderNotifier')
                    ->notify($order);
    }
}