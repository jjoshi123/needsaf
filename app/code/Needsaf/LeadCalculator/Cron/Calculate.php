<?php

namespace Needsaf\LeadCalculator\Cron;
use Magento\Catalog\Model\ProductFactory;
use Magento\Sales\Model\Order;
use Magento\User\Model\User;
use Needsaf\Membership\Model\MembershipFactory;
use Magento\Sales\Model\OrderFactory;

class Calculate
{
    protected $_orderCollectionFactory;
    protected $_vendorFactory;
    protected $_orderModel;
    protected $_productFactory;
    protected $_productModel;
    protected $_userModel;
    protected $_membershipFactory;
    protected $_membershipModel;
    protected $_orderFactory;
    public function __construct(
        \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory,
        \Needsaf\Membership\Model\VendorFactory $vendorFactory,
        ProductFactory $productFactory,
        User $userModel,
        MembershipFactory $membershipFactory,
        OrderFactory $orderFactory

    )
    {
        $this->_orderCollectionFactory = $orderCollectionFactory;
        $this->_vendorFactory = $vendorFactory;
        $this->_orderModel = $this->_orderCollectionFactory->create();
        $this->_productFactory = $productFactory;
        $this->_productModel = $this->_productFactory->create();
        $this->_userModel = $userModel;
        $this->_membershipFactory = $membershipFactory;
        $this->_membershipModel = $this->_membershipFactory->create();
        $this->_orderFactory = $orderFactory;
    }

    public function execute(){
        $collection = $this->_orderModel->addAttributeToSelect('*')->addFieldToFilter('status','vendor_lead_payment_pending');
        foreach ($collection as $order){
            $orderItems = $order->getAllItems();
            $orderObject = $this->_orderFactory->create()->load($order->getData()['entity_id']);
            foreach ($orderItems as $item){
                $itemData = $item->getData();
                $product = $this->_productModel->load($itemData['item_id']);
                $productData = $product->getData();
                $userData = $this->_userModel->load($productData['admin_user'])->getData();
                $vendorModel  = $this->_vendorFactory->create();
                $vendorData = $vendorModel->load($userData['vendor_id'])->getData();
                $membershipData = $this->_membershipModel->load($vendorData['membership_id'])->getData();
                if(strtolower($membershipData['membership_type']) == 'basic'){
                    //code for calculating leads
                    //Payment logic
                    $orderState = Order::STATE_PROCESSING;
                    $orderObject->setState($orderState)->setStatus('vendor_lead_payment_done');
                    $orderObject->save();
                    if ($order->getData()['grand_total'] <= 2000 ){
                    //payment logic
                    }elseif ($order->getData()['grand_total'] <= 2600 ){
                        //payment logic
                    }else{
                        //payment logic
                    }


                }else{
                    //code for calculating lead for paid members
                    if($vendorData['lead_count'] > 10){
                        //payment logic
                        $orderState = Order::STATE_PROCESSING;
                        $orderObject->setState($orderState)->setStatus('vendor_lead_payment_done');
                        $orderObject->save();
                    }
                    else{
                        //payment logic
                        $orderState = Order::STATE_PROCESSING;
                        $orderObject->setState($orderState)->setStatus('vendor_lead_payment_done');
                        $orderObject->save();

                    }

                }
            }
        }


    }
}