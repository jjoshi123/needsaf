<?php

namespace Needsaf\LeadPayment\Model;

use Magento\Paypal\Model\Config as PaypalConfig;
use Magento\Paypal\Model\Config;

class Paypal
{

    /**
     * Config instance
     *
     * @var PaypalConfig
     */
    protected $_config;

    /**
     * API instance
     *
     * @var \Magento\Paypal\Model\Api\Nvp
     */
    protected $_api;

    /**
     * @var \Magento\Paypal\Model\Api\Type\Factory
     */
    protected $_apiTypeFactory;

    /**
     * Api Model Type
     *
     * @var string
     */
    protected $_apiType = 'Magento\Paypal\Model\Api\Nvp';

    /**
     * Paypal constructor.
     *
     * @param \Magento\Paypal\Model\Config $_config
     * @param \Magento\Paypal\Model\Api\Type\Factory $_apiTypeFactory
     */
    public function __construct(\Magento\Paypal\Model\Config $_config, \Magento\Paypal\Model\Api\Type\Factory $_apiTypeFactory)
    {
        $this->_config = $_config;
        $this->_apiTypeFactory = $_apiTypeFactory;
        $this->_config->setMethod(Config::METHOD_EXPRESS);
        $this->_getApi();
    }

    /**
     * Get token and return paypal express redirect URL
     *
     * @param array $data
     *
     * @return string
     */
    public function start(array $data)
    {
        $solutionType = $this->_config->getMerchantCountry() == 'DE'
            ? \Magento\Paypal\Model\Config::EC_SOLUTION_TYPE_MARK
            : $this->_config->getValue('solutionType');

        $this->_api->setAmount($data['amount'])
            ->setCurrencyCode('CHF')
            ->setInvNum(rand(1,1000000))
            ->setReturnUrl($data['return_url'])
            ->setCancelUrl($data['cancel_url'])
            ->setSolutionType($solutionType)
            ->setPaymentAction($this->_config->getValue('paymentAction'));

        $this->_api->callSetExpressCheckout();

        $token = $this->_api->getToken();

        return $this->_config->getExpressCheckoutStartUrl($token);
    }

    /**
     * Place actual transaction using token
     *
     * @param array $data
     */
    public function placeOrder(array $data)
    {
        $this->_api
            ->setToken($data['token'])
            ->setPayerId($data['payer_id'])
            ->setAmount($data['amount'])
            ->setPaymentAction($this->_config->getValue('paymentAction'))
            ->setNotifyUrl($data['notify_url'])
            ->setInvNum(rand(1,1000000))
            ->setCurrencyCode('CHF');

        $this->_api->callDoExpressCheckoutPayment();

        return $this->_api->getTransactionId();
    }


    /**
     * @return \Magento\Paypal\Model\Api\Nvp
     */
    protected function _getApi()
    {
        if (null === $this->_api) {
            $this->_api = $this->_apiTypeFactory->create($this->_apiType)->setConfigObject($this->_config);
        }
        return $this->_api;
    }

}
