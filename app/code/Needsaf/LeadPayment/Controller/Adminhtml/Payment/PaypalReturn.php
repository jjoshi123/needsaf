<?php


namespace Needsaf\LeadPayment\Controller\Adminhtml\Payment;


use Magento\Backend\App\Action\Context;
use Magento\Sales\Model\OrderFactory;
use Needsaf\LeadPayment\Model\Paypal;
use Needsaf\Inquiry\Model\InquiryFactory;
use Needsaf\Membership\Model\VendorFactory;
use Magento\Store\Model\ScopeInterface;

class PaypalReturn extends \Magento\Backend\App\Action
{

    /**
     * Array of actions which can be processed without secret key validation
     *
     * @var array
     */
    protected $_publicActions = ['paypalreturn'];
    const XML_PATH_EMAIL_RECIPIENT = 'trans_email/ident_general/email';
    protected $_vendorFactory;
    protected $customerFactory;
    protected $_transportBuilder;
    protected $scopeConfig;

    /**
     * @var \Needsaf\LeadPayment\Model\Paypal
     */
    protected $paypal;
    protected $inquiryFactory;
    protected $orderFactory;
    protected $inlineTranslation;

    /**
     * @param Context $context
     */
    public function __construct(
        Context $context,
        Paypal $paypal,
        InquiryFactory $inquiryFactory,
        OrderFactory $orderFactory,
        VendorFactory $vendorFactory,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder,
        \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    ) {
        parent::__construct($context);
        $this->paypal = $paypal;
        $this->inquiryFactory = $inquiryFactory;
        $this->orderFactory = $orderFactory;
        $this->_vendorFactory = $vendorFactory;
        $this->customerFactory = $customerFactory;
        $this->_transportBuilder = $transportBuilder;
        $this->inlineTranslation = $inlineTranslation;
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * Return action
     *
     * @return void
     */
    public function execute()
    {

        if ($this->getRequest()->getParam('token') !== null &&
            $this->getRequest()->getParam('PayerID') !== null) {
            if ($this->getRequest()->getParam('type') == 'inquiry'){
                $data = [
                    'token' => $this->getRequest()->getParam('token'),
                    'payer_id' => $this->getRequest()->getParam('PayerID'),
                    'amount' => $this->getRequest()->getParam('amount'),
                    'inquiry_id' => $this->getRequest()->getParam('inquiry_id'),
                    'notify_url' => $this->getUrl('leadpayment/payment/notify')
                ];

                $transactionId = $this->paypal->placeOrder($data);
                if($transactionId){
                    $inquiryModel = $this->inquiryFactory->create();
                    $inquiryId = $data['inquiry_id'];
                    $inquiryModel->load($data['inquiry_id']);
                    $inquiryModel->setInquiryStatus('accepted');
                    $inquiryModel->save();
                    $inquiryStatus = $inquiryModel->getInquiryStatus();
                    /*email code start*/
                    /*customer data*/
                    $dataCustomer = [
                        'inquiry_status' => $inquiryStatus
                    ];
                    /*customer data end*/
                    $vendorModel =  $this->_vendorFactory->create();
                    $vendorModel->load($inquiryModel->getVendorId());
                    $vendorEmail = $vendorModel->getEmail();
                    $customerModel = $this->customerFactory->create();
                    $customerModel->load($inquiryModel->getCustomerId());
                    $customerEmail = $customerModel->getEmail();
                    $customerName = $customerModel->getName();
                    $vendorName = $vendorModel->getFirstname()." ".$vendorModel->getLastname();
                    $category = $inquiryModel->getOrderConjcatName();
                    $object = $inquiryModel->getObjectType();
                    $budget = $inquiryModel->getBudget();
                    $description = $inquiryModel->getDescription();
                    $dataVendor = [
                        'inquiry_id' => $inquiryId,
                        'customer_name' => $customerName,
                        'category' => $category,
                        'object' => $object,
                        'budget' => $budget,
                        'inquiry_status' => $inquiryStatus,
                        'description' => $description
                    ];
                    $dataAdmin = [
                        'inquiry_id' => $inquiryId,
                        'customer_name' => $customerName,
                        'vendor_name' => $vendorName,
                        'vendor_email' => $vendorEmail,
                        'category' => $category,
                        'object' => $object,
                        'budget' => $budget,
                        'inquiry_status' => $inquiryStatus,
                        'description' => $description
                    ];
                    $admin = $this->scopeConfig->getValue('trans_email/ident_general/email', ScopeInterface::SCOPE_STORE);
                    $postObject = new \Magento\Framework\DataObject();
                    /*customer email*/
                    $postObject->setData($dataCustomer);
                    $transport = $this->_transportBuilder
                        ->setTemplateVars(['data' => $postObject])
                        ->setTemplateIdentifier('accepted_status_template_customer')
                        ->setTemplateOptions(
                            [
                                'area' => \Magento\Framework\App\Area::AREA_FRONTEND,
                                'store' => \Magento\Store\Model\Store::DEFAULT_STORE_ID
                            ]
                        )
                        ->setFrom(['email' => $this->scopeConfig->getValue(self::XML_PATH_EMAIL_RECIPIENT, ScopeInterface::SCOPE_STORE),
                            'name' => $this->scopeConfig->getValue('trans_email/ident_general/email', ScopeInterface::SCOPE_STORE)])
                        ->addTo($customerEmail)
                        ->getTransport();
                    $transport->sendMessage();
                    /*customer email*/
                    /*vendor email*/
                    $postObject->setData($dataVendor);
                    $transportvendor = $this->_transportBuilder
                        ->setTemplateVars(['data' => $postObject])
                        ->setTemplateIdentifier('accepted_status_template_vendor')
                        ->setTemplateOptions(
                            [
                                'area' => \Magento\Framework\App\Area::AREA_FRONTEND,
                                'store' => \Magento\Store\Model\Store::DEFAULT_STORE_ID
                            ]
                        )
                        ->setFrom(['email' => $this->scopeConfig->getValue(self::XML_PATH_EMAIL_RECIPIENT, ScopeInterface::SCOPE_STORE),
                            'name' => $this->scopeConfig->getValue('trans_email/ident_general/email', ScopeInterface::SCOPE_STORE)])
                        ->addTo($vendorEmail)
                        ->getTransport();
                    $transportvendor->sendMessage();
                    /*vendor email*/
                    /*admin email*/
                    $postObject->setData($dataAdmin);
                    $transportadmin = $this->_transportBuilder
                        ->setTemplateVars(['data' => $postObject])
                        ->setTemplateIdentifier('accepted_status_template_admin')
                        ->setTemplateOptions(
                            [
                                'area' => \Magento\Framework\App\Area::AREA_FRONTEND,
                                'store' => \Magento\Store\Model\Store::DEFAULT_STORE_ID
                            ]
                        )
                        ->setFrom(['email' => $this->scopeConfig->getValue(self::XML_PATH_EMAIL_RECIPIENT, ScopeInterface::SCOPE_STORE),
                            'name' => $this->scopeConfig->getValue('trans_email/ident_general/email', ScopeInterface::SCOPE_STORE)])
                        ->addTo($admin)
                        ->getTransport();
                    $transportadmin->sendMessage();
                    /*admin email*/
                    $this->inlineTranslation->resume();
                    /*email code customer end*/
                    $this->messageManager->addSuccessMessage('Inquiry has been Accepted.');
                    $this->_redirect($this->getUrl('inquiry/inquiry/edit',array('id'=>$data['inquiry_id'])));
                }
                else{
                    $this->messageManager->addErrorMessage('Your inquiry could not be processed please try again.');
                    $this->_redirect($this->getUrl('inquiry/inquiry/edit',array('id'=>$data['inquiry_id'])));
                }
            }else{
                $data = [
                    'token' => $this->getRequest()->getParam('token'),
                    'payer_id' => $this->getRequest()->getParam('PayerID'),
                    'amount' => $this->getRequest()->getParam('amount'),
                    'order_id' => $this->getRequest()->getParam('order_id'),
                    'notify_url' => $this->getUrl('leadpayment/payment/notify')
                ];
                $transactionId = $this->paypal->placeOrder($data);
                if($transactionId){
                    $orderModel = $this->orderFactory->create();
                    $orderObject = $orderModel->load($data['order_id']);
                    $orderObject->setStatus('vendor_lead_done');
                    $orderObject->setSendEmail(true);
                    $orderObject->save();

                    $this->messageManager->addSuccessMessage('Your lead payment has been made.');
                    $this->_redirect($this->getUrl('sales/order/view/',array('order_id'=>$data['order_id'])));
                }
                else{
                    $this->messageManager->addErrorMessage('Your order lead payment could not be processed please try again.');
                    $this->_redirect($this->getUrl('sales/order/view/',array('order_id'=>$data['order_id'])));
                }
            }

        }
    }

}
