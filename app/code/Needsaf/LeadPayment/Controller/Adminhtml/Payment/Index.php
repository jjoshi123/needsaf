<?php
namespace Needsaf\LeadPayment\Controller\Adminhtml\Payment;

use Magento\Backend\App\Action\Context;
use Needsaf\LeadPayment\Model\Paypal;
use Needsaf\Membership\Model\VendorFactory;
use Needsaf\Membership\Model\MembershipFactory;
/**
 * Class Index
 * @package Panda\Testimonial\Controller\Adminhtml\Testimonial
 */
class Index extends \Magento\Backend\App\Action
{

    /**
     * @var \Needsaf\LeadPayment\Model\Paypal
     */
    protected $paypal;

    protected $vendorFactory;

    protected $membershipFactory;
    /**
     * @param Context $context
     */
    public function __construct(
        Context $context,
        Paypal $paypal,
        VendorFactory $vendorFactory,
        MembershipFactory $membershipFactory
    ) {
        parent::__construct($context);
        $this->paypal = $paypal;
        $this->vendorFactory = $vendorFactory;
        $this->membershipFactory = $membershipFactory;
    }

    /**
     * Index action
     *
     * @return void
     */
    public function execute()
    {
        $params = $this->getRequest()->getParams();
        $amount = 10;
        $vendorModel  = $this->vendorFactory->create();
        $vendorData = $vendorModel->load($params['vendor_id'])->getData();
        $membershipModel = $this->membershipFactory->create();
        $membershipData = $membershipModel->load($vendorData['membership_id'])->getData();

        if(strtolower($membershipData['membership_type']) == 'basic' || strtolower($membershipData['membership_type']) == 'free'){
            
            if ($params['budget']<= 2000 ){
                $amount = 19;
            }elseif ($params['budget'] <= 2600 ){
                $amount = 29;
            }else{
                $amount = 49;
            }


        }else{
            //code for calculating lead for paid members
            if($vendorData['lead_count'] > 10){
                $amount = 29;
            }
            else{
                $this->_redirect('*/*');

            }

        }

        if($params['type'] == 'inquiry'){
            $data = [
                'vendor_id' => $params['vendor_id'],
                'type' => $params['type'],
                'inquiry_id' => $params['inquiry_id'],
                'amount' => $amount
            ];
            $data['return_url'] = $this->getUrl('leadpayment/payment/paypalreturn', $data);
            $data['cancel_url'] = $this->getUrl('inquiry/inquiry/edit/',array('id'=>$params['inquiry_id']));
            $this->_redirect($this->paypal->start($data));
        }else{
            $data = [
                'vendor_id' => $params['vendor_id'],
                'type' => $params['type'],
                'order_id' => $params['order_id'],
                'amount' => $amount
            ];
            $data['return_url'] = $this->getUrl('leadpayment/payment/paypalreturn', $data);
            $data['cancel_url'] = $this->getUrl('sales/order/view/',array('order_id'=>$params['order_id']));
            $this->_redirect($this->paypal->start($data));
        }

    }

}
