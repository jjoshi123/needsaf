<?php
/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Needsaf\Catalog\Block\Adminhtml\Product\Edit\Button;

/**
 * Class AddAttribute
 */
class AddAttribute extends \Magento\Catalog\Block\Adminhtml\Product\Edit\Button\AddAttribute
{

    protected $authSession;

    /**
     * {@inheritdoc}
     */
    public function getButtonData()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $authModel = $objectManager->create('Magento\Backend\Model\Auth\Session');
        $user = $authModel->getUser();
        $userData = $user->getData();
        if($userData['is_vendor'] == 0) {
            return [
            'label' => __('Add Attribute'),
            'class' => 'action-secondary',
            'data_attribute' => [
                'mage-init' => [
                    'Magento_Ui/js/form/button-adapter' => [
                        'actions' => [
                            [
                                'targetName' => 'product_form.product_form.add_attribute_modal',
                                'actionName' => 'toggleModal'
                            ],
                            [
                                'targetName' => 'product_form.product_form.add_attribute_modal.product_attributes_grid',
                                'actionName' => 'render'
                            ]
                        ]
                    ]
                ]
            ],
            'on_click' => '',
            'sort_order' => 20
        ];
        }else{
            return '';
        }
    }
}
