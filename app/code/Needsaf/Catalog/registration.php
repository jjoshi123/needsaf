<?php

/**
 *module registration file
 */
\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Needsaf_Catalog',
    __DIR__
);