<?php
/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Needsaf\Catalog\Ui\Component\Listing\Columns;

use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Store\Model\StoreManagerInterface;

class Websites extends \Magento\Ui\Component\Listing\Columns\Column
{
    /**
     * Column name
     */
    const NAME = 'websites';

    /**
     * Store manager
     *
     * @var StoreManagerInterface
     */
    protected $storeManager;

    protected $authSession;

    /**
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param StoreManagerInterface $storeManager
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        StoreManagerInterface $storeManager,
        \Magento\Backend\Model\Auth\Session $authSession,
        array $components = [],
        array $data = []
    ) {
        parent::__construct($context, $uiComponentFactory, $components, $data);
        $this->storeManager = $storeManager;
        $this->authSession = $authSession;
    }

    /**
     * {@inheritdoc}
     * @deprecated
     */
    public function prepareDataSource(array $dataSource)
    {
        $user = $this->authSession->getUser();
        $userData = $user->getRole();
        $data =$userData->getData();
        $roleName = $data['role_name'];
        $websiteNames = [];
        foreach ($this->getData('options') as $website) {
            $websiteNames[$website->getWebsiteId()] = $website->getName();
        }
        if (isset($dataSource['data']['items'])) {
            $fieldName = $this->getData('name');
//            if($roleName =='Administrators') {
//                foreach ($dataSource['data']['items'] as & $item) {
//                    $websites = [];
//                    foreach ($item[$fieldName] as $websiteId) {
//                        if (!isset($websiteNames[$websiteId])) {
//                            continue;
//                        }
//                        $websites[] = $websiteNames[$websiteId];
//                    }
//                    $item[$fieldName] = implode(', ', $websites);
//                }
//            }
        }

        return $dataSource;
    }

    /**
     * Prepare component configuration
     * @return void
     */
    public function prepare()
    {
        parent::prepare();
        if ($this->storeManager->isSingleStoreMode()) {
            $this->_data['config']['componentDisabled'] = true;
        }
    }
}
