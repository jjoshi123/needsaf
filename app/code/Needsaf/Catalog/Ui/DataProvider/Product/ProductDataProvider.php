<?php
/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Needsaf\Catalog\Ui\DataProvider\Product;

use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;

/**
 * Class ProductDataProvider
 */
class ProductDataProvider extends \Magento\Ui\DataProvider\AbstractDataProvider
{
    /**
     * Product collection
     *
     * @var \Magento\Catalog\Model\ResourceModel\Product\Collection
     */
    protected $collection;
    protected $_productCollectionFactory;
    /**
     * @var \Magento\Ui\DataProvider\AddFieldToCollectionInterface[]
     */
    protected $addFieldStrategies;

    /**
     * @var \Magento\Ui\DataProvider\AddFilterToCollectionInterface[]
     */
    protected $addFilterStrategies;

    protected $authSession;

    protected $productnewcolln;


    /**
     * Construct
     *
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param CollectionFactory $collectionFactory
     * @param \Magento\Ui\DataProvider\AddFieldToCollectionInterface[] $addFieldStrategies
     * @param \Magento\Ui\DataProvider\AddFilterToCollectionInterface[] $addFilterStrategies
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        CollectionFactory $collectionFactory,
        \Magento\Backend\Model\Auth\Session $authSession,
        \Magento\Catalog\Model\Product $productnewcolln,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        array $meta = [],
        array $data = []

    ) {
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
        $this->_productCollectionFactory = $productCollectionFactory;
        $this->collection = $collectionFactory->create();
        $this->authSession = $authSession;
        $this->produccolln = $productnewcolln;
        $this->_objectManager = $objectManager;
    }

    /**
     * Get data
     *
     * @return array
     */
    public function getData()
    {
//        $collection = $this->userCollection->create()->load();
//        var_dump($collection->getData());
//        die();
        $resource = $this->_objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        $tableName = $resource->getTableName('authorization_role');
        $sql = "Select user_id FROM " . $tableName." Where role_name = 'admin' AND role_type = 'U'";
        $result = $connection->fetchAll($sql);
        $user = $this->authSession->getUser();
        $userData = $user->getRole();
        $data =$userData->getData();
        $roleName = $data['role_name'];
        if($roleName!='Administrators') {
            if (!$this->getCollection()->isLoaded()) {
                $this->getCollection()->load();
                $currentUserId = $user->getId();
                $product_collection = $this->getCollection()->load();
                $duplicates = array();
                $adminData = array();
                $currentArray = array($currentUserId);
                $adminArray = array($result);

                $adminData = $this->getAdmincollection($adminArray);
                $currentCollection = $product_collection->addFieldToFilter('admin_user',["in" => [$currentArray]]);
                $currentCollectionItems = $currentCollection->getData();
                if(!empty($currentCollectionItems)) {
                    foreach ($currentCollectionItems as $currentcolln) {
                        $currentabcEntityId[] = $currentcolln['entity_id'];
                    }

                    foreach ($currentabcEntityId as $test) {
                        $data = $this->produccolln->load($test);
                        $dupl[] = $data->getDuplicatedin();
                    }
                    $nonduplicate[] = array_diff($adminData, $dupl);
                    $finalentity[] = array_merge($nonduplicate[0], $currentabcEntityId);
                }
                if(empty($currentCollectionItems)) {
                    $newcollection = $this->getProductCollection()->addFieldToFilter('entity_id', ["in" => [$adminData]]);
                }
                else{
                    $newcollection = $this->getProductCollection()->addFieldToFilter('entity_id', ["in" => [$finalentity]]);
                }

            }
            $items = $newcollection->toArray();
            $number = count($product_collection->getData());
            return [
                'totalRecords' => $number,
                'items' => array_values($newcollection->getData()),
            ];
        }
        else
        {
            if (!$this->getCollection()->isLoaded()) {
                $this->getCollection()->load();
            }
            $items = $this->getCollection()->toArray();
            return [
                'totalRecords' => $this->getCollection()->getSize(),
                'items' => array_values($items),
            ];
        }
    }

    public function getAdmincollection($adminArray){
        $adminCollection = $this->getProductCollection()->addFieldToFilter('admin_user',["in" => [$adminArray]]);
        $adminCollectionItems = $adminCollection->getData();
        foreach ($adminCollectionItems as $admincolln)
        {
            $adminEntityId[] = $admincolln['entity_id'];
        }
        return $adminEntityId;
    }
    public function getProductCollection()
    {
        /** @var $collection \Magento\Catalog\Model\ResourceModel\Product\Collection */
        $collection = $this->_productCollectionFactory->create();
        return $collection;
    }
}
