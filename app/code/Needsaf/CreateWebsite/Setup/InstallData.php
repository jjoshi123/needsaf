<?php


namespace Needsaf\CreateWebsite\Setup;


use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Store\Model\ResourceModel\Website;
use Magento\Store\Model\WebsiteFactory;
use Magento\Store\Model\StoreFactory;
use Magento\Store\Model\ResourceModel\Group;
use Magento\Store\Model\ResourceModel\Store;
use Magento\Store\Model\GroupFactory;
use Magento\Framework\Event\ManagerInterface;


class InstallData implements InstallDataInterface
{

    /**
     * @var WebsiteFactory
     */
    private $websiteFactory;

    /**
     * @var Website
     */
    private $websiteResourceModel;

    /**
     * @var StoreFactory
     */
    private $storeFactory;
    /**
     * @var GroupFactory
     */
    private $groupFactory;
    /**
     * @var Group
     */
    private $groupResourceModel;
    /**
     * @var Store
     */
    private $storeResourceModel;
    /**
     * @var ManagerInterface
     */
    private $eventManager;

    public function __construct(
        WebsiteFactory $websiteFactory,
        Website $websiteResourceModel,
        Store $storeResourceModel,
        Group $groupResourceModel,
        StoreFactory $storeFactory,
        GroupFactory $groupFactory,
        ManagerInterface $eventManager
    )
    {
        $this->websiteFactory = $websiteFactory;
        $this->websiteResourceModel = $websiteResourceModel;
        $this->storeFactory = $storeFactory;
        $this->groupFactory = $groupFactory;
        $this->groupResourceModel = $groupResourceModel;
        $this->storeResourceModel = $storeResourceModel;
        $this->eventManager = $eventManager;
    }

    /**
     * Installs data for a module
     *
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     */
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        /** @var \Magento\Store\Model\Website $website */
        $website = $this->websiteFactory->create();
        $website->load('needsaf_dech');
        if(!$website->getId()){
            $website->setCode('needsaf_dech');
            $website->setName('Needsaf De Ch');
            $website->setDefaultGroupId(3);
            $this->websiteResourceModel->save($website);
        }

        if($website->getId()){
            /** @var \Magento\Store\Model\Group $group */
            $group = $this->groupFactory->create();
            $group->setWebsiteId($website->getWebsiteId());
            $group->setName('Needsaf DE CH Default');
            $group->setRootCategoryId(2);
            $group->setDefaultStoreId(3);
            $this->groupResourceModel->save($group);
        }


        /** @var  \Magento\Store\Model\Store $store */
        $store = $this->storeFactory->create();
        $store->load('needsaf_dech_default');
        if(!$store->getId()){
            $group = $this->groupFactory->create();
            $group->load('Needsaf DE CH Default', 'name');
            $store->setCode('needsaf_dech_default');
            $store->setName('Needsaf DE ch Default View');
            $store->setWebsite($website);
            $store->setGroupId($group->getId());
            $store->setData('is_active','1');
            $this->storeResourceModel->save($store);
            // Trigger event to insert some data to the sales_sequence_meta table (fix bug place order in checkout)
            $this->eventManager->dispatch('store_add', ['store' => $store]);
        }
        /** @var \Magento\Store\Model\Website $website */
        $website = $this->websiteFactory->create();
        $website->load('needsaf_romandy');
        if(!$website->getId()){
            $website->setCode('needsaf_romandy');
            $website->setName('Needsaf Romandy');
            $website->setDefaultGroupId(3);
            $this->websiteResourceModel->save($website);
        }

        if($website->getId()){
            /** @var \Magento\Store\Model\Group $group */
            $group = $this->groupFactory->create();
            $group->setWebsiteId($website->getWebsiteId());
            $group->setName('Needsaf Romandy Default');
            $group->setRootCategoryId(2);
            $group->setDefaultStoreId(3);
            $this->groupResourceModel->save($group);
        }


        /** @var  \Magento\Store\Model\Store $store */
        $store = $this->storeFactory->create();
        $store->load('needsaf_romandy_default');
        if(!$store->getId()){
            $group = $this->groupFactory->create();
            $group->load('Needsaf Romandy Default', 'name');
            $store->setCode('needsaf_romandy_default');
            $store->setName('Needsaf Romandy Default View');
            $store->setWebsite($website);
            $store->setGroupId($group->getId());
            $store->setData('is_active','1');
            $this->storeResourceModel->save($store);
            // Trigger event to insert some data to the sales_sequence_meta table (fix bug place order in checkout)
            $this->eventManager->dispatch('store_add', ['store' => $store]);
        }

        /** @var \Magento\Store\Model\Website $website */
        $website = $this->websiteFactory->create();
        $website->load('needsaf_ticino');
        if(!$website->getId()){
            $website->setCode('needsaf_ticino');
            $website->setName('Needsaf Ticino');
            $website->setDefaultGroupId(3);
            $this->websiteResourceModel->save($website);
        }

        if($website->getId()){
            /** @var \Magento\Store\Model\Group $group */
            $group = $this->groupFactory->create();
            $group->setWebsiteId($website->getWebsiteId());
            $group->setName('Needsaf Ticino Default');
            $group->setRootCategoryId(2);
            $group->setDefaultStoreId(3);
            $this->groupResourceModel->save($group);
        }


        /** @var  \Magento\Store\Model\Store $store */
        $store = $this->storeFactory->create();
        $store->load('needsaf_ticino_default');
        if(!$store->getId()){
            $group = $this->groupFactory->create();
            $group->load('Needsaf Ticino Default', 'name');
            $store->setCode('needsaf_ticino_default');
            $store->setName('Needsaf Ticino Default View');
            $store->setWebsite($website);
            $store->setGroupId($group->getId());
            $store->setData('is_active','1');
            $this->storeResourceModel->save($store);
            // Trigger event to insert some data to the sales_sequence_meta table (fix bug place order in checkout)
            $this->eventManager->dispatch('store_add', ['store' => $store]);
        }
    
        $setup->endSetup();
    }
}