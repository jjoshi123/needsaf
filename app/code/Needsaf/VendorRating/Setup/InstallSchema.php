<?php

namespace Needsaf\VendorRating\Setup;


use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class InstallSchema implements InstallSchemaInterface
{

    /**
     * Installs DB schema for a module
     *
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();
        $table = $installer->getConnection()->newTable(
            $installer->getTable('needsaf_vendorrating')
        )->addColumn(
            'vendorrating_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['identity' => true, 'nullable' => false, 'primary' => true],
            'Vendor Rating ID'
        )->addColumn(
            'vendor_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
            null,
            ['nullable' => false],
            'Vendor ID'
        )->addColumn(
            'customer_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            10,
            ['nullable' => false],
            'Customer ID'
        )->addColumn(
            'customer_name',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            256,
            ['nullable'=>false],
            'Customer Name'
        )->addColumn(
            'vendor_name',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            256,
            ['nullable'=>false],
            'Vendor Name'
        )->addColumn(
            'order_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['nullable' => true],
            'Order or Inquiry Id for which ratings are applied'
        )->addColumn(
            'rating_value',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['nullable' => false],
            'Rating Values'
        )->addColumn(
            'rating_description',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            1024,
            ['nullable' => true],
            'Rating Description'
        )->addColumn(
            'status',
            \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
            null,
            ['nullable' => false, 'default' => 0],
            'Rating Status'
        )->addColumn(
            'created_at',
            \Magento\Framework\DB\Ddl\Table::TYPE_DATETIME,
            null,
            ['nullable' => false],
            'Created At'
        )->addColumn(
            'updated_at',
            \Magento\Framework\DB\Ddl\Table::TYPE_DATETIME,
            null,
            ['nullable' => false],
            'Update At'
        )->addIndex($installer->getIdxName('needsaf_vendorrating','vendorrating_id'),'vendorrating_id'
        )->addForeignKey(
            $installer->getFkName(
                'needsaf_vendorrating',
                'vendor_id',
                'vendor_order',
                'order_id'
            ),
            'vendor_id',
            $installer->getTable('vendor_order'),
            'order_id',
            \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
        )->setComment('Vendor ratings');
        $installer->getConnection()->createTable($table);
        $installer->endSetup();
    }
}