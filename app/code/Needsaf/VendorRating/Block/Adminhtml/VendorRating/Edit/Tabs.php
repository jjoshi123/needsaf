<?php


namespace Needsaf\VendorRating\Block\Adminhtml\VendorRating\Edit;


class Tabs extends \Magento\Backend\Block\Widget\Tabs
{
    /**
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('vendorrating_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(__('Vendor Ratings Information'));
    }

    /**
     * @return $this
     */
    protected function _beforeToHtml()
    {
        return parent::_beforeToHtml();
    }
}