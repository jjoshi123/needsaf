<?php
/**
 * Created by PhpStorm.
 * User: krishaweb
 * Date: 8/8/17
 * Time: 6:42 PM
 */

namespace Needsaf\VendorRating\Block\Adminhtml\VendorRating\Edit\Tab;


class Main extends \Magento\Backend\Block\Widget\Form\Generic implements \Magento\Backend\Block\Widget\Tab\TabInterface
{


    /**
     * @var \Magento\Cms\Model\Wysiwyg\Config
     */
    protected $_wysiwygConfig;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Data\FormFactory $formFactory
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Magento\Cms\Model\Wysiwyg\Config $wysiwygConfig,
        array $data = []
    )
    {
        $this->_wysiwygConfig = $wysiwygConfig;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    protected function _prepareForm()
    {
        $model = $this->_coreRegistry->registry('current_model');
        $isElementDisabled = !$this->_isAllowedAction('Needsaf_VendorRating::vendorrating');
        $rating_status = array(
            array(
                'label' => 'Approved',
                'value' => '1',
            ),
            array(
                'label' => 'Dis Approved',
                'value' => '0',
            )
        );
        $rating_value = array(
            array(
                'label' => '1 Star',
                'value' => '1',
            ),
            array(
                'label' => '2 Star',
                'value' => '2',
            ),
            array(
                'label' => '3 Star',
                'value' => '3',
            ),
            array(
                'label' => '4 Star',
                'value' => '4',
            ),
            array(
                'label' => '5 Star',
                'value' => '5',
            )
        );

        $form = $this->_formFactory->create();
        $fieldset = $form->addFieldset('base_fieldset', ['legend' => __('Vendor Ratings')]);
        $wysiwygConfig = $this->_wysiwygConfig->getConfig(['tab_id' => $this->getTabId()]);
        if ($model->getId()) {
            $fieldset->addField('vendorrating_id', 'hidden', ['name' => 'id']);
        }
        $fieldset->addField(
            'customer_name',
            'text',
            [
                'name' => 'post[customer_name]',
                'label' => __('Customer Name'),
                'title' => __('Customer Name'),
                'disabled' => true
            ]
        );
        $fieldset->addField(
            'vendor_name',
            'text',
            [
                'name' => 'post[vendor_name]',
                'label' => __('Vendor Name'),
                'title' => __('Vendor Name'),
                'disabled' => true
            ]
        );

        $fieldset->addField(
            'rating_value',
            'select',
            [
                'name' => 'post[rating_value]',
                'label' => __('Rating Value'),
                'title' => __('Rating Value'),
                'disabled' => false,
                'values' => $rating_value,
                'style' => 'width:100%',
                'disabled' => true
            ]
        );
        $fieldset->addField(
            'status',
            'select',
            [
                'name' => 'post[status]',
                'label' => __('Rating Status'),
                'title' => __('Rating Status'),
                'values' => $rating_status,
                'style' => 'width:100%',
                'required' => true
            ]
        );
        $contentField = $fieldset->addField(
            'rating_description',
            'editor',
            [
                'name' => 'post[rating_description]',
                'label' => __('Rating Description'),
                'title' => __('Rating Description'),
                'config' => $wysiwygConfig
            ]
        );
        $renderer = $this->getLayout()->createBlock(
            'Magento\Backend\Block\Widget\Form\Renderer\Fieldset\Element'
        );
        $contentField->setRenderer($renderer);
        $this->_eventManager->dispatch('needsaf_vendorrating_post_edit_tab_main_prepare_form', ['form' => $form]);
        $form->setValues($model->getData());
        $this->setForm($form);
        return parent::_prepareForm();
    }

    /**
     * Return Tab label
     *
     * @return string
     * @api
     */
    public function getTabLabel()
    {
        return __('Vendor Ratings');
    }

    /**
     * Return Tab title
     *
     * @return string
     * @api
     */
    public function getTabTitle()
    {
        return __('Vendor Ratings');
    }

    /**
     * Can show tab in tabs
     *
     * @return boolean
     * @api
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * Tab is hidden
     *
     * @return boolean
     * @api
     */
    public function isHidden()
    {
        return false;
    }

    /**
     * Check permission for passed action
     *
     * @param string $resourceId
     * @return bool
     */
    protected function _isAllowedAction($resourceId)
    {
        return $this->_authorization->isAllowed($resourceId);
    }
}