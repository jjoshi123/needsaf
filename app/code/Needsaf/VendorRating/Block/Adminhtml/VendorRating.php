<?php

namespace Needsaf\VendorRating\Block\Adminhtml;


class VendorRating extends \Magento\Backend\Block\Widget\Grid\Container
{
   protected function _construct()
   {
       $this->_blockGroup = 'Needsaf_VendorRating';
       $this->_controller = 'adminhtml';
       $this->_headerText = __('Vendor Ratings');
       $this->removeButton('add');
   }

}