<?php

namespace Needsaf\VendorRating\Block;
use Needsaf\VendorRating\Model\VendorRatingFactory;

class View extends \Magento\Sales\Block\Order\View
{
    /**
     * @var string
     */
    protected $_template = 'Magento_Sales::order/view.phtml';

    protected $_vendorRatingFactory;

    protected $vendorRatingModel;
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\App\Http\Context $httpContext,
        \Magento\Payment\Helper\Data $paymentHelper,
        VendorRatingFactory $vendorRatingFactory,
        array $data = [])
    {
        $this->_vendorRatingFactory = $vendorRatingFactory;
        $this->vendorRatingModel = $this->_vendorRatingFactory->create();
        parent::__construct($context, $registry, $httpContext, $paymentHelper, $data);
    }


    public function getVendorRatingUrl()
    {
        return $this->getUrl('vendorrating/index/post/');
    }

    public function getPreviousRatings($orderId)
    {

        $vendorRatingData = $this->vendorRatingModel->load($orderId,'order_id');
        if ($vendorRatingData->hasData()&&$vendorRatingData->getStatus() != 1){
            return true;
        }else{
            return false;
        }
    }
    public function getVendorName($vendorId)
    {
        $vendorRatingData = $this->vendorRatingModel->load($vendorId,'vendor_id');
        return $vendorRatingData->getVendorName();
    }

    public function getVendorRating($orderId)
    {
        $vendorRatingData = $this->vendorRatingModel->load($orderId,'order_id');
        return $vendorRatingData->getData();
    }


}