<?php


namespace Needsaf\VendorRating\Model;

use Needsaf\VendorRating\Api\Data\VendorRatingInterface;
use Magento\Framework\DataObject\IdentityInterface;

class VendorRating extends \Magento\Framework\Model\AbstractModel implements VendorRatingInterface,IdentityInterface

{
    /**
     * rating's Status enabled
     */
    const STATUS_APPROVED = 1;

    /**
     * rating's Status disabled
     */
    const STATUS_DISAPPROVED = 0;


    /**
     * CMS page cache tag
     */
    const CACHE_TAG = 'needsaf_vendorrating';

    /**
     * @var string
     */
    protected $_cacheTag = 'needsaf_vendorrating';

    /**
     * Prefix of model events names
     *
     * @var string
     */
    protected $_eventPrefix = 'needsaf_vendorrating';

    public function _construct()
    {
        $this->_init('Needsaf\VendorRating\Model\ResourceModel\VendorRating');
    }

    /**
     * @param bool $plural
     * @return string
     */
    public function getOwnTitle($plural = false)
    {
        return $plural ? 'Vendor Rating' : 'Vendor Ratings';
    }

    /**
     * @return array
     */
    public function getAvailableStatuses()
    {
        return [self::STATUS_APPROVED => __('Approved'), self::STATUS_DISAPPROVED => __('Dis Approved')];
    }
    /**
     * Return unique ID(s) for each object in system
     *
     * @return string[]
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    public function getId()
    {
        return $this->getData(self::VENDORRATING_ID);
    }

    public function setId($id)
    {
        return $this->setData(self::VENDORRATING_ID,$id);
    }

    public function getVendorId()
    {
        return $this->getData(self::VENDOR_ID);

    }

    public function setVendorId($vendorId)
    {
        return $this->setData(self::VENDOR_ID,$vendorId);
    }

    public function getCustomerId()
    {
        return $this->getData(self::CUSTOMER_ID);
    }

    public function setCustomerId($customerId)
    {
        return $this->setData(self::CUSTOMER_ID,$customerId);
    }

    public function getRatingValue()
    {
        return $this->getData(self::RATING_VALUE);
    }

    public function setRatingValue($ratingValue)
    {
        return $this->setData(self::RATING_VALUE,$ratingValue);
    }

    public function getRatingDescription()
    {
        return $this->getData(self::RATING_DESCRIPTION);
    }

    public function setRatingDescription($ratingDescription)
    {
        return $this->setData(self::RATING_DESCRIPTION,$ratingDescription);
    }

    public function getStatus()
    {
        return (bool)$this->getData(self::STATUS);
    }

    public function setStatus($status)
    {
        return $this->setData(self::STATUS,$status);
    }

    public function getCreatedAt()
    {
        return $this->getData(self::CREATED_AT);
    }

    public function setCreatedAt($createdAt)
    {
        return $this->setData(self::CREATED_AT,$createdAt);
    }

    public function getUpdatedAt()
    {
        return $this->getData(self::UPDATED_AT);
    }

    public function setUpdatedAt($updatedAt)
    {
        return $this->setData(self::UPDATED_AT,$updatedAt);
    }

    public function getCustomerName()
    {
        return $this->getData(self::CUSTOMER_NAME);
    }

    public function setCutomerName($customerName)
    {
        return $this->setData(self::CUSTOMER_NAME,$customerName);
    }

    public function getVendorName()
    {
        return $this->getData(self::VENDOR_NAME);
    }

    public function setVendorName($vendorName)
    {
        return $this->setData(self::VENDOR_NAME,$vendorName);
    }

    public function getOrderId()
    {
        return $this->getData(self::ORDER_ID);
    }

    public function setOrderId($orderId)
    {
        return $this->setData(self::ORDER_ID,$orderId);
    }
}