<?php


namespace Needsaf\VendorRating\Model\ResourceModel\VendorRating;


class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected $_idFieldName = 'vendorrating_id';

    protected function _construct()
    {
        $this->_init('Needsaf\VendorRating\Model\VendorRating','Needsaf\VendorRating\Model\ResourceModel\VendorRating');
    }

}