<?php

namespace Needsaf\VendorRating\Controller\Adminhtml;


class VendorRating extends Actions
{
    /**
     * Form session key
     * @var string
     */
    protected $_formSessionKey  = 'needsaf_vendorrating_form_data';

    /**
     * Allowed Key
     * @var string
     */
    protected $_allowedKey      = 'Needsaf_VendorRating::vendorrating';

    /**
     * Model class name
     * @var string
     */
    protected $_modelClass      = 'Needsaf\VendorRating\Model\VendorRating';

    /**
     * Active menu key
     * @var string
     */
    protected $_activeMenu      = 'Needsaf_VendorRating::vendorrating';

    /**
     * Status field name
     * @var string
     */
    protected $_statusField     = 'status';

    /**
     * Save request params key
     * @var string
     */
    protected $_paramsHolder    = 'post';
}