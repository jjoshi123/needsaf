<?php


namespace Needsaf\VendorRating\Controller\Adminhtml\VendorRating;


use Magento\Backend\App\Action;
use Needsaf\Membership\Model\VendorFactory;
use Needsaf\VendorRating\Controller\Adminhtml\VendorRating;
use Needsaf\VendorRating\Model\VendorRating as VendorRatingModel;
use Needsaf\VendorRating\Model\VendorRatingFactory;
class Save extends VendorRating
{
    protected $_vendorRatingModel;
    protected $_vendorRatingFactory;
    protected $_vendorFactory;
public function __construct(VendorFactory $vendorFactory,VendorRatingModel $vendorRatingModel,VendorRatingFactory $vendorRatingFactory,Action\Context $context)
{
    $this->_vendorRatingModel = $vendorRatingModel;
    $this->_vendorFactory = $vendorFactory;
    $this->_vendorRatingFactory = $vendorRatingFactory;
    parent::__construct($context);
}

protected function _beforeSave($model, $request)
{
    $data = $model->getData();
    $vendorRatingData =$this->_vendorRatingModel->load($data['vendorrating_id'])->getData();
    $status = $data['status'];
    if($status == 1 && $status!=$vendorRatingData['status']){
        $vendorRatingModel = $this->_vendorRatingFactory->create();
        $vendorModel = $this->_vendorFactory->create();
        $vendorData = $vendorModel->load($data['vendor_id']);
        $fivestarRating = $vendorRatingModel->getCollection()->addFieldToFilter('vendor_id',$data['vendor_id'])->addFieldToFilter('rating_value',5)->addFieldToFilter('status',1)->load()->count();
        $fourstarRating = $vendorRatingModel->getCollection()->addFieldToFilter('vendor_id',$data['vendor_id'])->addFieldToFilter('rating_value',4)->addFieldToFilter('status',1)->load()->count();
        $threestarRating = $vendorRatingModel->getCollection()->addFieldToFilter('vendor_id',$data['vendor_id'])->addFieldToFilter('rating_value',3)->addFieldToFilter('status',1)->load()->count();
        $twostarRating = $vendorRatingModel->getCollection()->addFieldToFilter('vendor_id',$data['vendor_id'])->addFieldToFilter('rating_value',2)->addFieldToFilter('status',1)->load()->count();
        $onestarRating = $vendorRatingModel->getCollection()->addFieldToFilter('vendor_id',$data['vendor_id'])->addFieldToFilter('rating_value',1)->addFieldToFilter('status',1)->load()->count();

        $divisor = $fivestarRating+$fourstarRating+$threestarRating+$twostarRating+$onestarRating;
        $count = ((5*$fivestarRating) + (4*$fourstarRating) + (3*$threestarRating) + (2*$twostarRating) + (1*$onestarRating)) / ($divisor);
        $vendorData->setVendorRating($count);
        $vendorData->save();

    }

    parent::_beforeSave($model, $request);
}
}