<?php


namespace Needsaf\VendorRating\Controller\Index;
use \Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ResponseInterface;
use Magento\Customer\Model\CustomerFactory;
use Needsaf\Membership\Model\VendorFactory;
use Needsaf\VendorRating\Model\VendorRatingFactory;
use Magento\Framework\Controller\ResultFactory;


class Post extends Action
{
    protected $_customerFactory;
    protected $_vendorFactory;
    protected $_vendorRatingFactory;
    public function __construct(
        Context $context,
        VendorFactory $vendorFactory,
        CustomerFactory $customerFactory,
        VendorRatingFactory $vendorRatingFactory
    )
    {
        $this->_vendorFactory = $vendorFactory;
        $this->_customerFactory = $customerFactory;
        $this->_vendorRatingFactory = $vendorRatingFactory;
        parent::__construct($context);
    }

    /**
     * Dispatch request
     *
     * @return \Magento\Framework\Controller\ResultInterface|ResponseInterface
     * @throws \Magento\Framework\Exception\NotFoundException
     */
    public function execute()
    {
        $post = $this->getRequest()->getParams();
        $post['rating_value'] = $post['ratings'];
        $post['status'] = 0;
        $customerModel = $this->_customerFactory->create();
        $customerData = $customerModel->load($post['customer_id']);
        $post['customer_name'] = $customerData->getName();
        $vendorModel = $this->_vendorFactory->create();
        $vendorData = $vendorModel->load($post['vendor_id']);
        $post['vendor_name'] = $vendorData->getFirstname() .' '.$vendorData->getLastname();
        $vendorRatingModel = $this->_vendorRatingFactory->create();
        $vendorRatingModel->setData($post);
        $vendorRatingModel->save();
       /* $fivestarRating = $vendorRatingModel->getCollection()->addFieldToFilter('vendor_id',$post['vendor_id'])->addFieldToFilter('rating_value',5)->load()->count();
        $fourstarRating = $vendorRatingModel->getCollection()->addFieldToFilter('vendor_id',$post['vendor_id'])->addFieldToFilter('rating_value',4)->load()->count();
        $threestarRating = $vendorRatingModel->getCollection()->addFieldToFilter('vendor_id',$post['vendor_id'])->addFieldToFilter('rating_value',3)->load()->count();
        $twostarRating = $vendorRatingModel->getCollection()->addFieldToFilter('vendor_id',$post['vendor_id'])->addFieldToFilter('rating_value',2)->load()->count();
        $onestarRating = $vendorRatingModel->getCollection()->addFieldToFilter('vendor_id',$post['vendor_id'])->addFieldToFilter('rating_value',1)->load()->count();

        $divisor = $fivestarRating+$fourstarRating+$threestarRating+$twostarRating+$onestarRating;
        $count = ((5*$fivestarRating) + (4*$fourstarRating) + (3*$threestarRating) + (2*$twostarRating) + (1*$onestarRating)) / ($divisor);
        $vendorData->setVendorRating($count);
        $vendorData->save();*/

        $this->messageManager->addSuccessMessage(
            __('Thanks for submitting your vendor review.')
        );
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        $resultRedirect->setUrl($this->_redirect->getRefererUrl());
        return $resultRedirect;
    }
}