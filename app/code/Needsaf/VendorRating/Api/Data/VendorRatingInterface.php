<?php
namespace Needsaf\VendorRating\Api\Data;
/**
 * Interface VendorRatingInterface
 */
interface VendorRatingInterface
{

    const VENDORRATING_ID = 'vendorrating_id';
    const VENDOR_ID = 'vendor_id';
    const CUSTOMER_ID = 'customer_id';
    const CUSTOMER_NAME = 'customer_name';
    const VENDOR_NAME = 'vendor_name';
    const ORDER_ID = 'order_id';
    const RATING_VALUE = 'rating_value';
    const RATING_DESCRIPTION = 'rating_description';
    const STATUS = 'status';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    public function getId();

    public function setId($id);

    public function getVendorId();

    public function setVendorId($vendorId);

    public function getCustomerId();

    public function setCustomerId($customerId);

    public function getCustomerName();

    public function setCutomerName($customerName);

    public function getVendorName();

    public function setVendorName($vendorName);

    public function getOrderId();

    public function setOrderId($orderId);

    public function getRatingValue();

    public function setRatingValue($ratingValue);

    public function getRatingDescription();

    public function setRatingDescription($ratingDescription);

    public function getStatus();

    public function setStatus($status);

    public function getCreatedAt();

    public function setCreatedAt($createdAt);

    public function getUpdatedAt();

    public function setUpdatedAt($updatedAt);


 }