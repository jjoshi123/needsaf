<?php


namespace Needsaf\User\Setup;

use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\UpgradeSchemaInterface;

class UpgradeSchema implements UpgradeSchemaInterface
{

    /**
     * Upgrades DB schema for a module
     *
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();
        if (version_compare($context->getVersion(), '1.0.0', '<')) {
            $tableAdmins = $installer->getTable('admin_user');
            $installer->getConnection()->addColumn(
                $tableAdmins,
                'is_vendor',
                [
                    'type' =>\Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                    'nullable' => true,
                    'default' => 0,
                    'comment' => 'Is vendor'
                ]
            );
            $installer->getConnection()->addIndex(
                $tableAdmins,
                $installer->getIdxName('admin_user', ['is_vendor']),
                ['is_vendor']);
        }
        if (version_compare($context->getVersion(), '1.0.1', '<')) {
            $tableAdmins = $installer->getTable('admin_user');
            $installer->getConnection()->addColumn(
                $tableAdmins,
                'vendor_id',
                [
                    'type' =>\Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    'nullable' => true,
                    'default' => 0,
                    'comment' => 'Vendor ID'
                ]
            );
            $installer->getConnection()->addIndex(
                $tableAdmins,
                $installer->getIdxName('admin_user', ['vendor_id']),
                ['vendor_id']);
        }

        $installer->endSetup();
    }
}