<?php


namespace Needsaf\User\Setup;


use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Eav\Setup\EavSetup;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\ObjectManagerInterface;

class InstallData implements InstallDataInterface
{

    /**
     * EAV setup factory
     *
     * @var EavSetupFactory
     */
    private $eavSetupFactory;

    /**
     * EAV setup
     *
     * @var EavSetup
     */
    private $eavSetup;
    /**
     * @var ObjectManagerInterface
     */
    private $objectManager;

    public  function __construct(
        EavSetupFactory $eavSetupFactory,
        ObjectManagerInterface $objectManager
    )
    {
        $this->eavSetupFactory = $eavSetupFactory;
        $this->objectManager = $objectManager;
    }

    /**
     * Installs data for a module
     *
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     */
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        $this->eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
        $this->eavSetup->removeAttribute(\Magento\Catalog\Model\Product::ENTITY,'admin_user');
        $this->eavSetup->removeAttribute(\Magento\Catalog\Model\Product::ENTITY,'duplicatedin');
        $this->eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'admin_user',
            [
                'type' => 'int',
                'backend' => '',
                'frontend' => '',
                'label' => 'Admin Id',
                'input' => 'text',
                'class' => 'input-text',
                'source' => '',
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                'visible' => true,
                'required' => false,
                'user_defined' => false,
                'default' => '1',
                'searchable' => false,
                'filterable' => false,
                'comparable' => false,
                'visible_on_front' => false,
                'used_in_product_listing' => false,
                'unique' => false,
                'apply_to' => 'simple,configurable,virtual,bundle,downloadable,grouped',
                'group'=> 'General'
            ]
        );
        $this->eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'duplicatedin',
            [
                'type' => 'int',
                'backend' => '',
                'frontend' => '',
                'label' => 'Duplicated In',
                'input' => 'text',
                'class' => 'input-text',
                'source' => '',
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                'visible' => true,
                'required' => false,
                'user_defined' => false,
                'default' => '',
                'searchable' => false,
                'filterable' => false,
                'comparable' => false,
                'visible_on_front' => false,
                'used_in_product_listing' => false,
                'unique' => false,
                'apply_to' => 'simple,configurable,virtual,bundle,downloadable,grouped',
                'group'=> 'General'
            ]
        );
        $setup->endSetup();

    }
}