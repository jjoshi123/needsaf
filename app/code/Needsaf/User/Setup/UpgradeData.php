<?php


namespace Needsaf\User\Setup;


use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Eav\Setup\EavSetup;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\ObjectManagerInterface;

class UpgradeData implements UpgradeDataInterface
{

    /**
     * EAV setup factory
     *
     * @var EavSetupFactory
     */
    private $eavSetupFactory;

    /**
     * EAV setup
     *
     * @var EavSetup
     */
    private $eavSetup;
    /**
     * @var ObjectManagerInterface
     */
    private $objectManager;

    public  function __construct(
        EavSetupFactory $eavSetupFactory,
        ObjectManagerInterface $objectManager
    )
    {
        $this->eavSetupFactory = $eavSetupFactory;
        $this->objectManager = $objectManager;
    }
    /**
     * Upgrades data for a module
     *
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     */
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();
        if (version_compare($context->getVersion(),'1.0.2','<') )
        {
            $this->eavSetup = $this->eavSetupFactory->create(['setup' => $installer]);
            $this->eavSetup->removeAttribute(\Magento\Catalog\Model\Product::ENTITY,'vendor_id');
            $this->eavSetup->addAttribute(
                \Magento\Catalog\Model\Product::ENTITY,
                'vendor_id',
                [
                    'type' => 'int',
                    'backend' => '',
                    'frontend' => '',
                    'label' => 'Vendor Id',
                    'input' => 'text',
                    'class' => 'input-text',
                    'source' => '',
                    'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                    'visible' => true,
                    'required' => false,
                    'user_defined' => false,
                    'searchable' => false,
                    'filterable' => false,
                    'comparable' => false,
                    'visible_on_front' => false,
                    'used_in_product_listing' => false,
                    'unique' => false,
                    'apply_to' => 'simple,configurable,virtual,bundle,downloadable,grouped',
                    'group'=> 'General'
                ]
            );
        }
    }
}