<?php

namespace Needsaf\User\Observer;


use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

class ProductSaveBefore implements ObserverInterface
{
    protected $authSession;
    protected $_userFactory;
    protected $resultJsonFactory;
    protected $eavAttributeRepository;
    protected $_registry;
    protected $_productRepository;
    protected $connection;
    protected $resource;
    protected $messageManager;
    protected $response;
    protected $redirect;


    public function __construct(\Magento\Backend\Model\Auth\Session $authSession,
                                \Magento\Backend\Block\Template\Context $context,
                                \Magento\User\Model\UserFactory $userFactory,
                                \Magento\Eav\Api\AttributeRepositoryInterface $eavAttributeRepository,
                                \Magento\Catalog\Model\ProductRepository $productRepository,
                                \Magento\Framework\Registry $registry,
                                \Magento\Framework\App\ResourceConnection $resource,
                                \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
                                \Magento\Framework\Message\ManagerInterface $messageManager,
                                \Magento\Framework\App\Response\Http $response,
                                \Magento\Framework\App\Response\RedirectInterface $redirect
    )
    {
        $this->authSession = $authSession;
        $this->_userFactory = $userFactory;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->eavAttributeRepository = $eavAttributeRepository;
        $this->_productRepository = $productRepository;
        $this->resource = $resource;
        $this->_registry = $registry;
        $this->messageManager = $messageManager;
        $this->response = $response;
        $this->redirect = $redirect;

    }

    /**
     * @param Observer $observer
     * @return void
     */

    public function getUserRole()
    {
        $roleName = $this->authSession->getUser()->getRole()->getRoleName();

        return $roleName;
    }


    public function getCurrentProduct()
    {
        return $this->_registry->registry('current_product');
    }


    public function getProductBySku($sku)
    {
        return $this->_productRepository->get($sku);
    }

    public function getProductById($id)
    {
        return $this->_productRepository->getById($id);
    }


    public function getConnection()
    {
        if (!$this->connection) {
            $this->connection = $this->resource->getConnection();
        }
        return $this->connection;
    }

    public function getAttributeData($value)
    {
        $this->getConnection();
        $criteriaSql = "select * from eav_attribute_option_value where value_id=$value";
        $criteriaResult = $this->connection->fetchAll($criteriaSql);
        foreach ($criteriaResult as $value1) {
            $attributevalue = $value1['value'];
        }
        return $attributevalue;
    }


    public function getAttributeCodes()
    {
        $this->getConnection();
        $criteriaSql = "select attribute_id from eav_attribute where attribute_code IN('price','panel','entry_sensor','motion_sensor','motion_sensor_video','keychain_remote','keypad','auxiliary_siren','glassbreak_sensor','smoke_alarm','panic_button')";
        $criteriaResult = $this->connection->fetchAll($criteriaSql);
        return $criteriaResult;
    }


    public function getParentId($productid)
    {
        if ($this->getUserRole() != 'Administrators') {
            $productData = $this->_productRepository->getById($productid)->getCustomAttribute('duplicatedin');
            $parentId = '';
            if ($productData) {
                $parentId = $productData->getValue();
            }

            return $parentId;
        }


    }

    public function execute(Observer $observer)
    {

        $currentProduct = $this->getCurrentProduct();
        $productId = $currentProduct->getId();
        $parentId = $this->getParentId($productId);
        $flag = false;
        if ($parentId) {
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $productObject = $objectManager->create('\Magento\Catalog\Model\Product');
            $product = $productObject->load($parentId);


            /* for getting parent product int values */
            $entrysensor = $product->getData('entry_sensor');
            $motionsensor = $product->getData('motion_sensor');
            $motionsensorvideo = $product->getData('motion_sensor_video');
            $keychainremote = $product->getData('keychain_remote');

            $price = $product->getPrice();
            $productprice = (float)str_replace(",", "", $price);
            $vendorproductprice = (float)str_replace(",", "", $_POST['product']['price']);

            if ($this->getUserRole() != 'Administrators') {

                if ($_POST['product']['entry_sensor'] < $entrysensor) {

                    $this->messageManager->addErrorMessage(
                        __('Please enter value for entry sensor equal to ' . $entrysensor . ' or greater than ' . $entrysensor . '')
                    );
                    $flag = true;
                }

                if ($_POST['product']['motion_sensor'] < $motionsensor) {
                    $this->messageManager->addErrorMessage(
                        __('Please enter value for motion sensor equal to ' . $motionsensor . ' or greater than ' . $motionsensor . '')
                    );
                    $flag = true;
                }

                if ($_POST['product']['motion_sensor_video'] < $motionsensorvideo) {
                    $this->messageManager->addErrorMessage(
                        __('Please enter value for motion sensor video equal to ' . $motionsensorvideo . ' or greater than ' . $motionsensorvideo . '')
                    );
                    $flag = true;
                }

                if ($_POST['product']['keychain_remote'] < $keychainremote) {
                    $this->messageManager->addErrorMessage(
                        __('Please enter value for keychain remote equal to ' . $keychainremote . ' or greater than ' . $keychainremote . '')
                    );
                    $flag = true;
                }

                if ($vendorproductprice > $productprice) {
                    $this->messageManager->addErrorMessage(
                        __('Please enter value for price equal to ' . $price . ' or smaller than ' . $price . '')
                    );
                    $flag = true;
                }

                if ($flag === true) {
                    throw new \Exception('Product is not saved.');
                    $this->redirect->redirect($this->response, 'magento/catalog/product/edit', array('id' => $productId));
                    exit();
                }
            }
        }

        $product = $observer->getProduct();
        $product->setData('admin_user', $this->authSession->getUser()->getId());
        if ($this->authSession->getUser()->getData('vendor_id') > 0) {
            $product->setData('vendor_id', $this->authSession->getUser()->getData('vendor_id'));
        }
    }


}