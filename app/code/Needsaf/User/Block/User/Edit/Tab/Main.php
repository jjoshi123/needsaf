<?php

namespace Needsaf\User\Block\User\Edit\Tab;


class Main extends \Magento\User\Block\User\Edit\Tab\Main
{
    protected function _prepareForm()
    {
        $rs = parent::_prepareForm();
        $form = parent::getForm();
        $fieldset = $form->getElement('base_fieldset');
        $fieldset->addField(
            'is_vendor',
            'select',
            [
                'name' => 'is_vendor',
                'title' => __('Is Vendor'),
                'label' => __('Is Vendor'),
                'values' => [
                    [
                        'label' => __('No'),
                        'value' => 0
                    ],
                    [
                        'label' => __('Yes'),
                        'value' => 1
                    ]
                ],
                'required' => false
            ]
        );
        $fieldset->addField(
            'vendor_id',
            'text',
            [
                'name' => 'vendor_id',
                'title' => __('Vendor Id'),
                'label' => __('Vendor Id'),
                'required' => false,
                'class' => 'disabled',
                'disabled' => true
            ]
        );
        $model = $this->_coreRegistry->registry('permissions_user');
        $form->addValues(['is_vendor' => $model->getIsVendor()]);
        $form->addValues(['vendor_id' => $model->getVendorId()]);
        $this->setForm($form);
        return $rs;
    }

}