<?php
namespace Needsaf\Membership\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;

/**
 * Class InstallSchema
 * @package Needsaf\Membership\Setup
 */
class InstallSchema implements InstallSchemaInterface
{
    /**
     * Installs DB schema for a module
     *
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;

        $installer->startSetup();

        $table = $installer->getConnection()->newTable(
            $installer->getTable('needsaf_membership_details')
        )->addColumn(
            'membership_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
            null,
            ['identity' => true, 'nullable' => false, 'primary' => true],
            'Membership ID'
        )->addColumn(
            'membership_type',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            [
                'nullable' => true,
                'default' => null
            ],
            'Membership Type'
        )->addColumn(
            'product_numbers',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            11,
            [
                'nullable' => true,
                'default' => null
            ],
            'Number Of Products'
        )->addColumn(
            'usd_price',
            \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL, '12,2',
            [],
            'USD Price'
        )->addColumn(
            'euro_price',
            \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL, '12,2',
            [],
            'Euro Price'
        )->addColumn(
            'swiss_price',
            \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL, '12,2',
            [],
            'Swiss Price'
        )->addColumn(
            'months',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            [
                'nullable' => true,
                'default' => null
            ],
            'Number Of Months'
        )->addColumn(
            'days',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            [
                'nullable' => true,
                'default' => null
            ],
            'Days'
        )->addColumn(
            'time_from',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            [
                'nullable' => true,
                'default' => null
            ],
            'Time From'
        )->addColumn(
            'time_to',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            [
                'nullable' => true,
                'default' => null
            ],
            'Time To'
        )->addColumn(
            'storeadmin_numbers',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            11,
            [
                'nullable' => true,
                'default' => null
            ],
            'Number Of Store Admin'
        )->addColumn(
            'description',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            '2M',
            [],
            'Description'
        )->addColumn(
            'is_active',
            \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
            null,
            ['nullable' => false, 'default' => '1'],
            'Is Member Active?'
        )->addColumn(
            'creation_time',
            \Magento\Framework\DB\Ddl\Table::TYPE_DATETIME,
            null,
            ['nullable' => false],
            'Category Creation Time'
        )->addColumn(
            'update_time',
            \Magento\Framework\DB\Ddl\Table::TYPE_DATETIME,
            null,
            ['nullable' => false],
            'Category Update Time'
        )->setComment('Membership Details');
        $installer->getConnection()->createTable($table);
       /*order table*/
        $table = $installer->getConnection()->newTable(
            $installer->getTable('vendor_order')
        )->addColumn(
            'order_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
            null,
            ['identity' => true, 'nullable' => false, 'primary' => true],
            'Order Id'
        )->addColumn(
            'firstname',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            ['nullable' => false],
            'FirstName'
        )->addColumn(
            'lastname',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            ['nullable' => false],
            'LastName'
        )->addColumn(
            'username',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            ['nullable' => false],
            'Username'
        )->addColumn(
            'email',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            ['nullable' => false],
            'Email'
        )->addColumn(
            'membership_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
            null,
            ['nullable' => false, 'primary' => true],
            'Membership ID'
        )->addColumn(
            'order_date',
            \Magento\Framework\DB\Ddl\Table::TYPE_DATETIME,
            null,
            ['nullable' => false],
            'Order Date'
        )->addColumn(
            'transaction_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            512,
            ['nullable' => false],
            'Transaction ID'
        )->addColumn(
            'vendor_status',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            ['nullable' => false],
            'Vendor Status'
        )->addColumn(
            'street',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            '255',
            ['nullable' => false],
            'Street'
        )->addColumn(
            'city',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            '255',
            ['nullable' => false],
            'City'
        )->addColumn(
            'state',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            '255',
            ['nullable' => false],
            'State'
        )->addColumn(
            'country',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            '255',
            ['nullable' => false],
            'Country'
        )->addColumn(
            'zip_code',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            '255',
            ['nullable' => false],
            'Zip Code'
        )->addColumn(
            'is_active',
            \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
            null,
            ['nullable' => false, 'default' => '1'],
            'Is vendor Active?'
        )->addColumn(
            'store_name',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            ['nullable' => false],
            'Store Name'
        )->addColumn(
            'creation_time',
            \Magento\Framework\DB\Ddl\Table::TYPE_DATETIME,
            null,
            ['nullable' => false],
            'Vendor Creation Time'
        )->addColumn(
            'update_time',
            \Magento\Framework\DB\Ddl\Table::TYPE_DATETIME,
            null,
            ['nullable' => false],
            'Vendor Update Time'
        )->addForeignKey(
            $installer->getFkName(
                'member_registration','membership_id',
                'needsaf_membership_details','membership_id'),
            'membership_id',
            $installer->getTable(
                'needsaf_membership_details'),
            'membership_id',
            \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
        )->setComment('vendor registration table');
        $installer->getConnection()->createTable($table);
        $installer->endSetup();
    }

}