<?php
namespace Needsaf\Membership\Model\ResourceModel\Vendor;


/**
 * Class Collection
 * @package Needsaf\Membership\Model\ResourceModel\Member
 */
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'id';
    protected function _construct()
    {
        $this->_init('Needsaf\Membership\Model\Vendor', 'Needsaf\Membership\Model\ResourceModel\Vendor');
    }


}