<?php
namespace Needsaf\Membership\Model\ResourceModel\Membership;


/**
 * Class Collection
 * @package Needsaf\Membership\Model\ResourceModel\Membership
 */
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'membership_id';
    protected function _construct()
    {
        $this->_init('Needsaf\Membership\Model\Membership', 'Needsaf\Membership\Model\ResourceModel\Membership');
    }

}