<?php

namespace Needsaf\Membership\Model\ResourceModel\Vendormembershipdetails;


/**
 * Class Collection
 * @package Needsaf\Membership\Model\ResourceModel\Member
 */
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * @var string
     */
    protected $authSession;
    protected $userFactory;
    protected $_idFieldName = 'payment_id';

    protected function _construct()
    {
        $this->_init('Needsaf\Membership\Model\Vendormembershipdetails', 'Needsaf\Membership\Model\ResourceModel\Vendormembershipdetails');
    }

    /**
     * Collection constructor.
     * @param \Magento\Framework\Data\Collection\EntityFactoryInterface $entityFactory
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Magento\Framework\Data\Collection\Db\FetchStrategyInterface $fetchStrategy
     * @param \Magento\Framework\Event\ManagerInterface $eventManager
     * @param \Magento\Framework\DB\Adapter\AdapterInterface|null $connection
     * @param \Magento\Framework\Model\ResourceModel\Db\AbstractDb|null $resource
     * @param \Magento\Backend\Model\Auth\Session $authSession
     * @param \Magento\User\Model\UserFactory $userFactory
     */
    public function __construct(\Magento\Backend\Model\Auth\Session $authSession, \Magento\User\Model\UserFactory $userFactory, \Magento\Framework\Data\Collection\EntityFactoryInterface $entityFactory, \Psr\Log\LoggerInterface $logger, \Magento\Framework\Data\Collection\Db\FetchStrategyInterface $fetchStrategy, \Magento\Framework\Event\ManagerInterface $eventManager, \Magento\Framework\DB\Adapter\AdapterInterface $connection = null, \Magento\Framework\Model\ResourceModel\Db\AbstractDb $resource = null)
    {
        $this->authSession = $authSession;
        $this->userFactory = $userFactory;
        parent::__construct($entityFactory, $logger, $fetchStrategy, $eventManager, $connection, $resource);
    }

    protected function _initSelect()
    {
        parent::_initSelect();

        $this->getSelect()->joinLeft(
            ['secondTable' => $this->getTable('vendor')], //2nd table name by which you want to join mail table
            'main_table.vendor_id = secondTable.id', // common column which available in both table
            ['vendor_status', 'name' => "CONCAT(firstname,' ',lastname)",'address' => "CONCAT(street,',',city,',',state,',',country,',',zip_code)"] // '*' define that you want all column of 2nd table. if you want some particular column then you can define as ['column1','column2']
        )->joinLeft(['anotherTable' => $this->getTable('needsaf_membership_details')],
            'main_table.membership_id=anotherTable.membership_id',
            ['membership_type']);

        $roleName = $this->authSession->getUser()->getRole()->getRoleName();
        $userId = $this->authSession->getUser()->getId();
        $user = $this->userFactory->create()->load($userId);
        $vendorId = $user->getVendorId();
        if ($roleName != 'Administrators') {
            $this->addFieldToFilter('vendor_id', $vendorId)->load();
        }

    }


}
