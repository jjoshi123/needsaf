<?php
namespace Needsaf\Membership\Model;
use Needsaf\Membership\Api\Data\datetime;
use Needsaf\Membership\Api\Data\text;
use Needsaf\Membership\Api\Data\VendormembershipdetailsInterface;
use Magento\Framework\DataObject\IdentityInterface;


/**
 * Class Member
 * @package Needsaf\Membership\Model
 */
class Vendormembershipdetails  extends \Magento\Framework\Model\AbstractModel implements VendormembershipdetailsInterface, IdentityInterface
{


    /**
     * CMS page cache tag
     */
    const CACHE_TAG = 'needsaf_membership';

    /**
     * @var string
     */
    protected $_cacheTag = 'needsaf_membership';

    /**
     * Prefix of model events names
     *
     * @var string
     */
    protected $_eventPrefix = 'needsaf_membership';

    /**
     * @var $setup;
     */
    protected $setup;


    /**
     * Member constructor.
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Model\ResourceModel\AbstractResource|null $resource
     * @param \Magento\Framework\Data\Collection\AbstractDb|null $resourceCollection
     * @param array $data
     */
    function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = [])
    {
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);

    }
    protected function _construct()
    {
        $this->_init('Needsaf\Membership\Model\ResourceModel\Vendormembershipdetails');
    }


    /**
     * Return unique ID(s) for each object in system
     *
     * @return string[]
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getPaymentId()];
    }

    /**
     * Get payment Id
     *
     * @return Int
     */
    public function getPaymentId()
    {
        return $this->getData(self::PAYMENT_ID);
    }

    /**
     * Get vendor ID
     *
     * @return Int
     */
    public function getVendorId()
    {
        return $this->getData(self::VENDOR_ID );
    }

    /**
     * Get Membership Id
     *
     * @return Int
     */
    public function getMembershipId()
    {
        return $this->getData(self::MEMBERSHIP_ID);
    }

    /**
     * Get Start Date
     *
     * @return datetime
     */
    public function getStartDate()
    {
        return $this->getData(self::START_DATE);
    }

    /**
     * Get End Date
     *
     * @return datetime
     */
    public function getEndDate()
    {
        return $this->getData(self::END_DATE);
    }

    /**
     * Get Created Date
     *
     * @return datetime
     */
    public function getCreatedDate()
    {
        return $this->getData(self::CREATED_DATE);
    }

    /**
     * Get Membership data
     *
     * @return text
     */
    public function getMembershipData()
    {
        return $this->getData(self::MEMBERSHIP_DATA);
    }

    /**
     * Get Payment Data
     *
     * @return text
     */
    public function getPaymentData()
    {
        return $this->getData(self::PAYMENT_DATA);
    }

    /**
     * set Payment Id
     *
     * @param string $id
     * @return \Needsaf\Membership\Api\Data\VendormembershipdetailsInterface
     */
    public function setPaymentId($id)
    {
        return $this->setData(self:: PAYMENT_ID, $id);
    }

    /**
     * set Vendor Id
     *
     * @param string $VendorId
     * @return \Needsaf\Membership\Api\Data\VendormembershipdetailsInterface
     */
    public function setVendorId($VendorId)
    {
        return $this->setData(self:: VENDOR_ID, $VendorId);
    }

    /**
     * set Membership Id
     *
     * @param string $membershipId
     * @return \Needsaf\Membership\Api\Data\VendormembershipdetailsInterface
     */
    public function setMembershipId($membershipId)
    {
        return $this->setData(self:: MEMBERSHIP_ID, $membershipId);
    }

    /**
     * set Start Date
     *
     * @param string $StartDate
     * @return \Needsaf\Membership\Api\Data\VendormembershipdetailsInterface
     */
    public function setStartDate($StartDate)
    {
        return $this->setData(self::START_DATE, $StartDate);

    }

    /**
     * set End Date
     *
     * @param string $EndDate
     * @return \Needsaf\Membership\Api\Data\VendormembershipdetailsInterface
     */
    public function setEndDate($EndDate)
    {
        return $this->setData(self::END_DATE, $EndDate);
    }

    /**
     * set Created Date
     *
     * @param string $CreatedDate
     * @return \Needsaf\Membership\Api\Data\VendormembershipdetailsInterface
     */
    public function setCreatedDate($CreatedDate)
    {
        return $this->setData(self::CREATED_DATE, $CreatedDate);
    }

    /**
     * set Membership Data
     *
     * @param string $MembershipData
     * @return \Needsaf\Membership\Api\Data\VendormembershipdetailsInterface
     */
    public function setMembershipData($MembershipData)
    {
        return $this->setData(self::MEMBERSHIP_DATA, $MembershipData);
    }

    /**
     * set Payment Data
     *
     * @param string $PaymentData
     * @return \Needsaf\Membership\Api\Data\VendormembershipdetailsInterface
     */
    public function setPaymentData($PaymentData)
    {
        return $this->setData(self::PAYMENT_DATA, $PaymentData);
    }
}





