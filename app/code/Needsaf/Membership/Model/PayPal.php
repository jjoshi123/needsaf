<?php

namespace Needsaf\Membership\Model;


use Magento\Paypal\Model\Config as PaypalConfig;
use Magento\Paypal\Model\Config;

class PayPal
{

    /**
     * Config instance
     *
     * @var PaypalConfig
     */
    protected $_config;

    /**
     * API instance
     *
     * @var \Magento\Paypal\Model\Api\Nvp
     */
    protected $_api;

    /**
     * @var \Magento\Paypal\Model\Api\Type\Factory
     */
    protected $_apiTypeFactory;

    /**
     * Api Model Type
     *
     * @var string
     */
    protected $_apiType = 'Magento\Paypal\Model\Api\Nvp';

    /**
     * @var \Magento\Paypal\Model\CartFactory
     */
    protected $_cartFactory;

    /**
     * @var \Magento\Quote\Model\Quote
     */
    protected $_quote;

    /**
     * Paypal constructor.
     *
     * @param \Magento\Paypal\Model\Config $_config
     * @param \Magento\Paypal\Model\Api\Type\Factory $_apiTypeFactory
     */
    public function __construct(
        \Magento\Paypal\Model\Config $_config,
        \Magento\Paypal\Model\Api\Type\Factory $_apiTypeFactory,
        \Magento\Paypal\Model\CartFactory $cartFactory,
        \Magento\Quote\Model\Quote $quote
    )
    {
        $this->_config = $_config;
        $this->_apiTypeFactory = $_apiTypeFactory;
        $this->_config->setMethod(Config::METHOD_EXPRESS);
        $this->_quote = $quote;
        $this->_cartFactory = $cartFactory;
        $this->_getApi();
    }

    /**
     * Get token and return paypal express redirect URL
     *
     * @param array $data
     *
     * @return string
     */
    public function start(array $data)
    {
        $solutionType = $this->_config->getMerchantCountry() == 'DE'
            ? \Magento\Paypal\Model\Config::EC_SOLUTION_TYPE_MARK
            : $this->_config->getValue('solutionType');

        $this->_api->setAmount($data['amount'])
            ->setCurrencyCode('CHF')
            ->setTotal($data['amount'])
            ->setInvNum($data['registration_id'])
            ->setReturnUrl($data['return_url'])
            ->setCancelUrl($data['cancel_url'])
            ->setSolutionType($solutionType)
            ->setPaymentAction($this->_config->getValue('paymentAction'))
            ->setIsLineItemsEnabled(true);

        $this->_quote->setBaseGrandTotal($data['amount']);
        $cart = $this->_cartFactory->create(['salesModel' => $this->_quote]);

        $this->_api->setPaypalCart($cart);

        $this->_api->callSetExpressCheckout();

        $token = $this->_api->getToken();

        return $this->_config->getExpressCheckoutStartUrl($token);
    }


    /**
     * @param array $data
     * @return mixed
     */
    public function placeOrder(array $data)
    {
     $this->_api
            ->setToken($data['token'])
            ->setPayerId($data['payer_id'])
            ->setAmount($data['amount'])
            ->setTotal($data['amount'])
            ->setPaymentAction($this->_config->getValue('paymentAction'))
            ->setNotifyUrl($data['notify_url'])
            ->setInvNum($data['registration_id'])
            ->setCurrencyCode('CHF');


        $this->_api->callDoExpressCheckoutPayment();
        $this->_api->callGetExpressCheckoutDetails();
        return $this->_api->getData();
    }


    /**
     * @return \Magento\Paypal\Model\Api\Nvp
     */
    protected function _getApi()
    {
        if (null === $this->_api) {
            $this->_api = $this->_apiTypeFactory->create($this->_apiType)->setConfigObject($this->_config);
        }
        return $this->_api;
    }

}
