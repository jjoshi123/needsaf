<?php
namespace Needsaf\Membership\Model;
use Needsaf\Membership\Api\Data\VendorInterface;
use Magento\Framework\DataObject\IdentityInterface;


/**
 * Class Member
 * @package Needsaf\Membership\Model
 */
class Vendor  extends \Magento\Framework\Model\AbstractModel implements VendorInterface, IdentityInterface
{


    /**
     * member's Status enabled
     */
    const STATUS_ENABLED = 1;

    /**
     * member's Status disabled
     */
    const STATUS_DISABLED = 0;

    /**
     * CMS page cache tag
     */
    const CACHE_TAG = 'needsaf_membership';

    /**
     * @var string
     */
    protected $_cacheTag = 'needsaf_membership';

    /**
     * Prefix of model events names
     *
     * @var string
     */
    protected $_eventPrefix = 'needsaf_membership';

    /**
     * @var $setup;
     */
    protected $setup;


    /**
     * Member constructor.
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Model\ResourceModel\AbstractResource|null $resource
     * @param \Magento\Framework\Data\Collection\AbstractDb|null $resourceCollection
     * @param array $data
     */
    function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = [])
    {
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);

    }
    protected function _construct()
    {
        $this->_init('Needsaf\Membership\Model\ResourceModel\Vendor');
    }

    /**
     * @param bool $plural
     * @return string
     */
    public function getOwnTitle($plural = false)
    {
        return $plural ? 'Vendor' : 'Vendors';
    }

    /**
     * @return array
     */
    public function getAvailableStatuses()
    {
        return [self::STATUS_ENABLED => __('Enabled'), self::STATUS_DISABLED => __('Disabled')];
    }

    /**
     * Return unique ID(s) for each object in system
     *
     * @return array
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * Get ID
     *
     * @return int|null
     */
    public function getId()
    {
        return $this->getData(self::ID);
    }

    /**
     * Get Member Firstname
     *
     * @return String|null
     */
    public function getFirstname()
    {
        return $this->getData(self::FIRSTNAME);
    }

    /**
     * Get Member Firstname
     *
     * @return String|null
     */
    public function getLastname()
    {
        return $this->getData(self::LASTNAME);
    }

    /**
     * Get Member Email
     *
     * @return String|null
     */
    public function getEmail()
    {
        return $this->getData(self::EMAIL);
    }

    /**
     * Get username
     *
     * @return string|null
     */
    public function getUsername()
    {
        return $this->getData(self::USERNAME);
    }

    /*
     * Get membership id
     *
     * @return int/null
     */
    public function getMembershipId()
    {
        return $this->getData(self::MEMBERSHIP_ID);
    }

    /**
     * Get Order Date
     *
     * @return Int
     */
    public function getOrderDate()
    {
        return $this->getData(self::ORDER_DATE);
    }

    /**
     * Get Transaction Id
     *
     * @return Int
     */
    public function getTransactionId()
    {
        return $this->getData(self::TRANSACTION_ID);
    }

    /**
     * Get Transaction Id
     *
     * @return Int
     */
    public function getVendorStatus()
    {
        return $this->getData(self::VENDOR_STATUS);
    }

    /**
     * Get Store Name
     *
     * @return Int
     */
    public function getStoreView()
    {
        return $this->getData(self::STORE_VIEW);
    }

    /**
     * Get vendor category
     *
     * @return string|null
     */
    public function getVendorCategory()
    {
        return $this->getData(self::VENDOR_CATEGORY);
    }

    /**
     * Get Street
     *
     * @return string|null
     */
    public function getStreet()
    {
        return $this->getData(self::STREET);
    }

    /**
     * Get City
     *
     * @return string|null
     */
    public function getCity()
    {
        return $this->getData(self::CITY);
    }

    /**
     * Get State
     *
     * @return string|null
     */
    public function getState()
    {
        return $this->getData(self::STATE);
    }

    /**
     * Get country
     *
     * @return string|null
     */
    public function getCountry()
    {
        return $this->getData(self::COUNTRY);
    }

    /**
     * Get Zip Code
     *
     * @return string|null
     */
    public function getZipCode()
    {
        return $this->getData(self::ZIP_CODE);
    }

    /**
     * Get creation time
     *
     * @return string|null
     */
    public function getCreationTime()
    {
        return $this->getData(self::CREATION_TIME);
    }

    /**
     * Get update time
     *
     * @return string|null
     */
    public function getUpdateTime()
    {
        return $this->getData(self::UPDATE_TIME);
    }

    /**
     * Is active
     *
     * @return bool|null
     */
    public function isActive()
    {
        return (bool) $this->getData(self::IS_ACTIVE);
    }

    /**
     * Is Eligible for Lead
     * @return bool
     */
    public function isEligible()
    {
        return (bool) $this->getData(self::IS_ELIGIBLE);
    }

    /**
     * Get Lead Count
     * @return int
     */
    public function getLeadCount()
    {
        return $this->getData(self::LEAD_COUNT);
    }

    public function getVendorImage()
    {
        return $this->getData(self::VENDOR_IMAGE);
    }

    public function getVendorDescription()
    {
        return $this->getData(self::VENDOR_DESCRIPTION);
    }

    /**
     * Get  Vendor Rating
     * @return int
     */
    public function getVendorRating()
    {
        return $this->getData(self::VENDOR_RATING);
    }

    /**
     * Set id
     *
     * @param int $id
     * @return \Needsaf\Membership\Api\Data\VendorInterface
     */
    public function setId($id)
    {
        return $this->setData(self::ID, $id);
    }

    /**
     * Set firstname
     *
     * @param string $memberName
     * @return \Needsaf\Membership\Api\Data\VendorInterface
     */
    public function setFirstname($firstname)
    {
        return $this->setData(self::FIRSTNAME, $firstname);
    }

    /**
     * Set lastname
     *
     * @param string $lastname
     * @return \Needsaf\Membership\Api\Data\VendorInterface
     */
    public function setLastname($lastname)
    {
        return $this->setData(self::LASTNAME, $lastname);
    }

    /**
     * Set username
     *
     * @param string $username
     * @return \Needsaf\Membership\Api\Data\VendorInterface
     */

    public function setUsername($username)
    {
        return $this->setData(self::USERNAME, $username);
    }

    /**
     * Set email
     *
     * @param string $email
     * @return \Needsaf\Membership\Api\Data\VendorInterface
     */
    public function setEmail($email)
    {
        return $this->setData(self::EMAIL, $email);
    }

    /**
     * Set membership id
     *
     * @param string $membershipId
     * @return \Needsaf\Membership\Api\Data\VendorInterface
     */
    public function setMembershipId($membershipId)
    {
        return $this->setData(self::MEMBERSHIP_ID, $membershipId);
    }

    /**
     * set Order Date
     *
     * @param string $orderDate
     * @return \Needsaf\Membership\Api\Data\VendorInterface
     */
    public function setOrderDate($orderDate)
    {
        return $this->setData(self::ORDER_DATE, $orderDate);
    }

    /**
     * set Transaction Id
     *
     * @param string $transactionId
     * @return \Needsaf\Membership\Api\Data\VendorInterface
     */
    public function setTransactionId($transactionId)
    {
        return $this->setData(self::TRANSACTION_ID, $transactionId);
    }

    /**
     * Set vendorStatus
     *
     * @param string $vendorStatus
     * @return \Needsaf\Membership\Api\Data\VendorInterface
     */
    public function setVendorStatus($vendorStatus)
    {
        return $this->setData(self::VENDOR_STATUS, $vendorStatus);
    }

    /**
     * Set storeName
     *
     * @param string $storeName
     * @return \Needsaf\Membership\Api\Data\VendorInterface
     */
    public function setStoreView($storeView)
    {
        return $this->setData(self::STORE_VIEW, $storeView);
    }

    /**
     * Set  vendorcategory
     *
     * @param string $website
     * @return \Needsaf\Membership\Api\Data\VendorInterface
     */
    public function setVendorCategory($vendorCategory)
    {
        return $this->setData(self::VENDOR_CATEGORY, $vendorCategory);
    }

    public function setStreet($street)
    {
        return $this->setData(self::STREET, $street);
    }

    /**
     * set City
     *
     * @param string $city
     * @return \Needsaf\Membership\Api\Data\VendorInterface
     */
    public function setCity($city)
    {
        return $this->setData(self::CITY, $city);
    }

    /**
     * set State
     *
     * @param string $state
     * @return \Needsaf\Membership\Api\Data\VendorInterface
     */
    public function setState($state)
    {
        return $this->setData(self::STATE, $state);
    }

    /**
     * set Country
     *
     * @param string $country
     * @return \Needsaf\Membership\Api\Data\VendorInterface
     */
    public function setCountry($country)
    {
        return $this->setData(self::COUNTRY, $country);
    }

    /**
     * set Zip Code
     *
     * @param string $zipCode
     * @return \Needsaf\Membership\Api\Data\VendorInterface
     */
    public function setZipCode($zipCode)
    {
        return $this->setData(self::ZIP_CODE, $zipCode);
    }

    /**
     * Set creation time
     *
     * @param string $update_time
     * @return \Needsaf\Membership\Api\Data\VendorInterface
     */
    public function setCreationTime($creation_time)
    {
        return $this->setData(self::CREATION_TIME, $creation_time);
    }

    /**
     * Set update time
     *
     * @param string $update_time
     * @return \Needsaf\Membership\Api\Data\VendorInterface
     */
    public function setUpdateTime($update_time)
    {
        return $this->setData(self::UPDATE_TIME, $update_time);
    }

    /**
     * @param bool|int $is_active
     * @return $this
     */
    public function setIsActive($is_active)
    {
        return $this->setData(self::IS_ACTIVE, $is_active);
    }


    public function setIsEligible($isEligible)
    {
        return $this->setData(self::IS_ELIGIBLE, $isEligible);

    }

    public function setLeadCount($leadCount)
    {
        return $this->setData(self::LEAD_COUNT,$leadCount);
    }

    public function setVendorImage($vendorImage)
    {
        return $this->setData(self::VENDOR_IMAGE,$vendorImage);
    }

    public function setVendorDescription($vendorDescription)
    {
        return $this->setData(self::VENDOR_DESCRIPTION,$vendorDescription);
    }

    public function setVendorRating($vendorRating)
    {
        return $this->setData(self::VENDOR_RATING,$vendorRating);
    }

    public function getConsultCategory()
    {
        return $this->getData(self::CONSULT_CATEGORY);
    }

    public function setConsultCategory($consultCategory)
    {
        return $this->setData(self::CONSULT_CATEGORY,$consultCategory);
    }
    
    public function getValidationQuestions()
    {
        return $this->getData(self::VALIDATION_QUESTIONS);
    }
    
    public function setValidationQuestions($validationQuestions)
    {
        return $this->getData(self::VALIDATION_QUESTIONS,$validationQuestions);
    }
    
}


