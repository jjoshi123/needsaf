<?php
namespace Needsaf\Membership\Model;
use Needsaf\Membership\Api\Data\MembershipInterface;
use Magento\Framework\DataObject\IdentityInterface;


/**
 * Class Membership
 * @package Needsaf\Membership\Model
 */
class Membership  extends \Magento\Framework\Model\AbstractModel implements MembershipInterface, IdentityInterface
{

    /**
     * membership's Status enabled
     */
    const STATUS_ENABLED = 1;

    /**
     * membership's Status disabled
     */
    const STATUS_DISABLED = 0;

    /**
     * CMS page cache tag
     */
    const CACHE_TAG = 'needsaf_membership';

    /**
     * @var string
     */
    protected $_cacheTag = 'needsaf_membership';

    /**
     * Prefix of model events names
     *
     * @var string
     */
    protected $_eventPrefix = 'needsaf_membership';


    /**
     * Membership constructor.
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Model\ResourceModel\AbstractResource|null $resource
     * @param \Magento\Framework\Data\Collection\AbstractDb|null $resourceCollection
     * @param array $data
     */
    function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = [])
    {
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    protected function _construct()
    {
        $this->_init('Needsaf\Membership\Model\ResourceModel\Membership');
    }

    /**
     * @param bool $plural
     * @return string
     */
    public function getOwnTitle($plural = false)
    {
        return $plural ? 'Membership' : 'Memberships';
    }

    /**
     * @return array
     */
    public function getAvailableStatuses()
    {
        return [self::STATUS_ENABLED => __('Enabled'), self::STATUS_DISABLED => __('Disabled')];
    }
    /**
     * Return unique ID(s) for each object in system
     *
     * @return array
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }
    /**
     * Get ID
     *
     * @return int|null
     */
    public function getId()
    {
        return $this->getData(self::MEMBERSHIP_ID);
    }

    /**
     * @return mixed
     */
    public function getMembershipType()
    {
        return $this->getData(self::MEMBERSHIP_TYPE);
    }

    /**
     * @return mixed
     */
    public function getProductNumbers()
    {
        return $this->getData(self::PRODUCT_NUMBERS);
    }

    /**
     * @return mixed
     */
    public function getTimeFrom()
    {
        return $this->getData(self::TIME_FROM);
    }

    /**
     * @return mixed
     */
    public function getTimeTo()
    {
        return $this->getData(self::TIME_TO);
    }

    /**
     * @return mixed
     */
    public function getUsdPrice()
    {
        return $this->getData(self::USD_PRICE);
    }

    /**
     * @return mixed
     */
    public function getEuroPrice()
    {
        return $this->getData(self::EURO_PRICE);
    }

    /**
     * @return mixed
     */
    public function getSwissPrice()
    {
        return $this->getData(self::SWISS_PRICE);
    }

    /**
     * @return mixed
     */
    public function getMonths()
    {
        return $this->getData(self::MONTHS);
    }

    /**
     * @return mixed
     */
    public function getDays()
    {
        return $this->getData(self::DAYS);
    }

    /**
     * @return mixed
     */
    public function getStoreadminNumbers()
    {
        return $this->getData(self::STOREADMIN_NUMBERS);
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->getData(self::DESCRIPTION);
    }

    /**
     * Get Stores
     *
     * @return string|null
     */
    public function getCustomStores()
    {
        return $this->getData(self::CUSTOM_STORES);
    }

    /**
     * @return mixed
     */
    public function getCreationTime()
    {
        return $this->getData(self::CREATION_TIME);
    }

    /**
     * Get update time
     *
     * @return string|null
     */
    public function getUpdateTime()
    {
        return $this->getData(self::UPDATE_TIME);
    }

    /**
     * Is active
     *
     * @return bool|null
     */
    public function isActive()
    {
        return (bool) $this->getData(self::IS_ACTIVE);
    }

    /**
     * @param mixed $id
     * @return $this
     */
    public function setId($id)
    {
        return $this->setData(self::MEMBERSHIP_ID, $id);
    }

    /**
     * @param $membershipType
     * @return $this
     */
    public function setMembershipType($membershipType)
    {
        return $this->setData(self::MEMBERSHIP_TYPE, $membershipType);
    }

    /**
     * @param $productNumbers
     * @return $this
     */
    public function setProductNumbers($productNumbers)
    {
        return $this->setData(self::PRODUCT_NUMBERS, $productNumbers);
    }

    /**
     * @param $timeFrom
     * @return mixed
     */
    public function setTimeFrom($timeFrom)
    {
        return $this->setData(self::TIME_FROM, $timeFrom);
    }

    /**
     * @param $timeTo
     * @return mixed
     */
    public function setTimeTo($timeTo)
    {
        return $this->setData(self::TIME_TO, $timeTo);
    }

    /**
     * @param $usdPrice
     * @return $this
     */
    public function setUsdPrice($usdPrice)
    {
        return $this->setData(self::USD_PRICE, $usdPrice);
    }

    /**
     * @param $euroPrice
     * @return $this
     */
    public function setEuroPrice($euroPrice)
    {
        return $this->setData(self::EURO_PRICE, $euroPrice);
    }

    /**
     * @param $months
     * @return $this
     */
    public function setMonths($months)
    {
        return $this->setData(self::MONTHS, $months);
    }

    /**
     * @param $months
     * @return $this
     */
    public function setDays($days)
    {
        return $this->setData(self::DAYS, $days);
    }

    /**
     * @param $swissPrice
     * @return $this
     */
    public function setSwissPrice($swissPrice)
    {
        return $this->setData(self::SWISS_PRICE, $swissPrice);
    }

    /**
     * @param $storeadminNumbers
     * @return $this
     */
    public function setStoreadminNumbers($storeadminNumbers)
    {
        return $this->setData(self::STOREADMIN_NUMBERS, $storeadminNumbers);
    }

    /**
     * @param $description
     * @return $this
     */
    public function setDescription($description)
    {
        return $this->setData(self::DESCRIPTION, $description);
    }

    /**
     * @param $description
     * @return mixed
     */
    public function setCustomStores($customStores)
    {
        return $this->setData(self::CUSTOM_STORES, $customStores);
    }

    /**
     * @param $creation_time
     * @return $this
     */
    public function setCreationTime($creation_time)
    {
        return $this->setData(self::CREATION_TIME, $creation_time);
    }

    /**
     * @param $update_time
     * @return $this
     */
    public function setUpdateTime($update_time)
    {
        return $this->setData(self::UPDATE_TIME, $update_time);
    }

    /**
     * @param $is_active
     * @return $this
     */
    public function setIsActive($is_active)
    {
        return $this->setData(self::IS_ACTIVE, $is_active);
    }

}


