<?php

namespace Needsaf\Membership\Observer;

use Magento\Framework\Event\ObserverInterface;

class PaypalCart implements ObserverInterface
{

    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    protected $_request;

    /**
     * PaypalCart constructor.
     *
     * @param \Magento\Framework\App\RequestInterface $request
     */
    public function __construct(
        \Magento\Framework\App\RequestInterface $request
    )
    {
        $this->_request = $request;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        /* @var \Magento\Paypal\Model\Cart $cart */
        $cart = $observer->getData('cart');

        $cart->addCustomItem('Premium Membership', 1, $this->_request->getParam('amount')*12);
    }

}
