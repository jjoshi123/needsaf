<?php
namespace Needsaf\Membership\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Catalog\Model\Product;

class Productcopy implements ObserverInterface
{

    protected $product;
    protected $productCopier;
    protected $authSession;
    protected $_url;
    protected $response;

    public function __construct(
        \Magento\Catalog\Model\Product $product,
        \Magento\Catalog\Model\Product\Copier $productCopier,
        \Magento\Backend\Model\Auth\Session $authSession,
        \Magento\Framework\UrlInterface $url,
        \Magento\Framework\App\Response\Http $response
    )
    {
        $this->product = $product;
        $this->productCopier = $productCopier;
        $this->authSession = $authSession;
        $this->_url = $url;
        $this->response = $response;
    }
    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $user = $this->authSession->getUser();
        $userData = $user->getRole();
        $currentUserId = $user->getId();
        $userRole = $userData->getRoleName();
        $event = $observer->getEvent();
        $product = $event->getProduct();
        $oldId = $product->getEntityId();

        $adminUserId = $product->getAdminUser();
        if($currentUserId!=$adminUserId) {
            if (($userRole != 'Administrators') && ($oldId != NULL)) {
                $oldProduct = $this->product->load($oldId);
                $newProduct = $this->productCopier->copy($oldProduct);
                $newProduct->setStatus(\Magento\Catalog\Model\Product\Attribute\Source\Status::STATUS_ENABLED);
                       $newProduct->setData('duplicatedin', $oldId);
                $newProduct->save();
                $CustomRedirectionUrl = $this->_url->getUrl('catalog/*/edit', ['_current' => true, 'id' => $newProduct->getId()]);
                $this->response->setRedirect($CustomRedirectionUrl);
            }
        }
    }
}