<?php

namespace Needsaf\Membership\Block;

/**
 * Class Member
 * @package Needsaf\Membership\Block
 */
class Vendor extends \Magento\Framework\View\Element\Template implements
    \Magento\Framework\DataObject\IdentityInterface
{

    protected $_categoryRepository;
    /**
     * @var \Magento\Directory\Model\ResourceModel\Region\CollectionFactory
     */
    protected $_regionCollectionFactory;

    /**
     * @var \Magento\Framework\App\Cache\Type\Config
     */


    protected $_configCacheType;
    /**
     * @var \Magento\Directory\Model\ResourceModel\Country\CollectionFactory
     */
    protected $_countryCollectionFactory;
    /**
     * @var \Needsaf\Membership\Model\ResourceModel\Membership\CollectionFactory
     */
    protected $_membershipCollectionFactory;

    /**
     * @var \Magento\Framework\Json\EncoderInterface
     */
    protected $_jsonEncoder;

    /**
     * @var \Magento\Directory\Helper\Data
     */
    protected $directoryHelper;

    /**
     * Member constructor.
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Needsaf\Membership\Model\ResourceModel\Membership\CollectionFactory $membershipCollectionFactory
     * @param array $data
     */
    public function __construct(\Magento\Framework\View\Element\Template\Context $context,
                                \Magento\Directory\Helper\Data $directoryHelper,
                                \Magento\Framework\Json\EncoderInterface $jsonEncoder,
                                \Needsaf\Membership\Model\ResourceModel\Membership\CollectionFactory $membershipCollectionFactory,
                                \Magento\Catalog\Model\CategoryRepository $categoryRepository,
                                \Magento\Framework\App\Cache\Type\Config $configCacheType,
                                \Magento\Directory\Model\ResourceModel\Country\CollectionFactory $countryCollectionFactory,
                                array $data = []
    )
    {

        parent::__construct($context, $data);
        $this->directoryHelper = $directoryHelper;
        $this->_jsonEncoder = $jsonEncoder;
        $this->_categoryRepository = $categoryRepository;
        $this->_configCacheType = $configCacheType;
        $this->_membershipCollectionFactory = $membershipCollectionFactory;
        $this->_countryCollectionFactory = $countryCollectionFactory;
    }

    /**
     * @return $this
     */
    public function getMembershipList()
    {

        $membership = $this->_membershipCollectionFactory
            ->create()
            ->addFilter('is_active', 1);
        return $membership;
    }

    /**
     * Return identifiers for produced content
     *
     * @return array
     */
    public function getIdentities()
    {
        return [\Needsaf\Membership\Model\Membership::CACHE_TAG . '_' . 'list'];
    }

    public function getRootVendorCategory()
    {
        return $this->_scopeConfig->getValue('needsaf_category/vendorcategory/vendor_category', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    public function getSubCategory($parent_category_id)
    {
        $parentCategoryObject = $this->_categoryRepository->get($parent_category_id);
        return $parentCategoryObject->getChildrenCategories();
    }

    public function getConsultCategories()
    {
        $consultCategoryId = $this->_scopeConfig->getValue('needsaf_category/needsafConsult/consult_id', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $parentCategoryObject = $this->_categoryRepository->get($consultCategoryId);
        return $parentCategoryObject->getChildrenCategories();
    }

    public function getLoadrRegionUrl()
    {
        return $this->getUrl('directory/json/childRegion');
    }

    public function getCountryCollection()
    {
        $collection = $this->getData('country_collection');
        if ($collection === null) {
            $collection = $this->_countryCollectionFactory->create()->loadByStore();
            $this->setData('country_collection', $collection);
        }

        return $collection;
    }

    protected function getTopDestinations()
    {
        $destinations = (string)$this->_scopeConfig->getValue(
            'general/country/destinations',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
        return !empty($destinations) ? explode(',', $destinations) : [];
    }

    public function getCountryHtmlSelect($defValue = null, $name = 'country_id', $id = 'country', $title = 'Country')
    {
        \Magento\Framework\Profiler::start('TEST: ' . __METHOD__, ['group' => 'TEST', 'method' => __METHOD__]);
        if ($defValue === null) {
            $defValue = $this->getCountryId();
        }
        $cacheKey = 'DIRECTORY_COUNTRY_SELECT_STORE_' . $this->_storeManager->getStore()->getCode();
        $cache = $this->_configCacheType->load($cacheKey);
        if ($cache) {
            $options = unserialize($cache);
        } else {
            $options = $this->getCountryCollection()
                ->setForegroundCountries($this->getTopDestinations())
                ->toOptionArray();
            $this->_configCacheType->save(serialize($options), $cacheKey);
        }
        $html = $this->getLayout()->createBlock(
            'Magento\Framework\View\Element\Html\Select'
        )->setName(
            $name
        )->setId(
            $id
        )->setTitle(
            __($title)
        )->setValue(
            $defValue
        )->setOptions(
            $options
        )->setExtraParams(
            'data-validate="{\'validate-select\':true}"'
        )->getHtml();

        \Magento\Framework\Profiler::stop('TEST: ' . __METHOD__);
        return $html;
    }

    public function getRegionCollection()
    {
        $collection = $this->getData('region_collection');
        if ($collection === null) {
            $collection = $this->_regionCollectionFactory->create()->addCountryFilter($this->getCountryId())->load();

            $this->setData('region_collection', $collection);
        }
        return $collection;
    }

    /**
     * @return string
     */
    public function getRegionHtmlSelect()
    {
        \Magento\Framework\Profiler::start('TEST: ' . __METHOD__, ['group' => 'TEST', 'method' => __METHOD__]);
        $cacheKey = 'DIRECTORY_REGION_SELECT_STORE' . $this->_storeManager->getStore()->getId();
        $cache = $this->_configCacheType->load($cacheKey);
        if ($cache) {
            $options = unserialize($cache);
        } else {
            $options = $this->getRegionCollection()->toOptionArray();
            $this->_configCacheType->save(serialize($options), $cacheKey);
        }
        $html = $this->getLayout()->createBlock(
            'Magento\Framework\View\Element\Html\Select'
        )->setName(
            'region'
        )->setTitle(
            __('State/Province')
        )->setId(
            'state'
        )->setClass(
            'required-entry validate-state'
        )->setValue(
            intval($this->getRegionId())
        )->setOptions(
            $options
        )->getHtml();
        \Magento\Framework\Profiler::start('TEST: ' . __METHOD__, ['group' => 'TEST', 'method' => __METHOD__]);
        return $html;
    }

    /**
     * @return string
     */
    public function getCountryId()
    {
        $countryId = $this->getData('country_id');
        if ($countryId === null) {
            $countryId = $this->directoryHelper->getDefaultCountry();
        }
        return $countryId;
    }

    /**
     * @return string
     */
    public function getRegionsJs()
    {
        \Magento\Framework\Profiler::start('TEST: ' . __METHOD__, ['group' => 'TEST', 'method' => __METHOD__]);
        $regionsJs = $this->getData('regions_js');
        if (!$regionsJs) {
            $countryIds = [];
            foreach ($this->getCountryCollection() as $country) {
                $countryIds[] = $country->getCountryId();
            }
            $collection = $this->_regionCollectionFactory->create()->addCountryFilter($countryIds)->load();
            $regions = [];
            foreach ($collection as $region) {
                if (!$region->getRegionId()) {
                    continue;
                }
                $regions[$region->getCountryId()][$region->getRegionId()] = [
                    'code' => $region->getCode(),
                    'name' => $region->getName(),
                ];
            }
            $regionsJs = $this->_jsonEncoder->encode($regions);
        }
        \Magento\Framework\Profiler::stop('TEST: ' . __METHOD__);
        return $regionsJs;
    }


}