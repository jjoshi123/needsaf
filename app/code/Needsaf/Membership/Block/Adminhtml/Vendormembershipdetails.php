<?php

namespace  Needsaf\Membership\Block\Adminhtml;


/**
 * Class Membership
 * @package Needsaf\Membership\Block\Adminhtml
 */
class Vendormembershipdetails extends \Magento\Backend\Block\Widget\Grid\Container
{
    /**
     * Constructor
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_blockGroup = 'Needsaf_Membership';
        $this->_controller = 'adminhtml';
        $this->_headerText = __('Membership');
        parent::_construct();
        $this->removeButton('add');
    }
}
