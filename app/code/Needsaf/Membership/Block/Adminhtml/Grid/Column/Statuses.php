<?php

namespace Needsaf\Membership\Block\Adminhtml\Grid\Column;


/**
 * Class Statuses
 * @package Needsaf\Membership\Block\Adminhtml\Grid\Column
 */
class Statuses extends \Magento\Backend\Block\Widget\Grid\Column
{


    /**
     * @return array
     */
    public function getFrameCallback()
    {
        return [$this, 'decorateStatus'];
    }

    /**
     * @param $value
     * @param $row
     * @param $column
     * @param $isExport
     * @return string
     */
    public function decorateStatus($value, $row, $column, $isExport)
    {
        if ($row->getIsActive() || $row->getStatus()) {
            $cell = '<span class="grid-severity-notice"><span>' . $value . '</span></span>';
        } else {
            $cell = '<span class="grid-severity-critical"><span>' . $value . '</span></span>';
        }
        return $cell;
    }
}
