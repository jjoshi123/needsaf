<?php

namespace Needsaf\Membership\Block\Adminhtml;


/**
 * Class Member
 * @package Needsaf\Membership\Block\Adminhtml
 */
class Vendor extends \Magento\Backend\Block\Widget\Grid\Container
{
    protected function _construct()
    {
        parent::_construct();
        $this->removeButton('add');

    }
}
