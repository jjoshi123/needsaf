<?php

namespace Needsaf\Membership\Block\Adminhtml\Vendormembershipdetails\Edit\Tab;

/**
 * Class Main
 * @package Needsaf\Membership\Block\Adminhtml\Member\Edit\Tab
 */
class Main extends \Magento\Backend\Block\Widget\Form\Generic implements \Magento\Backend\Block\Widget\Tab\TabInterface
{


    /**
     * @var $_memberCollection
     */
    protected $_vendormembershipCollection;
    /**
     * @var \Magento\Framework\Message\ManagerInterface
     */
    protected $_messageManager;
    /**
     * @var \Magento\Framework\App\Response\RedirectInterface
     */
    protected $redirect;
    /**
     * @var \Magento\Framework\App\Response\Http
     */
    protected $response;

    /**
     * Main constructor.
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Data\FormFactory $formFactory
     * @param \Needsaf\Membership\Model\ResourceModel\Vendormembershipdetails\Collection $membershipCollection
     * @param \Magento\Framework\Message\ManagerInterface $messageManager
     * @param \Magento\Framework\App\Response\Http $response
     * @param \Magento\Framework\App\Response\RedirectInterface $redirect
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Needsaf\Membership\Model\ResourceModel\Vendormembershipdetails\Collection $vendormembershipCollection,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Magento\Framework\App\Response\Http $response,
        \Magento\Framework\App\Response\RedirectInterface $redirect,
        array $data = []
    )
    {

        $this->_vendormembershipCollection = $vendormembershipCollection;
        $this->_messageManager = $messageManager;
        $this->redirect = $redirect;
        $this->response = $response;
        parent::__construct($context, $registry, $formFactory, $data);

    }

    /* Prepare form
    *
    * @return $this
    * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
    */
    /**
     * @return $this
     */
    public function getMembershipCollection()
    {

        $collection = $this->_vendormembershipCollection->load();

        return $collection;
    }


    /**
     * Generate spaces
     * @param  int $n
     * @return string
     */
    protected function _getSpaces($n)
    {
        $s = '';
        for ($i = 0; $i < $n; $i++) {
            $s .= '--- ';
        }

        return $s;
    }

    /**
     * Prepare label for tab
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabLabel()
    {
        return __('Membership and Payment Information');
    }

    /**
     * Prepare title for tab
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabTitle()
    {
        return __('Membership and Payment Information');
    }

    /**
     * Returns status flag about this tab can be shown or not
     *
     * @return bool
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * Returns status flag about this tab hidden or not
     *
     * @return bool
     */
    public function isHidden()
    {
        return false;
    }

    /**
     * Check permission for passed action
     *
     * @param string $resourceId
     * @return bool
     */
    protected function _isAllowedAction($resourceId)
    {
        return $this->_authorization->isAllowed($resourceId);
    }



}