<?php

namespace Needsaf\Membership\Block\Adminhtml\Vendormembershipdetails\Edit;

/**
 * Class Tabs
 * @package Needsaf\Membership\Block\Adminhtml\Member\Edit
 */
class Tabs extends \Magento\Backend\Block\Widget\Tabs
{

    protected function _construct()
    {
        parent::_construct();
        $this->setId('vendormembershipdetails_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(__('Vendor Membership And Payment  Information'));
    }

    /**
     * @return $this
     */
    protected function _beforeToHtml()
    {
        return parent::_beforeToHtml();
    }
}