<?php

namespace  Needsaf\Membership\Block\Adminhtml;


/**
 * Class Membership
 * @package Needsaf\Membership\Block\Adminhtml
 */
class Membership extends \Magento\Backend\Block\Widget\Grid\Container
{
    /**
     * Constructor
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_blockGroup = 'Needsaf_Membership';
        $this->_controller = 'adminhtml';
        $this->_headerText = __('Membership');
        $this->_addButtonLabel = __('Add New Membership');
        parent::_construct();
    }
}
