<?php

namespace Needsaf\Membership\Block\Adminhtml\Membership\Edit\Tab;
/**
 * Class Main
 * @package Needsaf\Membership\Block\Adminhtml\Time\Edit\Tab
 */
class Main extends \Magento\Backend\Block\Widget\Form\Generic implements \Magento\Backend\Block\Widget\Tab\TabInterface
{

    /**
     * @var \Magento\Cms\Model\Wysiwyg\Config
     */
    protected $_wysiwygConfig;
    protected $_storeCollectionFactory;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Data\FormFactory $formFactory
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Magento\Store\Model\ResourceModel\Store\CollectionFactory $storeCollectionFactory,
        \Magento\Cms\Model\Wysiwyg\Config $wysiwygConfig,
        array $data = []
    )
    {
        $this->_wysiwygConfig = $wysiwygConfig;
        $this->_storeCollectionFactory = $storeCollectionFactory;
        parent::__construct($context, $registry, $formFactory, $data);
    }


    /**
     * Prepare form
     *
     * @return $this
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    protected function _prepareForm()
    {

        $model = $this->_coreRegistry->registry('current_model');
        $isElementDisabled = !$this->_isAllowedAction('Needsaf_Membership::membership');
        $form = $this->_formFactory->create();
        $fieldset = $form->addFieldset('base_fieldset', ['legend' => __('Membership Information')]);
        $wysiwygConfig = $this->_wysiwygConfig->getConfig(['tab_id' => $this->getTabId()]);

        if ($model->getId()) {
            $fieldset->addField('membership_id', 'hidden', ['name' => 'id']);
        }

        $days = array(
            array(
                'label' => 'Sunday',
                'value' => '7',
            ),
            array(
                'label' => 'Monday',
                'value' => '1',
            ),
            array(
                'label' => 'Tuesday',
                'value' => '2',
            ),
            array(
                'label' => 'Wednesday',
                'value' => '3',
            ),
            array(
                'label' => 'Thursday',
                'value' => '4',
            ),
            array(
                'label' => 'Friday',
                'value' => '5',
            ),
            array(
                'label' => 'Saturday' ,
                'value' => '6',
            )
        );

        $fieldset->addField(
            'membership_type',
            'text',
            [
                'name' => 'post[membership_type]',
                'label' => __('Type'),
                'title' => __('Type'),
                'required' => true,
                'disabled' => $isElementDisabled
            ]
        );
        $fieldset->addField(
            'product_numbers',
            'text',
            [
                'name' => 'post[product_numbers]',
                'label' => __('Number of Products'),
                'title' => __('Number of Products'),
                'required' => true,
                'class' => 'integer',
                'disabled' => $isElementDisabled
            ]
        );
        $fieldset->addField(
            'usd_price',
            'text',
            [
                'name' => 'post[usd_price]',
                'label' => __('Price in USD'),
                'title' => __('Price in USD'),
                'required' => true,
                'disabled' => $isElementDisabled
            ]
        );
        $fieldset->addField(
            'euro_price',
            'text',
            [
                'name' => 'post[euro_price]',
                'label' => __('Price in EUR'),
                'title' => __('Price in EUR'),
                'required' => true,
                'disabled' => $isElementDisabled
            ]
        );
        $fieldset->addField(
            'swiss_price',
            'text',
            [
                'name' => 'post[swiss_price]',
                'label' => __('Price in CHF'),
                'title' => __('Price in CHF'),
                'required' => true,
                'disabled' => $isElementDisabled
            ]
        );
        $fieldset->addField(
            'months',
            'text',
            [
                'name' => 'post[months]',
                'label' => __('Period in Months'),
                'title' => __('Number of Products'),
                'required' => true,
                'class' => 'integer',
                'disabled' => $isElementDisabled
            ]
        );
        $fieldset->addField(
            'days',
            'multiselect',
            [
                'name' => 'post[days]',
                'label' => __('Number of Days'),
                'title' => __('Number of Days'),
                'values' => $days,
                'disabled' => $isElementDisabled,
                'style' => 'width:100%',
                'required' => true
            ]
        );
        $fieldset->addField(
            'time_from',
            'time',
            [
                'name' => 'post[time_from]',
                'label' => __('Time From'),
                'title' => __('Time From'),
                'required' => true,
                'time_format' => 'hh:mm:ss',
                'disabled' => $isElementDisabled
            ]
        );
        $fieldset->addField(
            'time_to',
            'time',
            [
                'name' => 'post[time_to]',
                'label' => __('Time To'),
                'title' => __('Time To'),
                'required' => true,
                'time_format' => 'hh:mm:ss',
                'disabled' => $isElementDisabled
            ]
        );
        $fieldset->addField(
            'storeadmin_numbers',
            'text',
            [
                'name' => 'post[storeadmin_numbers]',
                'label' => __('Number of Sub Admin'),
                'title' => __('Number of Sub Admin'),
                'class' => 'integer',
                'required' => true,
                'disabled' => $isElementDisabled
            ]
        );
        $contentField = $fieldset->addField(
            'description',
            'editor',
            [
                'name' => 'post[description]',
                'label' => __('Description'),
                'title' => __('Description'),
                'config' => $wysiwygConfig,
                'disabled' => $isElementDisabled
            ]
        );
        $fieldset->addField(
            'is_active',
            'select',
            [
                'label' => __('Status'),
                'title' => __('timeslot Status'),
                'name' => 'post[is_active]',
                'required' => true,
                'options' => $model->getAvailableStatuses(),
                'disabled' => $isElementDisabled
            ]
        );

        if (!$model->getId()) {
            $model->setData('is_active', $isElementDisabled ? '0' : '1');
        }
        // Setting custom renderer for content field to remove label column
        $renderer = $this->getLayout()->createBlock(
            'Magento\Backend\Block\Widget\Form\Renderer\Fieldset\Element'
        );
        $contentField->setRenderer($renderer);
        $this->_eventManager->dispatch('needsaf_membership_post_edit_tab_main_prepare_form', ['form' => $form]);
        $form->setValues($model->getData());
        $this->setForm($form);
        return parent::_prepareForm();

    }

    /**
     * Generate spaces
     * @param  int $n
     * @return string
     */
    protected function _getSpaces($n)
    {
        $s = '';
        for ($i = 0; $i < $n; $i++) {
            $s .= '--- ';
        }

        return $s;
    }

    /**
     * Prepare label for tab
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabLabel()
    {
        return __('Membership  Information');
    }

    /**
     * Prepare title for tab
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabTitle()
    {
        return __('Membership Information');
    }

    /**
     * Returns status flag about this tab can be shown or not
     *
     * @return bool
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * Returns status flag about this tab hidden or not
     *
     * @return bool
     */
    public function isHidden()
    {
        return false;
    }

    /**
     * Check permission for passed action
     *
     * @param string $resourceId
     * @return bool
     */
    protected function _isAllowedAction($resourceId)
    {
        return $this->_authorization->isAllowed($resourceId);
    }
}


