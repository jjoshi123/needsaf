<?php

namespace Needsaf\Membership\Block\Adminhtml\Membership\Edit;

/**
 * Class Tabs
 * @package Needsaf\Membership\Block\Adminhtml\Membership\Edit
 */
class Tabs extends \Magento\Backend\Block\Widget\Tabs
{
    /**
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('Membership_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(__('Membership Information'));
    }

    /**
     * @return $this
     */
    protected function _beforeToHtml()
    {
        return parent::_beforeToHtml();
    }
}