<?php

namespace Needsaf\Membership\Block\Adminhtml\Vendor\Edit\Tab;
use Magento\Store\Model\ScopeInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;

/**
 * Class Main
 * @package Needsaf\Membership\Block\Adminhtml\Member\Edit\Tab
 */
class Main extends \Magento\Backend\Block\Widget\Form\Generic implements \Magento\Backend\Block\Widget\Tab\TabInterface
{


    /**
     * @var $_memberCollection
     */
    protected $_membershipCollection;
    /**
     * @var \Magento\Framework\Message\ManagerInterface
     */
    protected $_messageManager;
    /**
     * @var \Magento\Framework\App\Response\RedirectInterface
     */
    protected $redirect;
    /**
     * @var \Magento\Framework\App\Response\Http
     */
    protected $response;

    protected $_scopeConfig;

    protected $_categoryFactory;

    /**
     * Main constructor.
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Data\FormFactory $formFactory
     * @param \Needsaf\Membership\Model\ResourceModel\Membership\Collection $membershipCollection
     * @param \Magento\Framework\Message\ManagerInterface $messageManager
     * @param \Magento\Framework\App\Response\Http $response
     * @param \Magento\Framework\App\Response\RedirectInterface $redirect
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Needsaf\Membership\Model\ResourceModel\Membership\Collection $membershipCollection,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Magento\Framework\App\Response\Http $response,
        \Magento\Framework\App\Response\RedirectInterface $redirect,
        \Magento\Catalog\Model\CategoryRepository $categoryRepository,
        \Magento\Catalog\Model\CategoryFactory $categoryFactory,
        array $data = []
    )
    {

        $this->_membershipCollection = $membershipCollection;
        $this->_messageManager = $messageManager;
        $this->redirect = $redirect;
        $this->response = $response;
        $this->_categoryRepository = $categoryRepository;
        $this->_categoryFactory = $categoryFactory;
        parent::__construct($context, $registry, $formFactory, $data);

    }

    /* Prepare form
    *
    * @return $this
    * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
    */
    /**
     * @return $this
     */
    protected function _prepareForm()
    {

        $model = $this->_coreRegistry->registry('current_model');
        $data = $model->getData();
        $storeviews= $data['store_view'];
        $storeview= explode(',',$storeviews);
        foreach ($storeview as $store){
            $storeNames[] = array('label'=>$store,'value'=>$store);
        }
        $vendorCategory = $data['vendor_category'];
        $vendorCategories= explode(',',$vendorCategory);
        foreach ($vendorCategories as $vendors)
        {
            $categorynames[] = array('label'=>$vendors,'value'=>$vendors);
        }

        $isElementDisabled = !$this->_isAllowedAction('Needsaf_Membership::vendor');
        $form = $this->_formFactory->create();
        $fieldset = $form->addFieldset('base_fieldset', ['legend' => __('Vendor Information')]);
        if ($model->getId()) {
            $fieldset->addField('id', 'hidden', ['name' => 'id']);
        }
        $collection = $this->_membershipCollection;

        foreach ($collection as $member) {
            if ($member->getIsActive()) {
                $member_id[] = array(
                    'label' => $this->_getSpaces($member->getLevel()) . ' ' . $member->getMembershipType(),
                    'value' => $member->getMembershipId(),
                );
            }

        }
        $currentMemberStatus =  $data['vendor_status'];
        $membershipFlag = false;
        if($currentMemberStatus == 'approved' || $currentMemberStatus == 'disapproved')
        {
            $membershipFlag = true;
        }
        $member_status = array(
            array(
                'label' => 'Pending',
                'value' => 'pending',
            ),
            array(
                'label' => 'Processing',
                'value' => 'processing',
            ),
            array(
                'label' => 'Approved',
                'value' => 'approved',
            ),
            array(
                'label' => 'Disapproved',
                'value' => 'disapproved',
            )
        );

        $website_name = array(
            array(
                'label' => 'Needsaf De Ch',
                'value' => 'needsaf_dech',
            ),
            array(
                'label' => 'Needsaf Romandy',
                'value' => 'needsaf_romandy',
            ),
            array(
                'label' => 'Needsaf Ticino',
                'value' => 'needsaf_ticino',
            )
        );

        $validationquestion = array(
            array(
                'label' => 'YES',
                'value' => '1',
            ),

        );

        try {
            if (isset($member_id)) {
                $fieldset->addField(
                    'firstname',
                    'text',
                    [
                        'name' => 'post[firstname]',
                        'label' => __('FirstName'),
                        'title' => __('FirstName'),
                        'required' => true,
                        'class' => 'validate-alphanum-with-spaces',
                        'disabled' => $isElementDisabled
                    ]
                );
                $fieldset->addField(
                    'lastname',
                    'text',
                    [
                        'name' => 'post[lastname]',
                        'label' => __('LastName'),
                        'title' => __('LastName'),
                        'required' => true,
                        'class' => 'validate-alphanum-with-spaces',
                        'disabled' => $isElementDisabled
                    ]
                );
                $fieldset->addField(
                    'username',
                    'text',
                    [
                        'name' => 'post[username]',
                        'label' => __('Username'),
                        'title' => __('Username'),
                        'required' => true,
                        'disabled' => $isElementDisabled
                    ]
                );
                $fieldset->addField(
                    'email',
                    'text',
                    [
                        'name' => 'post[email]',
                        'label' => __('Email'),
                        'title' => __('Email'),
                        'required' => true,
                        'class' => 'validate-email',
                        'disabled' => $isElementDisabled
                    ]
                );
                $fieldset->addField(
                    'membership_id',
                    'select',
                    [
                        'name' => 'post[membership_id]',
                        'label' => __('Membership Type'),
                        'title' => __('Membership Type'),
                        'values' => $member_id,
                        'disabled' => $isElementDisabled,
                        'style' => 'width:100%',
                        'required' => true
                    ]
                );
                $fieldset->addField(
                    'vendor_status',
                    'select',
                    [
                        'name' => 'post[vendor_status]',
                        'label' => __('Vendor Status'),
                        'title' => __('Vendor Status'),
                        'values' => $member_status,
                        'disabled' => $membershipFlag,
                        'style' => 'width:100%',
                        'required' => true,
                        'read-only' => $membershipFlag
                    ]
                );
                $fieldset->addField(
                    'vendor_category',
                    'multiselect',
                    [
                        'name' => 'post[vendor_category]',
                        'label' => __('Vendor Category'),
                        'title' => __('Vendor Category'),
                        'values' => $categorynames,
                        'disabled' => true,
                        'required' => true
                    ]
                );
                $fieldset->addField(
                    'store_view',
                    'multiselect',
                    [
                        'name' => 'post[store_view]',
                        'label' => __('Regions'),
                        'title' => __('Regions  '),
                        'values' => $storeNames,
                        'disabled' => true
                    ]
                );
                $fieldset->addField(
                    'validation_questions',
                    'select',
                    [
                        'name' => 'post[validation_questions]',
                        'label' => __('Validation_Question'),
                        'title' => __('Validation_Question'),
                        'values' => $validationquestion,
                        'style' => 'width:100%',
                        'disabled' => $isElementDisabled,
                        'required' => true
                    ]
                );
                $fieldset->addField(
                    'street',
                    'text',
                    [
                        'name' => 'post[street]',
                        'label' => __('Street'),
                        'title' => __('Street'),
                        'required' => true,
                        'disabled' => $isElementDisabled
                    ]
                );
                $fieldset->addField(
                    'city',
                    'text',
                    [
                        'name' => 'post[city]',
                        'label' => __('City'),
                        'title' => __('City'),
                        'required' => true,
                        'disabled' => $isElementDisabled
                    ]
                );
                $fieldset->addField(
                    'state',
                    'text',
                    [
                        'name' => 'post[state]',
                        'label' => __('State'),
                        'title' => __('State'),
                        'required' => true,
                        'disabled' => $isElementDisabled
                    ]
                );
                $fieldset->addField(
                    'country',
                    'text',
                    [
                        'name' => 'post[country]',
                        'label' => __('Country'),
                        'title' => __('Country'),
                        'required' => true,
                        'disabled' => $isElementDisabled
                    ]
                );
                $fieldset->addField(
                    'zip_code',
                    'text',
                    [
                        'name' => 'post[zip_code]',
                        'label' => __('Zip Code'),
                        'title' => __('Zip Code'),
                        'required' => true,
                        'class' => 'validate-zip-international number',
                        'disabled' => $isElementDisabled
                    ]
                );
                $fieldset->addField(
                    'is_active',
                    'select',
                    [
                        'label' => __('Status'),
                        'title' => __('Post Status'),
                        'name' => 'post[is_active]',
                        'required' => true,
                        'options' => $model->getAvailableStatuses(),
                        'disabled' => $isElementDisabled
                    ]
                );
                if (!$model->getMemberId()) {
                    $model->setData('is_active', $isElementDisabled ? '0' : '1');
                }

                $this->_eventManager->dispatch('needsaf_membership_edit_tab_main_prepare_form', ['form' => $form]);
                $form->setValues($model->getData());
                $this->setForm($form);

                return parent::_prepareForm();
            } else {
                throw new \Exception();
            }
        } catch (\Exception $e) {
            $this->_messageManager->addExceptionMessage($e, __('Something went wrong while Adding Member , Please Add Membership First.'));
            $url = $this->redirect->getRefererUrl();
            $this->redirect->redirect($this->response, $url);

        }
    }


    /**
     * Generate spaces
     * @param  int $n
     * @return string
     */
    protected function _getSpaces($n)
    {
        $s = '';
        for ($i = 0; $i < $n; $i++) {
            $s .= '--- ';
        }

        return $s;
    }

    /**
     * Prepare label for tab
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabLabel()
    {
        return __('Member Information');
    }

    /**
     * Prepare title for tab
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabTitle()
    {
        return __('Member Information');
    }

    /**
     * Returns status flag about this tab can be shown or not
     *
     * @return bool
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * Returns status flag about this tab hidden or not
     *
     * @return bool
     */
    public function isHidden()
    {
        return false;
    }

    /**
     * Check permission for passed action
     *
     * @param string $resourceId
     * @return bool
     */
    protected function _isAllowedAction($resourceId)
    {
        return $this->_authorization->isAllowed($resourceId);
    }

}