<?php
/**
 * Select attributes suitable for product variations generation
 *
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Needsaf\Membership\Block\Product\Configurable;

use Needsaf\Membership\Model\VendorFactory;
use Needsaf\Membership\Model\MembershipFactory;

/**
 * @SuppressWarnings(PHPMD.LongVariable)
 */
class AttributeSelector extends \Magento\ConfigurableProduct\Block\Product\Configurable\AttributeSelector
{
    protected $_userFactory;
    protected $_vendorFactory;
    protected $_membershipFactory;
    protected $eavAttributeRepository;
    protected $_registry;
    protected $_productRepository;
    protected $connection;
    protected $resource;

    /**
     * Attribute set creation action URL
     *
     * @return string
     */
    protected $authSession;

    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Model\Auth\Session $authSession,
        \Magento\User\Model\UserFactory $userFactory,
        \Magento\Eav\Api\AttributeRepositoryInterface $eavAttributeRepository,
        \Magento\Catalog\Model\ProductRepository $productRepository,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\App\ResourceConnection $resource,

        VendorFactory $vendorFactory,
        MembershipFactory $membershipFactory,
        array $data = []
    )
    {
        parent::__construct($context, $data);
        $this->authSession = $authSession;
        $this->_vendorFactory = $vendorFactory;
        $this->_userFactory = $userFactory;
        $this->_membershipFactory = $membershipFactory;
        $this->eavAttributeRepository = $eavAttributeRepository;
        $this->_productRepository = $productRepository;
        $this->resource = $resource;
        $this->_registry = $registry;


    }

    public function getCurrentUser()
    {
        $userInfo = $this->authSession->getUser();
        $roleData = $this->authSession->getUser()->getRole()->getData();
        return $roleData['role_name'];
    }

    public function getmembershipType()
    {
        $vendorModel = $this->getVendorDetails();
        $membershipId = $vendorModel->getMembershipId();
        $membershipModel = $this->_membershipFactory->create();
        $membershipModel->load($membershipId);
        return $membershipModel->getMembershipType();
    }

    public function getVendorDetails()
    {
        $roleData = $this->authSession->getUser()->getData();
        $admin_id = $roleData['user_id'];
        $userModel = $this->_userFactory->create();
        $userModel->load($admin_id);
        $vendorId = $userModel->getVendorId();
        $vendorModel = $this->_vendorFactory->create();
        $vendorModel->load($vendorId);
        return $vendorModel;
    }

    public function getUserRole()
    {
        $roleName = $this->authSession->getUser()->getRole()->getRoleName();

        return $roleName;
    }


    public function getCurrentProduct()
    {
        return $this->_registry->registry('current_product');
    }


    public function getProductBySku($sku)
    {
        return $this->_productRepository->get($sku);
    }

    public function getProductById($id)
    {
        return $this->_productRepository->getById($id);
    }


    public function getConnection()
    {
        if (!$this->connection) {
            $this->connection = $this->resource->getConnection();
        }
        return $this->connection;
    }

    public function getAttributeData($value)
    {
        $this->getConnection();
        $criteriaSql = "select * from eav_attribute_option_value where value_id=$value";
        $criteriaResult = $this->connection->fetchAll($criteriaSql);
        foreach ($criteriaResult as $value1) {
            $attributevalue = $value1['value'];
        }
        return $attributevalue;
    }


    public function getAttributeCodes()
    {
        $this->getConnection();
        $criteriaSql = "select attribute_id from eav_attribute where attribute_code IN('price','panel','entry_sensor','motion_sensor','motion_sensor_video','keychain_remote','keypad','auxiliary_siren','glassbreak_sensor','smoke_alarm','panic_button')";
        $criteriaResult = $this->connection->fetchAll($criteriaSql);
        return $criteriaResult;
    }

    public function getParentId($productid)
    {
        if ($this->getUserRole() != 'Administrators') {
            $productData = $this->_productRepository->getById($productid)->getCustomAttribute('duplicatedin');
            $parentId = '';
            if ($productData) {
                $parentId = $productData->getValue();
            }

            return $parentId;
        }


    }

}
