<?php

namespace Needsaf\Membership\Controller\Index;

use \Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Authorization\Model\Acl\Role\Group as RoleGroup;
use Magento\Authorization\Model\UserContextInterface;
/*use Needsaf\Membership\Model\Payment\Paypal;*/
use Needsaf\Membership\Model\PayPal;
use Needsaf\Membership\Model\MembershipFactory;
use Magento\Store\Model\ScopeInterface;
use Needsaf\Membership\Model\Vendormembershipdetails;
use Needsaf\Membership\Model\VendormembershipdetailsFactory;

/**
 * Class Post
 * @package Needsaf\Membership\Controller\Index
 */
class Post extends Action
{
    const XML_PATH_EMAIL_RECIPIENT = 'trans_email/ident_general/email';
    
    const VENDOR_FREE_MEMBERSHIP = 'Basic';
    
    const DEFAULT_STORE_VIEW_CODE = 'default';
    
    const VENDOR_PREMIUM_MEMBERSHIP_CODE = 'Premium';
    /**
     * @var PageFactory
     */
    protected $resultPageFactory;
    /**
     * @var \Needsaf\Membership\Model\VendorFactory
     */
    protected $_vendorFactory;
    /**
     * @var \Magento\User\Model\UserFactory
     */
    protected $_userFactory;
    /**
     * @var \Magento\Framework\Stdlib\DateTime\TimezoneInterface
     */
    protected $timezone;
    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    protected $datetime;
    protected $roleFactory;
    protected $rulesFactory;
    protected $paypalPayment;
    protected $_membershipFactory;
    protected $_mediaDirectory;
    protected $_fileUploaderFactory;
    protected $_transportBuilder;
    protected $scopeConfig;
    protected $inlineTranslation;
    protected $HelperBackend;
    protected $_customerSession;
    protected $_vendormembershipFactory;
    protected $_storeFactory;

    /**
     * Post constructor.
     * @param Context $context
     * @param PageFactory $resultPageFactory
     * @param \Needsaf\Membership\Model\VendorFactory $vendorFactory
     * @param \Magento\User\Model\UserFactory $userFactory
     * @param \Magento\Framework\Stdlib\DateTime\DateTime $datetime
     * @param \Magento\Framework\Stdlib\DateTime\TimezoneInterface $timezone
     * @param \Magento\Authorization\Model\RoleFactory $roleFactory
     * @param PayPal $paypalPayment
     * @param MembershipFactory $membershipFactory
     * @param \Magento\Authorization\Model\RulesFactory $rulesFactory
     * @param \Magento\Framework\Filesystem $filesystem
     * @param \Magento\MediaStorage\Model\File\UploaderFactory $fileUploaderFactory
     * @param \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder
     * @param \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Backend\Helper\Data $HelperBackend
     */
    public function __construct(Context $context,
                                PageFactory $resultPageFactory,
                                \Needsaf\Membership\Model\VendorFactory $vendorFactory,
                                \Magento\User\Model\UserFactory $userFactory,
                                \Magento\Framework\Stdlib\DateTime\DateTime $datetime,
                                \Magento\Framework\Stdlib\DateTime\TimezoneInterface $timezone,
                                \Magento\Authorization\Model\RoleFactory $roleFactory,
                                PayPal $paypalPayment,
                                MembershipFactory $membershipFactory,
                                \Magento\Authorization\Model\RulesFactory $rulesFactory,
                                \Magento\Framework\Filesystem $filesystem,
                                \Magento\MediaStorage\Model\File\UploaderFactory $fileUploaderFactory,
                                \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder,
                                \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation,
                                \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
                                \Magento\Backend\Helper\Data $HelperBackend,
                                \Magento\Customer\Model\Session $customerSession,
                                VendormembershipdetailsFactory $vendormembershipdetailsFactory,
                                \Magento\Store\Model\StoreFactory $storeFactory
    )

    {
        $this->resultPageFactory = $resultPageFactory;
        $this->_vendorFactory = $vendorFactory;
        $this->_membershipFactory = $membershipFactory;
        $this->_userFactory = $userFactory;
        $this->timezone = $timezone;
        $this->datetime = $datetime;
        $this->paypalPayment = $paypalPayment;
        $this->roleFactory = $roleFactory;
        $this->rulesFactory = $rulesFactory;
        $this->_mediaDirectory = $filesystem->getDirectoryWrite(\Magento\Framework\App\Filesystem\DirectoryList::MEDIA);
        $this->_fileUploaderFactory = $fileUploaderFactory;
        $this->_transportBuilder = $transportBuilder;
        $this->inlineTranslation = $inlineTranslation;
        $this->scopeConfig = $scopeConfig;
        $this->HelperBackend = $HelperBackend;
        $this->_customerSession = $customerSession;
        $this->_vendormembershipFactory = $vendormembershipdetailsFactory;
        $this->_storeFactory = $storeFactory;
        parent::__construct($context);
    }

    /**
     * Execute when request is received
     */
    public function execute()

    {
        $username = $this->getRequest()->getParam('username');
        $userData= $this->_userFactory->create()->loadByUsername($username)->getData();
        if(!empty($userData)){
            $this->messageManager->addErrorMessage(__('Vendor with same username already exist. Please register with another username.'));
            return $this->_redirect('vendorregistration/');
        }
        
        $resource = $this->_objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        $postObject = new \Magento\Framework\DataObject();
        $post = $this->getRequest()->getParams();
        $email = $post['email'];
        $vendorModel = $this->_vendorFactory->create();
        $vendorData = $vendorModel->getCollection()->addFieldToFilter('vendor_status',array('in'=>array('pending','processing','approved')))->addFieldToFilter('email',$email);
        if ($vendorData->getData()) {
            $this->messageManager->addErrorMessage('You have already registered with Needsaf');
            return $this->_redirect('vendorregistration/');
        }
        /*Consult Category*/
        $vendorCategory = $post['vendor_category'];
        $vendorCategory = implode(",",$vendorCategory);
        $post['vendor_category'] = $vendorCategory;
        if ($vendorCategory == 'Services') {
            $consultData = '';
        } else {
            $consultCategory = $post['consult_category'];
            $consultData = implode(",", $consultCategory);
        }
        $post['consult_category'] = $consultData;
        /*Image Name*/
        $imageName = $_FILES['image']['name'];
        if ($imageName) {
            $this->uploadAction();
        }
        $post['imagename'] = $imageName;
        /*Role data*/
        
		$currentDate = $this->datetime->gmtDate();
        try {
            $membershipId = $post['membership_id'];
            $memberModel = $this->_membershipFactory->create();
            $membership = $memberModel->load($membershipId);
            $membershipType = $membership->getMembershipType();
            $role = $this->roleFactory->create();
            $zipcodeStoreName = self::VENDOR_PREMIUM_MEMBERSHIP_CODE;
            $role->load($zipcodeStoreName,'role_name');
            $storeId =$role->getGwsStoreGroups();
            $storeId = explode(',',$storeId);
            $storeModel = $this->_storeFactory->create();
            foreach ($storeId as $storeIds)
            {
                $storeModel->load($storeIds);
                $storeName[] =$storeModel->getCode();
            }
            $storeView = implode(',',$storeName);
            $post['store_view'] = $storeView;
            if($membershipType == self::VENDOR_FREE_MEMBERSHIP) {
                $tableName = $resource->getTableName('zipcode_store_mapping');
                $getMemberStoreViewSql = "Select * FROM " . $tableName." Where `zipcode`='".$post['zip_code']."'";
                $getMemberStoreViewResult = $connection->fetchAll($getMemberStoreViewSql);
                if(!empty($getMemberStoreViewResult)){
                    $storeView = $getMemberStoreViewResult[0]['store_view_id'];
                    $zipcodeStoreId = $getMemberStoreViewResult[0]['store_id'];
                    $storeModel->load($zipcodeStoreId);
                    $zipcodeStoreName = $storeModel->getCode();
                    $role = $this->roleFactory->create();
                    $role->load($zipcodeStoreName,'role_name');
                    $storeId =$role->getGwsStoreGroups();
                    $storeId = explode(',',$storeId);
                    $storeModel = $this->_storeFactory->create();
                    foreach ($storeId as $storeIds)
                    {
                        $storeModel->load($storeIds);
                        $basicStoreName[] =$storeModel->getCode();
                    }
                    $storeView = implode(',',$basicStoreName);
                }
                else
                {
                    $storeView = $post['store_view'];
                    $zipcodeStoreName = self::DEFAULT_STORE_VIEW_CODE;
                }    
            }
            
            $role->load($zipcodeStoreName,'role_name');
            $roleId = $role->getRoleId();
        
            $amount = $membership->getSwissPrice();
            if($amount > 0)
            {
                /*Send to Payment Controller*/
                $this->_customerSession->setVendorData($post);
                return $this->_redirect('vendorregistration/payment/index',['amount'=>$amount]);
            }else{
                $orderInfo = [
                    'validation_questions' => $post['validationquestions'],
                    'firstname' => $post['firstname'],
                    'lastname' => $post['lastname'],
                    'username' => $post['username'],
                    'email' => $post['email'],
                    'membership_id' => $post['membership_id'],
                    'order_date' => $currentDate,
                    'transaction_id' => '3',
                    'vendor_status' => $post['vendor_status'],
                    'street' => $post['street'],
                    'city' => $post['city'],
                    'state' => $post['state'],
                    'country' => $post['country_id'],
                    'zip_code' => $post['zip_code'],
                    'is_active' => 1,
                    'store_view' => $storeView,
                    'vendor_category' => $post['vendor_category'],
                    'consult_category' => $post['consult_category'],
                    'vendor_description' => $post['vendor_description'],
                    'vendor_image' => $post['imagename']
                ];

                $vendorModel->setData($orderInfo);
                $vendorModel->save();
            }


            /*email code vendor*/
            $email = $post['email'];
            $adminUrl = $this->HelperBackend->getHomePageUrl();
            $dataVendor = [
                'membership_type' => $membershipType,
                'email' => $email,
                'admin_url' => $adminUrl
            ];
            $this->sendVendorEmail($postObject,$dataVendor,$email);
            /*email code vendor end*/

            /*email code admin*/
            $admin = $this->scopeConfig->getValue(
                'trans_email/ident_general/email',
                ScopeInterface::SCOPE_STORE
            );
            $vendorName = $post['firstname'] . " " . $post['lastname'];
            $dataAdmin = [
                'vendor_name' => $vendorName,
                'admin_url' => $adminUrl
            ];
            $this->sendAdminEmail($postObject,$dataAdmin,$admin);
            /*email code for admin end*/

            $adminData = array(
                'username' => $post['username'],
                'firstname' => $post['firstname'],
                'lastname' => $post['lastname'],
                'email' => $post['email'],
                'password' => $post['password'],
                'interface_locale' => 'en_US',
                'is_active' => 0,
                'is_vendor' => 1,
                'vendor_id' => $vendorModel->getId()
            );
            $this->createAdmin($adminData,$roleId);

            $vendorMembershipDetailsData =array(
                'vendor_id' => $vendorModel->getId(),
                'membership_id' => $vendorModel->getMembershipId(),
                'start_date' => $vendorModel->getCreationTime(),
                'end_date' => date('Y-m-d',strtotime('+1 year')),
                'membership_data' => serialize($memberModel->getData()),
                'paypal_data' => ''
            );
            $vendorMembershipDetailsModel = $this->_vendormembershipFactory->create();
            $vendorMembershipDetailsModel->setData($vendorMembershipDetailsData);
            $vendorMembershipDetailsModel->save();

            $this->messageManager->addSuccessMessage(
                __('Thanks for submitting Membership Request.')
            );
            return $this->_redirect('vendorregistration/');
        } catch (\Exception $e) {
            $e->getMessage();
            $this->messageManager->addExceptionMessage($e,
                __('There is an issue submiting request please contact admin.')
            );
            return $this->_redirect('vendorregistration/');
        }

    }

    /**
     *
     * upload image
     * @return string|null
     */
    public function uploadAction()
    {

        try {
            $target = $this->_mediaDirectory->getAbsolutePath('vendorImage/');
            $uploader = $this->_fileUploaderFactory->create(['fileId' => 'image']);
            $uploader->setAllowedExtensions(['jpg', 'jpeg', 'gif', 'png', 'bmp']);
            $uploader->setAllowRenameFiles(true);
            $result = $uploader->save($target);
            return $result;
        } catch (\Exception $e) {
            if ($e->getCode() != \Magento\Framework\File\Uploader::TMP_NAME_EMPTY) {
                $e->getMessage();
                $this->messageManager->addExceptionMessage($e,
                    __('You have uploaded incorrect image data in form. Please refill the form')
                );
                return $this->_redirect('vendorregistration/');
            }
        }

    }

    protected function sendAdminEmail($postObject,$dataAdmin,$admin)
    {
        $postObject->setData($dataAdmin);
        $transportAdmin = $this->_transportBuilder
            ->setTemplateVars(['data' => $postObject])
            ->setTemplateIdentifier('admin_register_status')
            ->setTemplateOptions(
                [
                    'area' => \Magento\Framework\App\Area::AREA_FRONTEND,
                    'store' => \Magento\Store\Model\Store::DEFAULT_STORE_ID
                ]
            )
            ->setFrom(['email' => $this->scopeConfig->getValue(self::XML_PATH_EMAIL_RECIPIENT, ScopeInterface::SCOPE_STORE),
                'name' => $this->scopeConfig->getValue('trans_email/ident_general/email', ScopeInterface::SCOPE_STORE)])
            ->addTo($admin)
            ->getTransport();
        $transportAdmin->sendMessage();
        /*email code admin*/

        $this->inlineTranslation->resume();

    }

    protected function sendVendorEmail($postObject,$dataVendor,$email)
    {

        $postObject->setData($dataVendor);
        $transport = $this->_transportBuilder
            ->setTemplateVars(['data' => $postObject])
            ->setTemplateIdentifier('vendor_register')
            ->setTemplateOptions(
                [
                    'area' => \Magento\Framework\App\Area::AREA_FRONTEND,
                    'store' => \Magento\Store\Model\Store::DEFAULT_STORE_ID
                ]
            )
            ->setFrom(['email' => $this->scopeConfig->getValue(self::XML_PATH_EMAIL_RECIPIENT, ScopeInterface::SCOPE_STORE),
                'name' => $this->scopeConfig->getValue('trans_email/ident_general/email', ScopeInterface::SCOPE_STORE)])
            ->addTo($email)
            ->getTransport();
        $transport->sendMessage();
        $this->inlineTranslation->resume();
    }

    public function createAdmin($data,$roleId)
    {
        /*Static role added of germany*/
        $adminInfo = [
            'username' => $data['username'],
            'firstname' => $data['firstname'],
            'lastname' => $data['lastname'],
            'email' => $data['email'],
            'password' => $data['password'],
            'interface_locale' => 'en_US',
            'is_active' => 0,
            'is_vendor' => 1,
            'vendor_id' => $data['vendor_id']

        ];
        $userModel = $this->_userFactory->create();
        $userModel->setData($adminInfo);
        $userModel->setRoleId($roleId);
        $userModel->save();
    }


}
