<?php

namespace Needsaf\Membership\Controller\Index;

use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\View\Result\PageFactory;

class AnswerNo extends \Magento\Framework\App\Action\Action
{
    protected $resultPageFactory;

    /**
     * AnswerNo constructor.
     * @param Context $context
     * @param PageFactory $resultPageFactory
     */
    public function __construct(Context $context,
                                PageFactory $resultPageFactory)
    {
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }

    /**
     * Dispatch request
     *
     * @return \Magento\Framework\Controller\ResultInterface|ResponseInterface
     * @throws \Magento\Framework\Exception\NotFoundException
     */
    public function execute()
    {

        $this->messageManager->addErrorMessage('Please contact Needsaf Admin');
        $this->_redirect('contact/');
    }
}