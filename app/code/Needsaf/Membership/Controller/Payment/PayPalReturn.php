<?php

namespace Needsaf\Membership\Controller\Payment;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\View\Result\PageFactory;
use Needsaf\Membership\Model\PayPal;
use Needsaf\Membership\Model\VendorFactory;
use Needsaf\Membership\Model\VendormembershipdetailsFactory;
use Needsaf\Membership\Controller\Index\Post;
use Needsaf\Membership\Model\MembershipFactory;
use Magento\Store\Model\ScopeInterface;



class PayPalReturn extends Post
{
    protected $paypal;
    /**
     * @var \Magento\Framework\Json\Helper\Data
     */
    protected $jsonHelper;

    protected $resultJsonFactory;
    protected $_storeFactory;
    public function __construct(Context $context,
                                PageFactory $resultPageFactory,
                                \Needsaf\Membership\Model\VendorFactory $vendorFactory,
                                \Magento\User\Model\UserFactory $userFactory,
                                \Magento\Framework\Stdlib\DateTime\DateTime $datetime,
                                \Magento\Framework\Stdlib\DateTime\TimezoneInterface $timezone,
                                \Magento\Authorization\Model\RoleFactory $roleFactory,
                                PayPal $paypalPayment,
                                MembershipFactory $membershipFactory,
                                \Magento\Authorization\Model\RulesFactory $rulesFactory,
                                \Magento\Framework\Filesystem $filesystem,
                                \Magento\MediaStorage\Model\File\UploaderFactory $fileUploaderFactory,
                                \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder,
                                \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation,
                                \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
                                \Magento\Backend\Helper\Data $HelperBackend,
                                \Magento\Customer\Model\Session $customerSession,
                                VendormembershipdetailsFactory $vendormembershipdetailsFactory,
                                \Magento\Framework\Json\Helper\Data $jsonHelper,
                                \Magento\Store\Model\StoreFactory $storeFactory
)
    {
        parent::__construct($context, $resultPageFactory, $vendorFactory, $userFactory, $datetime, $timezone, $roleFactory, $paypalPayment, $membershipFactory, $rulesFactory, $filesystem, $fileUploaderFactory, $transportBuilder, $inlineTranslation, $scopeConfig, $HelperBackend, $customerSession,$vendormembershipdetailsFactory,$storeFactory);
        $this->paypal = $paypalPayment;
        $this->jsonHelper = $jsonHelper;
    }

    public function execute()

    {
        $postObject = new \Magento\Framework\DataObject();
        $vendorModel = $this->_vendorFactory->create();
        $vendorMembershipModel = $this->_vendormembershipFactory->create();
        if ($this->getRequest()->getParam('token') !== null &&
            $this->getRequest()->getParam('PayerID') !== null) {
            $data = [
                'token' => $this->getRequest()->getParam('token'),
                'payer_id' => $this->getRequest()->getParam('PayerID'),
                'amount' => $this->getRequest()->getParam('amount'),
                'registration_id' => $this->getRequest()->getParam('registration_id'),
                'notify_url' => $this->_url->getUrl('vendorregistration/payment/notify')
            ];
            $paypalData = $this->paypal->placeOrder($data);
            $vendorData = $this->_customerSession->getVendorData();
            if($paypalData['payment_status'] == 'failed'){
                 $admin = $this->scopeConfig->getValue(
                'trans_email/ident_general/email',
                ScopeInterface::SCOPE_STORE
            );
                 $postObject->setData($vendorData);
                 $transport = $this->_transportBuilder
               ->setTemplateVars(['data' => $postObject])
               ->setTemplateIdentifier('admin_paypal_failed')
               ->setTemplateOptions(
                [
                    'area' => \Magento\Framework\App\Area::AREA_FRONTEND,
                    'store' => \Magento\Store\Model\Store::DEFAULT_STORE_ID
                ]
            )
            ->setFrom(['email' => $this->scopeConfig->getValue(self::XML_PATH_EMAIL_RECIPIENT, ScopeInterface::SCOPE_STORE),
                'name' => $this->scopeConfig->getValue('trans_email/ident_general/email', ScopeInterface::SCOPE_STORE)])
            ->addTo($admin)
            ->getTransport();
            $transport->sendMessage();
            $this->inlineTranslation->resume();
            $this->messageManager->addErrorMessage(__('Sorry your payment failed please try again later.'));
            return $this->_redirect('vendorregistration/');
            }
            $vendorData = $this->_customerSession->getVendorData();
            $currentDate = $this->datetime->date('Y-m-d');
            $memberModel = $this->_membershipFactory->create();
            $membershipId = $vendorData['membership_id'];
            $membership = $memberModel->load($membershipId);
            $membershipType = $membership->getMembershipType();
            /*Role data*/
            $role = $this->roleFactory->create();
            $role->load($membershipType,'role_name');
            $roleId = $role->getRoleId();
            /*Static role added of germany, You can change change role to french,italian by changing name in load()*/
            $storeId =$role->getGwsStoreGroups();
            $storeId = explode(',',$storeId);
            $storeModel = $this->_storeFactory->create();
            foreach ($storeId as $storeIds)
            {
                $storeModel->load($storeIds);
                $storeName[] =$storeModel->getCode();
            }
            $storeView = implode(',',$storeName);
            $orderInfo = [
                'validation_questions' => $vendorData['validationquestions'],
                'firstname' => $vendorData['firstname'],
                'lastname' => $vendorData['lastname'],
                'username' => $vendorData['username'],
                'email' => $vendorData['email'],
                'membership_id' => $vendorData['membership_id'],
                'order_date' => $currentDate,
                'transaction_id' => '3',
                'vendor_status' => 'processing',
                'street' => $vendorData['street'],
                'city' => $vendorData['city'],
                'state' => $vendorData['state'],
                'country' => $vendorData['country_id'],
                'zip_code' => $vendorData['zip_code'],
                'is_active' => 1,
                'store_view' => $storeView,
                'vendor_category' => $vendorData['vendor_category'],
                'consult_category' => $vendorData['consult_category'],
                'vendor_description' => $vendorData['vendor_description'],
                'vendor_image' => $vendorData['imagename']
            ];
            $vendorModel->setData($orderInfo);
            $vendorModel->save();
            $adminData = array(
                'username' => $vendorData['username'],
                'firstname' => $vendorData['firstname'],
                'lastname' => $vendorData['lastname'],
                'email' => $vendorData['email'],
                'password' => $vendorData['password'],
                'interface_locale' => 'en_US',
                'is_active' => 0,
                'is_vendor' => 1,
                'vendor_id' => $vendorModel->getId()
            );

            $this->createAdmin($adminData,$roleId);
            $email = $vendorData['email'];
            $adminUrl = $this->HelperBackend->getHomePageUrl();
            $dataVendor = [
                'membership_type' => $membershipType,
                'email' => $email,
                'admin_url' => $adminUrl
            ];
            $this->sendVendorEmail($postObject,$dataVendor,$email);

            $vendorName = $vendorData['firstname'] . " " . $vendorData['lastname'];
            $admin = $this->scopeConfig->getValue(
                'trans_email/ident_general/email',
                ScopeInterface::SCOPE_STORE
            );
            $dataAdmin = [
                'vendor_name' => $vendorName,
                'admin_url' => $adminUrl
            ];

            $this->sendAdminEmail($postObject,$dataAdmin,$admin);
            $vendorMembershipDetailsData =array(
                'vendor_id' => $vendorModel->getId(),
                'membership_id' => $vendorModel->getMembershipId(),
                'start_date' => $vendorModel->getCreationTime(),
                'end_date' => date('Y-m-d',strtotime('+1 year')),
                'membership_data' => serialize($memberModel->getData()),
                'paypal_data' => serialize($paypalData)
            );

            $vendorMembershipModel->setData($vendorMembershipDetailsData);
            $vendorMembershipModel->save();


            $this->messageManager->addSuccessMessage(
                __('Thanks for submitting Membership Request. Your payment has been done.')
            );
            return $this->_redirect('vendorregistration/');

        }
        else{
            $this->messageManager->addErrorMessage(__('Sorry your payment failed please try again later.'));
            return $this->_redirect('vendorregistration/');
        }
    }
}