<?php


namespace Needsaf\Membership\Controller\Payment;


use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ResponseInterface;
use Needsaf\Membership\Model\PayPal;

class Index extends Action
{

    protected $paypal;
    protected $_customerSession;

    public function __construct(Context $context,PayPal $payPal,\Magento\Customer\Model\Session $customerSession)
    {
        parent::__construct($context);
        $this->paypal = $payPal;
        $this->_customerSession = $customerSession;
    }

    /**
     * Dispatch request
     *
     * @return \Magento\Framework\Controller\ResultInterface|ResponseInterface
     * @throws \Magento\Framework\Exception\NotFoundException
     */
    public function execute()
    {

        $amount = $this->getRequest()->getParam('amount')*12;
        $data = [
            'amount' => $amount,
            'registration_id' => rand(0,100000)
        ];
        $data['return_url'] = $this->_url->getUrl('vendorregistration/payment/paypalreturn', $data);
        $data['cancel_url'] = $this->_url->getUrl('vendorregistration/index/');
        return $this->_redirect($this->paypal->start($data));
    }
}