<?php

namespace Needsaf\Membership\Controller\Adminhtml\Membership;
use Magento\Authorization\Model\Acl\Role\Group as RoleGroup;
use Magento\Authorization\Model\UserContextInterface;


/**
 * Class Save
 * @package Needsaf\Membership\Controller\Adminhtml\Membership
 */
class Save extends \Needsaf\Membership\Controller\Adminhtml\Membership
{

    public function _beforeSave($model,$request)
    {
        $data = $model->getData();
        $timeFrom = implode(',',$data['time_from']);
        $timeTo = implode(',',$data['time_to']);
        $days = implode(',',$data['days']);
        $model->setDays($days);
        $model->setTimeFrom($timeFrom);
        $model->setTimeTo($timeTo);
    }
}