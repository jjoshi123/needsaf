<?php

namespace Needsaf\Membership\Controller\Adminhtml\Vendor;
use Magento\Store\Model\ScopeInterface;

/**
 * Class Save
 * @package Needsaf\Membership\Controller\Adminhtml\Member
 */
class Save extends \Needsaf\Membership\Controller\Adminhtml\Vendor
{

    const XML_PATH_EMAIL_RECIPIENT = 'trans_email/ident_general/email';

    protected $inlineTranslation;
    protected $HelperBackend;
    protected $_transportBuilder;
    protected $scopeConfig;
    protected $_userFactory;

    public function __construct(\Magento\Backend\App\Action\Context $context,
                                \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder,
                                \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation,
                                \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
                                \Magento\User\Model\UserFactory $userFactory
    )

    {
        $this->_transportBuilder = $transportBuilder;
        $this->inlineTranslation = $inlineTranslation;
        $this->scopeConfig = $scopeConfig;
        $this->_userFactory = $userFactory;
        parent::__construct($context);
    }
    /**
     * @param $model
     * @param $request
     */
    public function _beforeSave($model, $request)
    {
        $postObject = new \Magento\Framework\DataObject();
        $data = $model->getData();
        $vendorModel = $this->_objectManager->create('Needsaf\Membership\Model\Vendor');
        $vendorModel->load($data['id']);
        $vendorData = $vendorModel->getData();
        $status = $data['vendor_status'];
        $email = $data['email'];

        $user = $this->_userFactory->create();
        $userModel = $user->loadByUsername($data['username']);
        if($status=='approved' && $status != $vendorData['vendor_status']) {
            $adminUrl = $this->_helper->getHomePageUrl();
            $dataVendor = [
                'email' => $email,
                'admin_url' => $adminUrl
            ];
            $this->sendVendorEmail($postObject,$dataVendor,$email);
            $userModel->setIsActive(1);
            $userModel->save();
            $admin = $this->scopeConfig->getValue(
                'trans_email/ident_general/email',
                ScopeInterface::SCOPE_STORE
            );
            $dataApproved=[
                'email' => $email
            ];
            $this->sendAdminApprovedMail($postObject,$dataApproved,$admin);
        }
        if($status=='disapproved' && $status != $vendorData['vendor_status'])
        {
            $admin = $this->scopeConfig->getValue(
                'trans_email/ident_general/email',
                ScopeInterface::SCOPE_STORE
            );
            $dataDisapproved=[
                'email' => $email
            ];
            $this->sendVendorDisapproveEmail($postObject,$dataDisapproved,$email);
            $this->sendAdminDisapproveEmail($postObject,$dataDisapproved,$admin);
            $user->loadByUsername($data['username'])->delete();
        }
    }
    protected function sendVendorEmail($postObject,$dataVendor,$email)
    {
        $postObject->setData($dataVendor);
        $transportAdmin = $this->_transportBuilder
            ->setTemplateVars(['data' => $postObject])
            ->setTemplateIdentifier('vendor_approval_active')
            ->setTemplateOptions(
                [
                    'area' => \Magento\Framework\App\Area::AREA_FRONTEND,
                    'store' => \Magento\Store\Model\Store::DEFAULT_STORE_ID
                ]
            )
            ->setFrom(['email' => $this->scopeConfig->getValue(self::XML_PATH_EMAIL_RECIPIENT, ScopeInterface::SCOPE_STORE),
                'name' => $this->scopeConfig->getValue('trans_email/ident_general/email', ScopeInterface::SCOPE_STORE)])
            ->addTo($email)
            ->getTransport();
        $transportAdmin->sendMessage();
        /*email code admin*/

        $this->inlineTranslation->resume();

    }

    protected function sendVendorDisapproveEmail($postObject,$dataDisapproved,$email)
    {
        $postObject->setData($dataDisapproved);
        $transportAdmin = $this->_transportBuilder
            ->setTemplateVars(['data' => $postObject])
            ->setTemplateIdentifier('vendor_disapproved_status')
            ->setTemplateOptions(
                [
                    'area' => \Magento\Framework\App\Area::AREA_FRONTEND,
                    'store' => \Magento\Store\Model\Store::DEFAULT_STORE_ID
                ]
            )
            ->setFrom(['email' => $this->scopeConfig->getValue(self::XML_PATH_EMAIL_RECIPIENT, ScopeInterface::SCOPE_STORE),
                'name' => $this->scopeConfig->getValue('trans_email/ident_general/email', ScopeInterface::SCOPE_STORE)])
            ->addTo($email)
            ->getTransport();
        $transportAdmin->sendMessage();
        /*email code admin*/

        $this->inlineTranslation->resume();

    }

    protected function sendAdminDisapproveEmail($postObject,$dataDisapproved,$admin)
    {
        $postObject->setData($dataDisapproved);
        $transportAdmin = $this->_transportBuilder
            ->setTemplateVars(['data' => $postObject])
            ->setTemplateIdentifier('admin_disapproved_status')
            ->setTemplateOptions(
                [
                    'area' => \Magento\Framework\App\Area::AREA_FRONTEND,
                    'store' => \Magento\Store\Model\Store::DEFAULT_STORE_ID
                ]
            )
            ->setFrom(['email' => $this->scopeConfig->getValue(self::XML_PATH_EMAIL_RECIPIENT, ScopeInterface::SCOPE_STORE),
                'name' => $this->scopeConfig->getValue('trans_email/ident_general/email', ScopeInterface::SCOPE_STORE)])
            ->addTo($admin)
            ->getTransport();
        $transportAdmin->sendMessage();
        /*email code admin*/

        $this->inlineTranslation->resume();

    }

    protected function sendAdminApprovedMail($postObject,$dataApproved,$admin)
    {
        $postObject->setData($dataApproved);
        $transportAdmin = $this->_transportBuilder
            ->setTemplateVars(['data' => $postObject])
            ->setTemplateIdentifier('admin_approved_status')
            ->setTemplateOptions(
                [
                    'area' => \Magento\Framework\App\Area::AREA_FRONTEND,
                    'store' => \Magento\Store\Model\Store::DEFAULT_STORE_ID
                ]
            )
            ->setFrom(['email' => $this->scopeConfig->getValue(self::XML_PATH_EMAIL_RECIPIENT, ScopeInterface::SCOPE_STORE),
                'name' => $this->scopeConfig->getValue('trans_email/ident_general/email', ScopeInterface::SCOPE_STORE)])
            ->addTo($admin)
            ->getTransport();
        $transportAdmin->sendMessage();
        /*email code admin*/

        $this->inlineTranslation->resume();

    }

}