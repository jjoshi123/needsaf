<?php

namespace Needsaf\Membership\Controller\Adminhtml;


/**
 * Class Membership
 * @package Needsaf\Membership\Controller\Adminhtml
 */
class Membership extends Actions
{
    /**
     * Form session key
     * @var string
     */
    protected $_formSessionKey = 'needsaf_membership_form_data';

    /**
     * Allowed Key
     * @var string
     */
    protected $_allowedKey = 'Needsaf_Membership::membership';

    /**
     * Model class name
     * @var string
     */
    protected $_modelClass = 'Needsaf\Membership\Model\Membership';

    /**
     * Active menu key
     * @var string
     */
    protected $_activeMenu = 'Needsaf_Membership::membership';

    /**
     * Status field name
     * @var string
     */
    protected $_statusField = 'is_active';

    /**
     * Save request params key
     * @var string
     */
    protected $_paramsHolder = 'post';
}
