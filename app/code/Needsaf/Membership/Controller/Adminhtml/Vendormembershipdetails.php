<?php
namespace Needsaf\Membership\Controller\Adminhtml;


/**
 * Class Member
 * @package Needsaf\Membership\Controller\Adminhtml
 */
class Vendormembershipdetails extends Actions
{
    /**
     * Form session key
     * @var string
     */
    protected $_formSessionKey  = 'needsaf_membership_form_data';

    /**
     * Allowed Key
     * @var string
     */
    protected $_allowedKey      = 'Needsaf_Membership::vendormembershipdetails';

    /**
     * Model class name
     * @var string
     */
    protected $_modelClass      = 'Needsaf\Membership\Model\Vendormembershipdetails';

    /**
     * Active menu key
     * @var string
     */
    protected $_activeMenu      = 'Needsaf_Membership::vendormembershipdetails';

    /**
     * Status field name
     * @var string
     */
    protected $_statusField     = 'is_active';

    /**
     * Save request params key
     * @var string
     */
    protected $_paramsHolder    = 'post';

    /**
     * Index action
     * @return void
     */
    protected function _indexAction()
    {
        if($this->getRequest()->getParam('ajax')) {
            $this->_forward('grid');
            return;
        }

        $this->_view->loadLayout();
        $this->_setActiveMenu($this->_activeMenu);

        $title = __('Membership And Payment Details');
        $this->_view->getPage()->getConfig()->getTitle()->prepend($title);
        $this->_addBreadcrumb($title, $title);
        $this->_view->renderLayout();
    }

}