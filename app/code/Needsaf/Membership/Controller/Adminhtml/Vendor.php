<?php
namespace Needsaf\Membership\Controller\Adminhtml;


/**
 * Class Member
 * @package Needsaf\Membership\Controller\Adminhtml
 */
class Vendor extends Actions
{
    /**
     * Form session key
     * @var string
     */
    protected $_formSessionKey  = 'needsaf_membership_form_data';

    /**
     * Allowed Key
     * @var string
     */
    protected $_allowedKey      = 'Needsaf_Membership::vendor';

    /**
     * Model class name
     * @var string
     */
    protected $_modelClass      = 'Needsaf\Membership\Model\Vendor';

    /**
     * Active menu key
     * @var string
     */
    protected $_activeMenu      = 'Needsaf_Membership::vendor';

    /**
     * Status field name
     * @var string
     */
    protected $_statusField     = 'is_active';

    /**
     * Save request params key
     * @var string
     */
    protected $_paramsHolder    = 'post';

}