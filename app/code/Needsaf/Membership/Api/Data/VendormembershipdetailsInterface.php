<?php



namespace Needsaf\Membership\Api\Data;

/**
 * Interface MemberInterface
 * @package Needsaf\Membership\Api\Data
 */
interface VendormembershipdetailsInterface
{
    const PAYMENT_ID = 'payment_id';
    const VENDOR_ID = 'vendor_id';
    const MEMBERSHIP_ID = 'membership_id';
    const START_DATE = 'start_date';
    const END_DATE = 'end_Date';
    const CREATED_DATE = 'created_date';
    const MEMBERSHIP_DATA = 'membership_data';
    const PAYMENT_DATA = 'paypal_data';


    /**
     * Get payment Id
     *
     * @return Int
     */
    public function getPaymentId();

    /**
     * Get vendor ID
     *
     * @return Int
     */
    public function getVendorId();

    /**
     * Get Membership Id
     *
     * @return Int
     */
    public function getMembershipId();

    /**
     * Get Start Date
     *
     * @return datetime
     */
    public function getStartDate();

    /**
     * Get End Date
     *
     * @return datetime
     */
    public function getEndDate();

    /**
     * Get Created Date
     *
     * @return datetime
     */
    public function getCreatedDate();

    /**
     * Get Membership data
     *
     * @return text
     */
    public function getMembershipData();

    /**
     * Get Payment Data
     *
     * @return text
     */
    public function getPaymentData();

    /**
     * set Payment Id
     *
     * @param string $id
     * @return \Needsaf\Membership\Api\Data\VendormembershipdetailsInterface
     */
    public function setPaymentId($id);

    /**
     * set Vendor Id
     *
     * @param string $VendorId
     * @return \Needsaf\Membership\Api\Data\VendormembershipdetailsInterface
     */
    public function setVendorId($VendorId);

    /**
     * set Membership Id
     *
     * @param string $membershipId
     * @return \Needsaf\Membership\Api\Data\VendormembershipdetailsInterface
     */
    public function setMembershipId($membershipId);

    /**
     * set Start Date
     *
     * @param string $StartDate
     * @return \Needsaf\Membership\Api\Data\VendormembershipdetailsInterface
     */
    public function setStartDate($StartDate);

    /**
     * set End Date
     *
     * @param string $EndDate
     * @return \Needsaf\Membership\Api\Data\VendormembershipdetailsInterface
     */
    public function setEndDate($EndDate);

    /**
     * set Created Date
     *
     * @param string $CreatedDate
     * @return \Needsaf\Membership\Api\Data\VendormembershipdetailsInterface
     */
    public function setCreatedDate($CreatedDate);


    /**
     * set Membership Data
     *
     * @param string $MembershipData
     * @return \Needsaf\Membership\Api\Data\VendormembershipdetailsInterface
     */
    public function setMembershipData($MembershipData);

    /**
     * set Payment Data
     *
     * @param string $PaymentData
     * @return \Needsaf\Membership\Api\Data\VendormembershipdetailsInterface
     */
    public function setPaymentData($PaymentData);


}