<?php
namespace Needsaf\Membership\Api\Data;


/**
 * Interface MembershipInterface
 * @package Needsaf\Membership\Api\Data
 */
interface MembershipInterface
{
    const MEMBERSHIP_ID           = 'membership_id';
    const MEMBERSHIP_TYPE         = 'membership_type';
    const PRODUCT_NUMBERS         = 'product_numbers';
    const TIME_FROM               = 'time_from';
    const TIME_TO                 = 'time_to';
    const USD_PRICE               = 'usd_price';
    const EURO_PRICE              = 'euro_price';
    const SWISS_PRICE             = 'swiss_price';
    const MONTHS                  = 'months';
    const DAYS                    = 'days';
    const STOREADMIN_NUMBERS      = 'storeadmin_numbers';
    const DESCRIPTION             = 'description';
    const CREATION_TIME           = 'creation_time';
    const UPDATE_TIME             = 'update_time';
    const IS_ACTIVE               = 'is_active';
    const CUSTOM_STORES           = 'custom_stores';


    /**
     * @return int
     */
    public function getId();

    /**
     * Get Membership Type
     *
     * @return string|null
     */
    public function getMembershipType();

    /**
     * Get Product Numbers
     *
     * @return int
     */
    public function getProductNumbers();

    /**
     * Get TimeFrom
     *
     * @return int
     */
    public function getTimeFrom();

    /**
     * Get TimeTo
     *
     * @return int
     */
    public function getTimeTo();

    /**
     * Get Usd Price
     *
     * @return int
     */
    public function getUsdPrice();

    /**
     * Get Euro Price
     *
     * @return int
     */
    public function getEuroPrice();

    /**
     * Get Swiss Price
     *
     * @return int
     */
    public function getSwissPrice();

    /**
     * Get Months
     *
     * @return int
     */
    public function getMonths();

    /**
     * Get Months
     *
     * @return int
     */
    public function getDays();

    /**
     * Get StoreAdmin Numbers
     *
     * @return int
     */
    public function getStoreadminNumbers();

    /**
     * Get Description
     *
     * @return string|null
     */
    public function getDescription();

    /**
     * Get Stores
     *
     * @return string|null
     */
    public function getCustomStores();

    /**
     * Get Creation Time
     *
     * @return string|null
     */
    public function getCreationTime();

    /**
     * Get Update Time
     *
     * @return string|null
     */
    public function getUpdateTime();

    /**
     * Is active
     *
     * @return bool|null
     */
    public function isActive();

    /**
     * @param $id
     * @return mixed
     */
    public function setId($id);

    /**
     * @param $membershipType
     * @return mixed
     */
    public function setMembershipType($membershipType);

    /**
     * @param $productNumbers
     * @return mixed
     */
    public function setProductNumbers($productNumbers);

    /**
     * @param $timeFrom
     * @return mixed
     */
    public function setTimeFrom($timeFrom);

    /**
     * @param $timeTo
     * @return mixed
     */
    public function setTimeTo($timeTo);

    /**
     * @param $usdPrice
     * @return mixed
     */
    public function setUsdPrice($usdPrice);

    /**
     * @param $euroPrice
     * @return mixed
     */
    public function setEuroPrice($euroPrice);

    /**
     * @param $swissPrice
     * @return mixed
     */
    public function setSwissPrice($swissPrice);

    /**
     * @param $months
     * @return mixed
     */
    public function setMonths($months);

    /**
     * @param $days
     * @return mixed
     */
    public function setDays($days);

    /**
     * @param $storeadminNumbers
     * @return mixed
     */
    public function setStoreadminNumbers($storeadminNumbers);

    /**
     * @param $description
     * @return mixed
     */
    public function setDescription($description);

    /**
     * @param $description
     * @return mixed
     */
    public function setCustomStores($customStores);

    /**
     * @param $creationTime
     * @return mixed
     */
    public function setCreationTime($creationTime);

    /**
     * @param $updateTime
     * @return mixed
     */
    public function setUpdateTime($updateTime);

    /**
     * @param $isActive
     * @return mixed
     */
    public function setIsActive($isActive);

}