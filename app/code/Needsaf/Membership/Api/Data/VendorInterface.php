<?php

namespace Needsaf\Membership\Api\Data;

/**
 * Interface MemberInterface
 * @package Needsaf\Membership\Api\Data
 */
interface VendorInterface
{
    const ID                    = 'id';
    const FIRSTNAME             = 'firstname';
    const LASTNAME              = 'lastname';
    const USERNAME              = 'username';
    const EMAIL                 = 'email';
    const MEMBERSHIP_ID         = 'membership_id';
    const ORDER_DATE            = 'order_date';
    const TRANSACTION_ID        = 'transaction_id';
    const VENDOR_STATUS         = 'vendor_status';
    const STORE_VIEW            = 'store_view';
    const VENDOR_CATEGORY       = 'vendor_category';
    const STREET                = 'street';
    const CITY                  = 'city';
    const COUNTRY               = 'country';
    const STATE                 = 'state';
    const ZIP_CODE              = 'zip_code';
    const CREATION_TIME         = 'creation_time';
    const UPDATE_TIME           = 'update_time';
    const IS_ACTIVE             = 'is_active';
    const IS_ELIGIBLE           = 'is_eligible';
    const LEAD_COUNT            = 'lead_count';
    const VENDOR_IMAGE          = 'vendor_image';
    const VENDOR_DESCRIPTION    = 'vendor_description';
    const VENDOR_RATING         = 'vendor_rating';
    const CONSULT_CATEGORY      = 'consult_category';
    const VALIDATION_QUESTIONS  = 'validation_questions';

    /**
     * Get Vendor Id
     *
     * @return Int
     */
    public function getId();

    /**
     * Get Member Firstname
     *
     * @return String|null
     */
    public function getFirstname();

    /**
     * Get Member Firstname
     *
     * @return String|null
     */
    public function getLastname();

    /**
     * Get Member Username
     *
     * @return String|null
     */
    public function getUsername();

    /**
     * Get Member Email
     *
     * @return String|null
     */
    public function getEmail();

    /**
     * Get Membership Id
     *
     * @return Int
     */
    public function getMembershipId();

    /**
     * Get Order Date
     *
     * @return Int
     */
    public function getOrderDate();

    /**
     * Get Transaction Id
     *
     * @return Int
     */
    public function getTransactionId();

    /**
     * Get Member Status
     *
     * @return Int
     */
    public function getVendorStatus();

    /**
     * Get Store Name
     *
     * @return Int
     */
    public function getStoreView();

    /**
     * Get vendor category
     *
     * @return Int
     */
    public function getVendorCategory();

    /**
     * Get Street
     *
     * @return string|null
     */
    public function getStreet();

    /**
     * Get City
     *
     * @return string|null
     */
    public function getCity();

    /**
     * Get State
     *
     * @return string|null
     */
    public function getState();

    /**
     * Get country
     *
     * @return string|null
     */
    public function getCountry();

    /**
     * Get Zip Code
     *
     * @return string|null
     */
    public function getZipCode();

    /**
     * Get creation time
     *
     * @return string|null
     */
    public function getCreationTime();

    /**
     * Get update time
     *
     * @return string|null
     */
    public function getUpdateTime();

    /**
     * Is active
     *
     * @return bool|null
     */
    public function isActive();


    /**
     * Is Eligible for Lead
     * @return bool
     */
    public function isEligible();


    /**
     * Get Lead Count
     * @return int
     */
    public function getLeadCount();

        /**
     * Get  Vendor Image
     * @return int
     */
    public function getVendorImage();

            /**
     * Get  Vendor Image
     * @return int
     */
    public function getVendorDescription();

    /**
     * Get  Vendor Rating
     * @return int
     */
    public function getVendorRating();

    public function getConsultCategory();
    
    public function getValidationQuestions();

    /**
     * set Id
     *
     * @param string $id
     * @return \Needsaf\Membership\Api\Data\VendorInterface
     */
    public function setId($id);

    /**
     * Set firstname
     *
     * @param string $memberName
     * @return \Needsaf\Membership\Api\Data\VendorInterface
     */
    public function setFirstname($firstname);

    /**
     * Set lastname
     *
     * @param string $lastname
     * @return \Needsaf\Membership\Api\Data\VendorInterface
     */
    public function setLastname($lastname);

    /**
     * Set username
     *
     * @param string $Username
     * @return \Needsaf\Membership\Api\Data\VendorInterface
     */
    public function setUsername($username);

    /**
     * Set email
     *
     * @param string $email
     * @return \Needsaf\Membership\Api\Data\VendorInterface
     */
    public function setEmail($email);

    /**
     * set Membership Id
     *
     * @param string $membershipId
     * @return \Needsaf\Membership\Api\Data\VendorInterface
     */
    public function setMembershipId($membershipId);

    /**
     * set Street
     *
     * @param string $street
     * @return \Needsaf\Membership\Api\Data\VendorInterface
     */
    public function setStreet($street);

    /**
     * set City
     *
     * @param string $city
     * @return \Needsaf\Membership\Api\Data\VendorInterface
     */
    public function setCity($city);

    /**
     * set State
     *
     * @param string $state
     * @return \Needsaf\Membership\Api\Data\VendorInterface
     */
    public function setState($state);

    /**
     * set Country
     *
     * @param string $country
     * @return \Needsaf\Membership\Api\Data\VendorInterface
     */
    public function setCountry($country);

    /**
     * set Zip Code
     *
     * @param string $zipCode
     * @return \Needsaf\Membership\Api\Data\VendorInterface
     */
    public function setZipCode($zipCode);
    /**
     * set Order Date
     *
     * @param string $orderDate
     * @return \Needsaf\Membership\Api\Data\VendorInterface
     */
    public function setOrderDate($orderDate);

    /**
     * set Transaction Id
     *
     * @param string $transactionId
     * @return \Needsaf\Membership\Api\Data\VendorInterface
     */
    public function setTransactionId($transactionId);

    /**
     * Set memberStatus
     *
     * @param string $memberStatus
     * @return \Needsaf\Membership\Api\Data\VendorInterface
     */
    public function setVendorStatus($vendorStatus);

    /**
     * Set storeName
     *
     * @param string $storeName
     * @return \Needsaf\Membership\Api\Data\VendorInterface
     */
    public function setStoreView($storeView);

    /** Set  vendorCategory
     *
     * @param string $vendorCategory
     * @return \Needsaf\Membership\Api\Data\VendorInterface
     */
    public function setVendorCategory($vendorCategory);

    /**
     * Set creationTime
     *
     * @param string $creationTime
     * @return \Needsaf\Membership\Api\Data\VendorInterface
     */
    public function setCreationTime($creationTime);

    /**
     * Set updateTime
     *
     * @param string $updateTime
     * @return \Needsaf\Membership\Api\Data\VendorInterface
     */
    public function setUpdateTime($updateTime);

    /**
     * Set is active
     *
     * @param int|bool $isActive
     * @return \Needsaf\Membership\Api\Data\VendorInterface
     */
    public function setIsActive($isActive);

    public function setIsEligible($isEligible);

    public function setLeadCount($leadCount);

    public function setVendorImage($vendorImage);

    public function setVendorDescription($vendorDescription);

    public function setVendorRating($vendorRating);

    public function setConsultCategory($consultCategory);
    
    public function setValidationQuestions($validationQuestions);

}