<?php
namespace Needsaf\Inquiry\Cron;

use Needsaf\Membership\Model\Vendor;
use Needsaf\Membership\Model\VendorFactory;
use Magento\Store\Model\ScopeInterface;

class RemoveInquiry
{
    protected $inquiryFactory;
    protected $_timezoneInterface;
    protected $_vendorFactory;
    protected $customerFactory;
    protected $_transportBuilder;

    /**
     * @var \Magento\Framework\Translate\Inline\StateInterface
     */
    protected $inlineTranslation;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    const XML_PATH_EMAIL_RECIPIENT = 'trans_email/ident_general/email';

    public function __construct(\Needsaf\Inquiry\Model\InquiryFactory $inquiryFactory,
        VendorFactory $vendorFactory,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder,
        \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $timezoneInterface)
    {
        $this->_vendorFactory = $vendorFactory;
        $this->customerFactory = $customerFactory;
        $this->_transportBuilder = $transportBuilder;
        $this->inlineTranslation = $inlineTranslation;
        $this->scopeConfig = $scopeConfig;
        $this->_timezoneInterface = $timezoneInterface;
        $this->_inquiryFactory = $inquiryFactory;
    }

    public function execute()
    {
       $currentDate = $this->_timezoneInterface->date();
       $inquiryModel = $this->_inquiryFactory->create();
       $collection = $inquiryModel->getCollection();
       $collection->addFieldToFilter('inquiry_status','pending')->load();
       foreach($collection as $data){
        $added_at = $this->_timezoneInterface->date($data->getCreationTime());
        $dateDiff = $added_at->diff($currentDate);
           $totalHours = ($currentDate->getTimestamp() - $added_at->getTimestamp()) / 3600;

            if($totalHours >= (int)$this->scopeConfig->getValue('needsaf_category/inquirytime/inquiry_time', ScopeInterface::SCOPE_STORE)){
                $inquiryModel->load($data->getId());
                $vendorModel =  $this->_vendorFactory->create();
                $vendorModel->load($data->getVendorId());
                $vendorEmail = $vendorModel->getEmail();
                $customerModel = $this->customerFactory->create();
                $customerModel->load($data->getCustomerId());
                $customerEmail = $customerModel->getEmail();
               // $inquiryModel->delete();
                $inquiryModel->setInquiryStatus('auto_rejected');
                $inquiryModel->save();
                $admin = $this->scopeConfig->getValue('trans_email/ident_general/email', ScopeInterface::SCOPE_STORE);
                $recipients = array($vendorEmail,$customerEmail,$admin);
                $post =$data->getData();
                $postObject = new \Magento\Framework\DataObject();
                $postObject->setData($post);
                foreach($recipients as $recipient){
                    $transport = $this->_transportBuilder
                    ->setTemplateVars(['data' => $postObject])
                    ->setTemplateIdentifier('inquiry_status_expired_template')
                    ->setTemplateOptions(
                        [
                        'area' => \Magento\Framework\App\Area::AREA_FRONTEND,
                        'store' => \Magento\Store\Model\Store::DEFAULT_STORE_ID
                        ]
                        )
                    ->setFrom(['email' => $this->scopeConfig->getValue(self::XML_PATH_EMAIL_RECIPIENT, ScopeInterface::SCOPE_STORE),
                        'name' => $this->scopeConfig->getValue('trans_email/ident_general/email', ScopeInterface::SCOPE_STORE)])
                    ->addTo($recipient)
                    ->getTransport();
                    $transport->sendMessage();
                    $this->inlineTranslation->resume();
                }

            }   
        
        }
    }
}
