<?php 
namespace Needsaf\Inquiry\Model\ResourceModel\Inquiry;
 
/**
 * Inquiry Data collection
 */

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'id';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Needsaf\Inquiry\Model\Inquiry', 'Needsaf\Inquiry\Model\ResourceModel\Inquiry');
    }

}