<?php
namespace Needsaf\Inquiry\Model;
use Needsaf\Inquiry\Api\Data\InquiryInterface;
use Magento\Framework\DataObject\IdentityInterface;


/**
 * Class Member
 * @package Needsaf\Membership\Model
 */
class Inquiry  extends \Magento\Framework\Model\AbstractModel implements InquiryInterface, IdentityInterface
{

    /**
     * CMS page cache tag
     */
    const CACHE_TAG = 'needsaf_inquiry';

    /**
     * @var string
     */
    protected $_cacheTag = 'needsaf_inquiry';

    /**
     * Prefix of model events names
     *
     * @var string
     */
    protected $_eventPrefix = 'needsaf_inquiry';

    /**
     * @var $setup;
     */
    protected $setup;


    /**
     * Member constructor.
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Model\ResourceModel\AbstractResource|null $resource
     * @param \Magento\Framework\Data\Collection\AbstractDb|null $resourceCollection
     * @param array $data
     */
    function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = [])
    {
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);

    }
    protected function _construct()
    {
        $this->_init('Needsaf\Inquiry\Model\ResourceModel\Inquiry');
    }

    /**
     * @param bool $plural
     * @return string
     */
    public function getOwnTitle($plural = false)
    {
        return $plural ? 'Inquiry' : 'Inquiries';
    }

    /**
     * Return unique ID(s) for each object in system
     *
     * @return array
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * Get ID
     *
     * @return int|null
     */
    public function getId()
    {
        return $this->getData(self::INQUIRY_ID);
    }

    /**
     * Get Member Firstname
     *
     * @return String|null
     */
    public function getOrderConjcatName()
    {
        return $this->getData(self::ORDER_CONJCAT_NAME);
    }

    /**
     * Get Member Firstname
     *
     * @return String|null
     */
    public function getObjectType()
    {
        return $this->getData(self::OBJECT_TYPE);
    }

    /**
     * Get Member Email
     *
     * @return String|null
     */
    public function getBudget()
    {
        return $this->getData(self::BUDGET);
    }

    /**
     * Get username
     *
     * @return string|null
     */
    public function getDescription()
    {
        return $this->getData(self::DESCRIPTION);
    }

    /**
     * Get username
     *
     * @return string|null
     */
    public function getImage()
    {
        return $this->getData(self::IMAGE);
    }

    /*
     * Get membership id
     *
     * @return int/null
     */
    public function getInquiryStatus()
    {
        return $this->getData(self::INQUIRY_STATUS);
    }

    /**
     * Get Order Date
     *
     * @return Int
     */
    public function getIsLeadPaid()
    {
        return $this->getData(self::IS_LEAD_PAID);
    }

    /**
     * Get Transaction Id
     *
     * @return Int
     */
    public function getVendorId()
    {
        return $this->getData(self::VENDOR_ID);
    }
    public function getReason()
    {
        return $this->getData(self::REASON);
    }
    public function getCustomerId()
    {
        return $this->getData(self::CUSTOMER_ID);
    }


    /**
     * Get creation time
     *
     * @return string|null
     */
    public function getCreationTime()
    {
        return $this->getData(self::CREATION_TIME);
    }

    /**
     * Get update time
     *
     * @return string|null
     */
    public function getUpdateTime()
    {
        return $this->getData(self::UPDATE_TIME);
    }

    /**
     * Set id
     *
     * @param int $id
     * @return \Needsaf\Membership\Api\Data\VendorInterface
     */
    public function setId($id)
    {
        return $this->setData(self::INQUIRY_ID, $id);
    }

    /**
     * Set firstname
     *
     * @param string $memberName
     * @return \Needsaf\Membership\Api\Data\VendorInterface
     */
    public function setOrderConjcatName($orderConjcatName)
    {
        return $this->setData(self::ORDER_CONJCAT_NAME, $orderConjcatName);
    }

    /**
     * Set lastname
     *
     * @param string $lastname
     * @return \Needsaf\Membership\Api\Data\VendorInterface
     */
    public function setObjectType($objectType)
    {
        return $this->setData(self::OBJECT_TYPE, $objectType);
    }

    /**
     * Set username
     *
     * @param string $username
     * @return \Needsaf\Membership\Api\Data\VendorInterface
     */

    public function setBudget($budget)
    {
        return $this->setData(self::BUDGET, $budget);
    }

    /**
     * Set email
     *
     * @param string $email
     * @return \Needsaf\Membership\Api\Data\VendorInterface
     */
    public function setDescription($description)
    {
        return $this->setData(self::DESCRIPTION, $description);
    }

     /**
     * Set email
     *
     * @param string $email
     * @return \Needsaf\Membership\Api\Data\VendorInterface
     */
    public function setImage($image)
    {
        return $this->setData(self::IMAGE, $image);
    }

    /**
     * Set membership id
     *
     * @param string $membershipId
     * @return \Needsaf\Membership\Api\Data\VendorInterface
     */
    public function setInquiryStatus($inquiryStatus)
    {
        return $this->setData(self::INQUIRY_STATUS, $inquiryStatus);
    }

    /**
     * set Order Date
     *
     * @param string $orderDate
     * @return \Needsaf\Membership\Api\Data\VendorInterface
     */
    public function setIsLeadPaid($isLeadPaid)
    {
        return $this->setData(self::IS_LEAD_PAID, $isLeadPaid);
    }

    /**
     * set Transaction Id
     *
     * @param string $transactionId
     * @return \Needsaf\Membership\Api\Data\VendorInterface
     */
    public function setVendorId($vendorId)
    {
        return $this->setData(self::VENDOR_ID, $vendorId);
    }
    public function setReason($reason)
    {
        return $this->setData(self::REASON, $reason);
    }
    public function setCustomerId($customerId)
    {
        return $this->setData(self::CUSTOMER_ID, $customerId);
    }

    /**
     * Set creation time
     *
     * @param string $update_time
     * @return \Needsaf\Membership\Api\Data\VendorInterface
     */
    public function setCreationTime($creation_time)
    {
        return $this->setData(self::CREATION_TIME, $creation_time);
    }

    /**
     * Set update time
     *
     * @param string $update_time
     * @return \Needsaf\Membership\Api\Data\VendorInterface
     */
    public function setUpdateTime($update_time)
    {
        return $this->setData(self::UPDATE_TIME, $update_time);
    }

}


