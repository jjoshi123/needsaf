<?php
/**
 * Created by PhpStorm.
 * User: krishaweb
 * Date: 13/8/17
 * Time: 11:49 AM
 */

namespace Needsaf\Inquiry\Controller\Adminhtml\Inquiry;


use Magento\Backend\App\Action;
use Magento\Framework\App\ResponseInterface;
use Magento\Store\Model\ScopeInterface;
use Needsaf\Inquiry\Model\InquiryFactory;
use Needsaf\Membership\Model\VendorFactory;


class Decline extends \Magento\Backend\App\Action
{

    protected $inquiryFactory;
    const XML_PATH_EMAIL_RECIPIENT = 'trans_email/ident_general/email';
    protected $_vendorFactory;
    protected $customerFactory;
    protected $_transportBuilder;


    /**
     * @var \Magento\Framework\Translate\Inline\StateInterface
     */
    protected $inlineTranslation;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;
    public function __construct(InquiryFactory $inquiryFactory,
                                VendorFactory $vendorFactory,
                                \Magento\Customer\Model\CustomerFactory $customerFactory,
                                \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder,
                                \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation,
                                \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
                                Action\Context $context)
    {

        $this->inquiryFactory = $inquiryFactory;
        $this->_vendorFactory = $vendorFactory;
        $this->customerFactory = $customerFactory;
        $this->_transportBuilder = $transportBuilder;
        $this->inlineTranslation = $inlineTranslation;
        $this->scopeConfig = $scopeConfig;
        parent::__construct($context);
    }

    /**
     * Dispatch request
     *
     * @return \Magento\Framework\Controller\ResultInterface|ResponseInterface
     * @throws \Magento\Framework\Exception\NotFoundException
     */
    public function execute()
    {

        $params = $this->getRequest()->getParams();
        try{
            $inquiryModel = $this->inquiryFactory->create()->load($params['inquiry_id']);
            $vendorModel =  $this->_vendorFactory->create();
            $vendorModel->load($inquiryModel->getVendorId());
            $vendorEmail = $vendorModel->getEmail();
            $customerModel = $this->customerFactory->create();
            $customerModel->load($inquiryModel->getCustomerId());
            $customerEmail = $customerModel->getEmail();
            $inquiryModel->setInquiryStatus('declined');
            $inquiryModel->save();

            $admin = $this->scopeConfig->getValue('trans_email/ident_general/email', ScopeInterface::SCOPE_STORE);
            $recipients = array($vendorEmail,$customerEmail,$admin);
            $data =$inquiryModel->getData();
            $postObject = new \Magento\Framework\DataObject();
            $postObject->setData($data);
            foreach($recipients as $recipient) {
                $transport = $this->_transportBuilder
                    ->setTemplateVars(['data' => $postObject])
                    ->setTemplateIdentifier('inquiry_status_template')
                    ->setTemplateOptions(
                        [
                            'area' => \Magento\Framework\App\Area::AREA_FRONTEND,
                            'store' => \Magento\Store\Model\Store::DEFAULT_STORE_ID
                        ]
                    )
                    ->setFrom(['email' => $this->scopeConfig->getValue(self::XML_PATH_EMAIL_RECIPIENT, ScopeInterface::SCOPE_STORE),
                        'name' => $this->scopeConfig->getValue('trans_email/ident_general/email', ScopeInterface::SCOPE_STORE)])
                    ->addTo($recipient)
                    ->getTransport();
                $transport->sendMessage();
                $this->inlineTranslation->resume();
            }
                $this->messageManager->addSuccessMessage('Inquiry has been declined a mail will be sent to the customer.');

        }
        catch (\Exception $e){
            $this->messageManager->addExceptionMessage($e,'Inquiry was not saved properly please try again.');
        }

        $this->_redirect('*/*/');


    }
}