<?php

namespace Needsaf\Inquiry\Controller\Adminhtml\Inquiry;


use Needsaf\Membership\Model\VendorFactory;
use Magento\Store\Model\ScopeInterface;
/**
 * inquiry grid controller
 */

class Save extends \Needsaf\Inquiry\Controller\Adminhtml\Inquiry
{

	const XML_PATH_EMAIL_RECIPIENT = 'trans_email/ident_general/email';
	protected $_vendorFactory;
	protected $customerFactory;
	protected $_transportBuilder;

    /**
     * @var \Magento\Framework\Translate\Inline\StateInterface
     */
    protected $inlineTranslation;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    public function __construct( \Magento\Backend\App\Action\Context $context,
    	VendorFactory $vendorFactory,
    	\Magento\Customer\Model\CustomerFactory $customerFactory,
    	\Magento\Framework\Mail\Template\TransportBuilder $transportBuilder,
    	\Magento\Framework\Translate\Inline\StateInterface $inlineTranslation,
    	\Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
    	array $data = []
    	)
    {
    	$this->_vendorFactory = $vendorFactory;
    	$this->customerFactory = $customerFactory;
    	$this->_transportBuilder = $transportBuilder;
    	$this->inlineTranslation = $inlineTranslation;
    	$this->scopeConfig = $scopeConfig;
    	parent::__construct($context);
    }
    public function _afterSave($model)
    {
    	$vendorModel =  $this->_vendorFactory->create();
    	$vendorModel->load($model->getVendorId());
    	$vendorEmail = $vendorModel->getEmail();
    	$customerModel = $this->customerFactory->create();
    	$customerModel->load($model->getCustomerId());
    	$customerEmail = $customerModel->getEmail();
    	$inquiryStatus = $model->getInquiryStatus();
    	if($inquiryStatus=='accepted' || $inquiryStatus=='declined')
    	{
    		$admin = $this->scopeConfig->getValue('trans_email/ident_general/email', ScopeInterface::SCOPE_STORE);
    		$recipients = array($vendorEmail,$customerEmail,$admin);
    		$data =$model->getData();
    		$postObject = new \Magento\Framework\DataObject();
    		$postObject->setData($data);
    		foreach($recipients as $recipient){
    			$transport = $this->_transportBuilder
    			->setTemplateVars(['data' => $postObject])
    			->setTemplateIdentifier('inquiry_status_template')
    			->setTemplateOptions(
    				[
    				'area' => \Magento\Framework\App\Area::AREA_FRONTEND,
    				'store' => \Magento\Store\Model\Store::DEFAULT_STORE_ID
    				]
    				)
    			->setFrom(['email' => $this->scopeConfig->getValue(self::XML_PATH_EMAIL_RECIPIENT, ScopeInterface::SCOPE_STORE),
    				'name' => $this->scopeConfig->getValue('trans_email/ident_general/email', ScopeInterface::SCOPE_STORE)])
    			->addTo($recipient)
    			->getTransport();
    			$transport->sendMessage();
    			$this->inlineTranslation->resume();
    		}
    	}
    }
}