<?php

namespace Needsaf\Inquiry\Controller\Adminhtml;

/**
 * Class Inquiry
 * @package Needsaf\Category\Controller\Adminhtml
 * Admin Inquiry edit controller
 */
class Inquiry extends Actions
{
    /**
     * Form session key
     * @var string
     */
    protected $_formSessionKey = 'needsaf_inquiry_form_data';

    /**
     * Allowed Key
     * @var string
     */
    protected $_allowedKey = 'Needsaf_Inquiry::inquiry';

    /**
     * Model class name
     * @var string
     */
    protected $_modelClass = 'Needsaf\Inquiry\Model\Inquiry';

    /**
     * Active menu key
     * @var string
     */
    protected $_activeMenu = 'Needsaf_Inquiry::inquiry';

    /**
     * Status field name
     * @var string
     */
    protected $_statusField = 'is_active';

    /**
     * Save request params key
     * @var string
     */
    protected $_paramsHolder = 'post';
}
