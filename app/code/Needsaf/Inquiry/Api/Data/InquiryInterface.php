<?php
namespace Needsaf\Inquiry\Api\Data;


/**
 * Interface InquiryInterface
 * @package Needsaf\Category\Api\Data
 */
interface InquiryInterface
{
    const INQUIRY_ID              = 'id';
    const ORDER_CONJCAT_NAME      = 'order_conjcat_name';
    const OBJECT_TYPE             = 'object_type';
    const BUDGET                  = 'budget';
    const DESCRIPTION             = 'description';
    const IMAGE                   = 'image';
    const INQUIRY_STATUS          = 'inquiry_status';
    const IS_LEAD_PAID            = 'is_lead_paid';
    const VENDOR_ID               = 'vendor_id';
    const REASON                  = 'reason';
    const CUSTOMER_ID             = 'customer_id';
    const CREATION_TIME           = 'creation_time';
    const UPDATE_TIME             = 'update_time';


    /**
     * @return int
     */
    public function getId();

    /**
     * Get Membership Type
     *
     * @return string|null
     */
    public function getOrderConjcatName();

    /**
     * Get Product Numbers
     *
     * @return int
     */
    public function getObjectType();

    /**
     * Get TimeFrom
     *
     * @return int
     */
    public function getBudget();

    /**
     * Get TimeTo
     *
     * @return int
     */
    public function getDescription();
    /**
     * Get TimeTo
     *
     * @return int
     */
    public function getImage();

    /**
     * Get Usd Price
     *
     * @return int
     */
    public function getInquiryStatus();

    /**
     * Get Euro Price
     *
     * @return int
     */
    public function getIsLeadPaid();

    /**
     * Get Swiss Price
     *
     * @return int
     */
    public function getVendorId();

    /**
     * Get Swiss Price
     *
     * @return int
     */
    public function getReason();
    public function getCustomerId();


    /**
     * Get Creation Time
     *
     * @return string|null
     */
    public function getCreationTime();

    /**
     * Get Update Time
     *
     * @return string|null
     */
    public function getUpdateTime();

    /**
     * @param $id
     * @return mixed
     */
    public function setId($id);

    /**
     * @param $membershipType
     * @return mixed
     */
    public function setOrderConjcatName($orderConjcatName);

    /**
     * @param $productNumbers
     * @return mixed
     */
    public function setObjectType($objectType);

    /**
     * @param $timeFrom
     * @return mixed
     */
    public function setBudget($budget);

     public function setImage($image);
    public function setDescription($description);


    public function setInquiryStatus($inquiryStatus);

    /**
     * @param $months
     * @return mixed
     */
    public function setIsLeadPaid($isLeadPaid);

    /**
     * @param $days
     * @return mixed
     */
    public function setVendorId($vendorId);
    public function setReason($reason);
    public function setCustomerId($customerId);


    /**
     * @param $creationTime
     * @return mixed
     */
    public function setCreationTime($creationTime);

    /**
     * @param $updateTime
     * @return mixed
     */
    public function setUpdateTime($updateTime);


}