<?php
namespace Needsaf\Inquiry\Block;


use Magento\Framework\View\Element\Template;

class Inquiry extends \Magento\Framework\View\Element\Template
{
    protected $customerSession;
    protected $inquiryFactory;

    public function __construct(
        Template\Context $context,
        \Magento\Customer\Model\Session $customerSession,
        \Needsaf\Inquiry\Model\InquiryFactory $inquiryFactory,
        array $data = [])
    {
        parent::__construct($context, $data);
        $this->customerSession = $customerSession;
         $this->_inquiryFactory = $inquiryFactory;
    }

    public function getInquiries()
    {
        $customerId = $this->customerSession->getCustomer()->getId();
        $inquiryModel = $this->_inquiryFactory->create();
        $collection = $inquiryModel->getCollection();
        $collection->addFieldToFilter('customer_id',$customerId)
        ->addFieldToFilter('inquiry_status',array('neq'=>'auto_rejected'))
        ->load();
        return $collection;
    }
}