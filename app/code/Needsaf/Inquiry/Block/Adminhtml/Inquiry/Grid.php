<?php
namespace Needsaf\Inquiry\Block\Adminhtml\Inquiry;

use Magento\Backend\Block\Widget\Grid as WidgetGrid;

class Grid extends WidgetGrid
{
    protected $authSession;
    protected $_backendSession;
    protected $context;
    protected $userFactory;

    /**
     * @var \Magento\Backend\Helper\Data
     */
    protected $_backendHelper;
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Model\Auth\Session $authSession,
        \Magento\Backend\Helper\Data $backendHelper,
        \Magento\User\Model\UserFactory $userFactory,
        array $data = []
        ) {
        $this->_backendHelper = $backendHelper;
        $this->authSession = $authSession;
        $this->_backendSession = $context->getBackendSession();
        $this->userFactory = $userFactory;
        parent::__construct($context,$backendHelper, $data);
    }

    protected function _prepareCollection()
    {
        $roleName = $this->authSession->getUser()->getRole()->getRoleName();
        $userId = $this->authSession->getUser()->getId();
        $user = $this->userFactory->create()->load($userId);
        $vendorId= $user->getVendorId();
        if($roleName!='Administrators'){
            $collection = $this->getCollection();
            $collection->addFieldToFilter('vendor_id', $vendorId)
            ->addFieldToFilter('inquiry_status',array('neq'=>'auto_rejected'))->load();
        }
        parent::_prepareCollection();
    }
}