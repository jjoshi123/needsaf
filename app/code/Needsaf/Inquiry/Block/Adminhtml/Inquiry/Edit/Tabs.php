<?php

namespace Needsaf\Inquiry\Block\Adminhtml\Inquiry\Edit;


/**
 * Class Tabs
 * @package eedsaf\Inquiry\Block\Adminhtml\Banner\Edit
 */
class Tabs extends \Magento\Backend\Block\Widget\Tabs
{
    /**
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('Inquiry_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(__('Inquiry Information'));
    }


    /**
     * @return $this
     */
    protected function _beforeToHtml()
    {
        return parent::_beforeToHtml();
    }
}