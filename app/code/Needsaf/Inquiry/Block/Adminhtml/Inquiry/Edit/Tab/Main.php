<?php
namespace Needsaf\Inquiry\Block\Adminhtml\Inquiry\Edit\Tab;

use Magento\Framework\ObjectManagerInterface;
/**
 * Class Main
 * @package Spring\Spirits\Block\Adminhtml\Category\Edit\Tab
 */
class Main extends \Magento\Backend\Block\Widget\Form\Generic implements \Magento\Backend\Block\Widget\Tab\TabInterface
{
    /**
     * @var \Magento\Cms\Model\Wysiwyg\Config
     */
    protected $_wysiwygConfig;
    protected $authSession;
    protected $_backendSession;

    protected $objectManager;
    protected $inquiryFactory;
    protected $customerFactory;

    /**
     * Main constructor.
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Data\FormFactory $formFactory
     * @param \Magento\Cms\Model\Wysiwyg\Config $wysiwygConfig
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
         ObjectManagerInterface $objectManager,
         \Needsaf\Inquiry\Model\InquiryFactory $inquiryFactory,
        \Magento\Cms\Model\Wysiwyg\Config $wysiwygConfig,
        \Magento\Backend\Model\Auth\Session $authSession,
        array $data = []
    ) {
        $this->_wysiwygConfig = $wysiwygConfig;
        $this->objectManager = $objectManager;
        $this->inquiryFactory = $inquiryFactory;
        $this->customerFactory = $customerFactory;
        $this->authSession = $authSession;
        $this->_backendSession = $context->getBackendSession();
        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * @return $this
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function _prepareForm()
    {
        $model = $this->_coreRegistry->registry('current_model');
        $isElementDisabled = !$this->_isAllowedAction('Needsaf_Inquiry::inquiry');
        $form = $this->_formFactory->create();
        $fieldset = $form->addFieldset('base_fieldset', ['legend' => __('Inquiry Information')]);
        $wysiwygConfig = $this->_wysiwygConfig->getConfig(['tab_id' => $this->getTabId()]);
        $media_dir = $this->objectManager->get('Magento\Store\Model\StoreManagerInterface')
                ->getStore()
                ->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);

        if ($model->getId()) {
            $fieldset->addField('id', 'text', ['name' => 'id','label' => __('Inquiry ID'),
                'title' => __('Inquiry ID'),
                'required' => false,
                'disabled' => $isElementDisabled,
                'readonly' => true]);
        }
        $inquiry_status = array(
            array(
                'label' => 'Pending',
                'value' => 'pending',
            ),
            array(
                'label' => 'Accepted',
                'value' => 'accepted',
            ),
            array(
                'label' => 'Declined',
                'value' => 'declined',
            ),
            array(
                'label' => 'Auto Rejected',
                'value' => 'auto_rejected',
            )
        );
//

        $fieldset->addField(
           'order_conjcat_name',
           'text',
           [
               'name' => 'post[order_conjcat_name]',
               'label' => __('Category Name'),
               'title' => __('Category Name'),
               'required' => false,
               'disabled' => $isElementDisabled,
               'readonly' => true
           ]
       );
        $fieldset->addField(
            'object_type',
            'text',
            [
                'name' => 'post[object_type]',
                'label' => __('Object Type'),
                'title' => __('Object Type'),
                'required' => false,
                'disabled' => $isElementDisabled,
                'readonly' => true
            ]
        );
        $fieldset->addField(
           'budget',
           'text',
           [
               'name' => 'post[budget]',
               'label' => __('Budget'),
               'title' => __('Budget'),
               'required' => false,
               'disabled' => $isElementDisabled,
               'readonly' => true
           ]
       );

////
          $contentField= $fieldset->addField(
            'description',
            'textarea',
            [
                'name' => 'post[description]',
                'label' => __('Description'),
                'title' => __('Description'),
                'required' => false,
                'disabled' => $isElementDisabled,
                'config' => $wysiwygConfig,
                'readonly' => true
            ]
        );
         $fieldset->addField(
           'inquiry_status',
           'select',
           [
               'name' => 'post[inquiry_status]',
               'label' => __('Inquiry Status'),
               'title' => __('Inquiry Status'),
               'values'=> $inquiry_status,
               'required' => false,
               'disabled' => true,
               'readonly' => true
           ]
       );
        $fieldset->addField(
           'image',
           'link',
           [
               'name' => 'post[image]',
               'href' =>$media_dir.'customerAttachment/'.$model->getImage(),
               'title' => __('Inquiry Image'),
               'label' => __('Inquiry Image'),
               'value'=>'mylink',
               'target' => '_blank',
               'required' => false,
               'disabled' => $isElementDisabled
           ]
       );
          $fieldset->addField(
           'reason',
           'textarea',
           [
               'name' => 'post[reason]',
               'label' => __('Reason'),
               'title' => __('Reason'),
               'required' => false,
               'disabled' => $isElementDisabled
           ]
       );

        $renderer = $this->getLayout()->createBlock(
            'Magento\Backend\Block\Widget\Form\Renderer\Fieldset\Element'
        );
        $contentField->setRenderer($renderer);

        $this->_eventManager->dispatch('needsaf_inquiry_edit_tab_main_prepare_form', ['form' => $form]);

        $form->setValues($model->getData());
        $this->setForm($form);

        return parent::_prepareForm();
    }

    public function getCustomerData()
    {
      $inquiryId = $this->getRequest()->getParam('id');
      $inquiryModel = $this->inquiryFactory->create()->load($inquiryId);
      $custId = $inquiryModel->getCustomerId();
      $customerModel = $this->customerFactory->create()->load($custId);
      return $customerModel;

    }
    public function getInquiryStatus()
    {
      $inquiryId = $this->getRequest()->getParam('id');
      $inquiryModel = $this->inquiryFactory->create()->load($inquiryId);
      return $inquiryModel->getInquiryStatus();
    }
    /**
     * Prepare label for tab
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabLabel()
    {
        return __('Inquiry  Information');
    }

    /**
     * Prepare title for tab
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabTitle()
    {
        return __('Inquiry Information');
    }

    /**
     * Returns status flag about this tab can be shown or not
     *
     * @return bool
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * Returns status flag about this tab hidden or not
     *
     * @return bool
     */
    public function isHidden()
    {
        return false;
    }

    /**
     * Check permission for passed action
     *
     * @param string $resourceId
     * @return bool
     */
    protected function _isAllowedAction($resourceId)
    {
        return $this->_authorization->isAllowed($resourceId);
    }

    public function getInquiryId()
    {
      return $this->getRequest()->getParam('id');
    }

    public function getAcceptUrl()
    {
        $model = $this->_coreRegistry->registry('current_model');
        $url = 'leadpayment/payment/index/';
        return $this->getUrl($url,array('inquiry_id'=>$model->getId(),'type' => 'inquiry','vendor_id'=>$model->getVendorId(),'budget'=>$model->getBudget()));

    }

    public function getDeclineUrl()
    {
        $model = $this->_coreRegistry->registry('current_model');
        return $this->getUrl('inquiry/inquiry/decline/',array('inquiry_id'=> $model->getId()));

    }

    public function getUserType()
    {
        $roleName = $this->authSession->getUser()->getRole()->getRoleName();
        return $roleName;

    }
}
