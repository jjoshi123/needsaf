<?php

namespace Needsaf\Inquiry\Block\Adminhtml;


/**
 * Class Inquiry
 * @package Needsaf\Inquiry\Block\Adminhtml
 */
class Inquiry extends \Magento\Backend\Block\Widget\Grid\Container
{
    /**
     * Constructor
     *
     * @return void
     */
    protected function _construct()
    {
         parent::_construct();
        $this->_blockGroup = 'Needsaf_Inquiry';
        $this->_controller = 'adminhtml';
        $this->_headerText = __('Inquiry');
        $this->removeButton('add');
       
    }
}
