<?php
namespace Needsaf\Inquiry\Block;

use Needsaf\VendorRating\Model\VendorRatingFactory;
use Magento\Framework\View\Element\Template;
use Needsaf\Membership\Model\VendorFactory;

class InquiryDetail extends \Magento\Framework\View\Element\Template
{
    protected $customerSession;
    protected $inquiryFactory;
    protected $_objectManager;
    protected $_vendorRatingFactory;
    protected $vendorRatingModel;
    protected $vendorFactory;

    public function __construct(
        Template\Context $context,
        \Magento\Customer\Model\Session $customerSession,
         \Magento\Framework\ObjectManagerInterface $objectmanager,
        \Needsaf\Inquiry\Model\InquiryFactory $inquiryFactory,
        VendorRatingFactory $vendorRatingFactory,
        VendorFactory $vendorFactory,
        array $data = [])
    {
        parent::__construct($context, $data);
        $this->customerSession = $customerSession;
        $this->_objectManager = $objectmanager;
        $this->_vendorRatingFactory = $vendorRatingFactory;
        $this->vendorRatingModel = $this->_vendorRatingFactory->create();
        $this->_inquiryFactory = $inquiryFactory;
        $this->vendorFactory = $vendorFactory;
    }

    public function getInquiryDetails()
    {
        $id = $this->getRequest()->getParam('id');
        $customerId = $this->customerSession->getCustomer()->getId();
        $inquiryModel = $this->_inquiryFactory->create()->load($id);
        return $inquiryModel;
    }
    public function getMediaPath()
    {
        $media_dir = $this->_objectManager->get('Magento\Store\Model\StoreManagerInterface')
                ->getStore()
                ->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
                return $media_dir;
    }
    public function getVendorRatingUrl()
    {
        return $this->getUrl('vendorrating/index/post/');
    }

    public function getPreviousRatings($orderId)
    {
        $orderId = 'in_'.$orderId;

        $vendorRatingData = $this->vendorRatingModel->load($orderId,'order_id');
        if ($vendorRatingData->hasData()&&$vendorRatingData->getStatus()==1){
            return true;
        }else{
            return false;
        }
    }
    public function getVendorName($vendorId)
    {
        $vendorRatingData = $this->vendorRatingModel->load($vendorId,'vendor_id');
        return $vendorRatingData->getVendorName();
    }
    public function getOrderId($orderId)
    {
        $vendorRatingData = $this->vendorRatingModel->load($orderId,'order_id');
        return $vendorRatingData->getOrderId();
    }
    public function getRatingValue($orderId)
    {
        $vendorRatingData = $this->vendorRatingModel->load($orderId,'order_id');
        return $vendorRatingData->getRatingValue();
    }
    public function getRatingDescription($orderId)
    {
        $vendorRatingData = $this->vendorRatingModel->load($orderId,'order_id');
        return $vendorRatingData->getRatingDescription();
    }

    public function getVendorData($vendorId)
    {
        $vendorData = $this->vendorFactory->create()->load($vendorId);
        return $vendorData;
    }

}