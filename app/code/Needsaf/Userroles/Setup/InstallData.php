<?php
/**
 * Created by PhpStorm.
 * User: krishaweb
 * Date: 22/6/17
 * Time: 3:39 PM
 */

namespace Needsaf\Userroles\Setup;


use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

/* For get RoleType and UserType for create Role   */;
use Magento\Authorization\Model\Acl\Role\Group as RoleGroup;
use Magento\Authorization\Model\UserContextInterface;

/**
 * Class InstallData
 * @package Needsaf\Userroles\Setup
 */
class InstallData implements InstallDataInterface
{

    /**
     * @var \Magento\Authorization\Model\RoleFactory
     */
    private $roleFactory;

    /**
     * @var \Magento\Authorization\Model\RulesFactory
     */
    private $rulesFactory;

    /**
     * InstallData constructor.
     * @param \Magento\Authorization\Model\RoleFactory $roleFactory
     * @param \Magento\Authorization\Model\RulesFactory $rulesFactory
     */
    public function __construct(
        \Magento\Authorization\Model\RoleFactory $roleFactory, /* Instance of Role*/
        \Magento\Authorization\Model\RulesFactory $rulesFactory /* Instance of Rule */
        /*this define that which resource permitted to wich role */
        ){
        $this->roleFactory = $roleFactory;
        $this->rulesFactory = $rulesFactory;
    }
    /**
     * Installs data for a module
     *
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     */
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $role = $this->roleFactory->create();
        /* Change User role name */
        $role->setRoleName('Demo')
            ->setParentId(0)
            ->setRoleType(RoleGroup::ROLE_TYPE)
            ->setUserType(UserContextInterface::USER_TYPE_ADMIN);
        $role->save();
        /* Change user roles */
        $resource=['Magento_Backend::admin',
            'Magento_Sales::sales',
            'Magento_Sales::create',
            'Magento_Sales::actions_view', //you will use resource id which you want tp allow
            'Magento_Sales::cancel'
        ];
        $rules = $this->rulesFactory->create();
        $rules->setRoleId($role->getId())->setResources($resource)->saveRel();
    }
}