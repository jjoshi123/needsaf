<?php
namespace Needsaf\Userroles\Setup;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\ObjectManagerInterface;
use Magento\Authorization\Model\Acl\Role\Group as RoleGroup;
use Magento\Authorization\Model\UserContextInterface;

class UpgradeData implements UpgradeDataInterface
{

    /**
     * @var ObjectManagerInterface
     */
    private $objectManager;

    protected $roleFactory;

    protected $websiteFactory;

    protected $storeFactory;

    public  function __construct(
        ObjectManagerInterface $objectManager,
        \Magento\Authorization\Model\RoleFactory $roleFactory,
        \Magento\Store\Model\WebsiteFactory $websiteFactory,
        \Magento\Store\Model\StoreFactory $storeFactory
    )
    {
        $this->objectManager = $objectManager;
        $this->roleFactory = $roleFactory;
        $this->websiteFactory = $websiteFactory;
        $this->storeFactory = $storeFactory;
    }
    /**
     * Upgrades data for a module
     *
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     */
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();
        if (version_compare($context->getVersion(),'1.0.1','<') )
        {
            $resource = ['Magento_Backend::admin',
                'Magento_Backend::dashboard',
                'Magento_Catalog::catalog',
                'Magento_Backend::stores_attributes',
                'Magento_Catalog::catalog_inventory',
                'Magento_Catalog::products',
                'Magento_User::acl',
                'Magento_User::acl_users',
                'Magento_Backend::content',
                'Needsaf_Membership::membership_content',
                'Needsaf_Commission::commission',
                'Magento_Sales::sales',
                'Magento_Sales::sales_operation',
                'Magento_Sales::sales_order',
                'Magento_Sales::actions',
                'Magento_Sales::create',
                'Magento_Sales::actions_view',
                'Magento_Sales::email',
                'Magento_Sales::reorder',
                'Magento_Sales::actions_edit',
                'Magento_Sales::cancel',
                'Magento_Sales::review_payment',
                'Magento_Sales::capture',
                'Magento_Sales::invoice',
                'Magento_Sales::creditmemo',
                'Magento_Sales::hold',
                'Magento_Sales::unhold',
                'Magento_Sales::ship',
                'Magento_Sales::comment',
                'Magento_Sales::emails',
                'Magento_Sales::sales_invoice',
                'Magento_Sales::shipment',
                'Magento_Sales::sales_creditmemo',
                'Magento_Paypal::billing_agreement',
                'Magento_Paypal::billing_agreement_actions',
                'Magento_Paypal::billing_agreement_actions_view',
                'Magento_Paypal::actions_manage',
                'Magento_Paypal::use',
                'Magento_Sales::transactions',
                'Magento_Sales::transactions_fetch',
                'Magento_Sales::actions_view',
                'Needsaf_Inquiry::inquiry_content',
                'Needsaf_Inquiry::inquiry',
                "Needsaf_Salesgrid::orders",
                "Needsaf_Membership::vendormembershipdetails"
            ];
            $website = $this->websiteFactory->create();
            $website->load('base');
            $webId = $website->getId();
            /*get default id*/
            $default = 'default';
            $store = $this->storeFactory->create();
            $store->load($default,'code');
            $defaultId = $store->getStoreId();
            /*get default id*/
            /*germany roles creation*/
            $role = $this->roleFactory->create();
            $germany = 'germany';
            $store = $this->storeFactory->create();
            $store->load($germany,'code');
            $germanId = $store->getStoreId();
            $germanStoreId = $defaultId.",".$germanId;
            $role->load($germany,'role_name');
            if($role->getRoleId()==NULL) {
                $role->setRoleName($germany);
                $role->setParentId(0);
                $role->setRoleType(RoleGroup::ROLE_TYPE);
                $role->setUserType(UserContextInterface::USER_TYPE_ADMIN);
                $role->setGwsIsAll(0);
                $role->setGwsWebsites($webId);
                $role->setGwsStoreGroups($germanStoreId);
                $role->save();
                $rulesFactory = $this->objectManager->create('\Magento\Authorization\Model\RulesFactory');
                $rules = $rulesFactory->create();
                $rules->setRoleId($role->getId())->setResources($resource)->saveRel();
            }
            $role->save();
            /*germany roles creation*/
            /*french roles creation*/
            $role = $this->roleFactory->create();
            $french = 'french';
            $store = $this->storeFactory->create();
            $store->load($french,'code');
            $frenchId = $store->getStoreId();
            $frenchStoreId = $defaultId.",".$frenchId;
            $role->load($french,'role_name');
            if($role->getRoleId()==NULL) {
                $role->setRoleName($french);
                $role->setParentId(0);
                $role->setRoleType(RoleGroup::ROLE_TYPE);
                $role->setUserType(UserContextInterface::USER_TYPE_ADMIN);
                $role->setGwsIsAll(0);
                $role->setGwsWebsites($webId);
                $role->setGwsStoreGroups($frenchStoreId);
                $role->save();
                $rulesFactory = $this->objectManager->create('\Magento\Authorization\Model\RulesFactory');
                $rules = $rulesFactory->create();
                $rules->setRoleId($role->getId())->setResources($resource)->saveRel();
            }
            $role->save();
            /*french roles creation*/
            /*italian roles creation*/
            $role = $this->roleFactory->create();
            $italian = 'italian';
            $store = $this->storeFactory->create();
            $store->load($italian,'code');
            $italianId = $store->getStoreId();
            $italianIdStoreId = $defaultId.",".$italianId;
            $role->load($italian,'role_name');
            if($role->getRoleId()==NULL) {
                $role->setRoleName($italian);
                $role->setParentId(0);
                $role->setRoleType(RoleGroup::ROLE_TYPE);
                $role->setUserType(UserContextInterface::USER_TYPE_ADMIN);
                $role->setGwsIsAll(0);
                $role->setGwsWebsites($webId);
                $role->setGwsStoreGroups($italianIdStoreId);
                $role->save();
                $rulesFactory = $this->objectManager->create('\Magento\Authorization\Model\RulesFactory');
                $rules = $rulesFactory->create();
                $rules->setRoleId($role->getId())->setResources($resource)->saveRel();
            }
            $role->save();
            /*italian roles creation*/
            /*premium roles creation*/
            $role = $this->roleFactory->create();
            $premium = 'Premium';
            $premiumStoreId = $defaultId.",".$germanId.",".$frenchId.",".$italianId;
            $role->load($premium,'role_name');
            if($role->getRoleId()==NULL) {
                $role->setRoleName($premium);
                $role->setParentId(0);
                $role->setRoleType(RoleGroup::ROLE_TYPE);
                $role->setUserType(UserContextInterface::USER_TYPE_ADMIN);
                $role->setGwsIsAll(0);
                $role->setGwsWebsites($webId);
                $role->setGwsStoreGroups($premiumStoreId);
                $role->save();
                $rulesFactory = $this->objectManager->create('\Magento\Authorization\Model\RulesFactory');
                $rules = $rulesFactory->create();
                $rules->setRoleId($role->getId())->setResources($resource)->saveRel();
            }
            $role->save();
            /*premium roles creation*/
        }
    }
}