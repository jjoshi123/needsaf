<?php


namespace Needsaf\CreateRegions\Setup;


use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Store\Model\ResourceModel\Website;
use Magento\Store\Model\WebsiteFactory;
use Magento\Store\Model\StoreFactory;
use Magento\Store\Model\ResourceModel\Group;
use Magento\Store\Model\ResourceModel\Store;
use Magento\Store\Model\GroupFactory;
use Magento\Framework\Event\ManagerInterface;


class InstallData implements InstallDataInterface
{

    /**
     * @var WebsiteFactory
     */
    private $websiteFactory;

    /**
     * @var Website
     */
    private $websiteResourceModel;

    /**
     * @var StoreFactory
     */
    private $storeFactory;
    /**
     * @var GroupFactory
     */
    private $groupFactory;
    /**
     * @var Group
     */
    private $groupResourceModel;
    /**
     * @var Store
     */
    private $storeResourceModel;
    /**
     * @var ManagerInterface
     */
    private $eventManager;

    public function __construct(
        WebsiteFactory $websiteFactory,
        Website $websiteResourceModel,
        Store $storeResourceModel,
        Group $groupResourceModel,
        StoreFactory $storeFactory,
        GroupFactory $groupFactory,
        ManagerInterface $eventManager
    )
    {
        $this->websiteFactory = $websiteFactory;
        $this->websiteResourceModel = $websiteResourceModel;
        $this->storeFactory = $storeFactory;
        $this->groupFactory = $groupFactory;
        $this->groupResourceModel = $groupResourceModel;
        $this->storeResourceModel = $storeResourceModel;
        $this->eventManager = $eventManager;
    }

    /**
     * Installs data for a module
     *
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     */
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        /** @var \Magento\Store\Model\Website $website */
        $website = $this->websiteFactory->create();
        $website->load('base');
//        if(!$website->getId()){
//            $website->setCode('needsaf_dech');
//            $website->setName('Needsaf De Ch');
//            $website->setDefaultGroupId(3);
//            $this->websiteResourceModel->save($website);
//        }

        /*region one*/
        if($website->getId()){
            /** @var \Magento\Store\Model\Group $group */
            $group = $this->groupFactory->create();
            $group->setWebsiteId($website->getWebsiteId());
            $group->setName('Germany');
            $group->setRootCategoryId(2);
            $group->setDefaultStoreId(3);
            $this->groupResourceModel->save($group);
        }


        /** @var  \Magento\Store\Model\Store $store */
        $store = $this->storeFactory->create();
        $store->load('germany');
        if(!$store->getId()){
            $group = $this->groupFactory->create();
            $group->load('Germany', 'name');
            $store->setCode('germany');
            $store->setName('Germany View');
            $store->setWebsite($website);
            $store->setGroupId($group->getId());
            $store->setData('is_active','1');
            $this->storeResourceModel->save($store);
            // Trigger event to insert some data to the sales_sequence_meta table (fix bug place order in checkout)
            $this->eventManager->dispatch('store_add', ['store' => $store]);
        }
        /** @var \Magento\Store\Model\Website $website */
        /*region one end */
        /*region two*/
        if($website->getId()){
            /** @var \Magento\Store\Model\Group $group */
            $group = $this->groupFactory->create();
            $group->setWebsiteId($website->getWebsiteId());
            $group->setName('French');
            $group->setRootCategoryId(2);
            $group->setDefaultStoreId(3);
            $this->groupResourceModel->save($group);
        }


        /** @var  \Magento\Store\Model\Store $store */
        $store = $this->storeFactory->create();
        $store->load('french');
        if(!$store->getId()){
            $group = $this->groupFactory->create();
            $group->load('French', 'name');
            $store->setCode('french');
            $store->setName('French View');
            $store->setWebsite($website);
            $store->setGroupId($group->getId());
            $store->setData('is_active','1');
            $this->storeResourceModel->save($store);
            // Trigger event to insert some data to the sales_sequence_meta table (fix bug place order in checkout)
            $this->eventManager->dispatch('store_add', ['store' => $store]);
        }
        /** @var \Magento\Store\Model\Website $website */
        /*region two end */
        /*region three*/
        if($website->getId()){
            /** @var \Magento\Store\Model\Group $group */
            $group = $this->groupFactory->create();
            $group->setWebsiteId($website->getWebsiteId());
            $group->setName('Italian');
            $group->setRootCategoryId(2);
            $group->setDefaultStoreId(3);
            $this->groupResourceModel->save($group);
        }


        /** @var  \Magento\Store\Model\Store $store */
        $store = $this->storeFactory->create();
        $store->load('italian');
        if(!$store->getId()){
            $group = $this->groupFactory->create();
            $group->load('Italian', 'name');
            $store->setCode('italian');
            $store->setName('Italian View');
            $store->setWebsite($website);
            $store->setGroupId($group->getId());
            $store->setData('is_active','1');
            $this->storeResourceModel->save($store);
            // Trigger event to insert some data to the sales_sequence_meta table (fix bug place order in checkout)
            $this->eventManager->dispatch('store_add', ['store' => $store]);
        }
        /** @var \Magento\Store\Model\Website $website */
        /*region three end */
        $setup->endSetup();
    }
}