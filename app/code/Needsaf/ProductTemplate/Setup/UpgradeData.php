<?php
namespace Needsaf\ProductTemplate\Setup;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\ObjectManagerInterface;
use Magento\Authorization\Model\Acl\Role\Group as RoleGroup;
use Magento\Authorization\Model\UserContextInterface;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Eav\Api\AttributeSetManagementInterface;
use Magento\Eav\Api\Data\AttributeSetInterface;
use Magento\Catalog\Api\ProductAttributeRepositoryInterface;
use Magento\Catalog\Api\ProductAttributeManagementInterface;
use Magento\Eav\Api\Data\AttributeGroupInterface;
use Magento\Catalog\Api\ProductAttributeGroupRepositoryInterface;
use Magento\Catalog\Api\Data\ProductAttributeInterface;

class UpgradeData implements UpgradeDataInterface
{

    private $eavSetup;

    /**
     * EAV setup factory
     *
     * @var EavSetupFactory
     */
    protected $eavSetupFactory;

    /**
     * @var ObjectManagerInterface
     */
    protected $objectManager;

    /**
     * @var AttributeSetManagementInterface
     */
    protected $attributeSetManagement;

    /**
     * @var ProductAttributeRepositoryInterface
     */
    protected $attributeRepository;

    /**
     * @var AttributeGroupInterface
     */
    protected $attributeGroup;

    /**
     * @var ProductAttributeGroupRepositoryInterface
     */
    protected $attributeGroupRepository;

    /**
     * @var ProductAttributeManagementInterface
     */
    protected $productAttributeManagement;
    /**
     * @var array
     */
    protected $attributeValues;

    /**
     * @var \Magento\Eav\Model\Entity\Attribute\Source\TableFactory
     */
    protected $tableFactory;

    /**
     * @var \Magento\Eav\Api\AttributeOptionManagementInterface
     */
    protected $attributeOptionManagement;
    /**
     * @var \Magento\Eav\Api\Data\AttributeOptionLabelInterfaceFactory
     */
    protected $optionLabelFactory;

    /**
     * @var \Magento\Eav\Api\Data\AttributeOptionInterfaceFactory
     */
    protected $optionFactory;

    protected $_attributeSetCollection;

    protected $_groupCollection;

    public function __construct(
        EavSetupFactory $eavSetupFactory,
        AttributeSetManagementInterface $attributeSetManagement,
        ObjectManagerInterface $objectManager,
        ProductAttributeRepositoryInterface $attributeRepository,
        ProductAttributeManagementInterface $productAttributeManagement,
        AttributeGroupInterface $attributeGroup,
        \Magento\Eav\Model\Entity\Attribute\Source\TableFactory $tableFactory,
        \Magento\Eav\Api\AttributeOptionManagementInterface $attributeOptionManagement,
        \Magento\Eav\Api\Data\AttributeOptionLabelInterfaceFactory $optionLabelFactory,
        \Magento\Eav\Api\Data\AttributeOptionInterfaceFactory $optionFactory,
        ProductAttributeGroupRepositoryInterface $attributeGroupRepository,
        \Magento\Eav\Model\ResourceModel\Entity\Attribute\Set\CollectionFactory $attributeSetCollection,
        \Magento\Eav\Model\ResourceModel\Entity\Attribute\Group\CollectionFactory $groupCollection
    )
    {
        $this->eavSetupFactory = $eavSetupFactory;
        $this->attributeSetManagement = $attributeSetManagement;
        $this->objectManager = $objectManager;
        $this->attributeRepository = $attributeRepository;
        $this->productAttributeManagement = $productAttributeManagement;
        $this->attributeGroup = $attributeGroup;
        $this->attributeGroupRepository = $attributeGroupRepository;
        $this->tableFactory = $tableFactory;
        $this->attributeOptionManagement = $attributeOptionManagement;
        $this->optionLabelFactory = $optionLabelFactory;
        $this->optionFactory = $optionFactory;
        $this->_attributeSetCollection = $attributeSetCollection;
        $this->_groupCollection = $groupCollection;
    }

    /**
     * Upgrades data for a module
     *
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     */
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();
        $this->eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
        $setup->startSetup();
        if ($context->getVersion()
            && version_compare($context->getVersion(), '1.0.1') < 0
        ) {
            $attributeSet = $this->_attributeSetCollection->create()->addFieldToSelect(
                '*'
            )->addFieldToFilter(
                'attribute_set_name',
                'Needsaf'
            );
            $attributeSetId = 0;
            foreach($attributeSet as $attr):
                $attributeSetId = $attr->getAttributeSetId();
            endforeach;

            $this->createAttribute(
                [
                    'type' => 'text',
                    'label' => 'Regions',
                    'code' => 'regions',
                    'required' => false,
                    'input' => 'multiselect',
                    'frontend_class' => ''
                ],
                $attributeSetId,
                $this->getGroup()
            );
            $this->createOrGetId('regions','default');
            $this->createOrGetId('regions','germany');
            $this->createOrGetId('regions','french');
            $this->createOrGetId('regions','italian');
            $this->createAttribute(
                [
                    'type' => 'int',
                    'label' => 'Installation fix cost or cost per hour',
                    'code' => 'installation_cost',
                    'required' => false,
                    'input' => 'text',
                    'frontend_class' => 'validate-number'
                ],
                $attributeSetId,
                $this->getGroup()
            );
            $this->createAttribute(
                [
                    'type' => 'int',
                    'label' => '24/7 ARC-Monitoring',
                    'code' => 'arc_monitoring',
                    'required' => false,
                    'input' => 'text',
                    'frontend_class' => 'validate-number'
                ],
                $attributeSetId,
                $this->getGroup()
            );
        }
        if ($context->getVersion()
            && version_compare($context->getVersion(), '1.0.2') < 0
        ){
            $attributeSet = $this->_attributeSetCollection->create()->addFieldToSelect(
                '*'
            )->addFieldToFilter(
                'attribute_set_name',
                'Needsaf'
            );
            $attributeSetId = 0;
            foreach($attributeSet as $attr):
                $attributeSetId = $attr->getAttributeSetId();
            endforeach;
            $this->createAttribute(
                [
                    'type' => 'int',
                    'label' => '24/7 ARC-Monitoring cost per month',
                    'code' => 'arc_monitoring',
                    'required' => false,
                    'input' => 'text',
                    'frontend_class' => 'validate-number'
                ],
                $attributeSetId,
                $this->getGroup()
            );
        }
    }
       public function getGroup()
        {
            $group = $this->_groupCollection->create();
            $group->addFieldToFilter('attribute_group_name','Needsaf');
           $data = $group->getData();
           foreach ($data as $attribute)
           {
               $attributeGroupId = $attribute['attribute_group_id'];
           }
           return $attributeGroupId;
        }


        public function createAttributeSet($name)
        {
            $attributeSet = $this->objectManager->create(AttributeSetInterface::class)->setAttributeSetName($name);
            return $this->attributeSetManagement->create(
                ProductAttributeInterface::ENTITY_TYPE_CODE,
                $attributeSet,
                $this->eavSetup->getDefaultAttributeSetId(ProductAttributeInterface::ENTITY_TYPE_CODE)
            );

        }


        public function createAttribute($data, $attributeSetId, $groupId)
        {
            $existingAttribute = $this->eavSetup->getAttribute(ProductAttributeInterface::ENTITY_TYPE_CODE, $data['code']);

            if ($existingAttribute && isset($existingAttribute['attribute_id'])) {
                $this->attributeRepository->deleteById($data['code']);
            }

            /* @var ProductAttributeInterface $attribute */
            $attribute = $this->objectManager->create(ProductAttributeInterface::class);
            $attribute->setBackendType($data['type'])
                ->setDefaultFrontendLabel($data['label'])
                ->setAttributeCode($data['code'])
                ->setIsRequired($data['required'])
                ->setIsUserDefined(true)
                ->setIsVisible(true)
                ->setScope(ProductAttributeInterface::SCOPE_GLOBAL_TEXT)
                ->setFrontendInput($data['input'])
                ->setIsVisibleOnFront(true)
                ->setFrontendClass($data['frontend_class']);
            $this->attributeRepository->save($attribute);

            $this->productAttributeManagement->assign(
                $attributeSetId,
                $groupId,
                $attribute->getAttributeCode(),
                0
            );
        }

        /**
         * Get attribute by code.
         *
         * @param string $attributeCode
         * @return \Magento\Catalog\Api\Data\ProductAttributeInterface
         */
        public function getAttribute($attributeCode)
        {
            return $this->attributeRepository->get($attributeCode);
        }
    public function createOrGetId($attributeCode, $label)
    {
        if (strlen($label) < 1) {
            throw new \Magento\Framework\Exception\LocalizedException(
                __('Label for %1 must not be empty.', $attributeCode)
            );
        }

        // Does it already exist?
        $optionId = $this->getOptionId($attributeCode, $label);

        if (!$optionId) {
            // If no, add it.

            /** @var \Magento\Eav\Model\Entity\Attribute\OptionLabel $optionLabel */
            $optionLabel = $this->optionLabelFactory->create();
            $optionLabel->setStoreId(0);
            $optionLabel->setLabel($label);

            $option = $this->optionFactory->create();
            $option->setLabel($optionLabel);
            $option->setStoreLabels([$optionLabel]);
            $option->setSortOrder(0);
            $option->setIsDefault(false);

            $this->attributeOptionManagement->add(
                \Magento\Catalog\Model\Product::ENTITY,
                $this->getAttribute($attributeCode)->getAttributeId(),
                $option
            );

            // Get the inserted ID. Should be returned from the installer, but it isn't.
            $optionId = $this->getOptionId($attributeCode, $label, true);
        }

        return $optionId;
    }
    public function getOptionId($attributeCode, $label, $force = false)
    {
        /** @var \Magento\Catalog\Model\ResourceModel\Eav\Attribute $attribute */
        $attribute = $this->getAttribute($attributeCode);

        // Build option array if necessary
        if ($force === true || !isset($this->attributeValues[ $attribute->getAttributeId() ])) {
            $this->attributeValues[ $attribute->getAttributeId() ] = [];

            // We have to generate a new sourceModel instance each time through to prevent it from
            // referencing its _options cache. No other way to get it to pick up newly-added values.

            /** @var \Magento\Eav\Model\Entity\Attribute\Source\Table $sourceModel */
            $sourceModel = $this->tableFactory->create();
            $sourceModel->setAttribute($attribute);

            foreach ($sourceModel->getAllOptions() as $option) {
                $this->attributeValues[ $attribute->getAttributeId() ][ $option['label'] ] = $option['value'];
            }
        }

        // Return option ID if exists
        if (isset($this->attributeValues[ $attribute->getAttributeId() ][ $label ])) {
            return $this->attributeValues[ $attribute->getAttributeId() ][ $label ];
        }

        // Return false if does not exist
        return false;
    }
    }

