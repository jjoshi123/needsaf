<?php

namespace Needsaf\ProductTemplate\Setup;


use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Catalog\Api\Data\ProductAttributeInterface;
use Magento\Framework\ObjectManagerInterface;
use Magento\Eav\Setup\EavSetup;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Eav\Api\AttributeSetManagementInterface;
use Magento\Eav\Api\Data\AttributeSetInterface;
use Magento\Catalog\Api\ProductAttributeRepositoryInterface;
use Magento\Catalog\Api\ProductAttributeManagementInterface;
use Magento\Eav\Api\Data\AttributeGroupInterface;
use Magento\Catalog\Api\ProductAttributeGroupRepositoryInterface;


class InstallData implements InstallDataInterface
{
    private $eavSetup;

    /**
     * EAV setup factory
     *
     * @var EavSetupFactory
     */
    protected $eavSetupFactory;

    /**
     * @var ObjectManagerInterface
     */
    protected $objectManager;

    /**
     * @var AttributeSetManagementInterface
     */
    protected $attributeSetManagement;

    /**
     * @var ProductAttributeRepositoryInterface
     */
    protected $attributeRepository;

    /**
     * @var AttributeGroupInterface
     */
    protected $attributeGroup;

    /**
     * @var ProductAttributeGroupRepositoryInterface
     */
    protected $attributeGroupRepository;

    /**
     * @var ProductAttributeManagementInterface
     */
    protected $productAttributeManagement;
    /**
     * @var array
     */
    protected $attributeValues;

    /**
     * @var \Magento\Eav\Model\Entity\Attribute\Source\TableFactory
     */
    protected $tableFactory;

    /**
     * @var \Magento\Eav\Api\AttributeOptionManagementInterface
     */
    protected $attributeOptionManagement;
    /**
     * @var \Magento\Eav\Api\Data\AttributeOptionLabelInterfaceFactory
     */
    protected $optionLabelFactory;

    /**
     * @var \Magento\Eav\Api\Data\AttributeOptionInterfaceFactory
     */
    protected $optionFactory;


    public function __construct(
        EavSetupFactory $eavSetupFactory,
        AttributeSetManagementInterface $attributeSetManagement,
        ObjectManagerInterface $objectManager,
        ProductAttributeRepositoryInterface $attributeRepository,
        ProductAttributeManagementInterface $productAttributeManagement,
        AttributeGroupInterface $attributeGroup,
        \Magento\Eav\Model\Entity\Attribute\Source\TableFactory $tableFactory,
        \Magento\Eav\Api\AttributeOptionManagementInterface $attributeOptionManagement,
        \Magento\Eav\Api\Data\AttributeOptionLabelInterfaceFactory $optionLabelFactory,
        \Magento\Eav\Api\Data\AttributeOptionInterfaceFactory $optionFactory,
        ProductAttributeGroupRepositoryInterface $attributeGroupRepository
    )
    {
        $this->eavSetupFactory = $eavSetupFactory;
        $this->attributeSetManagement = $attributeSetManagement;
        $this->objectManager = $objectManager;
        $this->attributeRepository = $attributeRepository;
        $this->productAttributeManagement = $productAttributeManagement;
        $this->attributeGroup = $attributeGroup;
        $this->attributeGroupRepository = $attributeGroupRepository;
        $this->tableFactory = $tableFactory;
        $this->attributeOptionManagement = $attributeOptionManagement;
        $this->optionLabelFactory = $optionLabelFactory;
        $this->optionFactory = $optionFactory;
    }

    /**
     * Installs data for a module
     *
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     */
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        $this->eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
        $needsafAttributeSet = $this->createAttributeSet('Needsaf');
        $needsafGroup = $this->createGroup('Needsaf',$needsafAttributeSet->getAttributeSetId());
        $this->createAttribute(
            [
                'type' => 'int',
                'label' => 'Entry Sensor',
                'code' => 'entry_sensor',
                'required' => false,
                'input' => 'text',
                'frontend_class' => 'validate-number'
            ],
            $needsafAttributeSet->getAttributeSetId(),
            $needsafGroup->getAttributeGroupId()
        );

        $this->createAttribute(
            [
                'type' => 'int',
                'label' => 'Motion Sensor',
                'code' => 'motion_sensor',
                'required' => false,
                'input' => 'text',
                'frontend_class' => 'validate-number'
            ],
            $needsafAttributeSet->getAttributeSetId(),
            $needsafGroup->getAttributeGroupId()
        );
        $this->createAttribute(
            [
                'type' => 'int',
                'label' => 'Motion Sensor Video',
                'code' => 'motion_sensor_video',
                'required' => false,
                'input' => 'text',
                'frontend_class' => 'validate-number'
            ],
            $needsafAttributeSet->getAttributeSetId(),
            $needsafGroup->getAttributeGroupId()
        );
        $this->createAttribute(
            [
                'type' => 'int',
                'label' => 'Keychain Remote',
                'code' => 'keychain_remote',
                'required' => false,
                'input' => 'text',
                'frontend_class' => 'validate-number'
            ],
            $needsafAttributeSet->getAttributeSetId(),
            $needsafGroup->getAttributeGroupId()
        );


        $this->createAttribute(
            [
                'type' => 'text',
                'label' => 'Panel',
                'code' => 'panel',
                'required' => false,
                'input' => 'select',
                'frontend_class' => ''
            ],
            $needsafAttributeSet->getAttributeSetId(),
            $needsafGroup->getAttributeGroupId()
        );

        $this->createOrGetId('panel','Inclusive');
        $this->createOrGetId('panel','Exclusive');

        $this->createAttribute(
            [
                'type' => 'text',
                'label' => 'Keypad',
                'code' => 'keypad',
                'required' => false,
                'input' => 'select',
                'frontend_class' => ''
            ],
            $needsafAttributeSet->getAttributeSetId(),
            $needsafGroup->getAttributeGroupId()
        );

        $this->createOrGetId('keypad','Inclusive');
        $this->createOrGetId('keypad','Exclusive');

        $this->createAttribute(
            [
                'type' => 'text',
                'label' => 'Auxiliary Siren',
                'code' => 'auxiliary_siren',
                'required' => false,
                'input' => 'select',
                'frontend_class' => ''
            ],
            $needsafAttributeSet->getAttributeSetId(),
            $needsafGroup->getAttributeGroupId()
        );

        $this->createOrGetId('auxiliary_siren','Inclusive');
        $this->createOrGetId('auxiliary_siren','Exclusive');

        $this->createAttribute(
            [
                'type' => 'text',
                'label' => 'Glassbreak Sensor',
                'code' => 'glassbreak_sensor',
                'required' => false,
                'input' => 'select',
                'frontend_class' => ''
            ],
            $needsafAttributeSet->getAttributeSetId(),
            $needsafGroup->getAttributeGroupId()
        );

        $this->createOrGetId('glassbreak_sensor','Inclusive');
        $this->createOrGetId('glassbreak_sensor','Exclusive');

        $this->createAttribute(
            [
                'type' => 'text',
                'label' => 'Smoke Alarm',
                'code' => 'smoke_alarm',
                'required' => false,
                'input' => 'select',
                'frontend_class' => ''
            ],
            $needsafAttributeSet->getAttributeSetId(),
            $needsafGroup->getAttributeGroupId()
        );

        $this->createOrGetId('smoke_alarm','Inclusive');
        $this->createOrGetId('smoke_alarm','Exclusive');

        $this->createAttribute(
            [
                'type' => 'text',
                'label' => 'Panic Button',
                'code' => 'panic_button',
                'required' => false,
                'input' => 'select',
                'frontend_class' => ''
            ],
            $needsafAttributeSet->getAttributeSetId(),
            $needsafGroup->getAttributeGroupId()
        );

        $this->createOrGetId('panic_button','Inclusive');
        $this->createOrGetId('panic_button','Exclusive');

       $setup->endSetup();

    }

    public function createGroup($name, $attributeSetId)
    {
        $group = $this->attributeGroup
            ->setAttributeGroupName($name)
            ->setAttributeSetId($attributeSetId);
        return $this->attributeGroupRepository->save($group);
    }

    private function createAttributeSet($name)
    {
        $attributeSet = $this->objectManager->create(AttributeSetInterface::class)->setAttributeSetName($name);
        return $this->attributeSetManagement->create(
            ProductAttributeInterface::ENTITY_TYPE_CODE,
            $attributeSet,
            $this->eavSetup->getDefaultAttributeSetId(ProductAttributeInterface::ENTITY_TYPE_CODE)
        );

    }

    private function createAttribute($data, $attributeSetId,$groupId )
    {
        $existingAttribute = $this->eavSetup->getAttribute(ProductAttributeInterface::ENTITY_TYPE_CODE, $data['code']);

        if ($existingAttribute && isset($existingAttribute['attribute_id'])) {
            $this->attributeRepository->deleteById($data['code']);
        }

        /* @var ProductAttributeInterface $attribute */
        $attribute = $this->objectManager->create(ProductAttributeInterface::class);
        $attribute->setBackendType($data['type'])
            ->setDefaultFrontendLabel($data['label'])
            ->setAttributeCode($data['code'])
            ->setIsRequired($data['required'])
            ->setIsUserDefined(true)
            ->setIsVisible(true)
            ->setScope(ProductAttributeInterface::SCOPE_GLOBAL_TEXT)
            ->setFrontendInput($data['input'])
            ->setIsVisibleOnFront(true)
            ->setFrontendClass($data['frontend_class']);
        $this->attributeRepository->save($attribute);

        $this->productAttributeManagement->assign(
            $attributeSetId,
            $groupId,
            $attribute->getAttributeCode(),
            0
        );
    }
    /**
     * Get attribute by code.
     *
     * @param string $attributeCode
     * @return \Magento\Catalog\Api\Data\ProductAttributeInterface
     */
    public function getAttribute($attributeCode)
    {
        return $this->attributeRepository->get($attributeCode);
    }
    /**
     * Find or create a matching attribute option
     *
     * @param string $attributeCode Attribute the option should exist in
     * @param string $label Label to find or add
     * @return int
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function createOrGetId($attributeCode, $label)
    {
        if (strlen($label) < 1) {
            throw new \Magento\Framework\Exception\LocalizedException(
                __('Label for %1 must not be empty.', $attributeCode)
            );
        }

        // Does it already exist?
        $optionId = $this->getOptionId($attributeCode, $label);

        if (!$optionId) {
            // If no, add it.

            /** @var \Magento\Eav\Model\Entity\Attribute\OptionLabel $optionLabel */
            $optionLabel = $this->optionLabelFactory->create();
            $optionLabel->setStoreId(0);
            $optionLabel->setLabel($label);

            $option = $this->optionFactory->create();
            $option->setLabel($optionLabel);
            $option->setStoreLabels([$optionLabel]);
            $option->setSortOrder(0);
            $option->setIsDefault(false);

            $this->attributeOptionManagement->add(
                \Magento\Catalog\Model\Product::ENTITY,
                $this->getAttribute($attributeCode)->getAttributeId(),
                $option
            );

            // Get the inserted ID. Should be returned from the installer, but it isn't.
            $optionId = $this->getOptionId($attributeCode, $label, true);
        }

        return $optionId;
    }

    /**
     * Find the ID of an option matching $label, if any.
     *
     * @param string $attributeCode Attribute code
     * @param string $label Label to find
     * @param bool $force If true, will fetch the options even if they're already cached.
     * @return int|false
     */
    public function getOptionId($attributeCode, $label, $force = false)
    {
        /** @var \Magento\Catalog\Model\ResourceModel\Eav\Attribute $attribute */
        $attribute = $this->getAttribute($attributeCode);

        // Build option array if necessary
        if ($force === true || !isset($this->attributeValues[ $attribute->getAttributeId() ])) {
            $this->attributeValues[ $attribute->getAttributeId() ] = [];

            // We have to generate a new sourceModel instance each time through to prevent it from
            // referencing its _options cache. No other way to get it to pick up newly-added values.

            /** @var \Magento\Eav\Model\Entity\Attribute\Source\Table $sourceModel */
            $sourceModel = $this->tableFactory->create();
            $sourceModel->setAttribute($attribute);

            foreach ($sourceModel->getAllOptions() as $option) {
                $this->attributeValues[ $attribute->getAttributeId() ][ $option['label'] ] = $option['value'];
            }
        }

        // Return option ID if exists
        if (isset($this->attributeValues[ $attribute->getAttributeId() ][ $label ])) {
            return $this->attributeValues[ $attribute->getAttributeId() ][ $label ];
        }

        // Return false if does not exist
        return false;
    }
}