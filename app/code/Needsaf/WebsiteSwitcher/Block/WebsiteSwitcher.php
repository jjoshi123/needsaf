<?php


namespace Needsaf\WebsiteSwitcher\Block;


use Magento\Framework\View\Element\Template;

class WebsiteSwitcher extends \Magento\Framework\View\Element\Template
{


    public function __construct(Template\Context $context,
                                array $data = [])
    {
        parent::__construct($context, $data);
    }

    public function getWebsites()
    {
        $_websites = $this->_storeManager->getWebsites();
        $_websiteData = array();
        foreach($_websites as $website){
            foreach($website->getStores() as $store){
                $storeObj = $this->_storeManager->getStore($store);
                $name = $website->getName();
                $url = $storeObj->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_WEB);
                array_push($_websiteData, array('name' => $name,'url' => $url));
            }
        }

        return $_websiteData;
    }
}