<?php

namespace Needsaf\Salesgrid\Controller\Adminhtml;


/**
 * Class Testimonial
 * @package Panda\Testimonial\Controller\Adminhtml
 */
class Orders extends Actions
{
    /**
     * Form session key
     * @var string
     */
    protected $_formSessionKey = 'needsaf_orders_form_data';

    /**
     * Allowed Key
     * @var string
     */
    protected $_allowedKey = 'Needsaf_Salesgrid::orders';

    /**
     * Model class name
     * @var string
     */
   // protected $_modelClass = 'Panda\Testimonial\Model\Testimonial';

    /**
     * Active menu key
     * @var string
     */
    protected $_activeMenu = 'Needsaf_Salesgrid:orders';

    /**
     * Status field name
     * @var string
     */
    protected $_statusField = 'is_active';

    /**
     * Save request params key
     * @var string
     */
    protected $_paramsHolder = 'post';
}
