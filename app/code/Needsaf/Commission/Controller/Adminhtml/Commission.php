<?php

namespace Needsaf\Commission\Controller\Adminhtml;


/**
 * Class Testimonial
 * @package Panda\Testimonial\Controller\Adminhtml
 */
class Commission extends Actions
{
    /**
     * Form session key
     * @var string
     */
    protected $_formSessionKey = 'panda_testimonial_form_data';

    /**
     * Allowed Key
     * @var string
     */
    protected $_allowedKey = 'Panda_Testimonial::testimonial';

    /**
     * Model class name
     * @var string
     */
   // protected $_modelClass = 'Panda\Testimonial\Model\Testimonial';

    /**
     * Active menu key
     * @var string
     */
    protected $_activeMenu = 'Needsaf_Commission:commission';

    /**
     * Status field name
     * @var string
     */
    protected $_statusField = 'is_active';

    /**
     * Save request params key
     * @var string
     */
    protected $_paramsHolder = 'post';
}
