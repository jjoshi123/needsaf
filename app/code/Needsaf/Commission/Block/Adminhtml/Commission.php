<?php

namespace Needsaf\Commission\Block\Adminhtml;

/**
 * Class Testimonial
 * @package Panda\Testimonial\Block\Adminhtml
 */
class Commission extends \Magento\Backend\Block\Widget\Grid\Container
{
    /**
     * Constructor
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_blockGroup = 'Needsaf_Commission';
        $this->_controller = 'adminhtml';
        $this->_headerText = __('Commission');
        parent::_construct();
        $this->removeButton('add');
    }
}
