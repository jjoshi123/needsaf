<?php
/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Needsaf\Sales\Block\Adminhtml\Order;

/**
 * Adminhtml order totals block
 *
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Totals extends \Magento\Sales\Block\Adminhtml\Order\Totals//\Magento\Sales\Block\Adminhtml\Order\AbstractOrder
{
    /**
     * Initialize order totals array
     *
     * @return $this
     */

    protected $_adminHelper;
    protected $orderFactory;

    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Sales\Helper\Admin $adminHelper
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Sales\Helper\Admin $adminHelper,
        \Magento\Sales\Model\OrderFactory $orderFactory,
        array $data = []
    ) {
        $this->_adminHelper = $adminHelper;
        $this->orderFactory = $orderFactory;
        parent::__construct($context, $registry,$adminHelper, $data);
    }

        public function countCommission()
    {
        $orderId = $this->getRequest()->getParam('order_id');
        $orderData = $this->orderFactory->create()->load($orderId);
        $commissionValue = $orderData->getOrderCommission();
        return $commissionValue;
    }

    public function dueAmount()
    {
        $orderId = $this->getRequest()->getParam('order_id');
        $orderData = $this->orderFactory->create()->load($orderId);
        $paidValue = $orderData->getAmountPaid();
        return $paidValue;
    }

}
