<?php
/**
 * @category    Magento
 * @package     Magento_Sales
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Needsaf\Sales\Block\Adminhtml\Order;

/**
 * Adminhtml sales order view
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class View extends \Magento\Sales\Block\Adminhtml\Order\View
{
    /**
     * Return back url for view grid
     *
     * @return string
     */
    public function getBackUrl()
    {

        if(!$this->getRequest()->getParam('id')){
            if ($this->getOrder() && $this->getOrder()->getBackUrl()) {
                return $this->getOrder()->getBackUrl();
            }

            return $this->getUrl('sales/*/');
        }
        else
        {
            return $this->getUrl('commission/commission/index');
        }
    }


}
