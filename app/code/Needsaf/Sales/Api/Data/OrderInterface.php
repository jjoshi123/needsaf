<?php
namespace Needsaf\Sales\Api\Data;

/**
 * Order interface.
 *
 * An order is a document that a web store issues to a customer. Magento generates a sales order that lists the product
 * items, billing and shipping addresses, and shipping and payment methods. A corresponding external document, known as
 * a purchase order, is emailed to the customer.
 * @api
 */
interface OrderInterface extends \Magento\Sales\Api\Data\OrderInterface
{
    const ORDER_COMMISSION = 'order_commission';
    const PRODUCT_ID = 'product_id';
    const PRODUCT_NAME = 'product_name';
    const VENDOR_ID = 'vendor_id';
    const VENDOR_NAME = 'vendor_name';
    const AMOUNT_PAID = 'amount_paid';
    const BILLING_NAME = 'billing_name';

    public function getOrderCommission();

    public function getProductId();

    public function getProductName();

    public function getVendorId();

    public function getVendorName();

    public function getAmountPaid();

    public function getBillingName();

    public function setOrderCommission($orderCommission);

    public function setProductId($productId);

    public function setProductName($productName);

    public function setVendorId($vendorId);

    public function setVendorName($vendorName);

    public function setAmountPaid($amountPaid);

    public function setBillingName($billingName);

   }
