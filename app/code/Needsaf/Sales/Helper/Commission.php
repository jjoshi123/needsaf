<?php

namespace Needsaf\Sales\Helper;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\ScopeInterface;

class Commission extends \Magento\Framework\App\Helper\AbstractHelper
{

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
//    protected $_scopeConfig;

    /**
     * Commission constructor.
     *
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context
    )
    {
//        $this->_scopeConfig = $scopeConfig;
        parent::__construct($context);
    }

    /**
     * Calculate commission for given total value
     *
     * @param $total
     * @return float;
     */
    public function calculateCommission($total)
    {
        $commissionValue = (int)$this->scopeConfig->getValue('needsaf_category/needsafCommission/commission_amount', ScopeInterface::SCOPE_STORE);
        return floatval(($commissionValue / 100 ) * $total);
    }

}
