<?php
namespace Needsaf\Sales\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;

/**
 * Class InstallSchema
 * @package Needsaf\Membership\Setup
 */
class InstallSchema implements InstallSchemaInterface
{
    /**
     * Installs DB schema for a module
     *
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;

        $installer->startSetup();


        $tableName = $installer->getTable('sales_order');
        $connection = $installer->getConnection();
        $connection->addColumn(
            $tableName,
            'order_commission',
            ['type' => Table::TYPE_TEXT, 'nullable' => false, 'length' => 255 ,'comment' => 'Order Commission']
        );
        $connection->addColumn(
            $tableName,
            'product_id',
            ['type' => Table::TYPE_TEXT, 'nullable' => false, 'length' => 255 ,'comment' => 'Product Id']
        );
        $connection->addColumn(
            $tableName,
            'product_name',
            ['type' => Table::TYPE_TEXT, 'nullable' => false, 'length' => 255 ,'comment' => 'Product Name']
        );
        $connection->addColumn(
            $tableName,
            'vendor_id',
            ['type' => Table::TYPE_TEXT, 'nullable' => false, 'length' => 255 ,'comment' => 'Vedor Id']
        );
        $connection->addColumn(
            $tableName,
            'vendor_name',
            ['type' => Table::TYPE_TEXT, 'nullable' => false, 'length' => 255 ,'comment' => 'Vendor Name']
        );
        $connection->addColumn(
            $tableName,
            'amount_paid',
            ['type' => Table::TYPE_TEXT, 'nullable' => false, 'length' => 255 ,'comment' => 'Amount Paid']
        );

        $installer->endSetup();
    }

}