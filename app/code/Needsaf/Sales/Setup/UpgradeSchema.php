<?php

namespace Needsaf\Sales\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;

class UpgradeSchema implements UpgradeSchemaInterface
{
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        if (version_compare($context->getVersion(), '1.0.1', '<')) {

            $tableName = $setup->getTable('sales_order');
            $connection = $setup->getConnection();
            $connection->addColumn(
                $tableName,
                'billing_name',
                ['type' => Table::TYPE_TEXT, 'nullable' => false, 'afters' => 'product_name', 'length' => 255, 'comment' => 'Billing Name']
            );
        }

        $setup->endSetup();
    }
}