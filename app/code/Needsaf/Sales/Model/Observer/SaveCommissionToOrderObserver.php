<?php
namespace Needsaf\Sales\Model\Observer;

use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Event\ObserverInterface;
use Needsaf\Membership\Model\VendorFactory;

class SaveCommissionToOrderObserver implements ObserverInterface
{

    /**
     * @var \Needsaf\Sales\Helper\Commission
     */
    protected $_commissionHelper;

    protected $productFactory;

    protected $orderFactory;

    protected $userFactory;

    protected $_vendorFactory;

    /**
     * SaveCommissionToOrderObserver constructor.
     *
     * @param \Needsaf\Sales\Helper\Commission $_commissionHelper
     */
    public function __construct(
        \Needsaf\Sales\Helper\Commission $_commissionHelper,
        \Magento\Sales\Model\OrderFactory $orderFactory,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Magento\User\Model\UserFactory $userFactory,
        VendorFactory $vendorFactory
) {
        $this->_commissionHelper = $_commissionHelper;
        $this->orderFactory = $orderFactory;
        $this->productFactory = $productFactory;
        $this->userFactory = $userFactory;
        $this->_vendorFactory = $vendorFactory;
    }

    public function execute(EventObserver $observer)
    {
        $order = $observer->getOrder();
        $orderItems = $order->getAllItems();
        $orderItem = $orderItems[0];
        $product = $this->productFactory->create()->load($orderItem->getProductId());
        $baseGrandTotal = $order->getBaseGrandTotal();
        $commission = $this->_commissionHelper->calculateCommission($baseGrandTotal);
        $order->setOrderCommission($commission);
        $grandtotal_updated = $baseGrandTotal - $commission;
        $order->setProductId($orderItem->getProductId());
        $order->setProductName($product->getName());
        $vendorId = $product->getResource()->getAttribute('vendor_id')->getFrontend()->getValue($product);
        $vendorModel =  $this->_vendorFactory->create();
        $vendorModel->load($vendorId);
        $order->setVendorId($vendorId);
        $order->setVendorName($vendorModel->getUsername());
        $order->setAmountPaid($grandtotal_updated);
        $firstname = $order->getCustomerFirstname();
        $lastname = $order->getCustomerLastname();
        $billing_name = $firstname." ".$lastname;
        $order->setBillingName($billing_name);
        return $this;
    }

}