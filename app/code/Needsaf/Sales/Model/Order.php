<?php
/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Needsaf\Sales\Model;


use Needsaf\Sales\Api\Data\OrderInterface;

class Order extends \Magento\Sales\Model\Order implements OrderInterface
{
    public function getOrderCommission()
    {
        return $this->getData(OrderInterface::ORDER_COMMISSION);
    }

    public function setOrderCommission($orderCommission)
    {
        return $this->setData(OrderInterface::ORDER_COMMISSION, $orderCommission);
    }

    public function getProductId()
    {
        return $this->getData(OrderInterface::PRODUCT_ID);
    }

    public function getProductName()
    {
        return $this->getData(OrderInterface::PRODUCT_NAME);
    }

    public function getVendorId()
    {
        return $this->getData(OrderInterface::VENDOR_ID);
    }

    public function getVendorName()
    {
        return $this->getData(OrderInterface::VENDOR_NAME);
    }

    public function getAmountPaid()
    {
        return $this->getData(OrderInterface::AMOUNT_PAID);
    }

    public function getBillingName()
    {
        return $this->getData(OrderInterface::BILLING_NAME);
    }

    public function setProductId($productId)
    {
        return $this->setData(OrderInterface::PRODUCT_ID, $productId);
    }

    public function setProductName($productName)
    {
        return $this->setData(OrderInterface::PRODUCT_NAME, $productName);
    }

    public function setVendorId($vendorId)
    {
        return $this->setData(OrderInterface::VENDOR_ID, $vendorId);
    }

    public function setVendorName($vendorName)
    {
        return $this->setData(OrderInterface::VENDOR_NAME, $vendorName);
    }

    public function setAmountPaid($amountPaid)
    {
       return $this->setData(OrderInterface::AMOUNT_PAID, $amountPaid);
    }

    public function setBillingName($billingName)
    {
        return $this->setData(OrderInterface::BILLING_NAME, $billingName);
    }

}
