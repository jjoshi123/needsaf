<?php
namespace Needsaf\FrontProduct\Block\Product;


use Magento\Catalog\Api\CategoryRepositoryInterface;
use Magento\User\Model\UserFactory;
use Needsaf\Membership\Model\VendorFactory;
use Needsaf\Membership\Model\MembershipFactory;

class ListProduct extends \Magento\Catalog\Block\Product\ListProduct
{

    protected $_userFactory;
    protected $_vendorFactory;
    protected $_membershipFactory;

    public function __construct(
         \Magento\Catalog\Block\Product\Context $context,
        \Magento\Framework\Data\Helper\PostHelper $postDataHelper,
        \Magento\Catalog\Model\Layer\Resolver $layerResolver,
        CategoryRepositoryInterface $categoryRepository,
        \Magento\Framework\Url\Helper\Data $urlHelper,
        \Magento\User\Model\UserFactory $userFactory,
        VendorFactory $vendorFactory ,
        MembershipFactory $membershipFactory,
        array $data = [])
    {
        $this->_vendorFactory = $vendorFactory;
        $this->_userFactory = $userFactory;
        $this->_membershipFactory = $membershipFactory;
//        $this->_timezoneInterface = $timezoneInterface;
        parent::__construct($context, $postDataHelper, $layerResolver, $categoryRepository, $urlHelper, $data);
    }

    public function getVendorType($admin_id)
    {
        $userModel = $this->_userFactory->create();
        $userModel->load($admin_id);
        $vendorId = $userModel->getVendorId();
        $vendorModel = $this->_vendorFactory->create();
        $vendorModel->load($vendorId);
        $membershipId = $vendorModel->getMembershipId();
        $membershipModel = $this->_membershipFactory->create();
        $membershipModel->load($membershipId);
        return $membershipModel;
    }


    public function getCompareDays(){
        $today = $this->_localeDate->date()->format('N');
        return $today;
    }
    public function getCompareHours(){
        $today = $this->_localeDate->date()->format('H:i');
        return $today;
    }
    public function getFreeMembership()
    {

        $membershipModel = $this->_membershipFactory->create();
        $membershipModel->load('Basic','membership_type');
        return $membershipModel->getData();
    }

    public function getIsVendor($admin_id)
    {
        $userModel = $this->_userFactory->create();
        $userModel->load($admin_id);
        return $userModel->getIsVendor();
    }
}