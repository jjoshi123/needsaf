<?php

namespace Needsaf\FrontProduct\Plugin;
use Magento\Catalog\Api\ProductAttributeRepositoryInterface;
use Magento\Catalog\Model\Layer;
use Magento\Catalog\Model\ResourceModel\Product\Collection;
use Needsaf\Membership\Model\MembershipFactory;

class VendorProductsFilter
{
    protected $membershipFactory;
    protected $_timezoneInterface;
    protected $_eavAttribute;
    protected $_storeManager;
    /**
     * @var array
     */
    protected $attributeValues;
    protected $attributeRepository;
    protected $tableFactory;
    public function __construct(MembershipFactory $membershipFactory,
                                \Magento\Framework\Stdlib\DateTime\TimezoneInterface $timezoneInterface,
                                \Magento\Eav\Model\ResourceModel\Entity\Attribute $eavAttribute,
                                \Magento\Store\Model\StoreManagerInterface $storeManager,
                                ProductAttributeRepositoryInterface $attributeRepository,
                                \Magento\Eav\Model\Entity\Attribute\Source\TableFactory $tableFactory,
                                array $data = [])
    {
        $this->_timezoneInterface = $timezoneInterface;
        $this->membershipFactory = $membershipFactory;
        $this->_eavAttribute = $eavAttribute;
        $this->_storeManager = $storeManager;
        $this->attributeRepository = $attributeRepository;
        $this->tableFactory = $tableFactory;
    }

    /**
     * Filter-out free products when required
     *
     * @param \Magento\Catalog\Model\Layer $subject
     * @param \Magento\Catalog\Model\ResourceModel\Product\Collection $collection
     *
     * @return array
     */
    public function beforePrepareProductCollection(Layer $subject, Collection $collection)
    {

        $freeMembership = $this->getFreeMembership();
        $freeMembershipDays = explode(',',$freeMembership['days']);
        $freeMembershipTimeFrom = str_replace(",",":", $freeMembership['time_from']);
        $freeMembershipTimeTo = str_replace(",",":",$freeMembership['time_to']);
        $attributeId = $this->_eavAttribute->getIdByCode(\Magento\Catalog\Model\Product::ENTITY, 'vendor_id');
        $regionId = $this->_eavAttribute->getIdByCode(\Magento\Catalog\Model\Product::ENTITY, 'regions');
        $optionId = $this->getOptionId('regions',$this->getStoreCode());

        $collection->getSelect()->joinLeft(
            ['cat_varchar' => 'catalog_product_entity_varchar'],
            "e.entity_id = cat_varchar.entity_id AND cat_varchar.attribute_id = $regionId",
            ['region_id' => 'cat_varchar.value']
        );
        var_dump($collection->getData());
        die();
        if(in_array($this->getCompareDays(),$freeMembershipDays) && $this->getCompareHours() > $freeMembershipTimeFrom && $this->getCompareHours() < $freeMembershipTimeTo ){
            $collection->getSelect()
                ->joinLeft(
                    ['cat_int' => 'catalog_product_entity_int'],
                    "e.entity_id = cat_int.entity_id AND cat_int.attribute_id = $attributeId",
                    ['vendor_id' => 'cat_int.value']
                )
                ->joinLeft(
                    ['vendor' => 'vendor'],
                    "cat_int.value = vendor.id",
                    ['membership_id']
                )
                ->joinLeft(
                    ['membership' => 'needsaf_membership_details'],
                    "vendor.membership_id = membership.membership_id",
                    ['membership_type']
                )->order(new \Zend_Db_Expr("vendor.vendor_rating DESC"))->where('cat_int.value IS NOT NUll')->limit(5);
        }else{
            $collection->getSelect()
                ->joinLeft(
                    ['cat_int' => 'catalog_product_entity_int'],
                    "e.entity_id = cat_int.entity_id AND cat_int.attribute_id = $attributeId",
                    ['vendor_id' => 'cat_int.value']
                )
                ->joinLeft(
                    ['vendor' => 'vendor'],
                    "cat_int.value = vendor.id",
                    ['membership_id']
                )
                ->joinLeft(
                    ['membership' => 'needsaf_membership_details'],
                    "vendor.membership_id = membership.membership_id",
                    ['membership_type']
                )
                ->where('membership.membership_type != ?', 'Basic')
                ->order(new \Zend_Db_Expr("vendor.vendor_rating DESC"))->limit(5);

        }

        return [$collection];
    }

    public function getFreeMembership()
    {

        $membershipModel = $this->membershipFactory->create();
        $membershipModel->load('Basic','membership_type');
        return $membershipModel->getData();
    }
    public function getCompareDays(){
        $today = $this->_timezoneInterface->date()->format('N');
        return $today;
    }
    public function getCompareHours(){
        $today = $this->_timezoneInterface->date()->format('H:i');
        return $today;
    }

    /**
     * Get Store code
     *
     * @return string
     */
    public function getStoreCode()
    {
        return $this->_storeManager->getStore()->getCode();
    }


    public function getOptionId($attributeCode, $label, $force = false)
    {
        /** @var \Magento\Catalog\Model\ResourceModel\Eav\Attribute $attribute */
        $attribute = $this->getAttribute($attributeCode);

        // Build option array if necessary
        if ($force === true || !isset($this->attributeValues[ $attribute->getAttributeId() ])) {
            $this->attributeValues[ $attribute->getAttributeId() ] = [];

            // We have to generate a new sourceModel instance each time through to prevent it from
            // referencing its _options cache. No other way to get it to pick up newly-added values.

            /** @var \Magento\Eav\Model\Entity\Attribute\Source\Table $sourceModel */
            $sourceModel = $this->tableFactory->create();
            $sourceModel->setAttribute($attribute);

            foreach ($sourceModel->getAllOptions() as $option) {
                $this->attributeValues[ $attribute->getAttributeId() ][ $option['label'] ] = $option['value'];
            }
        }

        // Return option ID if exists
        if (isset($this->attributeValues[ $attribute->getAttributeId() ][ $label ])) {
            return $this->attributeValues[ $attribute->getAttributeId() ][ $label ];
        }

        // Return false if does not exist
        return false;
    }

    /**
     * Get attribute by code.
     *
     * @param string $attributeCode
     * @return \Magento\Catalog\Api\Data\ProductAttributeInterface
     */
    public function getAttribute($attributeCode)
    {
        return $this->attributeRepository->get($attributeCode);
    }
}