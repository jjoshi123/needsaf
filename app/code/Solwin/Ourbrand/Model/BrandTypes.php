<?php
namespace Solwin\Ourbrand\Model;

class BrandTypes extends \Magento\Framework\Model\AbstractModel
{
    
    /** 
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * URL Model instance
     *
     * @var \Magento\Framework\UrlInterface
     */
    protected $_url;

    /**
     * @var \Magento\Catalog\Helper\Category
     */
    protected $_brandHelper;

    /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Solwin\Ourbrand\Model\ResourceModel\Brand $resource
     * @param Collection $resourceCollection
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\UrlInterface $url
     * @param \Solwin\Ourbrand\Helper\Data $brandHelper
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Solwin\Ourbrand\Model\Resource\BrandTypes $resource = null,
        \Solwin\Ourbrand\Model\Resource\BrandTypes\Collection $resourceCollection = null,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\UrlInterface $url,
        \Solwin\Ourbrand\Helper\Data $brandHelper,
        array $data = []
    ) {
        $this->_storeManager = $storeManager;
        $this->_url = $url;
        $this->_brandHelper = $brandHelper;
        parent::__construct(
                $context,
                $registry,
                $resource,
                $resourceCollection,
                $data
                );
    }
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Solwin\Ourbrand\Model\Resource\BrandTypes');
    }
    
    public function getUrl()
    {
        $url = $this->_storeManager->getStore()->getBaseUrl();
        $route = $this->_brandHelper
                ->getConfig('brandsection/brandgroup/route');
        $urlPrefixConfig = $this->_brandHelper
                ->getConfig('brandsection/brandgroup/url_prefix');
        $urlPrefix = '';
        if ($urlPrefixConfig) {
            $urlPrefix = $urlPrefixConfig.'/';
        }
        $urlSuffix = $this->_brandHelper
                ->getConfig('brandsection/brandgroup/url_suffix');
        return $url.$urlPrefix.$this->getUrlKey().$urlSuffix;
    }
}