<?php

namespace Solwin\Ourbrand\Model\Source;

use Solwin\Ourbrand\Model\BrandTypesFactory;

class Brands extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource {

    protected $_collection;
    protected $_modelBrandTypesFactory;

    /**
     * Entity attribute factory
     *
     * @var \Magento\Eav\Model\Resource\Entity\AttributeFactory
     */
    protected $entityAttributeFactory;

    public function __construct(
    BrandTypesFactory $modelBrandTypesFactory, \Solwin\Ourbrand\Model\Resource\BrandTypes\Collection $collection, \Magento\Eav\Model\ResourceModel\Entity\AttributeFactory $entityAttributeFactory
    ) {
        $this->_modelBrandTypesFactory = $modelBrandTypesFactory;
        $this->_collection = $collection;
        $this->entityAttributeFactory = $entityAttributeFactory;

//        $collection = $this->_modelBrandTypesFactory->create()->getCollection();
//        $this->setCollection($collection);
    }

    /**
     * Retrieve all product tax class options.
     *
     * @param bool $withEmpty
     * @return array
     */
    public function getAllOptions() {
        if (!$this->_options) {
            $collection = $this->_modelBrandTypesFactory->create()->getCollection();

            foreach ($collection as $val) {
                $this->_options[] = [
                    'value' => $val->getBrandId(),
                    'label' => $val->getTitle(),
                ];
            }
        }
        return $this->_options;
    }

    /**
     * Get a text for option value
     *
     * @param string|integer $value
     * @return string
     */
//    public function getOptionText($value)
//    {
//        $options = $this->getAllOptions();
//
//        foreach ($options as $item) {
//            if ($item['value'] == $value) {
//                return $item['label'];
//            }
//        }
//        return false;
//    }

    /**
     * Retrieve flat column definition
     *
     * @return array
     */
    public function getFlatColumns() {
        $attributeCode = $this->getAttribute()->getAttributeCode();

        return [
            $attributeCode => [
                'unsigned' => true,
                'default' => null,
                'extra' => null,
                'type' => Table::TYPE_INTEGER,
                'nullable' => true,
                'comment' => $attributeCode . ' tax column',
            ],
        ];
    }

    /**
     * Retrieve Select for update attribute value in flat table
     *
     * @param   int $store
     * @return  \Magento\Framework\DB\Select|null
     */
    public function getFlatUpdateSelect($store) {
        return $this->entityAttributeFactory->create()->getFlatUpdateSelect($this->getAttribute(), $store);
    }

}
