<?php
namespace Solwin\Ourbrand\Model\Resource;

class BrandTypes extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('ourbrand', 'brand_id');
    }
}