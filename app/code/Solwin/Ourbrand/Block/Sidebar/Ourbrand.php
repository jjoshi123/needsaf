<?php

namespace Solwin\Ourbrand\Block\Sidebar;

use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\View\Element\Template;

class Ourbrand extends Template {

    protected $_collection;
    
    /**
     * @var \Magento\Catalog\Model\ProductFactory
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Solwin\Ourbrand\Model\Resource\BrandTypes\Collection $collection,
        array $data = []
    ) {
        $this->_collection = $collection;
        parent::__construct($context, $data);
    }

    public function getBrandSidebarData() {
        $limit = $this->getSidebarLimit();
        
        $collection = $this->_collection
                ->addFieldToFilter('status', 1);

        $collection->getSelect()
                ->order('brand_id')
                ->limit($limit);

        return $collection;
    }
    
    public function getEnableModule() {
        return $this->_scopeConfig->getValue('brandsection/brandgroup/active', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    public function getBaseUrl() {
        return $this->_storeManager->getStore()->getBaseUrl();
    }

    public function getMediaPath() {
        return $this->_filesystem->getDirectoryRead(
                        DirectoryList::MEDIA
                )->getAbsolutePath('');
    }

    public function getDefaultImage() {
        return $this->_assetRepo->getUrl('Solwin_Ourbrand::images/default-brand.png');
    }

    public function getSidebarBrandTitle() {
        return $this->_scopeConfig->getValue('brandsection/brandgroup/sidebartitle', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
    
    public function getSidebarLimit() {
        return $this->_scopeConfig->getValue('brandsection/brandgroup/sidebar_limit', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
}
