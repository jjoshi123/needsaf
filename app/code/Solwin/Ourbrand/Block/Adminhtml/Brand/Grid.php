<?php

namespace Solwin\Ourbrand\Block\Adminhtml\Brand;

class Grid extends \Magento\Backend\Block\Widget\Grid\Extended {

    /**
     * @var \Magento\Framework\Module\Manager
     */
    protected $moduleManager;

    /**
     * @var \Solwin\Ourbrand\Model\BrandTypesFactory
     */
    protected $_brandTypeFactory;

    /**
     * @var \Solwin\Ourbrand\Model\Status
     */
    protected $_status;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Backend\Helper\Data $backendHelper
     * @param \Solwin\Ourbrand\Model\BrandTypesFactory $brandTypeFactory
     * @param \Solwin\Ourbrand\Model\Status $status
     * @param \Magento\Framework\Module\Manager $moduleManager
     * @param array $data
     *
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
    \Magento\Backend\Block\Template\Context $context, \Magento\Backend\Helper\Data $backendHelper, \Solwin\Ourbrand\Model\BrandTypesFactory $brandTypeFactory, \Solwin\Ourbrand\Model\Status $status, \Magento\Framework\Module\Manager $moduleManager, array $data = []
    ) {
        $this->_brandTypeFactory = $brandTypeFactory;
        $this->_status = $status;
        $this->moduleManager = $moduleManager;
        parent::__construct($context, $backendHelper, $data);
    }

    /**
     * @return void
     */
    protected function _construct() {
        parent::_construct();
        $this->setId('postGrid');
        $this->setDefaultSort('brand_id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
        $this->setVarNameFilter('post_filter');
    }

    /**
     * @return $this
     */
    protected function _prepareCollection() {
        $collection = $this->_brandTypeFactory->create()->getCollection();
        $this->setCollection($collection);

        parent::_prepareCollection();
        return $this;
    }

    /**
     * @return $this
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    protected function _prepareColumns() {
        $this->addColumn(
                'brand_id', [
            'header' => __('ID'),
            'type' => 'number',
            'index' => 'brand_id',
            'header_css_class' => 'col-id',
            'column_css_class' => 'col-id'
                ]
        );
        
        $this->addColumn(
                'image', array(
            'header' => __('Image'),
            'index' => 'image',
            'class' => 'xxx',
            "renderer" => "Solwin\Ourbrand\Block\Adminhtml\Brand\Renderer\Image",
                )
        );
        
        $this->addColumn(
                'title', [
            'header' => __('Title'),
            'index' => 'title',
            'class' => 'xxx'
                ]
        );
        
        $this->addColumn(
                'position', [
            'header' => __('Position'),
            'index' => 'position',
            'class' => 'xxx'
                ]
        );
        
        $this->addColumn(
                'status', [
            'header' => __('Status'),
            'index' => 'status',
            'type' => 'options',
            'options' => $this->_status->getOptionArray()
                ]
        );

        $this->addColumn(
                'edit', [
            'header' => __('Edit'),
            'type' => 'action',
            'getter' => 'getId',
            'actions' => [
                [
                    'caption' => __('Edit'),
                    'url' => [
                        'base' => '*/*/edit'
                    ],
                    'field' => 'brand_id'
                ]
            ],
            'filter' => false,
            'sortable' => false,
            'index' => 'stores',
            'header_css_class' => 'col-action',
            'column_css_class' => 'col-action'
                ]
        );

        $block = $this->getLayout()->getBlock('grid.bottom.links');
        if ($block) {
            $this->setChild('grid.bottom.links', $block);
        }

        return parent::_prepareColumns();
    }

    /**
     * @return $this
     */
    protected function _prepareMassaction() {
        $this->setMassactionIdField('brand_id');
        $this->getMassactionBlock()->setTemplate('Solwin_Ourbrand::ourbrand/grid/massaction_extended.phtml');
        $this->getMassactionBlock()->setFormFieldName('brandtype');

        $this->getMassactionBlock()->addItem(
                'delete', [
            'label' => __('Delete'),
            'url' => $this->getUrl('ourbrand/*/massDelete'),
            'confirm' => __('Are you sure?')
                ]
        );

        $statuses = $this->_status->getOptionArray();

        array_unshift($statuses, ['label' => '', 'value' => '']);
        $this->getMassactionBlock()->addItem(
                'status', [
            'label' => __('Change status'),
            'url' => $this->getUrl('ourbrand/*/massStatus', ['_current' => true]),
            'additional' => [
                'visibility' => [
                    'name' => 'status',
                    'type' => 'select',
                    'class' => 'required-entry',
                    'label' => __('Status'),
                    'values' => $statuses
                ]
            ]
                ]
        );


        return $this;
    }

    /**
     * @return string
     */
    public function getGridUrl() {
        return $this->getUrl('ourbrand/*/grid', ['_current' => true]);
    }

    /**
     * @param \Solwin\Ourbrand\Model\BrandTypes|\Magento\Framework\Object $row
     * @return string
     */
    public function getRowUrl($row) {
        return $this->getUrl(
                        'ourbrand/*/edit', ['brand_id' => $row->getId()]
        );
    }

}
