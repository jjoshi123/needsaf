<?php

namespace Solwin\Ourbrand\Block\Adminhtml\Brand\Edit\Tab;

/**
 * Brand edit form main tab
 */
class Main extends \Magento\Backend\Block\Widget\Form\Generic implements \Magento\Backend\Block\Widget\Tab\TabInterface {

    /**
     * @var \Magento\Store\Model\System\Store
     */
    protected $_systemStore;

    /**
     * @var \Magento\Cms\Model\Wysiwyg\Config
     */
    protected $_wysiwygConfig;

    /**
     * @var \Solwin\Ourbrand\Model\Status
     */
    protected $_status;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Data\FormFactory $formFactory
     * @param \Magento\Store\Model\System\Store $systemStore
     * @param array $data
     */
    public function __construct(
    \Magento\Backend\Block\Template\Context $context, \Magento\Framework\Registry $registry, \Magento\Framework\Data\FormFactory $formFactory, \Magento\Store\Model\System\Store $systemStore, \Magento\Cms\Model\Wysiwyg\Config $wysiwygConfig, \Solwin\Ourbrand\Model\Status $status, array $data = []
    ) {
        $this->_systemStore = $systemStore;
        $this->_wysiwygConfig = $wysiwygConfig;
        $this->_status = $status;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * Prepare form
     *
     * @return $this
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    protected function _prepareForm() {
        /* @var $model \Solwin\Ourbrand\Model\BrandTypes */
        $model = $this->_coreRegistry->registry('our_brand');

        $isElementDisabled = false;

        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create();

        $form->setHtmlIdPrefix('page_');

        $fieldset = $form->addFieldset('base_fieldset', ['legend' => __('Brand Information')]);

        if ($model->getId()) {
            $fieldset->addField('brand_id', 'hidden', ['name' => 'brand_id']);
        }

        $fieldset->addField(
                'status', 'select', [
            'label' => __('Status'),
            'title' => __('Status'),
            'name' => 'status',
            'required' => true,
            'options' => $this->_status->getOptionArray(),
            'disabled' => $isElementDisabled
                ]
        );
        
        $fieldset->addField(
                'title', 'text', [
            'name' => 'title',
            'label' => __('Title'),
            'title' => __('Title'),
            'required' => true,
            'disabled' => $isElementDisabled
                ]
        );
        
        $fieldset->addField(
            'url_key',
            'text',
            [
                'name'  => 'url_key',
                'label' => __('URL Key'),
                'title' => __('URL Key'),
                'class' => 'validate-identifier',
                'note' => __('Empty to auto create url key'),
                'required' => false,
            ]
        );
        
        
        $fieldset->addField(
                'position', 'text', [
            'name' => 'position',
            'label' => __('Position'),
            'title' => __('Position'),
            'required' => true,
            'disabled' => $isElementDisabled
                ]
        );

        $fieldset->addField(
                'image', 'image', [
            'name' => 'image',
            'label' => __('Image'),
            'title' => __('Image'),
            'required' => true,
            'disabled' => $isElementDisabled,
            'note' => '(*.jpg, *.png, *.gif)'
                ]
        );

//        $wysiwygConfig = $this->_wysiwygConfig->getConfig(['tab_id' => $this->getTabId()]);
//
//        $contentField = $fieldset->addField(
//                'description', 'editor', [
//            'name' => 'description',
//            'label' => __('Description'),
//            'title' => __('Description'),
//            'style' => 'height:25em;',
//            'required' => false,
//            'disabled' => $isElementDisabled,
//            'config' => $wysiwygConfig
//                ]
//        );
        
        $fieldset->addField(
            'description',
            'editor',
            [
                'name'  => 'description',
                'label' => __('Description'),
                'title' => __('Description'),
                'required' => false,
                'config'    => $this->_wysiwygConfig->getConfig()
            ]
        );

        if ($model->getId()) {
            $fieldset->addField('update_time', 'hidden', ['name' => 'update_time']);
        }
        $dateFormat = date("Y-m-d h:i:sa");
        $model->setData('update_time', $dateFormat);
        
//        // Setting custom renderer for content field to remove label column
//        $renderer = $this->getLayout()->createBlock(
//                        'Magento\Backend\Block\Widget\Form\Renderer\Fieldset\Element'
//                )->setTemplate(
//                'Magento_Cms::page/edit/form/renderer/content.phtml'
//        );
//        $contentField->setRenderer($renderer);

        $form->setValues($model->getData());
        $this->setForm($form);

        return parent::_prepareForm();
    }

    /**
     * Prepare label for tab
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabLabel() {
        return __('Brand Information');
    }

    /**
     * Prepare title for tab
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabTitle() {
        return __('Brand Information');
    }

    /**
     * {@inheritdoc}
     */
    public function canShowTab() {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function isHidden() {
        return false;
    }

    /**
     * Check permission for passed action
     *
     * @param string $resourceId
     * @return bool
     */
    protected function _isAllowedAction($resourceId) {
        return $this->_authorization->isAllowed($resourceId);
    }

}
