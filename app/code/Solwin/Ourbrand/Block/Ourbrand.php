<?php

namespace Solwin\Ourbrand\Block;

use Solwin\Ourbrand\Model\BrandTypesFactory;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\View\Element\Template;

class Ourbrand extends Template {

    protected $_collection;
    protected $_modelBrandTypesFactory;
    
    /**
     * @var \Magento\Catalog\Model\ProductFactory
     */
    
    protected $_productFactory;
    protected $_productStatus;
    protected $_productVisibility;
    
    public function __construct(
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Magento\Framework\View\Element\Template\Context $context,
        BrandTypesFactory $modelBrandTypesFactory,
        \Solwin\Ourbrand\Model\Resource\BrandTypes\Collection $collection,
        \Magento\Catalog\Model\Product\Visibility $productVisibility,
        \Magento\Catalog\Model\Product\Attribute\Source\Status $productStatus,
        array $data = []
    ) {
        //product details
        $this->_productFactory = $productFactory;
        $this->_modelBrandTypesFactory = $modelBrandTypesFactory;
        $this->_collection = $collection;
        $this->_productStatus = $productStatus;
        $this->_productVisibility = $productVisibility;
        parent::__construct($context, $data);
        $collection = $this->_modelBrandTypesFactory->create()->getCollection();
        $this->setCollection($collection);
        $this->pageConfig->getTitle()->set(__('Shop by Brands'));
    }

    protected function _prepareLayout() {
        
        parent::_prepareLayout();
        
        $currId = $this->getRequest()->getParam('id');
        
        if ($currId != '') {
            $brandCollection = self::getSingleBrandCollection($currId);
            $metaKeyword = $brandCollection->getMetaKeyword();
            $metaDescription = $brandCollection->getMetaDescription();
            $title = $brandCollection->getTitle();
            
        } else {
            $title = 'Shop by Brands';
            $metaKeyword = $this->_scopeConfig->getValue('brandsection/brandgroup/meta_keyword', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
            $metaDescription = $this->_scopeConfig->getValue('brandsection/brandgroup/meta_description', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
            
        }
        $this->pageConfig->getTitle()->set($title);
        $this->pageConfig->setKeywords($metaKeyword);
        $this->pageConfig->setDescription($metaDescription);
        
        
        
        
        if ($this->getCollection()) {
            // create pager block for collection
            $pager = $this->getLayout()->createBlock(
                            'Magento\Theme\Block\Html\Pager', 'ourbrand.grid.record.pager'
                    )->setCollection(
                    $this->getCollection()->addFieldToFilter('status', 1)// assign collection to pager
            );
            $this->setChild('pager', $pager); // set pager block in layout
        }
        return $this;
    }

    public function getPagerHtml() {
        return $this->getChildHtml('pager');
    }

    public function getProductCollection($brandId) {
        //product collection
        $products = $this->_productFactory->create()->getCollection()
                ->addAttributeToSelect('name')
                ->addAttributeToSelect('sku')
                ->addAttributeToSelect('price')
                ->addAttributeToSelect('news_from_date')
                ->addAttributeToSelect('news_to_date')
                ->addAttributeToSelect('special_price')
                ->addAttributeToSelect('special_from_date')
                ->addAttributeToSelect('special_to_date')
                ->addAttributeToSelect('tax_class_id')
                 ->addAttributeToSelect('small_image')
                ->addAttributeToSelect('thumbnail')
                ->addAttributeToSelect('small_image')
                ->addAttributeToFilter('status', ['in' => $this->_productStatus->getVisibleStatusIds()])
                ->setVisibility($this->_productVisibility->getVisibleInSiteIds())
                ->addFieldToFilter('our_brand', ['eq' => $brandId]);
        return $products;
    }

    public function formatCurrency($price, $websiteId = null) {
        return $this->_storeManager->getWebsite($websiteId)->getBaseCurrency()->format($price);
    }

    public function getSingleBrandCollection($brand_id) {
        $brandModel = $this->_modelBrandTypesFactory->create();
        $brandCollection = $brandModel->load($brand_id);
        return $brandCollection;
    }

    public function getEnableModule() {
        return $this->_scopeConfig->getValue('brandsection/brandgroup/active', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    public function getBaseUrl() {
        return $this->_storeManager->getStore()->getBaseUrl();
    }

    public function getMediaPath() {
        return $this->_filesystem->getDirectoryRead(
                        DirectoryList::MEDIA
                )->getAbsolutePath('');
    }

    public function getDefaultImage() {
        return $this->_assetRepo->getUrl('Solwin_Ourbrand::images/default-brand.png');
    }

}
