<?php

namespace Solwin\Ourbrand\Block;

class Link extends \Magento\Framework\View\Element\Html\Link {

    protected $_template = 'Solwin_Ourbrand::link.phtml';

    public function getHref() {
        return $this->getUrl('ourbrand');
    }

    public function getLabel() {
        return __('Our brand');
    }

}

?>  