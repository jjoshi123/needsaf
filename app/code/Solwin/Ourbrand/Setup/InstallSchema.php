<?php

namespace Solwin\Ourbrand\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * @codeCoverageIgnore
 */
class InstallSchema implements InstallSchemaInterface {

    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context) {
        $installer = $setup;

        $installer->startSetup();

        $table = $installer->getConnection()->newTable(
                        $installer->getTable('ourbrand')
                )->addColumn(
                        'brand_id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, array('identity' => true, 'nullable' => false, 'primary' => true), 'Brand ID'
                )->addColumn(
                        'status', \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT, null, array(), 'Status'
                )->addColumn(
                        'title', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, array('nullable' => false), 'Title'
		)->addColumn(
                        'position', \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT, null, array(), 'Position'
                )->addColumn(
                        'image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 100, array('nullable' => false), 'Image'
                )->addColumn(
                        'description', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, '2M', array('nullable' => false), 'Description'
                )->addColumn(
                        'created_at', \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP, null, array('default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT), 'Creation Time'
                )->addColumn(
                        'update_time', \Magento\Framework\DB\Ddl\Table::TYPE_DATETIME, null, array(), 'Modification Time'
                )->setComment(
                'Ourbrand Table'
        );

        $installer->getConnection()->createTable($table);

        $installer->endSetup();
    }

}
