<?php
/**
 * Solwin Infotech
 * Solwin Our Brand Extension
 *
 * @category   Solwin
 * @package    Solwin_Ourbrand
 * @copyright  Copyright © 2006-2016 Solwin (https://www.solwininfotech.com)
 * @license    https://www.solwininfotech.com/magento-extension-license/ 
 */
?>
<?php
namespace Solwin\Ourbrand\Controller;

use Magento\Framework\App\RouterInterface;
use Magento\Framework\App\ActionFactory;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\DataObject;
use Magento\Framework\Event\ManagerInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\Url;
use Solwin\Ourbrand\Model\BrandTypesFactory;
use Solwin\Ourbrand\Model\Resource\BrandTypes\Collection;

class Router implements RouterInterface
{
    /**
     * @var \Magento\Framework\App\ActionFactory
     */
    protected $_actionFactory;

    /**
     * Event manager
     * @var \Magento\Framework\Event\ManagerInterface
     */
    protected $_eventManager;

    /**
     * Response
     * @var \Magento\Framework\App\ResponseInterface
     */
    protected $_response;

    /**
     * @var bool
     */
    protected $_dispatched;
    
    /**
     * @var \Solwin\Ourbrand\Model\BrandFactory
     */
    protected $_modelBrandTypesFactory;
    
    /**
     * Brand Factory
     *
     * @var 
     */
    /**
     * Brand Helper
     */
    protected $_brandHelper;
    
    /**
     * @var Collection
     */
    protected $_brandCollection;

    /**
     * Store manager
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @param ActionFactory $actionFactory
     * @param ResponseInterface $response    
     * @param ManagerInterface $eventManager    
     * @param BrandFactory $modelBrandTypesFactory
     * @param \Solwin\Ourbrand\Helper\Data $brandHelper
     * @param Collection $brandCollection
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        ActionFactory $actionFactory,
        ResponseInterface $response,
        ManagerInterface $eventManager,
        BrandTypesFactory $modelBrandTypesFactory,
        \Solwin\Ourbrand\Helper\Data $brandHelper,
        Collection $brandCollection,
        StoreManagerInterface $storeManager
    ) {
        $this->_actionFactory = $actionFactory;
        $this->_eventManager = $eventManager;
        $this->_response = $response;
        $this->_brandHelper = $brandHelper;
        $this->_modelBrandTypesFactory = $modelBrandTypesFactory;
        $this->_brandCollection = $brandCollection;
        $this->_storeManager = $storeManager;
    }
    
    /**
     * @param RequestInterface $request
     * @return \Magento\Framework\App\ActionInterface
     */
    public function match(RequestInterface $request)
    {
        $_brandHelper = $this->_brandHelper;
        if (!$this->_dispatched) {
            $urlKey = trim($request->getPathInfo(), '/');
             
            $origUrlKey = $urlKey;
            /** @var Object $condition */
            $condition = new DataObject(['url_key' => $urlKey,
                'continue' => true]);
            $this->_eventManager->dispatch(
                'solwin_ourbrand_controller_router_match_before',
                ['router' => $this, 'condition' => $condition]
                );
              
            $urlKey = $condition->getUrlKey();
            if ($condition->getRedirectUrl()) {
                $this->_response->setRedirect($condition->getRedirectUrl());
                $request->setDispatched(true);
                return $this->_actionFactory->create(
                    'Magento\Framework\App\Action\Redirect',
                    ['request' => $request]
                    );
            }
            if (!$condition->getContinue()) {
                return null;
            }
          
            $route = $_brandHelper->getConfig('brandsection/brandgroup/route');
            
            
            if ( $urlKey == $route ) {
                $request->setModuleName('ourbrand')
                ->setControllerName('index')
                ->setActionName('index');
                $request->setAlias(Url::REWRITE_REQUEST_PATH_ALIAS, $urlKey);
                $this->_dispatched = true;
                return $this->_actionFactory->create(
                    'Magento\Framework\App\Action\Forward',
                    ['request' => $request]
                    );
            }
            $urlPrefix = $_brandHelper->getConfig(
                    'brandsection/brandgroup/url_prefix'
                    );
            $urlSuffix = $_brandHelper->getConfig(
                    'brandsection/brandgroup/url_suffix'
                    );

            $identifiers = explode('/', $urlKey);
            
            $cnt = count($identifiers);
            
            if ($cnt > 1) {
                if(trim($urlSuffix) != '') {
                    $pos = strpos($identifiers[1], $urlSuffix);
                } else {
                    $pos = true;
                }
                
            } else {
                $pos = false;
            }
           
            /**
             * Check Brand Url Key
             */
            if ((count($identifiers) == 2 && $identifiers[0] == $urlPrefix 
                    && $pos !== false) 
                    || (trim($urlPrefix) == '' && count($identifiers) == 1)) {
                if (count($identifiers) == 2) {
                    $brandUrl = str_replace($urlSuffix, '', $identifiers[1]);
                }
                if (trim($urlPrefix) == '' && count($identifiers) == 1) {
                    $brandUrl = str_replace($urlSuffix, '', $identifiers[0]);
                }
                
                $brand = $this->_modelBrandTypesFactory->create()
                        ->getCollection()
                        ->addFieldToFilter('status', ['eq' => 1])
                        ->addFieldToFilter('url_key', ['eq' => $brandUrl]);
                $bcollection = [];
                $bcollection = $brand->getData();
                if ($bcollection && $bcollection[0]['brand_id']) {
                    $request->setModuleName('ourbrand')
                    ->setControllerName('index')
                    ->setActionName('index')
                    ->setParam('id', $bcollection[0]['brand_id']);
                    $request->setAlias(
                            \Magento\Framework\Url::REWRITE_REQUEST_PATH_ALIAS,
                            $origUrlKey
                            );
                    $request->setDispatched(true);
                    $this->_dispatched = true;
                    return $this->_actionFactory->create(
                        'Magento\Framework\App\Action\Forward',
                        ['request' => $request]
                        );
                }
            }
        }
    }
}