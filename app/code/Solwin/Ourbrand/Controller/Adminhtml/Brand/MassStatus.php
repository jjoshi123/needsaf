<?php
namespace Solwin\Ourbrand\Controller\Adminhtml\Brand;

use Magento\Backend\App\Action;

class MassStatus extends \Magento\Backend\App\Action
{
    /**
     * Update Brand(s) status action
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     * @throws \Magento\Framework\Exception\LocalizedException|\Exception
     */
    public function execute()
    {
        $brandtypeIds = $this->getRequest()->getParam('brandtype');
        if (!is_array($brandtypeIds) || empty($brandtypeIds)) {
            $this->messageManager->addError(__('Please select Brand(s).'));
        } else {
            try {
                $status = (int) $this->getRequest()->getParam('status');
                foreach ($brandtypeIds as $postId) {
                    $post = $this->_objectManager->get('Solwin\Ourbrand\Model\BrandTypes')->load($postId);
                    $post->setStatus($status)->save();
                }
                $this->messageManager->addSuccess(
                    __('A total of %1 record(s) have been updated.', count($brandtypeIds))
                );
            } catch (\Exception $e) {
                $this->messageManager->addError($e->getMessage());
            }
        }
        return $this->resultRedirectFactory->create()->setPath('ourbrand/*/index');
    }

}
