<?php
namespace Solwin\Ourbrand\Controller\Adminhtml\Brand;

use Magento\Backend\App\Action;

/**
 * Class MassDelete
 */
class MassDelete extends \Magento\Backend\App\Action
{
    /**
     * @return \Magento\Backend\Model\View\Result\Redirect
     */
    public function execute()
    {
        $brandtypeIds = $this->getRequest()->getParam('brandtype');
        if (!is_array($brandtypeIds) || empty($brandtypeIds)) {
            $this->messageManager->addError(__('Please select Brand(s).'));
        } else {
            try {
                foreach ($brandtypeIds as $postId) {
                    $post = $this->_objectManager->get('Solwin\Ourbrand\Model\BrandTypes')->load($postId);
                    $post->delete();
                }
                $this->messageManager->addSuccess(
                    __('A total of %1 record(s) have been deleted.', count($brandtypeIds))
                );
            } catch (\Exception $e) {
                $this->messageManager->addError($e->getMessage());
            }
        }
        return $this->resultRedirectFactory->create()->setPath('ourbrand/*/index');
    }
}
