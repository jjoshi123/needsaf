<?php

namespace Solwin\Ourbrand\Controller\Adminhtml\Brand;

use Magento\Backend\App\Action;
use Magento\Framework\App\Filesystem\DirectoryList;

class Save extends \Magento\Backend\App\Action {

    protected $_uploaderFactory;

    /**
     * Filesystem facade
     *
     * @var \Magento\Framework\Filesystem
     */
    protected $_filesystem;

    /**
     * File Uploader factory
     *
     * @var \Magento\MediaStorage\Model\File\UploaderFactory
     */
    protected $_fileUploaderFactory;

    /**
     * @param Action\Context $context
     */
    public function __construct(Action\Context $context, \Magento\Framework\Filesystem $filesystem, \Magento\MediaStorage\Model\File\UploaderFactory $fileUploaderFactory) {
        $this->_filesystem = $filesystem;
        $this->_fileUploaderFactory = $fileUploaderFactory;
        parent::__construct($context);
    }

    /**
     * Save action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute() {
        $data = $this->getRequest()->getPostValue();

        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($data) {
            $model = $this->_objectManager->create('Solwin\Ourbrand\Model\BrandTypes');


            try {

                if (isset($data['image']['delete']) && $data['image']['delete'] == 1) {

                    // Remove Image from the directory and Database
                    $deletepath = $this->_filesystem->getDirectoryRead(
                                    DirectoryList::MEDIA
                            )->getAbsolutePath('');

                    // Un link image
                    if (isset($data['image']['value']) && $data['image']['value'] != '') {
                        unlink($deletepath . '/' . $data['image']['value']);
                    }

                    //unset image data
                    $data['image'] = '';
                } else {
                    unset($data['image']);
                    if (isset($_FILES)) {
                        if ($_FILES['image']['name']) {
                            // Image Saving Code
                            $uploader = $this->_fileUploaderFactory->create(['fileId' => 'image']);
                            $uploader->setAllowedExtensions(['jpg', 'jpeg', 'gif', 'png']);
                            $uploader->setAllowRenameFiles(false);
                            $uploader->setFilesDispersion(true);

                            $path = $this->_filesystem->getDirectoryRead(
                                            DirectoryList::MEDIA
                                    )->getAbsolutePath(
                                    'ourbrand/'
                            );
                            $result = $uploader->save($path);
                            $data['image'] = 'ourbrand' . $result['file'];
                        }
                    }
                }
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }

            $id = $this->getRequest()->getParam('brand_id');
            if ($id) {
                $model->load($id);
            }

            $model->setData($data);
            
            if ($data['url_key']=='') {
                $data['url_key'] = $data['title'];
            }
            $urlKey = $this->_objectManager
                    ->create('Magento\Catalog\Model\Product\Url')
                    ->formatUrlKey($data['url_key']);
            $data['url_key'] = $urlKey;

            $model->setUrlKey($urlKey);

            try {
                $model->save();
                $this->messageManager->addSuccess(__('The Brand has been saved.'));
                $this->_objectManager->get('Magento\Backend\Model\Session')->setFormData(false);
                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['brand_id' => $model->getId(), '_current' => true]);
                }
                return $resultRedirect->setPath('*/*/');
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\RuntimeException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addException($e, __('Something went wrong while saving the Brand.'));
            }

            $this->_getSession()->setFormData($data);
            return $resultRedirect->setPath('*/*/edit', ['brand_id' => $this->getRequest()->getParam('brand_id')]);
        }
        return $resultRedirect->setPath('*/*/');
    }

}
