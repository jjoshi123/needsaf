<?php

namespace Solwin\Ourbrand\Helper;

class Data extends \Magento\Framework\App\Helper\AbstractHelper {
    
    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;
    
    /**
     * @var \Magento\Cms\Model\Template\FilterProvider
     */
    protected $_filterProvider;

    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Cms\Model\Template\FilterProvider $filterProvider,
        \Magento\Store\Model\StoreManagerInterface $storeManager
    ) {
        $this->_filterProvider = $filterProvider;
        $this->_storeManager = $storeManager;
        parent::__construct($context);
    }

    /* get Extension enable/disable */

    public function getEnableModule() {
        return $this->scopeConfig->getValue('brandsection/brandgroup/active', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
    
    /**
     * Return brand config value by key and store
     *
     * @param string $key
     * @param \Magento\Store\Model\Store|int|string $store
     * @return string|null
     */
    public function getConfig($key)
    {
        $result = $this->scopeConfig->getValue($key,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        return $result;
    }
    
    public function getFilterData($desc) {
        $html = $this->_filterProvider->getPageFilter()->filter($desc);
        return $html;
    }
    
    /**
     * Get base url with store code
     */
    public function getBaseUrl() {
        return $this->_storeManager
                ->getStore()
                ->getBaseUrl();
    }
    
    
}
