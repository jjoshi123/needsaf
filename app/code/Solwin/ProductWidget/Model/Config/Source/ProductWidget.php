<?php

namespace Solwin\ProductWidget\Model\Config\Source;

class ProductWidget implements \Magento\Framework\Option\ArrayInterface
{
    public function toOptionArray()
    {
        return [
        ['value' => 'bestdeals', 'label' => __('Bestdeals')],
        ['value' => 'bestseller', 'label' => __('Bestseller')]];
    }
}