<?php
namespace Solwin\Testimonial\Controller\Adminhtml\Testim;

use Magento\Backend\App\Action;

/**
 * Class MassDelete
 */
class MassDelete extends \Magento\Backend\App\Action
{
    /**
     * @return \Magento\Backend\Model\View\Result\Redirect
     */
    public function execute()
    {
        $testimonialpagesIds = $this->getRequest()->getParam('testimonialpages');
        if (!is_array($testimonialpagesIds) || empty($testimonialpagesIds)) {
            $this->messageManager->addError(__('Please select Testimonial(s).'));
        } else {
            try {
                foreach ($testimonialpagesIds as $postId) {
                    $post = $this->_objectManager->get('Solwin\Testimonial\Model\TestimonialPages')->load($postId);
                    $post->delete();
                }
                $this->messageManager->addSuccess(
                    __('A total of %1 record(s) have been deleted.', count($testimonialpagesIds))
                );
            } catch (\Exception $e) {
                $this->messageManager->addError($e->getMessage());
            }
        }
        return $this->resultRedirectFactory->create()->setPath('testimonial/*/index');
    }
}
