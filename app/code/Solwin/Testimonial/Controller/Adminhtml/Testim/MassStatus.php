<?php
namespace Solwin\Testimonial\Controller\Adminhtml\Testim;

use Magento\Backend\App\Action;

class MassStatus extends \Magento\Backend\App\Action
{
    /**
     * Update Testimonial(s) status action
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     * @throws \Magento\Framework\Exception\LocalizedException|\Exception
     */
    public function execute()
    {
        $testimonialpagesIds = $this->getRequest()->getParam('testimonialpages');
        if (!is_array($$testimonialpagesIds) || empty($$testimonialpagesIds)) {
            $this->messageManager->addError(__('Please select Testimonial(s).'));
        } else {
            try {
                $status = (int) $this->getRequest()->getParam('status');
                foreach ($$testimonialpagesIds as $postId) {
                    $post = $this->_objectManager->get('Solwin\Testimonial\Model\TestimonialPages')->load($postId);
                    $post->setStatus($status)->save();
                }
                $this->messageManager->addSuccess(
                    __('A total of %1 record(s) have been updated.', count($$testimonialpagesIds))
                );
            } catch (\Exception $e) {
                $this->messageManager->addError($e->getMessage());
            }
        }
        return $this->resultRedirectFactory->create()->setPath('testimonial/*/index');
    }

}
