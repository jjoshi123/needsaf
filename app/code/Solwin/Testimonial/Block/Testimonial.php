<?php

namespace Solwin\Testimonial\Block;

use Solwin\Testimonial\Model\TestimonialPagesFactory;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\View\Element\Template;

class Testimonial extends Template
{

    protected $_collection;
    protected $_modelTestimonialPagesFactory;
    
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        TestimonialPagesFactory $modelTestimonialPagesFactory,
        \Solwin\Testimonial\Model\Resource\TestimonialPages\Collection $collection,
        array $data = []
    ) {
        $this->_modelTestimonialPagesFactory = $modelTestimonialPagesFactory;
        $this->_collection = $collection;
        parent::__construct($context, $data);
        $collection = $this->_modelTestimonialPagesFactory->create()->getCollection();
        $this->setCollection($collection);
        $this->pageConfig->getTitle()->set(__('Testimonials'));
    }

    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        if ($this->getCollection()) {
            $tlimit = self::getPagerTestimoniallimit();
            // create pager block for collection
            $pager = $this->getLayout()->createBlock(
                'Magento\Theme\Block\Html\Pager',
                'testimonial.grid.record.pager'
            )->setLimit($tlimit)->setCollection(
                $this->getCollection()->addFieldToFilter('status', 1) // assign collection to pager
            );
            $this->setChild('pager', $pager);// set pager block in layout
        }
        return $this;
    }
    
    public function getPagerHtml()
    {
        return $this->getChildHtml('pager');
    }
    
    /* get testimonial list page config options */
    
    /* get testimonial limit on list page */
    public function getPagerTestimoniallimit() {
        return $this->_scopeConfig->getValue('testimsection/testimlistgroup/testimoniallistlimit', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
    
    /* enable testimonial name on list page */
    public function getShowtestimonialname() {
        return $this->_scopeConfig->getValue('testimsection/testimlistgroup/displayname', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
    
    /* enable testimonial email on list page */
    public function getShowtestimonialemail() {
        return $this->_scopeConfig->getValue('testimsection/testimlistgroup/displayemail', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
    
    /* enable testimonial company on list page */
    public function getShowtestimonialcompany() {
        return $this->_scopeConfig->getValue('testimsection/testimlistgroup/displaycompnay', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
    
    /* enable testimonial image on list page */
    public function getShowtestimonialimage() {
        return $this->_scopeConfig->getValue('testimsection/testimlistgroup/displayimage', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
    
    /* enable testimonial website on list page */
    public function getShowtestimonialwebsite() {
        return $this->_scopeConfig->getValue('testimsection/testimlistgroup/displaywebsite', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
    
    
    /* get testimonial detail page config options */
    
    /* enable testimonial name on detail page */
    public function getShowtestimonialdetname() {
        return $this->_scopeConfig->getValue('testimsection/testimdetailgroup/detdisplayname', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
    
    /* enable testimonial email on detail page */
    public function getShowtestimonialdetemail() {
        return $this->_scopeConfig->getValue('testimsection/testimdetailgroup/detdisplayemail', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
    
    /* enable testimonial company on detail page */
    public function getShowtestimonialdetcompany() {
        return $this->_scopeConfig->getValue('testimsection/testimdetailgroup/detdisplaycompnay', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
    
    /* enable testimonial image on detail page */
    public function getShowtestimonialdetimage() {
        return $this->_scopeConfig->getValue('testimsection/testimdetailgroup/detdisplayimage', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
    
    /* enable testimonial website on detail page */
    public function getShowtestimonialdetwebsite() {
        return $this->_scopeConfig->getValue('testimsection/testimdetailgroup/detdisplaywebsite', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
    
    public function getSingleTestimCollection($testim_id) {
        $testimModel = $this->_modelTestimonialPagesFactory->create();
        $testimCollection = $testimModel->load($testim_id);
        return $testimCollection;
    }

    /* get extension enable/disable */
    public function getEnableModule() {
        return $this->_scopeConfig->getValue('testimsection/testimgroup/active', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    public function getBaseUrl() {
        return $this->_storeManager->getStore()->getBaseUrl();
    }

    public function getMediaPath() {
        return $this->_filesystem->getDirectoryRead(
                        DirectoryList::MEDIA
                )->getAbsolutePath('');
    }

    public function getDefaultImage() {
        return $this->_assetRepo->getUrl('Solwin_Testimonial::images/manageteam.jpg');
    }

    public function getFacebookImage() {
        return $this->_assetRepo->getUrl('Solwin_Testimonial::images/facebook.png');
    }

    public function getGoogleImage() {
        return $this->_assetRepo->getUrl('Solwin_Testimonial::images/google-plus.png');
    }

    public function getTwitterImage() {
        return $this->_assetRepo->getUrl('Solwin_Testimonial::images/twitter.png');
    }

    public function getEmailImage() {
        return $this->_assetRepo->getUrl('Solwin_Testimonial::images/mail.png');
    }
    
}
