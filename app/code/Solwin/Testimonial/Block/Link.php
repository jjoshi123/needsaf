<?php

namespace Solwin\Testimonial\Block;

class Link extends \Magento\Framework\View\Element\Html\Link {

    protected $_template = 'Solwin_Testimonial::link.phtml';

    public function getHref() {
        return $this->getUrl('testimonial');
    }

    public function getLabel() {
        return __('Testimonials');
    }

}

?>  