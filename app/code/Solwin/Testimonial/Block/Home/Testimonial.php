<?php

namespace Solwin\Testimonial\Block\Home;

use Solwin\Testimonial\Model\TestimonialPagesFactory;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\View\Element\Template;

class Testimonial extends Template {

    protected $_collection;
    protected $_modelTestimonialPagesFactory;
    
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        TestimonialPagesFactory $modelTestimonialPagesFactory,
        \Solwin\Testimonial\Model\Resource\TestimonialPages\Collection $collection,
        array $data = []
    ) {
        $this->_modelTestimonialPagesFactory = $modelTestimonialPagesFactory;
        $this->_collection = $collection;
        parent::__construct($context, $data);
    }

    public function getTestimonialData() {
        $limit = $this->getTestimonialLimit();
        
        $collection = $this->_collection
                ->addFieldToFilter('status', 1);

        $collection->getSelect()
                ->order('testimonial_id')
                ->limit($limit);

        return $collection;
    }

    public function getSingleTestimCollection($testim_id) {
        $testimModel = $this->_modelTestimonialPagesFactory->create();
        $testimCollection = $testimModel->load($testim_id);
        return $testimCollection;
    }

    /* get extension enable/disable */
    public function getEnableModule() {
        return $this->_scopeConfig->getValue('testimsection/testimgroup/active', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    public function getBaseUrl() {
        return $this->_storeManager->getStore()->getBaseUrl();
    }

    public function getMediaPath() {
        return $this->_filesystem->getDirectoryRead(
                        DirectoryList::MEDIA
                )->getAbsolutePath('');
    }

    public function getDefaultImage() {
        return $this->_assetRepo->getUrl('Solwin_Testimonial::images/manageteam.jpg');
    }
    
    /* get testimonial display limit on home page */
    public function getTestimonialLimit() {
        return $this->_scopeConfig->getValue('testimsection/testimhomegroup/homelimit', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
    
    /* get enable testimonial on home page */
    public function getShowonhome() {
        return $this->_scopeConfig->getValue('testimsection/testimhomegroup/enablehome', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
    
    /* get testimonial title on home page */
    public function getTestimonialhometitle() {
        return $this->_scopeConfig->getValue('testimsection/testimhomegroup/hometestimonialtitle', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
    
    /* get enable testimonial name on home page */
    public function getShowhomename() {
        return $this->_scopeConfig->getValue('testimsection/testimhomegroup/showname', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
    
    /* get enable testimonial email on home page */
    public function getShowhomeemail() {
        return $this->_scopeConfig->getValue('testimsection/testimhomegroup/showemail', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
    
    /* get enable testimonial company on home page */
    public function getShowhomecompany() {
        return $this->_scopeConfig->getValue('testimsection/testimhomegroup/showcompnay', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
    
    /* get enable testimonial image on home page */
    public function getShowhomeimage() {
        return $this->_scopeConfig->getValue('testimsection/testimhomegroup/showimage', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
    
    /* get enable testimonial website on home page */
    public function getShowhomewebsite() {
        return $this->_scopeConfig->getValue('testimsection/testimhomegroup/showwebsite', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
    
}
