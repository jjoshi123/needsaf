<?php

namespace Solwin\Testimonial\Block\Adminhtml\Testim\Edit\Tab;

/**
 * Testim edit form main tab
 */
class Main extends \Magento\Backend\Block\Widget\Form\Generic implements \Magento\Backend\Block\Widget\Tab\TabInterface {

    /**
     * @var \Magento\Store\Model\System\Store
     */
    protected $_systemStore;

    /**
     * @var \Magento\Cms\Model\Wysiwyg\Config
     */
    protected $_wysiwygConfig;

    /**
     * @var \Solwin\Testimonial\Model\Status
     */
    protected $_status;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Data\FormFactory $formFactory
     * @param \Magento\Store\Model\System\Store $systemStore
     * @param array $data
     */
    public function __construct(
    \Magento\Backend\Block\Template\Context $context, \Magento\Framework\Registry $registry, \Magento\Framework\Data\FormFactory $formFactory, \Magento\Store\Model\System\Store $systemStore, \Magento\Cms\Model\Wysiwyg\Config $wysiwygConfig, \Solwin\Testimonial\Model\Status $status, array $data = []
    ) {
        $this->_systemStore = $systemStore;
        $this->_wysiwygConfig = $wysiwygConfig;
        $this->_status = $status;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * Prepare form
     *
     * @return $this
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    protected function _prepareForm() {
        /* @var $model \Solwin\Testimonial\Model\TestimonialPages */
        $model = $this->_coreRegistry->registry('testimonial');

        $isElementDisabled = false;

        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create();

        $form->setHtmlIdPrefix('page_');

        $fieldset = $form->addFieldset('base_fieldset', ['legend' => __('Testim Information')]);

        if ($model->getId()) {
            $fieldset->addField('testimonial_id', 'hidden', ['name' => 'testimonial_id']);
        }

        $fieldset->addField(
                'status', 'select', [
            'label' => __('Status'),
            'title' => __('Status'),
            'name' => 'status',
            'required' => true,
            'options' => $this->_status->getOptionArray(),
            'disabled' => $isElementDisabled
                ]
        );
        if (!$model->getId()) {
            $model->setData('status', $isElementDisabled ? '0' : '1');
        }
        $dateFormat = date("Y-m-d h:i:sa");

        if ($model->getId()) {
            $fieldset->addField('update_time', 'hidden', ['name' => 'update_time']);
        }
        $model->setData('update_time', $dateFormat);
        
        $fieldset->addField(
                'name', 'text', [
            'name' => 'name',
            'label' => __('Name'),
            'title' => __('Name'),
            'required' => true,
            'disabled' => $isElementDisabled
                ]
        );

        $fieldset->addField(
                'email', 'text', [
            'name' => 'email',
            'label' => __('Email ID'),
            'title' => __('Email ID'),
            'required' => false,
            'disabled' => $isElementDisabled
                ]
        );

        $fieldset->addField(
                'company', 'text', [
            'name' => 'company',
            'label' => __('Company Name'),
            'title' => __('Company Name'),
            'required' => false,
            'disabled' => $isElementDisabled
                ]
        );

        $fieldset->addField(
                'website', 'text', [
            'name' => 'website',
            'label' => __('Website Url'),
            'title' => __('Website Url'),
            'required' => false,
            'disabled' => $isElementDisabled
                ]
        );

        $fieldset->addField(
                'image', 'image', [
            'name' => 'image',
            'label' => __('Profile Image'),
            'title' => __('Profile Image'),
            'required' => true,
            'disabled' => $isElementDisabled,
            'note' => '(*.jpg, *.png, *.gif)'
                ]
        );

        $wysiwygConfig = $this->_wysiwygConfig->getConfig(['tab_id' => $this->getTabId()]);

        $contentField = $fieldset->addField(
                'description', 'editor', [
            'name' => 'description',
            'label' => __('Description'),
            'title' => __('Description'),
            'style' => 'height:25em;',
            'required' => false,
            'disabled' => $isElementDisabled,
            'config' => $wysiwygConfig
                ]
        );

        // Setting custom renderer for content field to remove label column
        $renderer = $this->getLayout()->createBlock(
                        'Magento\Backend\Block\Widget\Form\Renderer\Fieldset\Element'
                )->setTemplate(
                'Magento_Cms::page/edit/form/renderer/content.phtml'
        );
        $contentField->setRenderer($renderer);

        $form->setValues($model->getData());
        $this->setForm($form);

        return parent::_prepareForm();
    }

    /**
     * Prepare label for tab
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabLabel() {
        return __('Testim Information');
    }

    /**
     * Prepare title for tab
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabTitle() {
        return __('Testim Information');
    }

    /**
     * {@inheritdoc}
     */
    public function canShowTab() {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function isHidden() {
        return false;
    }

    /**
     * Check permission for passed action
     *
     * @param string $resourceId
     * @return bool
     */
    protected function _isAllowedAction($resourceId) {
        return $this->_authorization->isAllowed($resourceId);
    }

}
