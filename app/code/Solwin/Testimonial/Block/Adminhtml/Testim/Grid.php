<?php

namespace Solwin\Testimonial\Block\Adminhtml\Testim;

class Grid extends \Magento\Backend\Block\Widget\Grid\Extended {

    /**
     * @var \Magento\Framework\Module\Manager
     */
    protected $moduleManager;

    /**
     * @var \Solwin\Testimonial\Model\TestimonialPagesFactory
     */
    protected $_testimMemberFactory;

    /**
     * @var \Solwin\Testimonial\Model\Status
     */
    protected $_status;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Backend\Helper\Data $backendHelper
     * @param \Solwin\Testimonial\Model\TestimonialPagesFactory $testimMemberFactory
     * @param \Solwin\Testimonial\Model\Status $status
     * @param \Magento\Framework\Module\Manager $moduleManager
     * @param array $data
     *
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
    \Magento\Backend\Block\Template\Context $context, \Magento\Backend\Helper\Data $backendHelper, \Solwin\Testimonial\Model\TestimonialPagesFactory $testimMemberFactory, \Solwin\Testimonial\Model\Status $status, \Magento\Framework\Module\Manager $moduleManager, array $data = []
    ) {
        $this->_testimMemberFactory = $testimMemberFactory;
        $this->_status = $status;
        $this->moduleManager = $moduleManager;
        parent::__construct($context, $backendHelper, $data);
    }

    /**
     * @return void
     */
    protected function _construct() {
        parent::_construct();
        $this->setId('postGrid');
        $this->setDefaultSort('testimonial_id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
        $this->setVarNameFilter('post_filter');
    }

    /**
     * @return $this
     */
    protected function _prepareCollection() {
        $collection = $this->_testimMemberFactory->create()->getCollection();
        $this->setCollection($collection);

        parent::_prepareCollection();
        return $this;
    }

    /**
     * @return $this
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    protected function _prepareColumns() {
        $this->addColumn(
                'testimonial_id', [
            'header' => __('ID'),
            'type' => 'number',
            'index' => 'testimonial_id',
            'header_css_class' => 'col-id',
            'column_css_class' => 'col-id'
                ]
        );
        
        $this->addColumn(
                'image', array(
            'header' => __('Image'),
            'index' => 'image',
            'class' => 'xxx',
            "renderer" => "Solwin\Testimonial\Block\Adminhtml\Testim\Renderer\Image",
                )
        );
        
        $this->addColumn(
                'name', [
            'header' => __('Name'),
            'index' => 'name',
            'class' => 'xxx'
                ]
        );

        $this->addColumn(
                'email', [
            'header' => __('Email'),
            'index' => 'email',
            'class' => 'xxx'
                ]
        );
        
        $this->addColumn(
                'company', [
            'header' => __('Company Name'),
            'index' => 'company',
            'class' => 'xxx'
                ]
        );
       
        $this->addColumn(
                'description', [
            'header' => __('Description'),
            'index' => 'description',
            'class' => 'xxx'
                ]
        );

        $this->addColumn(
                'status', [
            'header' => __('Status'),
            'index' => 'status',
            'type' => 'options',
            'options' => $this->_status->getOptionArray()
                ]
        );

        $this->addColumn(
                'edit', [
            'header' => __('Edit'),
            'type' => 'action',
            'getter' => 'getId',
            'actions' => [
                [
                    'caption' => __('Edit'),
                    'url' => [
                        'base' => '*/*/edit'
                    ],
                    'field' => 'testimonial_id'
                ]
            ],
            'filter' => false,
            'sortable' => false,
            'index' => 'stores',
            'header_css_class' => 'col-action',
            'column_css_class' => 'col-action'
                ]
        );

        $block = $this->getLayout()->getBlock('grid.bottom.links');
        if ($block) {
            $this->setChild('grid.bottom.links', $block);
        }

        return parent::_prepareColumns();
    }

    /**
     * @return $this
     */
    protected function _prepareMassaction() {
        $this->setMassactionIdField('testimonial_id');
        $this->getMassactionBlock()->setTemplate('Solwin_Testimonial::testimonial/grid/massaction_extended.phtml');
        $this->getMassactionBlock()->setFormFieldName('testimonial');

        $this->getMassactionBlock()->addItem(
                'delete', [
            'label' => __('Delete'),
            'url' => $this->getUrl('testimonial/*/massDelete'),
            'confirm' => __('Are you sure?')
                ]
        );

        $statuses = $this->_status->getOptionArray();

        array_unshift($statuses, ['label' => '', 'value' => '']);
        $this->getMassactionBlock()->addItem(
                'status', [
            'label' => __('Change status'),
            'url' => $this->getUrl('testimonial/*/massStatus', ['_current' => true]),
            'additional' => [
                'visibility' => [
                    'name' => 'status',
                    'type' => 'select',
                    'class' => 'required-entry',
                    'label' => __('Status'),
                    'values' => $statuses
                ]
            ]
                ]
        );


        return $this;
    }

    /**
     * @return string
     */
    public function getGridUrl() {
        return $this->getUrl('testimonial/*/grid', ['_current' => true]);
    }

    /**
     * @param \Solwin\Testimonial\Model\TestimonialPages|\Magento\Framework\Object $row
     * @return string
     */
    public function getRowUrl($row) {
        return $this->getUrl(
                        'testimonial/*/edit', ['testimonial_id' => $row->getId()]
        );
    }

}
