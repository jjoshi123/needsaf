<?php
namespace Solwin\Testimonial\Model\Resource;

class TestimonialPages extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('testimonial', 'testimonial_id');
    }
}