<?php
namespace Solwin\Testimonial\Model;

class TestimonialPages extends \Magento\Framework\Model\AbstractModel
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Solwin\Testimonial\Model\Resource\TestimonialPages');
    }
}