<?php

namespace Solwin\Ourblog\Controller\Comment;

//use Magento\Framework\App\Action\NotFoundException;
use Magento\Framework\App\RequestInterface;

class Save extends \Magento\Framework\App\Action\Action {

    protected $customerSession;

    /**
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder
     * @param \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     */
    public function __construct(
    \Magento\Framework\App\Action\Context $context, \Magento\Customer\Model\Session $customerSession, \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder, \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation, \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig, \Magento\Store\Model\StoreManagerInterface $storeManager
    ) {

        parent::__construct($context);
        $this->customerSession = $customerSession;
        $this->_transportBuilder = $transportBuilder;
        $this->inlineTranslation = $inlineTranslation;
        $this->scopeConfig = $scopeConfig;
        $this->storeManager = $storeManager;
    }

    /*
     * load ourblog.phtml file in frontend
     */

    public function getCustId() {
        return $this->customerSession->getCustomerId();
    }

    public function getCustName() {
        return $this->customerSession->getCustomer();
    }

    public function execute() {
        $data = $this->getRequest()->getPostValue();

        $resultRedirect = $this->resultRedirectFactory->create();
        if (!$data) {
            return $resultRedirect->setPath('ourblog/index/index');
        }

        try {
            $model = $this->_objectManager->create('Solwin\Ourblog\Model\CommentPosts');

            $error = false;

            if (!\Zend_Validate::is(trim($data['blog_id']), 'NotEmpty')) {
                $error = true;
            }

            if (!\Zend_Validate::is(trim($data['blog_title']), 'NotEmpty')) {
                $error = true;
            }

            if (!\Zend_Validate::is(trim($data['comment']), 'NotEmpty')) {
                $error = true;
            }

            if (!\Zend_Validate::is(trim($data['user_name']), 'NotEmpty')) {
                $error = true;
            }

            if ($error) {
                throw new \Exception();
            }

            $id = $this->getRequest()->getParam('comment_id');
            if ($id) {
                $model->load($id);
            }

            $cust_id = 0;
            $cust_name = '';
            $cust_id = $this->getCustId();
            if (isset($cust_id)) {
                $cust_id = $this->getCustId();
                $cust_name = $this->getCustName()->getFirstname() . ' '. $this->getCustName()->getLastname();
            } else {
                $cust_id = 0;
                $cust_name = 'Guest customer';
            }

            $model->setData($data);
            $model->setUserId($cust_id);
            $model->setCustomerName(ucfirst($cust_name));
            
            $model->save();

            $this->messageManager->addSuccess(__('The Comment has been saved.'));
            $this->_objectManager->get('Magento\Backend\Model\Session')->setFormData(false);
            return $resultRedirect->setPath('ourblog/index/index/id/' . $this->getRequest()->getParam('blog_id'));
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            $this->messageManager->addError($e->getMessage());
        } catch (\RuntimeException $e) {
            $this->messageManager->addError($e->getMessage());
        } catch (\Exception $e) {
            $this->messageManager->addException($e, __('Something went wrong while saving the Comment.'));
        }
        return $resultRedirect->setPath('ourblog/index/index/id/' . $this->getRequest()->getParam('blog_id'));
    }

}
