<?php

namespace Solwin\Ourblog\Controller\Adminhtml\Blog;

use Magento\Backend\App\Action;
use Magento\Framework\App\Filesystem\DirectoryList;

class Save extends \Magento\Backend\App\Action {

    protected $_uploaderFactory;

    /**
     * Filesystem facade
     *
     * @var \Magento\Framework\Filesystem
     */
    protected $_filesystem;

    /**
     * File Uploader factory
     *
     * @var \Magento\MediaStorage\Model\File\UploaderFactory
     */
    protected $_fileUploaderFactory;

    /**
     * @param Action\Context $context
     */
    public function __construct(Action\Context $context, \Magento\Framework\Filesystem $filesystem, \Magento\MediaStorage\Model\File\UploaderFactory $fileUploaderFactory) {
        $this->_filesystem = $filesystem;
        $this->_fileUploaderFactory = $fileUploaderFactory;
        parent::__construct($context);
    }

    /**
     * Save action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute() {
        $data = $this->getRequest()->getPostValue();

//        echo "<pre>";
//        print_r($data);
//        exit;


        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($data) {
            $model = $this->_objectManager->create('Solwin\Ourblog\Model\BlogPosts');

            try {

                if (isset($data['image']['delete']) && $data['image']['delete'] == 1) {

                    // Remove Image from the directory and Database
                    $deletepath = $this->_filesystem->getDirectoryRead(
                                    DirectoryList::MEDIA
                            )->getAbsolutePath('');

                    // Un link image
                    if (isset($data['image']['value']) && $data['image']['value'] != '') {
                        unlink($deletepath . '/' . $data['image']['value']);
                    }

                    //unset image data
                    $data['image'] = '';
                } else {
                    unset($data['image']);
                    if (isset($_FILES)) {
                        if ($_FILES['image']['name']) {
                            // Image Saving Code
                            $uploader = $this->_fileUploaderFactory->create(['fileId' => 'image']);
                            $uploader->setAllowedExtensions(['jpg', 'jpeg', 'gif', 'png']);
                            $uploader->setAllowRenameFiles(false);
                            $uploader->setFilesDispersion(true);

                            $path = $this->_filesystem->getDirectoryRead(
                                            DirectoryList::MEDIA
                                    )->getAbsolutePath(
                                    'ourblog/'
                            );
                            $result = $uploader->save($path);
                            $data['image'] = 'ourblog' . $result['file'];
                        }
                    }
                }
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }

            $id = $this->getRequest()->getParam('blog_id');
            if ($id) {
                $model->load($id);
            }

            $model->setData($data);

            //set publish date
            //$is_active = $data['is_active'];
            $dateFormat = date("Y-m-d h:i:sa");
            if (isset($id)) {
                $model->setData('publish_date', $data['update_time']);
            }else{
                $model->setData('publish_date', $dateFormat);
            }
            try {
                $model->save();
                $this->messageManager->addSuccess(__('The Blog has been saved.'));
                $this->_objectManager->get('Magento\Backend\Model\Session')->setFormData(false);
                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['blog_id' => $model->getId(), '_current' => true]);
                }
                return $resultRedirect->setPath('*/*/');
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\RuntimeException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addException($e, __('Something went wrong while saving the Blog.'));
            }

            $this->_getSession()->setFormData($data);
            return $resultRedirect->setPath('*/*/edit', ['blog_id' => $this->getRequest()->getParam('blog_id')]);
        }
        return $resultRedirect->setPath('*/*/');
    }

}
