<?php
namespace Solwin\Ourblog\Controller\Adminhtml\Comment;

use Magento\Backend\App\Action;

/**
 * Class MassDelete
 */
class MassDelete extends \Magento\Backend\App\Action
{
    /**
     * @return \Magento\Backend\Model\View\Result\Redirect
     */
    public function execute()
    {
        $blogpostIds = $this->getRequest()->getParam('commentpost');
        if (!is_array($blogpostIds) || empty($blogpostIds)) {
            $this->messageManager->addError(__('Please select Comment(s).'));
        } else {
            try {
                foreach ($blogpostIds as $postId) {
                    $post = $this->_objectManager->get('Solwin\Ourblog\Model\CommentPosts')->load($postId);
                    $post->delete();
                }
                $this->messageManager->addSuccess(
                    __('A total of %1 record(s) have been deleted.', count($blogpostIds))
                );
            } catch (\Exception $e) {
                $this->messageManager->addError($e->getMessage());
            }
        }
        return $this->resultRedirectFactory->create()->setPath('ourblog/*/index');
    }
}
