<?php
namespace Solwin\Ourblog\Controller\Adminhtml\Category;

use Magento\Backend\App\Action;

class MassStatus extends \Magento\Backend\App\Action
{
    /**
     * Update Category(s) status action
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     * @throws \Magento\Framework\Exception\LocalizedException|\Exception
     */
    public function execute()
    {
        $blogpostIds = $this->getRequest()->getParam('categorypost');
        if (!is_array($blogpostIds) || empty($blogpostIds)) {
            $this->messageManager->addError(__('Please select Category(s).'));
        } else {
            try {
                $status = (int) $this->getRequest()->getParam('status');
                foreach ($blogpostIds as $postId) {
                    $post = $this->_objectManager->get('Solwin\Ourblog\Model\CategoryPosts')->load($postId);
                    $post->setCatActive($status)->save();
                }
                $this->messageManager->addSuccess(
                    __('A total of %1 record(s) have been updated.', count($blogpostIds))
                );
            } catch (\Exception $e) {
                $this->messageManager->addError($e->getMessage());
            }
        }
        return $this->resultRedirectFactory->create()->setPath('ourblog/*/index');
    }

}
