<?php

namespace Solwin\Ourblog\Helper;

class Data extends \Magento\Framework\App\Helper\AbstractHelper {

    public function __construct(
        \Magento\Framework\App\Helper\Context $context
    ) {
        parent::__construct($context);
    }

    /* get Extension enable/disable */

    public function getEnableModule() {
        return $this->scopeConfig->getValue('blogsection/bloggroup/active', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

}
