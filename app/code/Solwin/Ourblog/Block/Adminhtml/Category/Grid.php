<?php

namespace Solwin\Ourblog\Block\Adminhtml\Category;

class Grid extends \Magento\Backend\Block\Widget\Grid\Extended {

    /**
     * @var \Magento\Framework\Module\Manager
     */
    protected $moduleManager;

    /**
     * @var \Solwin\Ourblog\Model\CategoryPostsFactory
     */
    protected $_blogPostFactory;

    /**
     * @var \Solwin\Ourblog\Model\Status
     */
    protected $_status;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Backend\Helper\Data $backendHelper
     * @param \Solwin\Ourblog\Model\CategoryPostsFactory $blogPostFactory
     * @param \Solwin\Ourblog\Model\Status $status
     * @param \Magento\Framework\Module\Manager $moduleManager
     * @param array $data
     *
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
    \Magento\Backend\Block\Template\Context $context, \Magento\Backend\Helper\Data $backendHelper, \Solwin\Ourblog\Model\CategoryPostsFactory $blogPostFactory, \Solwin\Ourblog\Model\Status $status, \Magento\Framework\Module\Manager $moduleManager, array $data = []
    ) {
        $this->_blogPostFactory = $blogPostFactory;
        $this->_status = $status;
        $this->moduleManager = $moduleManager;
        parent::__construct($context, $backendHelper, $data);
    }

    /**
     * @return void
     */
    protected function _construct() {
        parent::_construct();
        $this->setId('postGrid');
        $this->setDefaultSort('cat_id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
        $this->setVarNameFilter('post_filter');
    }

    /**
     * @return $this
     */
    protected function _prepareCollection() {
        $collection = $this->_blogPostFactory->create()->getCollection();
        $this->setCollection($collection);

        parent::_prepareCollection();
        return $this;
    }

    /**
     * @return $this
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    protected function _prepareColumns() {
        $this->addColumn(
                'cat_id', [
            'header' => __('ID'),
            'type' => 'number',
            'index' => 'cat_id',
            'header_css_class' => 'col-id',
            'column_css_class' => 'col-id'
                ]
        );
        $this->addColumn(
                'cat_name', [
            'header' => __('Category'),
            'index' => 'cat_name',
            'class' => 'xxx'
                ]
        );

        $this->addColumn(
                'cat_active', [
            'header' => __('Status'),
            'index' => 'cat_active',
            'type' => 'options',
            'options' => $this->_status->getOptionArray()
                ]
        );

        $this->addColumn(
                'edit', [
            'header' => __('Edit'),
            'type' => 'action',
            'getter' => 'getId',
            'actions' => [
                [
                    'caption' => __('Edit'),
                    'url' => [
                        'base' => '*/*/edit'
                    ],
                    'field' => 'cat_id'
                ]
            ],
            'filter' => false,
            'sortable' => false,
            'index' => 'stores',
            'header_css_class' => 'col-action',
            'column_css_class' => 'col-action'
                ]
        );

        $block = $this->getLayout()->getBlock('grid.bottom.links');
        if ($block) {
            $this->setChild('grid.bottom.links', $block);
        }

        return parent::_prepareColumns();
    }

    /**
     * @return $this
     */
    protected function _prepareMassaction() {
        $this->setMassactionIdField('cat_id');
        $this->getMassactionBlock()->setTemplate('Solwin_Ourblog::ourblog/grid/massaction_extended.phtml');
        $this->getMassactionBlock()->setFormFieldName('categorypost');

        $this->getMassactionBlock()->addItem(
                'delete', [
            'label' => __('Delete'),
            'url' => $this->getUrl('ourblog/*/massDelete'),
            'confirm' => __('Are you sure?')
                ]
        );

        $statuses = $this->_status->getOptionArray();

        array_unshift($statuses, ['label' => '', 'value' => '']);
        $this->getMassactionBlock()->addItem(
                'status', [
            'label' => __('Change status'),
            'url' => $this->getUrl('ourblog/*/massStatus', ['_current' => true]),
            'additional' => [
                'visibility' => [
                    'name' => 'status',
                    'type' => 'select',
                    'class' => 'required-entry',
                    'label' => __('Status'),
                    'values' => $statuses
                ]
            ]
                ]
        );


        return $this;
    }

    /**
     * @return string
     */
    public function getGridUrl() {
        return $this->getUrl('ourblog/*/grid', ['_current' => true]);
    }

    /**
     * @param \Solwin\Ourblog\Model\CategoryPosts|\Magento\Framework\Object $row
     * @return string
     */
    public function getRowUrl($row) {
        return $this->getUrl(
                        'ourblog/*/edit', ['cat_id' => $row->getId()]
        );
    }

}
