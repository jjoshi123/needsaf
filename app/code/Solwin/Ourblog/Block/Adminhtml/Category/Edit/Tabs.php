<?php
namespace Solwin\Ourblog\Block\Adminhtml\Category\Edit;

/**
 * Admin page left menu
 */
class Tabs extends \Magento\Backend\Block\Widget\Tabs
{
    /**
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('post_cattabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(__('Category Information'));
    }
}
