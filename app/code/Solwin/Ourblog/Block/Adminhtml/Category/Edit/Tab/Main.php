<?php

namespace Solwin\Ourblog\Block\Adminhtml\Category\Edit\Tab;

/**
 * Category edit form main tab
 */
class Main extends \Magento\Backend\Block\Widget\Form\Generic implements \Magento\Backend\Block\Widget\Tab\TabInterface {

    /**
     * @var \Magento\Store\Model\System\Store
     */
    protected $_systemStore;

    /**
     * @var \Magento\Cms\Model\Wysiwyg\Config
     */
    protected $_wysiwygConfig;

    /**
     * @var \Solwin\Ourblog\Model\Status
     */
    protected $_status;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Data\FormFactory $formFactory
     * @param \Magento\Store\Model\System\Store $systemStore
     * @param array $data
     */
    public function __construct(
    \Magento\Backend\Block\Template\Context $context, 
            \Magento\Framework\Registry $registry, 
            \Magento\Framework\Data\FormFactory $formFactory, 
            \Magento\Store\Model\System\Store $systemStore, 
            \Magento\Cms\Model\Wysiwyg\Config $wysiwygConfig,
            \Solwin\Ourblog\Model\Status $status, 
            array $data = []
    ) {
        $this->_systemStore = $systemStore;
        $this->_wysiwygConfig = $wysiwygConfig;
        $this->_status = $status;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * Prepare form
     *
     * @return $this
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    protected function _prepareForm() {
        /* @var $model \Solwin\Ourblog\Model\CategoryPosts */
        $model = $this->_coreRegistry->registry('our_category');

        $isElementDisabled = false;

        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create();

        $form->setHtmlIdPrefix('page_');

        $fieldset = $form->addFieldset('base_fieldset', ['legend' => __('Category Information')]);

        if ($model->getId()) {
            $fieldset->addField('cat_id', 'hidden', ['name' => 'cat_id']);
        }

        $fieldset->addField(
                'cat_active', 'select', [
            'label' => __('Status'),
            'title' => __('Status'),
            'name' => 'cat_active',
            'required' => true,
            'options' => $this->_status->getOptionArray(),
            'disabled' => $isElementDisabled
                ]
        );
        if (!$model->getId()) {
            $model->setData('status', $isElementDisabled ? '0' : '1');
        }

        $fieldset->addField(
                'cat_name', 'text', [
            'name' => 'cat_name',
            'label' => __('Category'),
            'title' => __('Category'),
            'required' => true,
            'disabled' => $isElementDisabled
                ]
        );

        // Setting custom renderer for content field to remove label column
//        $renderer = $this->getLayout()->createBlock(
//                        'Magento\Backend\Block\Widget\Form\Renderer\Fieldset\Element'
//                )->setTemplate(
//                'Magento_Cms::page/edit/form/renderer/content.phtml'
//        );
//       $contentField->setRenderer($renderer);
//        $descField->setRenderer($renderer);

        $form->setValues($model->getData());
        $this->setForm($form);

        return parent::_prepareForm();
    }

    /**
     * Prepare label for tab
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabLabel() {
        return __('Category Information');
    }

    /**
     * Prepare title for tab
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabTitle() {
        return __('Category Information');
    }

    /**
     * {@inheritdoc}
     */
    public function canShowTab() {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function isHidden() {
        return false;
    }

    /**
     * Check permission for passed action
     *
     * @param string $resourceId
     * @return bool
     */
    protected function _isAllowedAction($resourceId) {
        return $this->_authorization->isAllowed($resourceId);
    }

}
