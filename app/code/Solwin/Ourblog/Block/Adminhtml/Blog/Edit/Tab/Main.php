<?php

namespace Solwin\Ourblog\Block\Adminhtml\Blog\Edit\Tab;

use Solwin\Ourblog\Model\CategoryPostsFactory;

/**
 * Blog edit form main tab
 */
class Main extends \Magento\Backend\Block\Widget\Form\Generic implements \Magento\Backend\Block\Widget\Tab\TabInterface {

    /**
     * @var \Magento\Store\Model\System\Store
     */
    protected $_systemStore;

    /**
     * @var \Magento\Cms\Model\Wysiwyg\Config
     */
    protected $_wysiwygConfig;

    /**
     * @var \Solwin\Ourblog\Model\Status
     */
    protected $_status;
    protected $_categorylist;
    protected $_collection;
    protected $_modelCategoryPostsFactory;
    protected $backendAuthSession;
    protected $_session;
    protected $_backendAuthSession;

    /**
     * 
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Data\FormFactory $formFactory
     * @param \Magento\Store\Model\System\Store $systemStore
     * @param array $data
     */
    public function __construct(
    \Magento\Backend\Block\Template\Context $context, 
            \Magento\Backend\Model\Auth\Session $backendAuthSession, 
            \Magento\Framework\Session\SessionManagerInterface $session, 
            CategoryPostsFactory $modelCategoryPostsFactory, 
            \Magento\Framework\Registry $registry, 
            \Magento\Framework\Data\FormFactory $formFactory, 
            \Magento\Store\Model\System\Store $systemStore, 
            \Magento\Cms\Model\Wysiwyg\Config $wysiwygConfig, 
            \Solwin\Ourblog\Model\Status $status, 
            \Solwin\Ourblog\Model\Categorylist $categorylist, 
            \Solwin\Ourblog\Model\Resource\CategoryPosts\Collection $collection, 
            array $data = []
    ) {
        $this->_modelCategoryPostsFactory = $modelCategoryPostsFactory;
        $this->_session = $session;
        $this->_systemStore = $systemStore;
        $this->_wysiwygConfig = $wysiwygConfig;
        $this->_status = $status;
        $this->_categorylist = $categorylist;
        $this->_collection = $collection;
        parent::__construct($context, $registry, $formFactory, $data);

        $this->_backendAuthSession = $backendAuthSession;
//        $collections = $this->_modelCategoryPostsFactory->create()->getCollection();
//        $this->setCollection($collections);
    }

    /**
     * Prepare form
     *
     * @return $this
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    protected function _prepareForm() {
        /* @var $model \Solwin\Ourblog\Model\BlogPosts */
        $model = $this->_coreRegistry->registry('our_blog');

        $isElementDisabled = false;

        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create();

        $form->setHtmlIdPrefix('page_');

        $fieldset = $form->addFieldset('base_fieldset', ['legend' => __('Blog Information')]);

        if ($model->getId()) {
            $fieldset->addField('blog_id', 'hidden', ['name' => 'blog_id']);
        }

        $user = $this->_backendAuthSession->isLoggedIn();
        
        $fieldset->addField('createdby', 'hidden', ['name' => 'createdby']);
        $model->setData('createdby', $user);

        $fieldset->addField(
                'is_active', 'select', [
            'label' => __('Status'),
            'title' => __('Status'),
            'name' => 'is_active',
            'required' => true,
            'options' => $this->_status->getOptionArray(),
            'disabled' => $isElementDisabled
                ]
        );
        if (!$model->getId()) {
            $model->setData('status', $isElementDisabled ? '0' : '1');
        }

        $fieldset->addField(
                'title', 'text', [
            'name' => 'title',
            'label' => __('Title'),
            'title' => __('Title'),
            'required' => true,
            'disabled' => $isElementDisabled
                ]
        );

        $fieldset->addField(
                'category', 'select', [
            'name' => 'category',
            'label' => __('Category'),
            'title' => __('Category'),
            'required' => false,
            'options' => $this->_categorylist->getOptionArray(),
            'disabled' => $isElementDisabled
                ]
        );

        $fieldset->addField(
                'tags', 'text', [
            'name' => 'tags',
            'label' => __('Tags'),
            'title' => __('Tags'),
            'required' => false,
            'disabled' => $isElementDisabled
                ]
        );

        $fieldset->addField(
                'embad_code', 'text', [
            'name' => 'embad_code',
            'label' => __('Embed Code'),
            'title' => __('Embed Code'),
            'required' => false,
            'disabled' => $isElementDisabled
                ]
        );

        $fieldset->addField(
                'image', 'image', [
            'name' => 'image',
            'label' => __('Profile Image'),
            'title' => __('Profile Image'),
            'required' => true,
            'disabled' => $isElementDisabled,
            'note' => '(*.jpg, *.png, *.gif)'
                ]
        );
        
         

        $wysiwygConfig = $this->_wysiwygConfig->getConfig(['tab_id' => $this->getTabId()]);

        $contentField = $fieldset->addField(
                'description', 'editor', [
            'name' => 'description',
            'label' => __('Description'),
            'title' => __('Description'),
            'style' => 'height:25em;',
            'required' => false,
            'disabled' => $isElementDisabled,
            'config' => $wysiwygConfig
                ]
        );

        $descField = $fieldset->addField(
                'content', 'editor', [
            'name' => 'content',
            'label' => __('Content'),
            'title' => __('Content'),
            'style' => 'height:25em;',
            'required' => false,
            'disabled' => $isElementDisabled,
            'config' => $wysiwygConfig
                ]
        );

        if ($model->getId()) {
            $fieldset->addField('update_time', 'hidden', ['name' => 'update_time']);
        }
        $dateFormat = date("Y-m-d h:i:sa");
        $model->setData('update_time', $dateFormat);


        // Setting custom renderer for content field to remove label column
        $renderer = $this->getLayout()->createBlock(
                        'Magento\Backend\Block\Widget\Form\Renderer\Fieldset\Element'
                )->setTemplate(
                'Magento_Cms::page/edit/form/renderer/content.phtml'
        );
        $contentField->setRenderer($renderer);
        $descField->setRenderer($renderer);

        $form->setValues($model->getData());
        $this->setForm($form);

        return parent::_prepareForm();
    }

    /**
     * Prepare label for tab
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabLabel() {
        return __('Blog Information');
    }

    /**
     * Prepare title for tab
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabTitle() {
        return __('Blog Information');
    }

    /**
     * {@inheritdoc}
     */
    public function canShowTab() {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function isHidden() {
        return false;
    }

    /**
     * Check permission for passed action
     *
     * @param string $resourceId
     * @return bool
     */
    protected function _isAllowedAction($resourceId) {
        return $this->_authorization->isAllowed($resourceId);
    }

}
