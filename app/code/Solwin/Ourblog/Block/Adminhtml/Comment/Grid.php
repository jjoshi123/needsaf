<?php

namespace Solwin\Ourblog\Block\Adminhtml\Comment;

class Grid extends \Magento\Backend\Block\Widget\Grid\Extended {

    /**
     * @var \Magento\Framework\Module\Manager
     */
    protected $moduleManager;

    /**
     * @var \Solwin\Ourblog\Model\CommentPostsFactory
     */
    protected $_blogPostFactory;

    /**
     * @var \Solwin\Ourblog\Model\Status
     */
    protected $_status;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Backend\Helper\Data $backendHelper
     * @param \Solwin\Ourblog\Model\CommentPostsFactory $blogPostFactory
     * @param \Solwin\Ourblog\Model\Status $status
     * @param \Magento\Framework\Module\Manager $moduleManager
     * @param array $data
     *
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
    \Magento\Backend\Block\Template\Context $context, \Magento\Backend\Helper\Data $backendHelper, \Solwin\Ourblog\Model\CommentPostsFactory $blogPostFactory, \Solwin\Ourblog\Model\Status $status, \Magento\Framework\Module\Manager $moduleManager, array $data = []
    ) {
        $this->_blogPostFactory = $blogPostFactory;
        $this->_status = $status;
        $this->moduleManager = $moduleManager;
        parent::__construct($context, $backendHelper, $data);
    }

    /**
     * @return void
     */
    protected function _construct() {
        parent::_construct();
        $this->setId('postGrid');
        $this->setDefaultSort('comment_id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
        $this->setVarNameFilter('post_filter');
    }

    /**
     * @return $this
     */
    protected function _prepareCollection() {
        $collection = $this->_blogPostFactory->create()->getCollection();
        $this->setCollection($collection);

        parent::_prepareCollection();
        return $this;
    }

    /**
     * @return $this
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    protected function _prepareColumns() {
        $this->addColumn(
                'comment_id', [
            'header' => __('ID'),
            'type' => 'number',
            'index' => 'comment_id',
            'header_css_class' => 'col-id',
            'column_css_class' => 'col-id'
                ]
        );
        
        $this->addColumn(
                'user_name', [
            'header' => __('Name'),
            'index' => 'user_name',
            'class' => 'xxx'
                ]
        );
        
        $this->addColumn(
                'blog_title', [
            'header' => __('Blog Title'),
            'index' => 'blog_title',
            'class' => 'xxx'
                ]
        );
        
        $this->addColumn(
                'comment', [
            'header' => __('Comment'),
            'index' => 'comment',
            'class' => 'xxx'
                ]
        );

        $this->addColumn(
                'status', [
            'header' => __('Status'),
            'index' => 'status',
            'type' => 'options',
            'options' => $this->_status->getOptionArray()
                ]
        );

        $this->addColumn(
                'edit', [
            'header' => __('Edit'),
            'type' => 'action',
            'getter' => 'getId',
            'actions' => [
                [
                    'caption' => __('Edit'),
                    'url' => [
                        'base' => '*/*/edit'
                    ],
                    'field' => 'comment_id'
                ]
            ],
            'filter' => false,
            'sortable' => false,
            'index' => 'stores',
            'header_css_class' => 'col-action',
            'column_css_class' => 'col-action'
                ]
        );

        $block = $this->getLayout()->getBlock('grid.bottom.links');
        if ($block) {
            $this->setChild('grid.bottom.links', $block);
        }

        return parent::_prepareColumns();
    }

    /**
     * @return $this
     */
    protected function _prepareMassaction() {
        $this->setMassactionIdField('comment_id');
        $this->getMassactionBlock()->setTemplate('Solwin_Ourblog::ourblog/grid/massaction_extended.phtml');
        $this->getMassactionBlock()->setFormFieldName('commentpost');

        $this->getMassactionBlock()->addItem(
                'delete', [
            'label' => __('Delete'),
            'url' => $this->getUrl('ourblog/*/massDelete'),
            'confirm' => __('Are you sure?')
                ]
        );

        $statuses = $this->_status->getOptionArray();

        array_unshift($statuses, ['label' => '', 'value' => '']);
        $this->getMassactionBlock()->addItem(
                'status', [
            'label' => __('Change status'),
            'url' => $this->getUrl('ourblog/*/massStatus', ['_current' => true]),
            'additional' => [
                'visibility' => [
                    'name' => 'status',
                    'type' => 'select',
                    'class' => 'required-entry',
                    'label' => __('Status'),
                    'values' => $statuses
                ]
            ]
                ]
        );


        return $this;
    }

    /**
     * @return string
     */
    public function getGridUrl() {
        return $this->getUrl('ourblog/*/grid', ['_current' => true]);
    }

    /**
     * @param \Solwin\Ourblog\Model\CommentPosts|\Magento\Framework\Object $row
     * @return string
     */
    public function getRowUrl($row) {
        return $this->getUrl(
                        'ourblog/*/edit', ['comment_id' => $row->getId()]
        );
    }

}
