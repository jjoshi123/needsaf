<?php
namespace Solwin\Ourblog\Block\Adminhtml\Comment\Edit;

/**
 * Admin page left menu
 */
class Tabs extends \Magento\Backend\Block\Widget\Tabs
{
    /**
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('post_comtabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(__('Comment Information'));
    }
}
