<?php

namespace Solwin\Ourblog\Block;

use Solwin\Ourblog\Model\BlogPostsFactory;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\View\Element\Template;

class Ourblog extends Template
{
    protected $_collection;
    protected $_catcollection;
    protected $_comcollection;
    protected $_modelBlogPostsFactory;
    protected $customerSession;
    protected $_resource;
    protected $connection;
    
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        BlogPostsFactory $modelBlogPostsFactory,
        \Magento\Framework\App\ResourceConnection $resource,
        \Magento\Customer\Model\Session $customerSession,
        \Solwin\Ourblog\Model\Resource\BlogPosts\Collection $collection,
        \Solwin\Ourblog\Model\Resource\CategoryPosts\Collection $catcollection,
        \Solwin\Ourblog\Model\Resource\CommentPosts\Collection $comcollection,
        array $data = []
    ) {
        $this->_modelBlogPostsFactory = $modelBlogPostsFactory;
        $this->_collection = $collection;
        $this->_catcollection = $catcollection;
        $this->_comcollection = $comcollection;
        $this->customerSession = $customerSession;
        $this->_resource = $resource;
        parent::__construct($context, $data);
        $blogcollection = $this->_modelBlogPostsFactory->create()->getCollection();
        $this->setCollection($blogcollection);
        $this->pageConfig->getTitle()->set(__('Blog'));
    }

    protected function _prepareLayout() {
        parent::_prepareLayout();
        if ($this->getCollection()) {
            $bloglimit = self::getPagerbloglimit();
            // create pager block for collection
            $pager = $this->getLayout()->createBlock(
                            'Magento\Theme\Block\Html\Pager', 'ourblogs.grid.record.pager'
                    )->setLimit($bloglimit)->setCollection(
                    $this->getCollection()->addFieldToFilter('is_active', 1)
            );
            $this->setChild('pager', $pager);
        }
        return $this;
    }

    public function getPagerHtml() {
        return $this->getChildHtml('pager');
    }

    public function getPagerbloglimit() {
        return $this->_scopeConfig->getValue('blogsection/postpagegroup/bloglistlimit', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    public function getLoggendinCustomerId() {
        return $this->customerSession->getCustomerId();
    }

    public function getCategoryCollection() {
        $catCollection = $this->_catcollection;
        return $catCollection;
    }

    public function getCommentsCollection($blog_id, $commentLimit) {
        $comCollection = $this->_comcollection
                ->addFieldToFilter('blog_id', $blog_id)
                ->addFieldToFilter('status', 1);
        $comCollection->getSelect()->limit($commentLimit);
        return $comCollection;
    }

    public function getSingleBlogCollection($blog_id) {
        $blogModel = $this->_modelBlogPostsFactory->create();
        $blogCollection = $blogModel->load($blog_id);
        return $blogCollection;
    }

    public function getBlogTagsCollection($blogtags) {
        $tagCollection = $this->getCollection()
                ->addFieldToFilter('tags', array('finset' => $blogtags));
        return $tagCollection;
    }

    public function getFormAction() {
        return $this->getUrl('ourblog/comment/save');
    }

    public function getEnableModule() {
        return $this->_scopeConfig->getValue('blogsection/bloggroup/active', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /* get show blog image */
    public function getShowimage() {
        return $this->_scopeConfig->getValue('blogsection/postpagegroup/showimage', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /* get show blog title */
    public function getShowtitle() {
        return $this->_scopeConfig->getValue('blogsection/postpagegroup/showtitle', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /* get show blog content */
    public function getShowcontent() {
        return $this->_scopeConfig->getValue('blogsection/postpagegroup/showcontent', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /* get show readmore btn */

    public function getShowreadmore() {
        return $this->_scopeConfig->getValue('blogsection/postpagegroup/showreadmore', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /* get show blog tags  */

    public function getShowtags() {
        return $this->_scopeConfig->getValue('blogsection/postpagegroup/showtags', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /* get show blog date */

    public function getShowdate() {
        return $this->_scopeConfig->getValue('blogsection/postpagegroup/showdate', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /* blog detail page config */

    /* get show blog image on detail page */

    public function getIshowimage() {
        return $this->_scopeConfig->getValue('blogsection/ipostpagegroup/ishowimage', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /* get blog title on blog detail page */

    public function getishowtitle() {
        return $this->_scopeConfig->getValue('blogsection/ipostpagegroup/ishowtitle', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /* get show blog content on detail page */

    public function getIshowcontent() {
        return $this->_scopeConfig->getValue('blogsection/ipostpagegroup/ishowcontent', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /* get show blog description on detail page */

    public function getIshowdesc() {
        return $this->_scopeConfig->getValue('blogsection/ipostpagegroup/ishowdesc', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /* get show blog tags on detail page */

    public function getIshowtags() {
        return $this->_scopeConfig->getValue('blogsection/ipostpagegroup/ishowtags', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /* get show blog date on detail page */

    public function getIshowdate() {
        return $this->_scopeConfig->getValue('blogsection/ipostpagegroup/ishowdate', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /* get allow to add blog comment on detail page */

    public function getIsaddcomment() {
        return $this->_scopeConfig->getValue('blogsection/ipostpagegroup/iaddcomment', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /* get allow to show blog comment on detail page */

    public function getIsshowcomment() {
        return $this->_scopeConfig->getValue('blogsection/ipostpagegroup/ishowcomment', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /* get comment limit on detail page */

    public function getCommentlimit() {
        return $this->_scopeConfig->getValue('blogsection/ipostpagegroup/icommentlimit', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /* get comment count on detail page */

    public function getCountcomment() {
        return $this->_scopeConfig->getValue('blogsection/ipostpagegroup/icountcomment', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /* get comment user on detail page */

    public function getCommentname() {
        return $this->_scopeConfig->getValue('blogsection/ipostpagegroup/icommentname', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /* get comment title on detail page */

    public function getCommenttitle() {
        return $this->_scopeConfig->getValue('blogsection/ipostpagegroup/icommenttitle', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /* get comment description on detail page */

    public function getCommentdesc() {
        return $this->_scopeConfig->getValue('blogsection/ipostpagegroup/icommentdesc', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /* get comment date on detail page */

    public function getCommentdate() {
        return $this->_scopeConfig->getValue('blogsection/ipostpagegroup/icommentdate', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    public function getBaseUrl() {
        return $this->_storeManager->getStore()->getBaseUrl();
    }

    public function getMediaPath() {
        return $this->_filesystem->getDirectoryRead(
                        DirectoryList::MEDIA
                )->getAbsolutePath('');
    }

    public function getBannerCss() {
        return $this->_assetRepo->getUrl('Solwin_Ourblog::css/');
    }

    public function getBannerJs() {
        return $this->_assetRepo->getUrl('Solwin_Ourblog::js/');
    }

    public function getDefaultImage() {
        return $this->_assetRepo->getUrl('Solwin_Ourblog::images/userlogo.png');
    }

    protected function getConnection() {
        if (!$this->connection) {
            $this->connection = $this->_resource->getConnection('core_write');
        }
        return $this->connection;
    }

    public function getCustomCommentsCollection($blog_id) {

        $connection = $this->getConnection();
        $select = $connection->select()
                ->from(['c' => 'blog_comment'], ['c.comment_id', new \Zend_Db_Expr('COUNT(*) as count')])
                ->where('c.blog_id=?', $blog_id)
                ->where('c.status=?', 1);

        $countRow = $connection->fetchRow($select);
        return $countRow['count'];
    }

}
