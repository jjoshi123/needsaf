<?php

namespace Solwin\Ourblog\Block;

use Solwin\Ourblog\Model\BlogPostsFactory;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\View\Element\Template;

class Bloghome extends Template {

    protected $_collection;
    protected $_catcollection;
    protected $_comcollection;
    protected $_modelBlogPostsFactory;
    protected $customerSession;
    protected $_resource;
    protected $connection;
    
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        BlogPostsFactory $modelBlogPostsFactory,
        \Magento\Customer\Model\Session $customerSession,
        \Solwin\Ourblog\Model\Resource\BlogPosts\Collection $collection,
        \Solwin\Ourblog\Model\Resource\CategoryPosts\Collection $catcollection,
        \Solwin\Ourblog\Model\Resource\CommentPosts\Collection $comcollection,
        \Magento\Framework\App\ResourceConnection $resource,
        array $data = []
    ) {
        $this->_modelBlogPostsFactory = $modelBlogPostsFactory;
        $this->_collection = $collection;
        $this->_catcollection = $catcollection;
        $this->_comcollection = $comcollection;
        $this->customerSession = $customerSession;
        $this->_resource = $resource;
        parent::__construct($context, $data);
        $blogcollection = $this->_modelBlogPostsFactory->create()->getCollection();
        $this->setCollection($blogcollection);
    }

    protected function _prepareLayout() {
        parent::_prepareLayout();
        if ($this->getCollection()) {
            
            // create pager block for collection
            $pager = $this->getLayout()->createBlock(
                            'Magento\Theme\Block\Html\Pager', 'ourblog.grid.record.pager'
                    )->setCollection(
                    $this->getCollection()->addFieldToFilter('is_active', 1) // assign collection to pager
            );
            $this->setChild('pager', $pager); // set pager block in layout
        }
        return $this;
    }

    public function getPagerHtml() {
        return $this->getChildHtml('pager');
    }

    public function getLoggendinCustomerId() {
        return $this->customerSession->getCustomerId();
    }

    public function getCategoryCollection() {
        $catCollection = $this->_catcollection;
//                ->addFieldToFilter('status', 1);
        return $catCollection;
    }
    
    public function getBlogCollection() {
        $blog_limit = self::getHomedisplaylimit();
        $catCollection = $this->_collection
                ->addFieldToFilter('is_active', 1);
        
        $catCollection->getSelect()->limit($blog_limit);
        return $catCollection;
    }

    public function getCommentsCollection($blog_id, $commentLimit) {
        $comCollection = $this->_comcollection
                ->addFieldToFilter('blog_id', $blog_id)
                ->addFieldToFilter('status', 1);
        $comCollection->getSelect()->limit($commentLimit);
        return $comCollection;
    }

    public function getSingleBlogCollection($blog_id) {
        $blogModel = $this->_modelBlogPostsFactory->create();
        $blogCollection = $blogModel->load($blog_id);
        return $blogCollection;
    }

    public function getBlogTagsCollection($blogtags) {
        $tagCollection = $this->_collection->addFieldToFilter('tags', array('finset' => $blogtags))
                ->addFieldToFilter('is_active', 1);
        return $tagCollection;
    }

    public function getFormAction() {
        return $this->getUrl('ourblog/comment/save');
    }

    /* get extension enable/disable */
    public function getEnableModule() {
        return $this->_scopeConfig->getValue('blogsection/bloggroup/active', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
    
    /* get show image on home page */
    public function getHomeshowimage() {
        return $this->_scopeConfig->getValue('blogsection/homepagegrp/homeshowimage', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
    
    /* get show title on home page */
    public function getHomeshowtitle() {
        return $this->_scopeConfig->getValue('blogsection/homepagegrp/homeshowtitle', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
    
    /* get home blog title */
    public function getHomeblogtitle() {
        return $this->_scopeConfig->getValue('blogsection/homepagegrp/homeblogtitle', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
    
    /* get show content on home page */
    public function getHomeshowcontent() {
        return $this->_scopeConfig->getValue('blogsection/homepagegrp/homeshowcontent', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
    
    /* get show readmore on home page */
    public function getHomeshowreadmore() {
        return $this->_scopeConfig->getValue('blogsection/homepagegrp/homeshowreadmore', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
    
    /* get show tags on home page */
    public function getHomeshowtags() {
        return $this->_scopeConfig->getValue('blogsection/homepagegrp/homeshowtags', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
    
    /* get show date on home page */
    public function getHomeshowdate() {
        return $this->_scopeConfig->getValue('blogsection/homepagegrp/homeshowdate', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
    
    /* get enable slider on home page */
    public function getHomeenableslider() {
        return $this->_scopeConfig->getValue('blogsection/homepagegrp/homeenableslider', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
    
    /* get blog limit home page */
    public function getHomedisplaylimit() {
        return $this->_scopeConfig->getValue('blogsection/homepagegrp/homelimit', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    public function getBaseUrl() {
        return $this->_storeManager->getStore()->getBaseUrl();
    }

    public function getMediaPath() {
        return $this->_filesystem->getDirectoryRead(
                        DirectoryList::MEDIA
                )->getAbsolutePath('');
    }

    public function getBannerCss() {
        return $this->_assetRepo->getUrl('Solwin_Ourblog::css/');
    }

    public function getBannerJs() {
        return $this->_assetRepo->getUrl('Solwin_Ourblog::js/');
    }

    protected function getConnection() {
        if (!$this->connection) {
            $this->connection = $this->_resource->getConnection('core_write');
        }
        return $this->connection;
    }

    public function getCustomCommentsCollection($blog_id) {

        $connection = $this->getConnection();
        $select = $connection->select()
                ->from(['c' => 'blog_comment'], ['c.comment_id', new \Zend_Db_Expr('COUNT(*) as count')])
                ->where('c.blog_id=?', $blog_id)
                ->where('c.status=?', 1);

        $countRow = $connection->fetchRow($select);
        return $countRow['count'];
    }

}
