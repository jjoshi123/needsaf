<?php

namespace Solwin\Ourblog\Block;

class Link extends \Magento\Framework\View\Element\Html\Link {

    protected $_template = 'Solwin_Ourblog::link.phtml';

    public function getHref() {
        return $this->getUrl('ourblog');
    }

    public function getLabel() {
        return __('Blog');
    }

}

?>  