<?php

namespace Solwin\Ourblog\Block\Sidebar;

use Solwin\Ourblog\Model\BlogPostsFactory;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\View\Element\Template;

class Ourblog extends Template {

    protected $_collection;
    protected $_catcollection;
    protected $_comcollection;
    protected $_modelBlogPostsFactory;
    protected $customerSession;
    protected $_resource;
    protected $connection;

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        BlogPostsFactory $modelBlogPostsFactory,
        \Magento\Customer\Model\Session $customerSession,
        \Solwin\Ourblog\Model\Resource\BlogPosts\Collection $collection,
        \Solwin\Ourblog\Model\Resource\CategoryPosts\Collection $catcollection,
        \Solwin\Ourblog\Model\Resource\CommentPosts\Collection $comcollection,
        \Magento\Framework\App\ResourceConnection $resource,
        array $data = []
    ) {
        $this->_modelBlogPostsFactory = $modelBlogPostsFactory;
        $this->_collection = $collection;
        $this->_catcollection = $catcollection;
        $this->_comcollection = $comcollection;
        $this->customerSession = $customerSession;
        $this->_resource = $resource;
        parent::__construct($context, $data);
        $blogcollection = $this->_modelBlogPostsFactory->create()->getCollection();
        $this->setCollection($blogcollection);
    }

    public function getBlogCollection() {
        $limit = $this->getSidebarLimit();
        $collection = $this->_collection
                ->addFieldToFilter('is_active', 1);

        $collection->getSelect()
                ->order('rand()')
                ->limit($limit);

        return $collection;
    }

    public function getLoggendinCustomerId() {
        return $this->customerSession->getCustomerId();
    }

    public function getCategoryCollection() {
        $catCollection = $this->_catcollection;
//                ->addFieldToFilter('status', 1);
        return $catCollection;
    }

    public function getCommentsCollection($blog_id, $commentLimit) {
        $comCollection = $this->_comcollection
                ->addFieldToFilter('blog_id', $blog_id)
                ->addFieldToFilter('status', 1);
        $comCollection->getSelect()->limit($commentLimit);
        return $comCollection;
    }

    public function getSingleBlogCollection($blog_id) {
        $blogModel = $this->_modelBlogPostsFactory->create();
        $blogCollection = $blogModel->load($blog_id);
        return $blogCollection;
    }

    public function getBlogTagsCollection($blogtags) {
        $tagCollection = $this->_collection->addFieldToFilter('tags', array('finset' => $blogtags))
                ->addFieldToFilter('is_active', 1);
        return $tagCollection;
    }

    public function getFormAction() {
        return $this->getUrl('ourblog/comment/save');
    }

    /* get extension enable or not */
    public function getEnableModule() {
        return $this->_scopeConfig->getValue('blogsection/bloggroup/active', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    public function getBaseUrl() {
        return $this->_storeManager->getStore()->getBaseUrl();
    }

    /* get blog sidebar limit */
    public function getSidebarLimit() {
        return $this->_scopeConfig->getValue('blogsection/sidebargrp/sidebarlimit', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
    
    /* enable blog title in sidebar title */
    public function getSidebarshowtitle() {
        return $this->_scopeConfig->getValue('blogsection/sidebargrp/sidebarshowtitle', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
    
    /* enable blog image in sidebar */
    public function getSidebarshowimage() {
        return $this->_scopeConfig->getValue('blogsection/sidebargrp/sidebarimage', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
    
    /* get blog title in sidebar */
    public function getSidebartitle() {
        return $this->_scopeConfig->getValue('blogsection/sidebargrp/sidebarblogtitle', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
    
    /* enable blog date in sidebar */
    public function getSidebarshowdate() {
        return $this->_scopeConfig->getValue('blogsection/sidebargrp/sidebarshowdate', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
    

    public function getMediaPath() {
        return $this->_filesystem->getDirectoryRead(
                        DirectoryList::MEDIA
                )->getAbsolutePath('');
    }

    protected function getConnection() {
        if (!$this->connection) {
            $this->connection = $this->_resource->getConnection('core_write');
        }
        return $this->connection;
    }

    public function getCustomCommentsCollection($blog_id) {

        $connection = $this->getConnection();
        $select = $connection->select()
                ->from(['c' => 'blog_comment'], ['c.comment_id', new \Zend_Db_Expr('COUNT(*) as count')])
                ->where('c.blog_id=?', $blog_id)
                ->where('c.status=?', 1);

        $countRow = $connection->fetchRow($select);
        return $countRow['count'];
    }

}
