<?php

namespace Solwin\Ourblog\Model;

class Categorylist {

    protected $_collection;

    public function __construct(
    \Solwin\Ourblog\Model\Resource\CategoryPosts\Collection $collection
    ) {
        $this->_collection = $collection;
    }

    public function getOptionArray() {
        $collection = $this->_collection;
        $col = array();
        foreach ($collection as $val) {
            $col[$val['cat_id']] = $val['cat_name'];
        }
        
        return $col;
    }

}
