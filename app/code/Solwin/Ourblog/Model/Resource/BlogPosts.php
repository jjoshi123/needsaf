<?php
namespace Solwin\Ourblog\Model\Resource;

class BlogPosts extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('ourblog', 'blog_id');
    }
}