<?php
namespace Solwin\Ourblog\Model\Resource;

class CommentPosts extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('blog_comment', 'comment_id');
    }
}