<?php
namespace Solwin\Ourblog\Model;

class CommentPosts extends \Magento\Framework\Model\AbstractModel
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Solwin\Ourblog\Model\Resource\CommentPosts');
    }
}