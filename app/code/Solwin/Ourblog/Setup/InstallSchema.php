<?php

namespace Solwin\Ourblog\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * @codeCoverageIgnore
 */
class InstallSchema implements InstallSchemaInterface
{
    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;

        $installer->startSetup();
        
        /* ourblog table */
        $table = $installer->getConnection()->newTable(
            $installer->getTable('ourblog')
        )->addColumn(
            'blog_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            array('identity' => true, 'nullable' => false, 'primary' => true),
            'Blog Post ID'
        )->addColumn(
            'title',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            '255',
            array('nullable' => false),
            'Blog Title'
        )->addColumn(
            'category',
            \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
            null,
            array(),
            'Blog Category Id'
        )->addColumn(
            'tags',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            '255',
            array('nullable' => false),
            'Blog Tags'
        )->addColumn(
            'embad_code',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            '255',
            array('nullable' => false),
            'Blog Embad code'
        )->addColumn(
            'image',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            '255',
            array('nullable' => false),
            'Blog image'
        )->addColumn(
            'description',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            '2M',
            array('nullable' => false),
            'Blog Description'
        )->addColumn(
            'content',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            '2M',
            array('nullable' => false),
            'Blog Content'
        )->addColumn(
            'publish_date',
            \Magento\Framework\DB\Ddl\Table::TYPE_DATETIME,
            null,
            array(),
            'Blog Publish Date'
        )->addColumn(
            'is_active',
            \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
            null,
            array(),
            'Blog Active Status'
        )->addColumn(
            'createdby',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            '2M',
            array('nullable' => false),
            'Blog Creator'
        )->addColumn(
            'created_at',
            \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
            null,
            array('default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT),
            'Blog Creation Time'
        )->addColumn(
            'update_time',
            \Magento\Framework\DB\Ddl\Table::TYPE_DATETIME,
            null,
            array(),
            'Blog Modification Time'
        )->setComment(
            'Ourblog Table'
        );
        $installer->getConnection()->createTable($table);
        
        /* blog category table */
        
        $table = $installer->getConnection()->newTable(
            $installer->getTable('blog_category')
        )->addColumn(
            'cat_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            array('identity' => true, 'nullable' => false, 'primary' => true),
            'Blog Category ID'
        )->addColumn(
            'cat_name',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            '255',
            array('nullable' => false),
            'Blog Category Name'
        )->addColumn(
            'cat_active',
            \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
            null,
            array(),
            'Blog Category Active Status'
        )->setComment(
            'Blog category Table'
        );
        $installer->getConnection()->createTable($table);

        /* blog comment table */
        
        $table = $installer->getConnection()->newTable(
            $installer->getTable('blog_comment')
        )->addColumn(
            'comment_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            array('identity' => true, 'nullable' => false, 'primary' => true),
            'Comment ID'
        )->addColumn(
            'blog_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
            null,
            array(),
            'Blog Id'
        )->addColumn(
            'blog_title',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            '255',
            array('nullable' => false),
            'Comment Title'
        )->addColumn(
            'user_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
            null,
            array(),
            'User Id'
        )->addColumn(
            'customer_name',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            '255',
            array('nullable' => false),
            'Logged in customer name'
        )->addColumn(
            'user_name',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            '255',
            array('nullable' => false),
            'User Name'
        )->addColumn(
            'comment',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            '2M',
            array('nullable' => false),
            'Comment'
        )->addColumn(
            'status',
            \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
            null,
            array(),
            'Comment Active Status'
        )->addColumn(
            'created_date',
            \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
            null,
            array('default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT),
            'Comment Creation Time'
        )->setComment(
            'Blog category Table'
        );
        $installer->getConnection()->createTable($table);
        
        $installer->endSetup();

    }
}
