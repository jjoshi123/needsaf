<?php

namespace Solwin\Cpanel\Model\System\Config\Source;

class Footermiddle extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource {

    public function getAllOptions() {
        if (!$this->_options) {
            $this->_options = array(
                array('label' => 'None', 'value' => ''),
                array('label' => 'Static Block', 'value' => 'static'),
                array('label' => 'Information Block', 'value' => 'info'),
                array('label' => 'Contact Block', 'value' => 'contact'),
                array('label' => 'Social Block', 'value' => 'social'),
                array('label' => 'Flickr', 'value' => 'flickr'),
                array('label' => 'Instagram', 'value' => 'instagram'),
                array('label' => 'Newsletter Block', 'value' => 'news')
            );
        }
        return $this->_options;
    }

}
