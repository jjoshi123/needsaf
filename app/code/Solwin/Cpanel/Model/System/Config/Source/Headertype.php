<?php

namespace Solwin\Cpanel\Model\System\Config\Source;

class Headertype extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource {

    
    public function getAllOptions() {
        if (!$this->_options) {
            $this->_options = array(
                array('label' => 'Header Type 1', 'value' => '1'),
                array('label' => 'Header Type 2', 'value' => '2')
            );
        }
        return $this->_options;
    }
}
