<?php

namespace Solwin\Cpanel\Model\System\Config\Source;

class ListPattern {

    public function toOptionArray() {

        $arrPattern = array();
        for ($i = 1; $i < 7; $i++) {
            $arrPattern[] = array('value' => 'theme_' . $i, 'label' => 'theme_' . $i);
        }

        return $arrPattern;
    }

}
