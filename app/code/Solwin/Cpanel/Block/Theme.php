<?php

namespace Solwin\Cpanel\Block;

class Theme extends \Magento\Backend\Block\AbstractBlock {

    /**
     * @override
     * @see \Magento\Backend\Block\AbstractBlock::_construct()
     * @return void
     */
    protected $_storeManager;
    protected $_helper;

    public function __construct(
        \Magento\Backend\Block\Context $context,
        \Solwin\Cpanel\Helper\Data $helper,
        array $data = []
    ) {
        $this->_helper = $helper;
        parent::__construct($context, $data);

        $om = \Magento\Framework\App\ObjectManager::getInstance();
        $page = $om->get('Magento\Framework\View\Page\Config');

        $theme = $this->_helper->getCurrenttheme();


        if ($theme == 'theme_1') {
            $page->addPageAsset('css/kosmic1-m.css');
            $page->addPageAsset('css/kosmic1-l.css');
        } else if ($theme == 'theme_2') {
            $page->addPageAsset('css/kosmic2-m.css');
            $page->addPageAsset('css/kosmic2-l.css');
        } else if ($theme == 'theme_3') {
            $page->addPageAsset('css/kosmic3-m.css');
            $page->addPageAsset('css/kosmic3-l.css');
        } else if ($theme == 'theme_4') {
            $page->addPageAsset('css/kosmic4-m.css');
            $page->addPageAsset('css/kosmic4-l.css');
        } else if ($theme == 'theme_5') {
            $page->addPageAsset('css/kosmic5-m.css');
            $page->addPageAsset('css/kosmic5-l.css');
        } else if ($theme == 'theme_6') {
            $page->addPageAsset('css/kosmic6-m.css');
            $page->addPageAsset('css/kosmic6-l.css');
        } else {
            $page->addPageAsset('css/kosmic1-m.css');
            $page->addPageAsset('css/kosmic1-l.css');
        }
    }
}
