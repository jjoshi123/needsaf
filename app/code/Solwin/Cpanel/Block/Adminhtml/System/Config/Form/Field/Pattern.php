<?php

namespace Solwin\Cpanel\Block\Adminhtml\System\Config\Form\Field;

class Pattern extends \Magento\Config\Block\System\Config\Form\Field {

    protected function _getElementHtml(\Magento\Framework\Data\Form\Element\AbstractElement $element) {

        $html = '';
        $options = $element->getValues();
        $colorCode = array(1 => '#0088CD', 2 => '#84c351', 3 => '#f95656', 4 => '#D8B359', 5 => '#4eabf0', 6 => '#ee8041');
        $colorName = array(1 => 'Blue', 2 => 'Green', 3 => 'Red', 4 => 'Yellow', 5 => 'Light Blue', 6 => 'Orange');
        $c = 1;
        $html .= '<div class="radio_img_group" id="' . $element->getHtmlId() . '">';
        foreach ($options as $option) {

            $checked = ($element->getEscapedValue() == $option['value']) ? ' checked' : '';
            
            $html .= '<label style="display: inline-block;" title="' . $option['label'] . '">';

            $html .= '<input class="cus_radio" type="radio" name="' . $element->getName() . '" value="' . $option['value'] . '" ' . $checked . ' />';

            $html .= '<span style="background-color: ' . $colorCode[$c] . '">' . $colorName[$c] . '</span>';

            $html .= '</label>';
            $c++;
        }

        $html .= '</div>';

        $html .= '<style>
					.radio_img_group .cus_radio {
                                            position:absolute !important;
                                            top:5px;
                                            left:5px;
                                            z-index:0;
					}
					.radio_img_group label {
						display: inline-block;
						margin-right: 3px;
                                                position:relative;
					}
					.radio_img_group input {
						display: none;
					}
					.radio_img_group span {
						display: block;
						border: 1px solid #FFFFFF;
						line-height: 30px;
                                                text-align: center;
                                                width: auto;
						height: 30px;
						cursor: pointer;
                                                padding: 0 10px;
                                                color: #ffffff;
                                                z-index:1;
                                                position:relative;
					}
					input:checked + span {
                                                border: 1px solid #333333;
					}
					
				</style>';

        $html .= $element->getAfterElementHtml();

        return $html;
    }

}