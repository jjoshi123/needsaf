<?php

namespace Solwin\Cpanel\Helper;

class Data extends \Magento\Framework\App\Helper\AbstractHelper {

    protected $_storeManager;
    protected $logoblock;

    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Theme\Block\Html\Header\Logo $logoblock,
        \Magento\Store\Model\StoreManagerInterface $storeManager
    ) {
        $this->logoblock = $logoblock;
        $this->_storeManager = $storeManager;
        parent::__construct($context);
    }

    public function getBaseUrl() {
        return $this->_storeManager->getStore()->getBaseUrl();
    }

    public function getIsHomePage() {
        return $this->logoblock->isHomePage();
    }

    public function getMediaUrl() {
        return $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
    }

    public function getCurrentUrl() {
        return $this->_urlBuilder->getCurrentUrl();
    }

    public function getThemeConfigOptionBlocks() {
        return $this->scopeConfig->getValue('cpanelsection/importgroup/overwrite_block', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
    public function getThemeConfigOptionPages() {
        return $this->scopeConfig->getValue('cpanelsection/importgroup/overwrite_pages', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /* Get home page static block id */

    public function getHomepageblock() {
        return $this->scopeConfig->getValue('cpanelsection/staticcontentsettings/home_static_block_id', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
    
    /* Get account page static block id */

    public function getRegisterpageblock() {
        return $this->scopeConfig->getValue('cpanelsection/staticcontentsettings/account_static_block_id', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
    
    /* Get register page static block id */

    public function getContactpageblock() {
        return $this->scopeConfig->getValue('cpanelsection/staticcontentsettings/contact_static_block_id', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
    
    /* Get product detail page static block id */

    public function getProductpageblock() {
        return $this->scopeConfig->getValue('cpanelsection/staticcontentsettings/productdetail_static_block_id', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
    
    /* Get current theme color */

    public function getEnableBlog() {
        return $this->scopeConfig->getValue('blogsection/bloggroup/active', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /* Get current theme color */

    public function getCurrenttheme() {
        return $this->scopeConfig->getValue('cpanelsection/cpanelgroup/theme', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /* top header notification */

    public function getTopheadernotification() {
        return $this->scopeConfig->getValue('cpanelsection/headergroup/top_header_notification', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /* top header notification */

    public function getHeaderstaticblockid() {
        return $this->scopeConfig->getValue('cpanelsection/headergroup/header_static_block_id', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /* Show sharing addon or not */

    public function getShowsharingaddon() {
        return $this->scopeConfig->getValue('cpanelsection/headergroup/sharingaddon', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /* Get Add this pub ID */

    public function getAddthispubid() {
        return $this->scopeConfig->getValue('cpanelsection/headergroup/pubid', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /* Get enable home link in navigation */

    public function getEnablehomelink() {
        return $this->scopeConfig->getValue('cpanelsection/headergroup/addhomelink', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /* Get enable blog link in navigation */

    public function getEnablebloglink() {
        return $this->scopeConfig->getValue('cpanelsection/headergroup/addbloglink', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
    
    /* Get header type */

    public function getHeadertype() {
        return $this->scopeConfig->getValue('cpanelsection/headergroup/header_type', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /* Get Footer Newsletter Title */

    public function getShowfooternewsletter() {
        return $this->scopeConfig->getValue('cpanelsection/footergroup/shownewsletter', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /* Get Footer Newsletter Title */

    public function getFooternewslettertitle() {
        return $this->scopeConfig->getValue('cpanelsection/footergroup/footer_newsletter_title', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /* Get Footer Newsletter Test */

    public function getFooternewslettertext() {
        return $this->scopeConfig->getValue('cpanelsection/footergroup/footer_newsletter_text', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /* Get Footer Newsletter background Image */

    public function getFooternewsletterbackimage() {
        return $this->scopeConfig->getValue('cpanelsection/footergroup/footer_newsletter_image', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /* Get Footer logo */

    public function getFooterlogo() {
        return $this->scopeConfig->getValue('cpanelsection/footergroup/footer_logo', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /* Get Footer logo */

    public function getFooteraboutstaticblock() {
        return $this->scopeConfig->getValue('cpanelsection/footergroup/footer_about_text_static_block', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /* Get Footer social icons */

    public function getShowsocialicons() {
        return $this->scopeConfig->getValue('cpanelsection/footergroup/showsocial', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /* Get Footer facebook enable/disable */

    public function getShowfacebook() {
        return $this->scopeConfig->getValue('cpanelsection/footergroup/showfacebook', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /* Get Footer facebook url */

    public function getFacebookurl() {
        return $this->scopeConfig->getValue('cpanelsection/footergroup/facebook_link', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /* Get Footer twitter enable/disable */

    public function getShowtwitter() {
        return $this->scopeConfig->getValue('cpanelsection/footergroup/showtwitter', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /* Get Footer twitter url */

    public function getTwitterurl() {
        return $this->scopeConfig->getValue('cpanelsection/footergroup/twitter_link', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /* Get Footer linkdn enable/disable */

    public function getShowlinkedin() {
        return $this->scopeConfig->getValue('cpanelsection/footergroup/showlinkedin', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /* Get Footer linkdn url */

    public function getLinkdnurl() {
        return $this->scopeConfig->getValue('cpanelsection/footergroup/linkedin_link', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /* Get Footer google enable/disable */

    public function getShowgoogle() {
        return $this->scopeConfig->getValue('cpanelsection/footergroup/showgoogle', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /* Get Footer googleplus url */

    public function getGoogleurl() {
        return $this->scopeConfig->getValue('cpanelsection/footergroup/google_link', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /* Get Footer pinterest enable/disable */

    public function getShowpinterest() {
        return $this->scopeConfig->getValue('cpanelsection/footergroup/showpinterest', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /* Get Footer Pinterest url */

    public function getPinteresturl() {
        return $this->scopeConfig->getValue('cpanelsection/footergroup/pinterest_link', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
    
    /* Get Footer Youtube enable/disable */

    public function getShowyoutube() {
        return $this->scopeConfig->getValue('cpanelsection/footergroup/showyoutube', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /* Get Footer Youtube url */

    public function getYoutubeurl() {
        return $this->scopeConfig->getValue('cpanelsection/footergroup/youtube_link', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
    
    /* Get Footer Flickr enable/disable */

    public function getShowflickr() {
        return $this->scopeConfig->getValue('cpanelsection/footergroup/showflickr', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /* Get Footer Flickr url */

    public function getFlickrurl() {
        return $this->scopeConfig->getValue('cpanelsection/footergroup/flickr_link', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
    
    /* Get Footer Vimeo enable/disable */

    public function getShowvimeo() {
        return $this->scopeConfig->getValue('cpanelsection/footergroup/showvimeo', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /* Get Footer Vimeo url */

    public function getVimeourl() {
        return $this->scopeConfig->getValue('cpanelsection/footergroup/vimeo_link', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
    
    /* Get Footer Instagram enable/disable */

    public function getShowinstagram() {
        return $this->scopeConfig->getValue('cpanelsection/footergroup/showinstagram', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /* Get Footer Instagram url */

    public function getInstagramurl() {
        return $this->scopeConfig->getValue('cpanelsection/footergroup/instagram_link', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
    
    /* Get Enable/Disable Map in Footer */

    public function getEnablefmap() {
        return $this->scopeConfig->getValue('cpanelsection/footergroup/enable_fmap', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
    
    /* Get embeded code for footer map*/

    public function getFembedcode() {
        return $this->scopeConfig->getValue('cpanelsection/footergroup/fembedcode', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /* Get Enable Footer links  */

    public function getEnablefooterlinks() {
        return $this->scopeConfig->getValue('cpanelsection/footergroup/show_footer_links', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /* Get Enable Footer column 1 option  */

    public function getFootercolumnone() {
        return $this->scopeConfig->getValue('cpanelsection/footergroup/footer_column_1', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /* Get Enable Footer column 1 static block id  */

    public function getColumnonestatic() {
        return $this->scopeConfig->getValue('cpanelsection/footergroup/footer_column_1_static', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /* Get Enable Footer column 1 width  */

    public function getColumnonewidth() {
        return $this->scopeConfig->getValue('cpanelsection/footergroup/footer_column_1_width', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /* Get Enable Footer column 1 option  */

    public function getFootercolumntwo() {
        return $this->scopeConfig->getValue('cpanelsection/footergroup/footer_column_2', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /* Get Enable Footer column 1 static block id  */

    public function getColumntwostatic() {
        return $this->scopeConfig->getValue('cpanelsection/footergroup/footer_column_2_static', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /* Get Enable Footer column 1 width  */

    public function getColumntwowidth() {
        return $this->scopeConfig->getValue('cpanelsection/footergroup/footer_column_2_width', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /* Get Enable Footer column 1 option  */

    public function getFootercolumnthree() {
        return $this->scopeConfig->getValue('cpanelsection/footergroup/footer_column_3', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /* Get Enable Footer column 1 static block id  */

    public function getColumnthreestatic() {
        return $this->scopeConfig->getValue('cpanelsection/footergroup/footer_column_3_static', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /* Get Enable Footer column 1 width  */

    public function getColumnthreewidth() {
        return $this->scopeConfig->getValue('cpanelsection/footergroup/footer_column_3_width', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /* Get Enable Footer column 1 option  */

    public function getFootercolumnfour() {
        return $this->scopeConfig->getValue('cpanelsection/footergroup/footer_column_4', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /* Get Enable Footer column 1 static block id  */

    public function getColumnfourstatic() {
        return $this->scopeConfig->getValue('cpanelsection/footergroup/footer_column_4_static', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /* Get Enable Footer column 1 width  */

    public function getColumnfourwidth() {
        return $this->scopeConfig->getValue('cpanelsection/footergroup/footer_column_4_width', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /* Get Footer copyright before static block  */

    public function getFootercopyrightbeforestatic() {
        return $this->scopeConfig->getValue('cpanelsection/footergroup/copyright_before_static', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /* Get Footer copyright after static block  */

    public function getFootercopyrightafterstatic() {
        return $this->scopeConfig->getValue('cpanelsection/footergroup/copyright_after_static', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /* Get product new label enable/disable */

    public function getShowproductnewlabel() {
        return $this->scopeConfig->getValue('cpanelsection/listpagegroup/newlabel', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /* Get product sale label enable/disable */

    public function getShowproductsalelabel() {
        return $this->scopeConfig->getValue('cpanelsection/listpagegroup/salelabel', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /* Get product block effect enable/disable */

    public function getShowproducteffect() {
        return $this->scopeConfig->getValue('cpanelsection/listpagegroup/enable_product_effect', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /* Get product image aspect ratio */

    public function getImageaspectratio() {
        return $this->scopeConfig->getValue('cpanelsection/listpagegroup/image_aspect_ratio', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /* Get product image width */

    public function getProductimageweight() {
        return $this->scopeConfig->getValue('cpanelsection/listpagegroup/image_ratio_width', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /* Get product image height */

    public function getProductimageheight() {
        return $this->scopeConfig->getValue('cpanelsection/listpagegroup/image_ratio_height', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /* Get display product alternative image */

    public function getDiaplyalternativeimage() {
        return $this->scopeConfig->getValue('cpanelsection/listpagegroup/show_alternative_image', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /* Get product view page sharing addon enable/disable */

    public function getShowproductviwesharing() {
        return $this->scopeConfig->getValue('cpanelsection/viewpagegroup/sharingaddon', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /* Get product view page sharing addon pub ID */

    public function getShowproductviwesharingid() {
        return $this->scopeConfig->getValue('cpanelsection/viewpagegroup/pubid', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /* Get product view page related and crosssell products */

    public function getShowrelatedupsell() {
        return $this->scopeConfig->getValue('cpanelsection/viewpagegroup/showproducts', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /* Get show contact page google map enable/disable */

    public function getShowrgooglemap() {
        return $this->scopeConfig->getValue('cpanelsection/contactpagegroup/showgooglemap', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /* Get show contact page google map Embeded Code */

    public function getGooglemapembedcode() {
        return $this->scopeConfig->getValue('cpanelsection/contactpagegroup/embedcode', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /* Get show contact store name */

    public function getContactstorename() {
        return $this->scopeConfig->getValue('cpanelsection/contactpagegroup/storename', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /* Get show contact store address */

    public function getContactstoreaddress() {
        return $this->scopeConfig->getValue('cpanelsection/contactpagegroup/storeaddress', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /* Get show store contactno */

    public function getStorecontactno() {
        return $this->scopeConfig->getValue('cpanelsection/contactpagegroup/contactinfo', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /* Get show store contactno */

    public function getStorecontactemail() {
        return $this->scopeConfig->getValue('cpanelsection/contactpagegroup/storeemail', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /* Get enable loader */

    public function getEnableloader() {
        return $this->scopeConfig->getValue('cpanelsection/loadergroup/enable_loader', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /* Get loader image */

    public function getLoaderimage() {
        return $this->scopeConfig->getValue('cpanelsection/loadergroup/loader_image', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /* Get diaply loader option */

    public function getLoaderdisplayon() {
        return $this->scopeConfig->getValue('cpanelsection/loadergroup/loader_displayon', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /* get sidebar content static block id */

    public function getSidebarstatic() {
        return $this->scopeConfig->getValue('cpanelsection/sidebarcontent/sidebar_static', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

}
