<?php

namespace Solwin\Productzoom\Model\Source;

class Place extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource {

    protected $_collection;

    /**
     * Entity attribute factory
     *
     * @var \Magento\Eav\Model\Resource\Entity\AttributeFactory
     */
    protected $entityAttributeFactory;

    public function __construct(
     \Magento\Eav\Model\ResourceModel\Entity\AttributeFactory $entityAttributeFactory
    ) {
        $this->entityAttributeFactory = $entityAttributeFactory;
    }

    /**
     * Retrieve all product tax class options.
     *
     * @param bool $withEmpty
     * @return array
     */
    public function getAllOptions() {
        if (!$this->_options) {


            $this->_options[] = [
                'value' => 'top',
                'label' => 'Top',
            ];
            $this->_options[] = [
                'value' => 'bottom',
                'label' => 'Bottom',
            ];
        }
        return $this->_options;
    }

    /**
     * Retrieve flat column definition
     *
     * @return array
     */
    public function getFlatColumns() {
        $attributeCode = $this->getAttribute()->getAttributeCode();

        return [
            $attributeCode => [
                'unsigned' => true,
                'default' => null,
                'extra' => null,
                'type' => Table::TYPE_INTEGER,
                'nullable' => true,
                'comment' => $attributeCode . ' tax column',
            ],
        ];
    }

    /**
     * Retrieve Select for update attribute value in flat table
     *
     * @param   int $store
     * @return  \Magento\Framework\DB\Select|null
     */
    public function getFlatUpdateSelect($store) {
        return $this->entityAttributeFactory->create()->getFlatUpdateSelect($this->getAttribute(), $store);
    }

}
