<?php

namespace Solwin\Productzoom\Helper;

class Data extends \Magento\Framework\App\Helper\AbstractHelper {

    public function __construct(
        \Magento\Framework\App\Helper\Context $context
    ) {
        parent::__construct($context);
    }

    /* get Extension enable/disable */
    public function getZoomenable() {
        return $this->scopeConfig->getValue('product_zoom/zoomsettings/active', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
    
    /* get zoom type */
    public function getZoomtype() {
        return $this->scopeConfig->getValue('product_zoom/zoomsettings/zoom_type', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
    
    /* get type shape */
    public function getTypeshape() {
        return $this->scopeConfig->getValue('product_zoom/zoomsettings/type_shape', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
    
    /* get type shape */
    public function getTypesize() {
        return $this->scopeConfig->getValue('product_zoom/zoomsettings/type_size', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
    
    /* get product gallery enable/disable */
    public function getGalleryenable() {
        return $this->scopeConfig->getValue('product_zoom/zoomsettings/product_gallery', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }    
    
    /* get zoom effect */
    public function getZoomeffects() {
        return $this->scopeConfig->getValue('product_zoom/zoomsettings/zoom_effects', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
    
    /* get Tint effect */
    public function getTinteffects() {
        return $this->scopeConfig->getValue('product_zoom/zoomsettings/tint_effects', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
    
    /* get Tint color */
    public function getTintcolor() {
        return $this->scopeConfig->getValue('product_zoom/zoomsettings/tintColour', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
    
    /* get Tint opacity */
    public function getTintopacity() {
        return $this->scopeConfig->getValue('product_zoom/zoomsettings/tintOpacity', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
    
    /* get zoom Window Width */
    public function getZoomwindowwidth() {
        return $this->scopeConfig->getValue('product_zoom/zoomsettings/zoomWindowWidth', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
    
    /* get zoom Window Height */
    public function getZoomwindowheight() {
        return $this->scopeConfig->getValue('product_zoom/zoomsettings/zoomWindowHeight', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
    
    /* get zoom Window Position */
    public function getZoomwindowposition() {
        return $this->scopeConfig->getValue('product_zoom/zoomsettings/wind_pos', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
    
    /* get zoom lense Color */
    public function getLensecolor() {
        return $this->scopeConfig->getValue('product_zoom/zoomsettings/lenseColour', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
    
    /* get zoom lense Color */
    public function getLenseopacity() {
        return $this->scopeConfig->getValue('product_zoom/zoomsettings/lenseOpacity', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
    
    /* get window fade-in */
    public function getWindowfadein() {
        return $this->scopeConfig->getValue('product_zoom/zoomsettings/windowFadeIn', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
    
    /* get window fade-out */
    public function getWindowfadeout() {
        return $this->scopeConfig->getValue('product_zoom/zoomsettings/windowFadeOut', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
    
    /* get lens fade-in */
    public function getLensfadein() {
        return $this->scopeConfig->getValue('product_zoom/zoomsettings/lensFadeIn', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
    
    /* get lens fade-out */
    public function getLensfadeout() {
        return $this->scopeConfig->getValue('product_zoom/zoomsettings/lensFadeOut', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
    
    /* get gallery position */
    public function getGalleryposition() {
        return $this->scopeConfig->getValue('product_zoom/zoomsettings/gallery_place', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
    
}