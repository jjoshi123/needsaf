/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
require([
    'jquery',
], function () {
    jQuery(window).load(function () {
        jQuery('.cus-ajax-loader').css('display', 'none');
        jQuery('.page-wrapper').css('opacity', '1');
    });
});
